import { Asset, AssetManager, assetManager, builtinResMgr, isValid, Material, Prefab, sp, SpriteAtlas, SpriteFrame } from "cc";
import { BeginInfo } from "../../../begin/scripts/BeginInfo";

class ResPack {
    public m_bundle: AssetManager.Bundle;
    private all_res: Map<string, any>;//所有路径-》资源
    private all_tag: Map<string, Set<string>>;//用tag保存，路径-》资源的集合
    constructor(bundle: AssetManager.Bundle) {
        this.m_bundle = bundle;
        this.all_res = new Map<string, any>();
        this.all_tag = new Map<string, Set<string>>();
    }
    public loadWithTag(tag: string, path: string, type: any, callback: Function) {
        let set = this.all_tag.get(tag);
        if (!set) {//还没有标签
            set = new Set();
            this.all_tag.set(tag, set);
        }
        let add_ref = false;
        if (!set.has(path)) {//标签下还没有资源
            add_ref = true;
            set.add(path);
        }
        let old_res = this.all_res.get(path);
        if (old_res) {
            if (isValid(old_res)) {
                if (add_ref) {//其他标签下可能加载过
                    old_res.addRef();
                }
                callback(null, old_res);
                return;
            } else {
                this.all_res.delete(path);
            }
        }
        this.m_bundle.load(path, type, (err, res: Asset) => {
            if (err) {
                console.log(err);
                callback(err, null);
                return;
            }
            if (this.all_tag.has(tag)) {
                //正常加载
                if (add_ref)res.addRef();
                this.all_res.set(path, res);
                callback(err, res);
            } 
            //加载成功时已经关掉界面
            else if (res.refCount <= 0)assetManager.releaseAsset(res);
        });
    }
    public realeaseByTag(tag: string) {
        let paths = this.all_tag.get(tag);
        if (paths) {
            let self = this;
            let res: any;
            paths.forEach(element => {
                res = self.all_res.get(element);
                if (isValid(res)) {
                    if (res._ref == 1) this.all_res.delete(element);
                    res.decRef();
                }
            });
            this.all_tag.delete(tag);
        }
    }
    public realeseAll() {
        // for (let k in this.all_tag) {
        //     this.realeaseByTag(k);
        // }
        if (isValid(this.m_bundle)) {
            this.all_res.forEach((res)=>{
                assetManager.releaseAsset(res);
            })
            // this.m_bundle.releaseAll();
            // assetManager.removeBundle(this.m_bundle);
        }
        this.all_tag.clear();
        this.all_res.clear();
    }
    public logRes() {
        if(isValid(this.m_bundle))console.log(this.m_bundle.name + " log::");
        else console.log("bundle无效");
        console.log(this.all_tag);
        console.log(this.all_res);
    }
}


export class ResMgr{
    public static atlas_head: SpriteAtlas = null;
    public static atlas_common: SpriteAtlas = null;
    public static mat_round: Material = null;
    public static mat_comer: Material = null;
    public static spine_brow: sp.SkeletonData = null;
    
    private static main_pack: ResPack = null;
    private static game_pack: ResPack = null;
    private static all_res = new Map<string, any>();

    public static init() {
        let bundle = assetManager.getBundle("master");
        this.main_pack = new ResPack(bundle);
        BeginInfo.all_atlas.forEach(element => {
            element.addRef();
            if(element._name == "head")ResMgr.atlas_head = element;
            else if(element._name == "common")ResMgr.atlas_common = element;
        });
        ResMgr.mat_round = BeginInfo.mat_round.addRef();
        ResMgr.mat_comer = BeginInfo.mat_comer.addRef();
        ResMgr.spine_brow = BeginInfo.spine_brow.addRef();
    }
    public static loadWithTag(tag: string, path: string, type: any, callback: Function) {
        ResMgr.main_pack.loadWithTag(tag, path, type, callback);
    }
    public static realeaseByTag(tag: string) {
        console.log("---------------------------release:", tag);
        ResMgr.main_pack.realeaseByTag(tag);
    }

    //游戏子包的加载和释放
    public static gameLoadWithTag(tag: string, path: string, type: any, callback: Function) {
        ResMgr.game_pack && ResMgr.game_pack.loadWithTag(tag, path, type, callback);
    }
    public static gameRealeaseByTag(tag: string) {
        ResMgr.game_pack && ResMgr.game_pack.realeaseByTag(tag);
    }

    //设置子包
    public static setGameBundle(bundle: any) {
        if (ResMgr.game_pack && ResMgr.game_pack.m_bundle &&
            ResMgr.game_pack.m_bundle.name == bundle.name) {
            return;
        }
        ResMgr.realeaseGameBundle();
        ResMgr.game_pack = new ResPack(bundle);
    }
    //释放子包所有内容，包括子包的配置文件
    public static realeaseGameBundle() {
        if (ResMgr.game_pack) {
            ResMgr.game_pack.realeseAll();
            ResMgr.game_pack = null;
        }
    }

    public static getResPack(is_game:boolean){
        return is_game ? this.game_pack : this.main_pack;
    }
    public static getBundle(is_game:boolean) {
        if(is_game)return ResMgr.game_pack && ResMgr.game_pack.m_bundle;
        else return ResMgr.main_pack.m_bundle;
    }

    public static addRes(name: string, res: any) {
        ResMgr.all_res.set(name, res);
    }
    public static deleteRes(name: string) {
        ResMgr.all_res.delete(name);
    }
    public static getSfByAtlas(atlas_name: string, sp_name: string) {
        let atlas = ResMgr.all_res.get(atlas_name);
        return atlas ? atlas.getSpriteFrame(sp_name) : null;
    }
    public static getRes(name: string) {
        return ResMgr.all_res.get(name);
    }
    public static log() {
        ResMgr.main_pack.logRes();
        ResMgr.game_pack?.logRes();
    }

}
