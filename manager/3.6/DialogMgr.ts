
import { Node, instantiate, isValid, Prefab, find, Sprite, UITransform, tween, view, NodeEventType, BlockInputEvents, Layers, Color, game, director, Director, Asset } from 'cc';

import { ResMgr } from './ResMgr';
import { ExtraMgr } from './ExtraMgr';

export class DialogMgr {
    // public static Cache_Count = 3;
    private static cache_keys = new Array();
    //目前显示的页面代码
    private static showing_arr = [];
    //预制体表
    private static cache_pres: Map<string, Prefab> = new Map();
    //节点表,用来存一些需要缓存的节点
    private static cache_nodes: Map<string, Node> = new Map();
    //正在等待加载的界面
    private static wait_queue = [];

    // private static continue_info = [];
    private static cur_holds = [];
    private static hold_info = [];

    private static is_waiting = false;
    private static loading_name = "";

    private static sf_bg = null;
    private static nodes_bg = new Array();

    public static init() {
        DialogMgr.sf_bg = ResMgr.atlas_common.getSpriteFrame("white");
        for(let i = 0 ; i < 2 ; i++){
            DialogMgr.cur_holds.push("");
            DialogMgr.hold_info.push(new Array());
        }
        director.on(Director.EVENT_BEFORE_SCENE_LAUNCH, DialogMgr.onChangeScene, DialogMgr);
    }
    private static onChangeScene(){
        for(let i = 0 ; i < 2 ; i++){
            if(DialogMgr.cur_holds[i].length == 0)continue;
            DialogMgr.cur_holds[i] = "";
            DialogMgr.hold_info[i].length = 0;
        }
        DialogMgr.showing_arr.length = 0;
        DialogMgr.wait_queue.length = 0;
        DialogMgr.loading_name = "";
    }
    public static hide(script: any) {
        for (let i = DialogMgr.showing_arr.length - 1; i >= 0; i--) {
            if (DialogMgr.showing_arr[i][1] == script) {
                DialogMgr.hideNode(i);
                return;
            }
        }
    }
    public static hideByName(script_name: string) {
        if(script_name == DialogMgr.loading_name)DialogMgr.loading_name = "";
        for (let i = DialogMgr.showing_arr.length - 1; i >= 0; i--) {
            if (DialogMgr.showing_arr[i][0] == script_name) {
                DialogMgr.hideNode(i);
                return;
            }
        }
    }

    public static hideNode(index: number) {
        let info = DialogMgr.showing_arr[index];
        DialogMgr.showing_arr.splice(index, 1);
        if (isValid(info[1])) info[1].hideAnim(info);

        let script = info[1];
        let hold_index = script.hold_index;
        if(script.hold_index > -1){
            DialogMgr.cur_holds[hold_index] = "";
            if(DialogMgr.hold_info[hold_index].length > 0){
                let info = DialogMgr.hold_info[hold_index].shift();
                DialogMgr.showNode(info[0], info[1], info[2], info[3]);
            }
        }
        // else if(isValid(info[2])){
        //     info[2].removeFromParent();
        //     DialogMgr.nodes_bg.push(info[2]);
        // }
        // if (DialogMgr.continue_info.length == 0) return;
        // let continues = DialogMgr.continue_info;
        // if (continues[0][0] == info[0]) {
        //     continues.shift();
        //     if (continues.length > 0) {
        //         DialogMgr.showByResUrl(continues[0]);
        //     }
        // }
    }
    private static addKey(key: string) {
        //测试直接释放的情况
        // let pre = DialogMgr.cache_pres.get(key);
        // if (isValid(pre)) pre.decRef();
        // DialogMgr.cache_pres.delete(key);
        // return;
        let count = 3;
        let index = DialogMgr.cache_keys.indexOf(key);
        if (DialogMgr.cache_keys.length < count) {
            if (index == -1) DialogMgr.cache_keys.push(key);
        }
        else {
            if (index == -1) {
                let del_key = DialogMgr.cache_keys[0];
                let pre = DialogMgr.cache_pres.get(del_key);
                if (isValid(pre)) pre.decRef();
                DialogMgr.cache_pres.delete(del_key);
                index = 0;
            }
            for (let i = index; i < count - 1; i++) {
                DialogMgr.cache_keys[i] = DialogMgr.cache_keys[i + 1]
            }
            DialogMgr.cache_keys[count - 1] = key;
        }
    }
    public static afterHide(info: any) {
        let script = info[1];
        let node = script.node;
        if (script.cache_mode == 0) DialogMgr.addKey(info[0]);
        if (!isValid(node)) return;
        if (script.cache_mode == 2) {
            node.removeFromParent();
            DialogMgr.cache_nodes.set(info[0], node);
        }
        else {
            node.destroy();
        }
        if (isValid(info[2])) {
            info[2].removeFromParent();
            DialogMgr.nodes_bg.push(info[2]);
        }
    }
    public static getDialogByName(name: string) {
        for (let i = DialogMgr.showing_arr.length - 1; i >= 0; i--) {
            if (DialogMgr.showing_arr[i][0] == name) {
                if (isValid(DialogMgr.showing_arr[i][1])) {
                    return DialogMgr.showing_arr[i][1];
                } else {
                    DialogMgr.showing_arr.splice(i, 1);
                }
            }
        }
        return null;
    }
    // public static getInfoByName(name: string) {
    //     for (let i = DialogMgr.showing_arr.length - 1; i >= 0; i--) {
    //         if (DialogMgr.showing_arr[i][0] == name) {
    //             if (isValid(DialogMgr.showing_arr[i][1])) {
    //                 return DialogMgr.showing_arr[i];
    //             } else {
    //                 DialogMgr.showing_arr.splice(i, 1);
    //             }
    //         }
    //     }
    //     return null;
    // }

    private static getNodeMask() {
        if (DialogMgr.nodes_bg.length > 0) {
            let bg = DialogMgr.nodes_bg.pop();
            bg.off(NodeEventType.TOUCH_END);
            return bg;
        }
        let node = new Node("BG");
        node.layer = Layers.Enum.DEFAULT;
        node.addComponent(UITransform);
        let sp = node.addComponent(Sprite);
        sp.sizeMode = Sprite.SizeMode.CUSTOM;
        sp.spriteFrame = DialogMgr.sf_bg;
        sp.color = new Color(0, 0, 0, 255);
        return node;
    }
    private static showNode(node: Node, script_name: string, data: any, parent: Node = null) {
        parent = parent || find("Canvas");
        let script: any = node.getComponent(script_name);
        let hold_index = script.hold_index;
        if(hold_index > -1){
            if(DialogMgr.cur_holds[hold_index].length > 0){
                if(script_name != DialogMgr.cur_holds[hold_index]){
                    DialogMgr.hold_info[hold_index].push([node, script_name, data, parent]);
                }
                return;
            }
            else{
                DialogMgr.cur_holds[hold_index] = script_name;
            }
        }
        let bg = null;
        if (script.fill_screen) {
            script.node.getComponent(UITransform).height = ExtraMgr.fill_height;
            script.node.toY(ExtraMgr.fill_bottom);
            //(view.getVisibleSize());
        }
        else if (script.auto_mask > 0) {
            bg = DialogMgr.getNodeMask();
            bg.setParent(parent);
            bg.toOpacity(0);
            tween(bg).toPro(0.2, { opacity: script.mask_opacity }).start();
            let uitrans_bg = bg.getComponent(UITransform);
            uitrans_bg.setContentSize(view.getVisibleSize());
            let block = bg.getComponent(BlockInputEvents);
            if (script.auto_mask == 2) {
                if(block)block.destroy();
                bg.on(NodeEventType.TOUCH_END, function () {
                    DialogMgr.hideByName(script_name);
                }, DialogMgr);
            }
            else{
                if(!block)bg.addComponent(BlockInputEvents);
            }
        }
        node.setParent(parent);
        node.addComponent(BlockInputEvents);
        DialogMgr.showing_arr.push([script_name, script, bg]);
        script.show(data);
    }

    public static showByResUrl(info: Array<string>, data?: any, is_game = false, parent: Node = null) {
        console.log("%cshowByResUrl " + info[0], "color:green");
        if (DialogMgr.loading_name == info[0]) return;
        let dialog = DialogMgr.getDialogByName(info[0]);
        if (dialog) {
            dialog.updateData(data);
            return;
        }
        let node = DialogMgr.cache_nodes.get(info[0]);
        if (node) {
            if (isValid(node)) {
                DialogMgr.showNode(node, info[0], data);
                return;
            } else {
                DialogMgr.cache_nodes.delete(info[0]);
            }
        }
        let pre = DialogMgr.cache_pres.get(info[1]);
        if (isValid(pre)) {
            let node = instantiate(pre);
            DialogMgr.showNode(node, info[0], data, parent);
            return;
        }
        if (DialogMgr.is_waiting) {
            DialogMgr.wait_queue.push([info, data, is_game, parent]);
            return;
        }
        DialogMgr.is_waiting = true;
        DialogMgr.loading_name = info[0];
        let bundle = ResMgr.getBundle(is_game);
        console.time(`LoadDialog ${info[0]}`);
        bundle.load("prefab/ui/" + info[1], Prefab, (err: any, prefab: Prefab) => {
            console.timeEnd(`LoadDialog ${info[0]}`);
            if (!err) {
                if(DialogMgr.loading_name == info[0]){
                    prefab.addRef();
                    DialogMgr.cache_pres.set(info[0], prefab);
                    let node = instantiate(prefab);
                    DialogMgr.showNode(node, info[0], data, parent);
                }
            } else {
                console.log(err);
            }
            DialogMgr.is_waiting = false;
            DialogMgr.loading_name = "";
            DialogMgr.next();
        });
    }
    // public static showDialogs(arr_info: Array<any>) {
    //     DialogMgr.continue_info = arr_info;
    //     DialogMgr.showByResUrl(arr_info[0])
    // }
    public static script_can_load(script_name: string) {
        if (DialogMgr.loading_name == script_name) return false;
        for (let i = 0, len = DialogMgr.showing_arr.length; i < len; i++) {
            if (DialogMgr.showing_arr[i][0] == script_name) {
                return false;
            }
        }
        return true;
    }

    public static next() {
        if (DialogMgr.is_waiting) return;
        if (DialogMgr.wait_queue.length == 0) return;
        let data = DialogMgr.wait_queue.shift();
        DialogMgr.showByResUrl(data[0], data[1], data[2], data[3]);
    }

    public static hideTop() {
        let len = DialogMgr.showing_arr.length;
        if (len == 0) return false;
        DialogMgr.hideNode(len - 1);
        return true;
    }
    // public onSceneChange(){

    // }
    public static closeShow(clear_cache: boolean) {
        for (let i = DialogMgr.showing_arr.length - 1; i >= 0; i--) {
            DialogMgr.hideNode(i);
        }
        DialogMgr.showing_arr.length = 0;
        DialogMgr.wait_queue.length = 0;
        DialogMgr.loading_name = "";
        for(let i = 0 ; i < DialogMgr.hold_info.length ; i++){
            for(let j = 0 ; j < DialogMgr.hold_info[i].length ; j++){
                let pre = this.cache_pres.get(DialogMgr.hold_info[i][j][0]);
                if(isValid(pre))pre.decRef();
            }
            DialogMgr.hold_info[i].length = 0;
        }
        for(let i = 0 ; i < DialogMgr.cur_holds.length; i++)DialogMgr.cur_holds[i] = "";
        if (!clear_cache) return;
        DialogMgr.cache_nodes.forEach(function (v) {
            v.destroy();
        })
        DialogMgr.cache_nodes.clear();
    }
    public static clearGameDialog() {
        for (let i = DialogMgr.showing_arr.length - 1; i >= 0; i--) {
            let info = DialogMgr.showing_arr[i];
            if (!isValid(info[1])){
                DialogMgr.showing_arr.splice(i, 1);
                continue;
            }
            if (!info[1].game_dialog) continue;
            info[1].node.destroy();
            if (isValid(info[2])) {
                info[2].removeFromParent();
                DialogMgr.nodes_bg.push(info[2]);
            }
        }
        DialogMgr.cache_nodes.forEach(function(v, k){
            if(!isValid(v)){
                DialogMgr.cache_nodes.delete(k);
                return;
            }
            if((v.getComponent(k) as any).game_dialog){
                DialogMgr.cache_nodes.delete(k);
                v.destroy();
            }
        })
        DialogMgr.cache_pres.forEach(function(v, k){
            if(!isValid(v)){
                DialogMgr.cache_pres.delete(k);
                return;
            } 
            if(v.data.getComponent(k).game_dialog){
                v.decRef();
                DialogMgr.cache_pres.delete(k);
            }
        })
    }
    public static log() {
        console.log("显示的界面", DialogMgr.showing_arr);
        console.log("缓存的节点", DialogMgr.cache_nodes);
        console.log("缓存的预制体", DialogMgr.cache_pres);
        console.log("等待的界面", DialogMgr.wait_queue);
        console.log("缓存key", DialogMgr.cache_keys);
    }
}
