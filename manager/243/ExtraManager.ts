
import { App } from "../App";
function initBtnSound(){
    cc.Button.prototype["_onTouchEnded"] = function (event) {
        if (!this.interactable || !this.enabledInHierarchy) return;
        if (this._pressed) {
            App.AudioManager.playSound("sounds/click")
            cc.Component.EventHandler.emitEvents(this.clickEvents, event);
            this.node.emit('click', this);
        }
        this._pressed = false;
        this._updateState();
        event.stopPropagation();
    }
    cc.Toggle.prototype["_onTouchEnded"] = function (event) {
        if (!this.interactable || !this.enabledInHierarchy) return;
        if (this._pressed) {
            App.AudioManager.playSound("sounds/tab")
            cc.Component.EventHandler.emitEvents(this.clickEvents, event);
            this.node.emit('click', this);
        }
        this._pressed = false;
        this._updateState();
        event.stopPropagation();
    }
}

var old_dispatch = null;
var startTime=0;
var intervalTime=500;
function changeDispatch(){
    //@ts-ignore
    cc.view.multiTouchEnable=false;
    old_dispatch = cc.Node.prototype.dispatchEvent;
    cc.Node.prototype.dispatchEvent = function (event) {
        if(event.type == "touchstart"){
            let nowTime=cc.sys.now();
            if(startTime > 0 && (nowTime-startTime) < intervalTime){
                console.log("%cclick too fast", "color:red");
                return;
            }
            startTime = nowTime;
        }
        old_dispatch.call(this, event);  
    };
}

function editBegin(){
    if(cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID){
        cc.EditBox.prototype["_onTouchBegan"]=function (event){
            let touch=event.touch
            let node=event.target
            let upLoadY=touch.getLocationY()
            let nodePos=node.convertToNodeSpaceAR(touch.getLocation())
            upLoadY-=nodePos.y+node.height/2
            jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity", "upLoadEditBoxY", "(Ljava/lang/String;)V",upLoadY*cc.view.getScaleY())                                
        }
    }
}
export class ExtraManager{
    public static init(){
        initBtnSound();
        changeDispatch();
        editBegin();
    }
    public static resetDispatch(node:cc.Node){
        node.dispatchEvent = old_dispatch;
    }
}
