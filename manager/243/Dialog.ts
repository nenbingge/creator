
import { DialogManager } from "../manager/DialogManager"
import { WebControl } from "../web/WebControl";

const {ccclass, property} = cc._decorator;

@ccclass
export class Dialog extends cc.Component {
    protected  _play_show: boolean = false;
    protected _play_close: boolean = false;
    public _can_sys_return: boolean = true;
    public _is_full_screen: boolean = false;
    public _cache_mode: number = 0;

    @property(cc.Node)
    node_action: cc.Node = null;

    public init(data:any){

    }

    protected afterShow(){}
    protected afterHide(){}
    protected show(data: any){
        let scenes_name = this.name;
        WebControl.getInstance().trackRecord(scenes_name,"","",this,null);
        this.init(data);
        if(this._play_show){
            this.showAnim();
        }
    }
    protected showAnim(){
        let t = 0.2;
        this.node_action.setScale(0.8);
        this.node_action.opacity = 120;
        cc.tween(this.node_action)
            .to(t , {scale:1 , opacity:255})
            .call(()=>{this.afterShow();})
            .start();
    }
    protected hide(){
        if(this._play_close){
            this.hideAnim();
        }else{
            DialogManager.hide(this);
        }
    }
    protected hideAnim(){
        let t = 0.2;
        cc.tween(this.node_action)
            .to(t , {scale: 0.2})
            .call(()=>{
                this.afterHide();
                DialogManager.hide(this);
            })
            .start();
    }
}
