



import { JsbCommon } from "../JsbCommon";
import { DialogManager } from "./DialogManager"
import { AudioManager } from "./AudioManager"
import { ResManager } from "./ResManager";
import { AtlasManager } from "./AtlasManager";
import { i18nMgr } from "./i18nMgr";
import { AudioCommon } from "../common/AudioCommon";


export class SetManager{
    private static have_init:boolean = false;
    private static force_debug = true;
    public static init(){
        if(SetManager.have_init)return;
        //关掉多点触摸
        i18nMgr.initLanguage();
        cc.macro.ENABLE_MULTI_TOUCH = false;
        if(cc.dynamicAtlasManager){
            cc.dynamicAtlasManager.enabled = false;
            cc.dynamicAtlasManager.reset();
        }
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, onKeyDown, this);
        //关闭浏览器自动全屏
        cc.view.enableAutoFullScreen(false);
        if (cc.sys.isNative) {
            //屏幕常亮
            //@ts-ignore
            jsb.Device.setKeepScreenOn(true);
        }
        AudioCommon.addBtnSound()
        ResManager.init();
        AudioManager.getSaveSet();
        if(SetManager.force_debug){
            //@ts-ignore
            cc.profiler.showStats();
        }
        SetManager.have_init = true;
    }
}


function onKeyDown (e) {
    switch(e.keyCode) {
        case cc.macro.KEY.back:
            let ret = DialogManager.hideTop();
            if (ret) {//第一次点击返回                
                return;
            }

            if(JsbCommon.exitSdk()) {
                return;
            }

            // require("DialogCommon").showMsgBox2("退出","确认",function(){
            //     cc.game.end();
            // });
            
            break;
        case cc.macro.KEY.a:
            //@ts-ignore
            cc.log(cc.assetManager.assets);//._count);
            break;
        case cc.macro.KEY.s:
            ResManager.logAtlas();
            AtlasManager.logAtlas();
            break;
        case cc.macro.KEY.z:
            AtlasManager.showAtlas("lobby",true);
            break;
        case cc.macro.KEY.x:
            AtlasManager.showAtlas("",false);
            break;
        // case cc.macro.KEY.q:
        //     cc.log(cc.dynamicAtlasManager.enabled)
        //     cc.dynamicAtlasManager.showDebug(true);
        //     break;
        // case cc.macro.KEY.w:
        //     cc.dynamicAtlasManager.showDebug(false);
        //     break;
    }
}