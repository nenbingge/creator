import { App } from "../App";
import { BaseDlg } from "./BaseDlg";

const MaxWaitIndex = 1;

export class DlgManager{
    // private _dlgLayer:cc.Node=null;
    private _dlgBgSpriteFrame:cc.SpriteFrame;
    private loading: any = null;

    //[key, dlg, dlgbg]
    private dlgs_is_show: Array<any> = null;

    private loading_dlg = "";
    //[dlgname, data]
    private cache_need_load: Array<any> = null;

    //[[key, dlg, data]]
    private cache_wait_info: Array<any> = null;
    private cur_wait_dlg: Array<string> = null;

    public init(bg_sf:cc.SpriteFrame, loading_pre: cc.Prefab) {
        bg_sf.addRef();
        this._dlgBgSpriteFrame = bg_sf;
        this.dlgs_is_show = new Array();
        this.cache_need_load = new Array();
        this.cache_wait_info = new Array();
        this.cur_wait_dlg = new Array();
        for(let i = 0 ; i <= MaxWaitIndex ; i++){
            this.cache_wait_info.push([]);
            this.cur_wait_dlg.push("");
        }
        let node_loading = cc.instantiate(loading_pre);
        cc.game.addPersistRootNode(node_loading);
        node_loading.zIndex = 2;
        this.loading = node_loading.getComponent("Loading");
        node_loading.active = false;
    }
    public clearAllDlgs(): void {
        cc.warn("clearAllDlgs")
        for(let i = 0 ; i < this.dlgs_is_show.length ; i++){
            let info = this.dlgs_is_show[i];
            info[1].destroy();
            info[2].destroy();
        }
        this.dlgs_is_show.length = 0;
        this.cache_need_load.length = 0;
        for(let i = 0 ; i <= MaxWaitIndex ; i++){
            this.cache_wait_info[i].length = 0;
            this.cur_wait_dlg[i] = "";
        }
        this.loading_dlg = "";
        App.DlgManager.hideLoading();
    }
    
    private getIndexByPath(dlg_path: string){
        for(let i = this.dlgs_is_show.length - 1 ; i >= 0 ; i--){
            if(this.dlgs_is_show[i][0] == dlg_path)return i;
        }
        return -1;
    }
    private getIndexByDialog(dialog: BaseDlg){
        for(let i = this.dlgs_is_show.length - 1 ; i >= 0 ; i--){
            if(this.dlgs_is_show[i][1] == dialog)return i;
        }
        return -1;
    }


    public showLoading(desc:string = ""){
        if(this.loading){
            this.loading.node.active = true;
            this.loading.init(desc);
        }
    }
    public hideLoading(){
        if(this.loading && this.loading.node.active){
            App.AudioManager.playSound("sounds/return");
            this.loading.node.active = false;
        }
    }
    private next(){
        if(this.cache_need_load.length > 0){
            let info = this.cache_need_load.shift();
            // console.log("next");
            if(this.showDlg(info[0], info[1]) == -1){
                this.next();
            }
        }
    }

    public showDlg(dlg_path: string, data?: any) {
        console.log("%cdlgmgr::showDlg------","color:#18039D",dlg_path);
        let index = this.getIndexByPath(dlg_path);
        let info = this.dlgs_is_show[index];
        let have_loading = this.loading_dlg.length > 0;
        if(info && cc.isValid(info[1])){
            info[1].updateData(data);
            return have_loading ? 1 : -1;
        }
        for(let i = 1 ; i < this.cache_wait_info.length ; i++){
            let arr = this.cache_wait_info[i];
            for(let j = 0 ; j < arr.length ; j++){
                if(arr[j][0] == dlg_path){
                    arr[j][2] = arr[j][1].dealNewData(arr[j][2], data);
                    return 1;
                }
            }
        }
        if(have_loading){
            if(dlg_path == this.loading_dlg)console.log("%c"+dlg_path+" is loading","color:red");
            else {
                for(let i = 0 ; i < this.cache_need_load.length ; i++){
                    if(this.cache_need_load[i][0] == dlg_path)return 1;
                }
                this.cache_need_load.push([dlg_path, data]);
            }
            return 1;
        }
        this.loading_dlg = dlg_path;
        App.main_bundle.load(`dialogs/${dlg_path}/main`,cc.Prefab,(error,prefab:cc.Prefab)=>{
            if(error){
                cc.warn(error);
            }
            else{
                if(this.loading_dlg == dlg_path){
                    let node = cc.instantiate(prefab);
                    let dlg = node.getComponent(BaseDlg);
                    this.showNode(dlg_path, dlg, data);
                }
            }
            this.loading_dlg = "";
            this.next();
        });  
        return 1;
    }
    private showNode(dlg_path:string, dlg:BaseDlg, data:any){
        if(dlg.wait_mode > 0){
            if(this.cur_wait_dlg[dlg.wait_mode].length > 0){
                this.addWaitInfo([dlg_path, dlg, data]);
                return;
            }
            else{
                this.cur_wait_dlg[dlg.wait_mode] = dlg_path;
            }
        }
        let node_dlg = dlg.node;
        let parent = cc.Canvas.instance.node;
        let bg = null;
        let auto_mask = dlg.auto_mask;
        if(data && data.hasOwnProperty("auto_mask")){
            auto_mask = data.auto_mask;
        }
        node_dlg.addComponent(cc.BlockInputEvents);
        //是否创建背景
        if(auto_mask > 0){
            bg=new cc.Node(dlg_path + "_bg");
            let sp=bg.addComponent(cc.Sprite);
            sp.spriteFrame=this._dlgBgSpriteFrame;
            sp.sizeMode = cc.Sprite.SizeMode.CUSTOM;
            bg.setParent(parent);
            bg.setContentSize(parent.getContentSize());
            bg.setPosition(cc.Vec2.ZERO);
            bg.opacity=0;
            let tween_bg = cc.tween(bg).to(0.2, {opacity: dlg.bg_opacity});
            if(auto_mask > 1){
                tween_bg.call(function(){
                    bg.on(cc.Node.EventType.TOUCH_START,function(){
                        dlg.hide();
                    });
                })
            }
            else{
                bg.addComponent(cc.BlockInputEvents);
            }
            tween_bg.start();
        }
        node_dlg.setParent(parent);
        dlg.init(data);
    
        if(data && data.zIndex){
            if(bg)bg.zIndex = data.zIndex;
            node_dlg.zIndex = data.zIndex;
        }
        let widget=node_dlg.getComponent(cc.Widget);
        if(!widget && dlg.is_full){
            node_dlg.setContentSize(parent.getContentSize());
        }
        dlg.showDlgAnim();
        this.dlgs_is_show.push([dlg_path, dlg, bg]);
    }
    public moveToTop(dlg:BaseDlg){
        let index = this.getIndexByDialog(dlg);
        if(index == this.dlgs_is_show.length - 1)return;
        let last = this.dlgs_is_show[this.dlgs_is_show.length - 1];
        let info = this.dlgs_is_show[index];
        if(last.node.zIndex > 0){
            if(info[2])info[2].node.zIndex = last.node.zIndex;
            info[1].node.zIndex = last.node.zIndex;
        }
        if(info[2])info[2].node.setSiblingIndex();
        info[1].node.setSiblingIndex();
    }
    private addWaitInfo(info:any){
        let arr = this.cache_wait_info[info[1].wait_mode];
        arr.push(info);
    }

    public hideDlgByPath(dlg_path: string) {
        console.log("%cdlgmgr::hideDlgByPath-----","color:#9D0325",dlg_path);
        let index = this.getIndexByPath(dlg_path);
        if(index == -1)return;
        this.hideDlgByIndex(index);
    }
    public hideDlgByDialog(dlg: BaseDlg) {
        console.log("%cdlgmgr::hideDlgByDialog-----","color:#9D0325");
        let index = this.getIndexByDialog(dlg);
        if(index == -1)return;
        this.hideDlgByIndex(index);
    }
    private hideDlgByIndex(index: number) {
        let info = this.dlgs_is_show[index];
        console.log("%cdlgmgr::hide:"+info[0], "color:#9D0325");
        let wait_mode = info[1].wait_mode;
        let bg = info[2];
        // let check_next = info[1].wait_mode > 0;
        if(cc.isValid(bg)){
            cc.tween(bg)
                .to(0.2, {opacity:0})
                .call(function(){
                    bg.destroy();
                })
                .start();
        }
        let dlg = info[1];
        if(cc.isValid(dlg)){
            dlg.hideDlgAnim();
            this.dlgs_is_show.splice(index, 1);
        }
        if(wait_mode > 0 && info[0] == this.cur_wait_dlg[wait_mode]){
            this.cur_wait_dlg[wait_mode] = "";
            let arr = this.cache_wait_info[wait_mode];
            for(let i = 0 ; i < arr.length ; i++){
                console.log("dlgmgr::",arr[i][0]);
            }
            if(arr.length > 0){
                let cache_info = arr.shift();
                this.showNode(cache_info[0], cache_info[1], cache_info[2]);
            }
        }
    }


    public updateData(dlg_path: string, data?: any) {
        let index = this.getIndexByPath(dlg_path);
        if(index == -1)return;
        let info = this.dlgs_is_show[index];
        cc.isValid(info[1]) && info[1].updateData(data);
    }

    public closeTopDlg():boolean{
        if(this.dlgs_is_show.length > 0){
            let info = this.dlgs_is_show[this.dlgs_is_show.length - 1];
            if(info[1].auto_hide){
                this.hideDlgByIndex(this.dlgs_is_show.length - 1);
                return true;
            }
        }
        return false;
    }

    public getInfo(dlg_path:string){
        let index = this.getIndexByPath(dlg_path);
        return this.dlgs_is_show[index];
    }

    
    // private log(){
    //     let str = "dlgmanager::showing:";
    //     for(let i = 0 ; i < this.dlgs_is_show.length ; i++){
    //         str += i +  "  " + this.dlgs_is_show[i][0] + ",";
    //     }
    //     console.log(str);
    //     str = "dlgmanager::load::";
    //     for(let i = 0 ; i < this.cache_need_load.length ; i++){
    //         str += i + "  " + this.cache_need_load[i][0] + ",";
    //     }
    //     console.log(str);
    //     str = "dlgmanager::wait::";
    //     for(let i = 0 ; i < this.cache_wait_info[1].length ; i++){
    //         str += i + "  " + this.cache_wait_info[1][i][0] + ",";
    //     }
    //     console.log(str);
    // }

}