import { i18nLabel } from "../components/i18nLabel";
import { i18nSprite } from "../components/i18nSprite";
import { TSCommon } from "../TSCommon";
/*
使用说明
在label或richtext组件添加多语言组件i18nLabel

修改内容：
1在编辑器设置字符串索引
2动态修改
i18nLabel.setString(index,[可选参数])
参数会替换字符串中#0，#1

3动态获取字符串
i18nMgr.getString(12,[可选参数])

4获取配置字符串后再修改
i18nLabel.setStringFunc((str)=>{
    return str+'xxx'
})


*/
export class i18nMgr {
    public static language = "";     // 当前语言
    private static inited = false;     // 当前语言

    private static labelArr: i18nLabel.i18nLabel[] = [];        // i18nLabel 列表
    private static labelData: { [key: string]: string } = {};   // 文字配置
    private static spriteArr: i18nSprite.i18nSprite[] = [];       // i18nSprite 列表
    private static bundleLabelData = {}
    private static bundleSpriteData = {}
    private static bundleLabelArr = {}//label引用
    private static bundleSpriteArr = {}
    private static bundleNameArr = []

    //编辑器加载，提前初始化
    private static checkInit(bundleName) {
        if (!this.inited) {
            this.inited=true
            this.setLanguage("en",bundleName);
            return false
        }
        return true;
    }

    public static initLanguage(){
        let l=TSCommon.getCookie('language','en')
        this.setLanguage(l,null)
    }
    private static initBundle(bundleName){

    }

    /**
     * 设置语言
     */
    public static setLanguage(language: string,bundleName) {
        if (this.language === language) {
            return;
        }

        TSCommon.setCookie('language',language)
        this.inited=true

        this.language = language;

        let p = this.reloadLabel();//promise
        let p2 = this.reloadBundleLabel(bundleName);//promise.all
        let self=this
        p.then(()=>{
            p2.then(_=>{
                self.refreshLabel()
                self.refreshBundleLabel()
            })
        })

        this.reloadSprite();
    }

    //获取对应的字符串
    public static t(opt) {
        return this.labelData[opt] || opt;
    }

    //得到引用后可以直接访问
    public static getBundleObject(bundleName) {
        return this.bundleLabelData[bundleName]
    }

    public static addBundle(bundleName) {
        let p=this._loadBundle(bundleName)
        let self=this;
        p.then(_=>{
            self.refreshBundleLabel()
        })
    }

    //添加bundle里的资源
    private static _loadBundle(bundleName) {
        if(!this.language){
            this.setLanguage('en')
        }
        let url = "i18n/label/" + this.language;
        let self = this

        //编辑器可能某个界面打开突然加载。正常使用要手动提前加载
        if (CC_EDITOR) {
            let p = new Promise((resolve, reject) => {
                self.loadJsonByEditor(bundleName, (e, j) => {
                    if (e) {
                        cc.log(e)
                        return;
                    }
                    self.addBundleLabelData(e, j, bundleName)

                    resolve()
                })
            })

            return p


        } else {
            let p = new Promise((resolve, reject) => {
                cc.assetManager.loadBundle(bundleName, (err, b) => {
                    if (err) {
                        cc.log(err)
                        return;
                    }

                    b.load('i18n/label/' + this.language, cc.JsonAsset, (err, data: cc.JsonAsset) => {
                        self.addBundleLabelData(err, data, bundleName)
                        resolve()
                    });
                })
            })

            return p

        }
    }

    private static loadJsonByEditor(bundleName, callback) {
        const assetPath = 'db://assets/' + 'games/' + bundleName + '/i18n/label/' + this.language + '.json';
        const fileUuid = Editor.assetdb.remote.urlToUuid(assetPath);

        Editor.assetdb.remote.queryAssets(assetPath, 'json', (e, j) => {
            if (e) {
                cc.log(e)
                return;
            }
            if (j.length == 0) {
                cc.log('找不到bundle', bundleName,assetPath)
                return
            }
            cc.assetManager.loadAny({ uuid: j[0].uuid }, (err, json) => {
                if (err) {
                    cc.log(err, json)
                    return;
                }
                callback(err, json)
            });

        })
    }

    public static addBundleLabelData(e, data, bundleName) {
        let self = this
        if (e) {
            cc.log(e)

            self.bundleLabelData[bundleName] = {};
        } else {
            self.bundleLabelData[bundleName] = data.json;
        }
    }


    /**
     * 添加或移除 i18nLabel
     */
    public static _addOrDelLabel(label: i18nLabel.i18nLabel, isAdd: boolean) {
        if (isAdd) {
            this.labelArr.push(label);
        } else {
            let index = this.labelArr.indexOf(label);
            if (index !== -1) {
                this.labelArr.splice(index, 1);
            }
        }
    }

    /**
     * 添加或移除 i18nLabel
     */
    public static addBundleLabel(label: i18nLabel.i18nLabel, bundleName, isAdd) {

        if (!this.bundleLabelArr[bundleName]) {
            this.bundleLabelArr[bundleName] = []
        }

        if (isAdd) {
            this.bundleLabelArr[bundleName].push(label);
        } else {
            let index = this.bundleLabelArr[bundleName].indexOf(label);
            if (index !== -1) {
                this.bundleLabelArr[bundleName].splice(index, 1);
            }
        }
    }

    public static getString(opt: number, params: string[]=[]): string {
        if(!this.checkInit())return;
        if (!params || params.length === 0) {
            return this.labelData[opt] || opt;
        }

        let str = this.labelData[opt] || opt;
        for (let i = 0; i < params.length; i++) {
            str = str.replace('##'+i, params[i]);
        }
        return str;
    }

    public static getBundleString(opt: number, bundleName, params: string[]=[]): string {
        if(CC_EDITOR){
            if(!this.bundleLabelData[bundleName]){
                return this._loadBundle(bundleName)
            }else{
                return this.bundleLabelData[bundleName][opt] || opt;
            }
        }


        if (!this.bundleLabelData[bundleName]) {
            return;
        }

        if (!params ||params.length === 0) {
            return this.bundleLabelData[bundleName][opt] || opt;
        }
        let str = this.bundleLabelData[bundleName][opt] || opt;
        for (let i = 0; i < params.length; i++) {
            str = str.replace('##'+i, params[i]);
        }
        return str;
    }

    /**
     * 添加或移除 i18nSprite
     */
    public static _addOrDelSprite(sprite: i18nSprite.i18nSprite, isAdd: boolean) {
        if (isAdd) {
            this.spriteArr.push(sprite);
        } else {
            let index = this.spriteArr.indexOf(sprite);
            if (index !== -1) {
                this.spriteArr.splice(index, 1);
            }
        }
    }

    public static _getSprite(path: string, cb: (spriteFrame: cc.SpriteFrame) => void) {
        this.checkInit();
        cc.resources.load("i18n/sprite/" + this.language + "/" + path, cc.SpriteFrame, (err, spriteFrame: cc.SpriteFrame) => {
            if (err) {
                return cb(null);
            }
            cb(spriteFrame);
        });
    }


    private static reloadLabel() {
        let url = "i18n/label/" + this.language;
        let self = this

        let p = new Promise((resolve, reject) => {
            cc.resources.load(url, (err, data: cc.JsonAsset) => {
                if (err) {
                    console.error(err);
                    self.labelData = {};
                } else {
                    self.labelData = data.json;
                }
                resolve()
            });
        })

        return p
    }

    private static reloadBundleLabel(bundleName) {
        let keys = Object.keys(this.bundleLabelArr)
        let pArr=[]
        if(bundleName && !this.bundleLabelArr[bundleName]){
            pArr.push(this._loadBundle(bundleName))
        }

        for (var i = 0; i < keys.length; ++i) {
            let bn = keys[i]
            pArr[i] = this._loadBundle(bn)
        }
        return Promise.all(pArr)
    }

    public static refreshLabel() {
        for (let one of this.labelArr) {
            one._resetValue();
        }
    }

    private static refreshBundleLabel(bundleName){
        let keys = Object.keys(this.bundleLabelArr)

        for (var i = 0; i < keys.length; ++i) {
            let bundleArr = this.bundleLabelArr[keys[i]]

            if (!bundleArr) continue
            for (var j = 0; j < bundleArr.length; ++j) {
                bundleArr[j]._resetValue();
            }
        }
    }

    private static reloadSprite() {
        for (let one of this.spriteArr) {
            one._resetValue();
        }
    }



}
