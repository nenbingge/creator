


export class DialogManager{
    //目前显示得得页面代码
    private static showing_arr = [];
    //预制体表
    private static prefab_map = {};
    //节点表,用来存一些需要缓存得节点
    private static cache_node = {};
    //正在等待加载的界面
    private static wait_queue = [];

    private static is_waiting = false;
    private static loading_name = "";


    public static hide(script:any){
        for(let i = DialogManager.showing_arr.length - 1 ; i >= 0 ; i--){
            if(DialogManager.showing_arr[i][1] == script){
                DialogManager.hideNode(i);
                break;
            }
        }
    }

    public static hideByName(script_name:string){
        for(let i = DialogManager.showing_arr.length - 1 ; i >= 0 ; i--){
            if(DialogManager.showing_arr[i][0] == script_name){
                DialogManager.hideNode(i);
                break;
            }
        }
    }

    public static hideNode(index:number){
        let script_name = DialogManager.showing_arr[index][0];
        let script = DialogManager.showing_arr[index][1];
        let mode = script._cache_mode;
        let node = script.node;
        if(mode == 2){
            if(cc.isValid(node)){
                node.removeFromParent(false);
                DialogManager.cache_node[script_name] = node;
            }
        }else{
            if(cc.isValid(node)){
                node.destroy();
            }
            if(mode == 0 && DialogManager.prefab_map[script_name]){
                DialogManager.prefab_map[script_name].decRef();
                delete DialogManager.prefab_map[script_name];
            }
        }
        DialogManager.showing_arr.splice(index , 1);
    }

    public static showNode(node:cc.Node , script_name:string , dialog_data:any){
        node.parent = cc.Canvas.instance.node;
        let script = node.getComponent(script_name);
        // script._script_name = script_name;
        DialogManager.showing_arr.push([script_name , script]);
        script.show(dialog_data);
    }

    public static showByResUrl(script_name:string, prefab_path:string, dialog_data:any){
        if(DialogManager.loading_name == script_name)return;
        for(let i = 0 , len = DialogManager.showing_arr.length ; i < len ; i++){
            if(DialogManager.showing_arr[i][0] == script_name ){
                if(cc.isValid(DialogManager.showing_arr[i][1])){
                    DialogManager.showing_arr[i][1].show(dialog_data);
                    return;
                }else{
                    DialogManager.showing_arr.splice(i , 1);
                    DialogManager.showByResUrl(script_name, prefab_path, dialog_data);
                    return;
                }
            }     
        }
        if(DialogManager.cache_node[script_name]){
            let node = DialogManager.cache_node[script_name];
            if(cc.isValid(node)){
                DialogManager.showNode(node , script_name , dialog_data);
                return;
            }
        }
        if(DialogManager.prefab_map[script_name]){
            let node = cc.instantiate(DialogManager.prefab_map[script_name]);
            DialogManager.showNode(node , script_name , dialog_data);
            return;
        }
        if(DialogManager.is_waiting){
            DialogManager.wait_queue.push([script_name, prefab_path, dialog_data]);
            return;
        }
        DialogManager.is_waiting = true;
        DialogManager.loading_name = script_name;
        cc.resources.load(prefab_path, cc.Prefab , (err, prefab) => {
            if (!err) {
                prefab.addRef();
                let node = cc.instantiate(prefab);
                //@ts-ignore
                DialogManager.showNode(node , script_name , dialog_data);
                DialogManager.prefab_map[script_name] = prefab;
            }else{
                cc.log(err);
            }
            DialogManager.is_waiting = false;
            DialogManager.loading_name = "";
            DialogManager.next();
        });
    }

    public static script_can_load(script_name:string){
        if(DialogManager.loading_name == script_name)return false;
        for(let i = 0 , len = DialogManager.showing_arr.length ; i < len ; i++){
            if(DialogManager.showing_arr[i][0] == script_name){
                return false;
            }
        }
        return true;
    }

    public static next(){
        if(DialogManager.is_waiting)return;
        if(DialogManager.wait_queue.length == 0)return;
        let data = DialogManager.wait_queue.shift();
        DialogManager.showByResUrl(data[0], data[1], data[2]);
    }

    public static hideTop(){
        let len = DialogManager.showing_arr.length;
        if(len == 0)return false;
        DialogManager.hideNode(len-1);
        return true;
    }

    public static closeShow(){
        for(let i = DialogManager.showing_arr.length - 1 ; i >= 0 ; i--){
            DialogManager.showing_arr[i][1].hide();
        }
        DialogManager.showing_arr = [];
        DialogManager.wait_queue = [];
    }
}