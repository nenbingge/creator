

class ResPack{
    public m_bundle: cc.AssetManager.Bundle;
    private all_res: Map<string , any>;
    private all_tag: Map<string , Set<string>>;

    constructor(bundle:cc.AssetManager.Bundle){
        this.m_bundle = bundle;
        this.all_res = new Map<string , any>();
        this.all_tag = new Map<string , Set<string>>();
    }
    public loadWithTag(tag:string , path:string, type:any, callback:Function){
        let set = this.all_tag.get(tag);
        if(!set){
            set = new Set();
            this.all_tag.set(tag , set);
        }
        let add_ref = false;
        if(!set.has(path)){
            add_ref = true;
            set.add(path);
        }
        let old_res = this.all_res.get(path);
        if (old_res) {
            if(cc.isValid(old_res)){
                if(add_ref){
                    old_res.addRef();
                }
                callback(null,old_res);
                return;
            }else{
                this.all_res.delete(path);
            }
        }
        let self = this;
        this.m_bundle.load(path, type, function(err, res:cc.Asset) {
            if (err) {
                callback(err, null);
                return;
            }
            if(!self.all_tag.get(tag)){
                if(res.refCount <= 0){
                    cc.assetManager.releaseAsset(res);
                }
                return;
            }else{
                if(add_ref){
                    res.addRef();
                }
                if (!self.all_res.has(path)) {
                    self.all_res.set(path, res);
                }
            }
            callback(err , res);
        });
    }
    public realeaseByTag(tag:string){
        let paths = this.all_tag.get(tag);
        if(paths){
            let self = this;
            let res:any;
            paths.forEach(element => {
                res = self.all_res.get(element);
                cc.isValid(res) && res.decRef();
            });
            this.all_tag.delete(tag);
        }
    }
    public realeseAll(){
        for(let k in this.all_tag){
            this.realeaseByTag(k);
        }
        cc.assetManager.removeBundle(this.m_bundle);
    }
}

export class ResManager{
    private static res_pack: ResPack = null;
    private static game_pack: ResPack = null;
    private static all_altas = new Map<string,any>();

    public static init(){
        this.res_pack = new ResPack(cc.resources);
    }
    //加载resource资源
    public static loadWithTag(tag:string , path:string, type:any, callback:Function){
        ResManager.res_pack.loadWithTag(tag , path, type, callback);
    }
    //释放resource相关资源
    public static realeaseByTag(tag:string){
        ResManager.res_pack.realeaseByTag(tag);
    }

    //游戏子包的加载和释放
    public static gameLoadWithTag(tag:string , path:string, type:any, callback:Function){
        ResManager.game_pack && ResManager.game_pack.loadWithTag(tag , path, type, callback);
    }
    public static gameRealeaseByTag(tag:string){
        ResManager.game_pack && ResManager.game_pack.realeaseByTag(tag);
    }

    //设置子包
    public static setGameBundle(bundle:any){
        if(ResManager.game_pack && ResManager.game_pack.m_bundle && 
           ResManager.game_pack.m_bundle.name == bundle.name)
        {
            return;
        }
        ResManager.realeaseGameBundle();
        ResManager.game_pack = new ResPack(bundle);
    }
    //释放子包所有内容，包括子包的配置文件
    public static realeaseGameBundle(){
        if(ResManager.game_pack){
            ResManager.game_pack.realeseAll();
            ResManager.game_pack = null;
        }
    }

    //增加一个图集的引用
    public static addAtlas(name:string , atlas:any){
        ResManager.all_altas.set(name , atlas);
    }
    public static deleteAtlas(name:string){
        ResManager.all_altas.delete(name);
    }
    public static getSfByAtlas(atlas_name: string , sp_name: string){
        let atlas = ResManager.all_altas.get(atlas_name);
        return atlas ? atlas.getSpriteFrame(sp_name) : null;
    }
    public static logAtlas(){
        cc.log("static atlas");
        cc.log(ResManager.all_altas);
    }

}