/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.

 http://www.cocos.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated engine source code (the "Software"), a limited,
 worldwide, royalty-free, non-assignable, revocable and non-exclusive license
 to use Cocos Creator solely to develop games on your target platforms. You
 shall not use Cocos Creator software for developing other software or tools
 that's used for developing games. You are not granted to publish, distribute,
 sublicense, and/or sell copies of Cocos Creator.

 The software or tools in this License Agreement are licensed, not sold.
 Xiamen Yaji Software Co., Ltd. reserves all rights not expressly granted to
 you.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/
#include "Game.h"
#include "cocos/platform/java/jni/JniHelper.h"
#include "bindings/jswrapper/SeApi.h"
#include "network/HttpClient.h"
#include "network/HttpRequest.h"
#include "application/ApplicationManager.h"
//#include "network/Uri.h"
//#import <cocos/bindings/jswrapper/SeApi.h>
#ifndef GAME_NAME
#define GAME_NAME "CocosGame";
#endif

#ifndef SCRIPT_XXTEAKEY
#define SCRIPT_XXTEAKEY "";
#endif

static bool have_exception = false;
static ccstd::string lastUrl = "";
static cc::network::HttpRequest *request = new cc::network::HttpRequest();

unsigned char ToHex(unsigned char x) {
  return  x > 9 ? x + 55 : x + 48;
}
ccstd::string UrlEncode(const char *str, ccstd::string& strTemp) {
  size_t len = strlen(str);
  for (size_t i = 0; i < len; i++) {
    if (isalnum((unsigned char)str[i]) ||
        (str[i] == '-') ||
        (str[i] == '_') ||
        (str[i] == '.') ||
        (str[i] == '~'))
      strTemp += str[i];
    else if (str[i] == ' ')
      strTemp += "%20";
    else {
      strTemp += '%';
      strTemp += ToHex((unsigned char)str[i] >> 4);
      strTemp += ToHex((unsigned char)str[i] % 16);
    }
  }
  return strTemp;
}

Game::Game() = default;

int Game::init() {
    _windowInfo.title = GAME_NAME;
    // configurate window size
    // _windowInfo.height = 600;
    // _windowInfo.width  = 800;

  #if CC_DEBUG
    _debuggerInfo.enabled = true;
  #else
    _debuggerInfo.enabled = false;
  #endif
    _debuggerInfo.port = 6086;
    _debuggerInfo.address = "0.0.0.0";
    _debuggerInfo.pauseOnStart = false;


    _xxteaKey = SCRIPT_XXTEAKEY;


    const char* pkey = "";
    char pKey[16];
    memset(pKey, 0, sizeof(pKey));
    pKey[0] = '6';
    pKey[1] = 'O';
    pKey[2] = 'b';
    pKey[3] = 'A';
    pKey[4] = 'C';
    pKey[5] = 'V';
    pKey[6] = 'R';
    pKey[7] = 'T';
    pKey[8] = '\0';
    char pSign[8];
    memset(pSign, 0, sizeof(pSign));
    pSign[0] = 'X';
    pSign[1] = 'X';
    pSign[2] = 'T';
    pSign[3] = 'E';
    pSign[4] = 'A';
    pSign[5] = '\0';
    setXXTeaKeyAndSign(pKey, strlen(pKey), pSign, strlen(pSign));


    BaseGame::init();

    CC_LOG_ERROR("JsbGame Game init log");

//  se::ScriptEngine::getInstance()->setExceptionCallback([](const char *location, const char *message, const char *stack){
//      CC_LOG_ERROR("JsbGame setExceptionCallback");
//    cc::JniHelper::callStaticVoidMethod("org/cocos2dx/jsb/JsbClass", "postException", 5, "JSError", message, stack);
//  });
    return 0;
}

void Game::handleException(const char *location, const char *message, const char *stack){
  if(have_exception){
//      CC_LOG_ERROR("err exit%c",&message);
//    cc::ApplicationManager::getInstance()->getCurrentApp()->close();
    return;
  }
    CC_LOG_ERROR("err %c",&message);
  have_exception = true;
  ccstd::string url = getExceptionUrl();
  if(url.empty())return;
  ccstd::string msg = "";
  UrlEncode(message, msg);
  UrlEncode(stack, msg);
  lastUrl = url + "?UserId=" + getExceptionUserid() + "&Msg=" + msg;
  request->setRequestType(cc::network::HttpRequest::Type::GET);
  request->setUrl(lastUrl);
  cc::network::HttpClient::getInstance()->send(request);
//  request.release();
//  cc::JniHelper::callStaticVoidMethod("org/cocos2dx/jsb/JsbClass", "postException", 5, "JSError", message, stack);
}

void Game::onPause() { BaseGame::onPause(); }

void Game::onResume() { BaseGame::onResume(); }

void Game::onClose() { BaseGame::onClose(); }

CC_REGISTER_APPLICATION(Game);
