//@ts-nocheck

const {ccclass, menu, property, requireComponent} = cc._decorator;

var _touchStartHandler = function (touch, event) {
    var sp_btn = this.owner;
    if(!cc.isValid(sp_btn))return false;
    var pos = touch.getLocation();
    pos = sp_btn.node.convertToNodeSpaceAR(pos);
    if(cc.Intersection.pointInPolygon(pos, sp_btn.points)){
        event.type = cc.Node.EventType.TOUCH_START;
        event.touch = touch;
        event.bubbles = true;
        sp_btn.btn._onTouchBegan(event);
        return true;
    }
    return false;
};
var _touchMoveHandler = function (touch, event) {
    var sp_btn = this.owner;
    if(!cc.isValid(sp_btn))return;
    event.type = cc.Node.EventType.TOUCH_MOVE;
    event.touch = touch;
    event.bubbles = true;
    sp_btn.btn._onTouchMove(event);
};
var _touchEndHandler = function (touch, event) {
    var sp_btn = this.owner;
    if(!cc.isValid(sp_btn))return;
    var pos = touch.getLocation();
    event.touch = touch;
    event.bubbles = true;
    pos = sp_btn.node.convertToNodeSpaceAR(pos);
    if (cc.Intersection.pointInPolygon(pos, sp_btn.points)) {
        event.type = cc.Node.EventType.TOUCH_END;
        sp_btn.btn._onTouchEnded(event);
    }
    else {
        event.type = cc.Node.EventType.TOUCH_CANCEL;
        sp_btn.btn._onTouchCancel(event);
    }
};
var _touchCancelHandler = function (touch, event) {
    var sp_btn = this.owner;
    if(!cc.isValid(sp_btn))return;
    event.type = cc.Node.EventType.TOUCH_CANCEL;
    event.touch = touch;
    event.bubbles = true;
    sp_btn.btn._onTouchCancel(event);
};

@ccclass
@requireComponent(cc.Button)
@menu("自定义组件/特殊按钮")
export class ButtonSpecial extends cc.Component {
    private btn: cc.Button;
    @property([cc.Vec2])
    public points: Array<cc.Vec2> = [];

    public onLoad(){
        if(this.points.length < 3)return;
        this.btn = this.node.getComponent(cc.Button);
        if(!this.btn)this.btn = this.node.addComponent(cc.Button);
        let empty_fun = function(){};
        this.btn._registerNodeEvent = empty_fun;
        this.btn._unregisterNodeEvent = empty_fun;

        this.node._touchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            owner: this,
            onTouchBegan: _touchStartHandler,
            onTouchMoved: _touchMoveHandler,
            onTouchEnded: _touchEndHandler,
            onTouchCancelled: _touchCancelHandler
        });

    }
    public onEnable(){
        cc.internal.eventManager.addListener(this.node._touchListener, this.node);
    }
    public onDisable(){
        cc.internal.eventManager.removeListener(this.node._touchListener);
    }
}
