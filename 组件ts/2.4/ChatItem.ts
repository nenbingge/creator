import { GEventType } from "../../GEventType";
import { TSCommon } from "../../TSCommon";
import { ViewItemPro } from "../../components/ViewItemPro";
import { ResManager } from "../../manager/ResManager";

const { ccclass, property } = cc._decorator;

@ccclass
export class MsgItem extends ViewItemPro {

    @property(cc.Boolean)
    public is_self: Boolean = false;

    @property(cc.Label)
    public lb_nickname: cc.Label = null;

    @property(cc.Label)
    public lb_content: cc.Label = null;

    @property(cc.Sprite)
    public sp_blow: cc.Sprite = null;

    @property(cc.Node)
    public node_content_bg: cc.Node = null;

    @property(cc.Sprite)
    public sp_head: cc.Sprite = null;

    @property(cc.Sprite)
    public sp_sex: cc.Sprite = null;

    @property(cc.Sprite)
    public sp_vip: cc.Sprite = null;

    private state = 0;
    private content_state = 0;

    /*
        ChannelID: 0
        ChatMsg: "aaa"
        FaceId: 3
        FaceUrl: ""
        NickName: "guest"
        RecvUserID: 0
        SendUserID: 24612568
        Sex: 0
        Vip: 0
    */
    //初始化
    public init(data:any,index:number) {
        this.lb_nickname.string = data.NickName;
        let sex_path = data.Sex == 0 ? "other_nan" : "other_nv";
        this.sp_sex.spriteFrame = ResManager.getSfByAtlas("chat" , sex_path);

        if(!data.escape){
            if(data.ChatMsg[0] == "f"){
                data.ChatMsg = data.ChatMsg.slice(1 , data.ChatMsg.length);
                data.escape = 1;
            }else{
                data.escape = 2;
            }
        }
        let is_font = data.escape == 1;
        this.lb_content.node.active = is_font;
        this.sp_blow.node.active = !is_font;
        if(is_font){
            this.lb_content.overflow = cc.Label.Overflow.RESIZE_HEIGHT;
            this.lb_content.node.width = 340;
            this.lb_content.string = data.ChatMsg;
            this.state = 1;
        }else{
            this.state = 0;
            this.content_state = 2;
            this.updateSize();
            // cc.log(data.ChatMsg);
        }
        // if (data.length < 22) {
            // this.is_one_line = true;
            // this.updateSize();
            // TSCommon.dispatchEvent(GEventType.NODE_SIZE_CHANGED);
        // }
    }
    // protected start() {
    //     if (!this.is_self) return;
    //     //头像逻辑还未处理
    //     // ResManager.loadHead(0, UserInfo.getHeadUrl(), (sp) => {
    //     //   if (!cc.isValid(this.node)) return;
    //     //   this.sp_head.spriteFrame = sp;
    //     // });
    // }

    private updateSize(){
        if(this.content_state == 0){
            this.lb_content.node.y = 34;
            this.node_content_bg.height = 83;
            this.lb_nickname.node.y = 110;
            this.node.height = 115;
            this.node_content_bg.width = this.lb_content.string.length * 26;
        }else if(this.content_state == 1){
            this.node_content_bg.width = this.lb_content.node.width + 108;
            this.lb_content.node.y = 17;
            this.node_content_bg.height = this.lb_content.node.height + 14;
            this.lb_nickname.node.y = this.lb_content.node.height + 40;
            this.node.height = this.lb_content.node.height + 50;
        }else if(this.content_state == 2){
            this.node_content_bg.setContentSize(cc.size(220 , 100));
            this.lb_nickname.node.y = 126;
            this.node.height = 138;
        }
        
    }

    private updateWidth(){
        this.node_content_bg.width = this.lb_content.node.width + 108;
    }

    protected update(dt:number) {
        if (this.state == 1) {
            this.state = 2;
        }
        else if (this.state == 2) {
            TSCommon.dispatchEvent(GEventType.NODE_SIZE_CHANGED);
            if (this.lb_content.node.height > 50) {
                this.state = 0;
                this.content_state = 1;
                this.updateSize();
            } else {
                this.content_state = 0;
                this.updateSize();
                this.lb_content.overflow = cc.Label.Overflow.NONE;
                this.state = 3;
            }
        }
        else if (this.state == 3) {
                this.updateWidth();
                this.state = 0;
                // console.log(this.node_content_bg.width);
                // console.log(this.lb_content.node.width);
                // TSCommon.dispatchEvent(GEventType.NODE_SIZE_CHANGED);
            }
        }
}
