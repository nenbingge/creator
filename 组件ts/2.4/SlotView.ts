import { SlotItem } from "./SlotItem";


const {ccclass, property} = cc._decorator;

const Dir = cc.Enum({
    ToBottom: 0,
    ToTop: 1,
    ToLeft: 2,
    ToRight: 3,
})
 //0静止 1加速中 2最大速度 3停止中 4 回弹1 5 回弹2
export enum SlotViewState{
    Normal,
    AddSpeed,
    MaxSpeed,
    Stoping,
    Beyond1,
    Beyond2,
    Remove,
    Fall,
}

@ccclass
export class SlotView extends cc.Component {
    @property(cc.Prefab)
    public pre_item: cc.Prefab = null;
    
    @property({type:Dir})
    public dir = Dir.ToTop;
    @property({tooltip:"起始速度"})
    public begin_speed:number = 600;
    @property({tooltip:"加速度"})
    public acc_speed: number = 2400;
    @property({tooltip:"最大速度"})
    public max_speed: number = 3000;
    @property({tooltip:"停止时速度"})
    public stop_speed = 4000;
    @property({tooltip:"回弹距离"})
    public beyond: number = 60;
    @property({tooltip:"回弹速度"})
    public back_speed:number = 800;
    @property({tooltip:"最大元素数量"})
    public max_count: number = 10;
    @property({tooltip:"元素间距,固定大小元素可填"})
    public item_offset: number = 0;
   
    @property({tooltip:"是否需要有元素掉落"})
    public need_fall = false;
    @property({type:cc.Float,tooltip:"掉落时运动时间,可能是多段运动(上下上)"})
    public fall_times:number[] = [];
    @property({tooltip:"掉落回弹距离"})
    public fall_beyond = 50;
    //components

    //0静止 1滚动 2最大速度 3停止中 4 回弹1 5 回弹2
    //6
    private state: number = 0;
    private index: number = 0;
    private cur_speed: number = 0;
    private cur_time: number = 0;
    private data:Array<any>;
    private data_index: number = 0;
    private first_index = 0;
    private fall_bottom = 0;
    private list: Array<SlotItem> = [];
    private last = 0;
    private last2 = 0;
    private forward = 0;
    private behind = 0;
    private target_dis = 0;
    private temp_arr: Array<any>;
    private fall_index = 0;
    private fall_dis_arr:Array<number>;
    private fall_begin_pos:Array<number>;

    private call_obj: any = null;
    private func_move_over: Function = null;
    private func_fall_over: Function = null;

    public init(index:number){
        this.index = index;
        this.temp_arr = new Array(this.last);
        this.last = this.max_count - 1;
        this.last2 = this.last - 1;
        this._calculateBoundary();
        this.list = new Array();
        for(let i = 0; i < this.max_count; i++){
            let node = cc.instantiate(this.pre_item);
            node.setParent(this.node);
            this.list.push(node.getComponent(SlotItem));
            this.list[i].random();
        }
        this.resetPos(this.last);
        if(this.need_fall){
            this.fall_dis_arr = new Array(this.max_count);
            this.fall_begin_pos = new Array(this.max_count);
        }
    }
    public setCallBack(obj:any, func_move_over:Function, func_fall_over:Function = null){
        this.call_obj = obj;
        this.func_move_over = func_move_over;
        this.func_fall_over = func_fall_over;
    }
    public getPos(index:number, len:number){
         switch(this.dir){
            case Dir.ToBottom:
                index = this.first_index - len + this.index;
                return this.list[index].node.y;
            case Dir.ToTop:
                break;
            case Dir.ToLeft:
                break;
            case Dir.ToRight:
                break;
    }
    }
    public resetPos(end_index:number){
        let total = 0, add = 0;
        let node:cc.Node = null;
        let base = this.forward;
        if(this.item_offset > 0){
            switch(this.dir){
                case Dir.ToBottom:
                    if(end_index < this.last){
                        node = this.list[end_index + 1].node;
                        base = node.y + this.item_offset * (1 - node.anchorY);
                    }
                    for(let i = end_index ; i >= 0; i--){
                        node = this.list[i].node;
                        add = node.height * node.scaleY;
                        node.y = base + total + add * node.anchorY;
                        total += add;
                    }
                    break;
                case Dir.ToTop:
                    if(end_index < this.last){
                        node = this.list[end_index + 1].node;
                        base = node.y - this.item_offset * node.scaleY;
                    }
                    for(let i = end_index ; i >= 0; i--){
                        node = this.list[i].node;
                        add = node.height * node.scaleY;
                        node.y = base - total - add * (1 - node.anchorY);
                        total += add;
                    }
                    break;
                case Dir.ToLeft:
                    break;
                case Dir.ToRight:
                    break;
    
            }
        }
        else{
            switch(this.dir){
                case Dir.ToBottom:
                    if(end_index < this.last){
                        node = this.list[end_index + 1].node;
                        base = node.y + node.height * (1 - node.anchorY) * node.scaleY;
                    }
                    for(let i = end_index ; i >= 0; i--){
                        node = this.list[i].node;
                        add = node.height * node.scaleY;
                        node.y = base + total + add * node.anchorY;
                        total += add;
                    }
                    break;
                case Dir.ToTop:
                    if(end_index < this.last){
                        node = this.list[end_index + 1].node;
                        base = node.y - node.height * node.anchorY * node.scaleY;
                    }
                    for(let i = end_index ; i >= 0; i--){
                        node = this.list[i].node;
                        add = node.height * node.scaleY;
                        node.y = base - total - add * (1 - node.anchorY);
                        total += add;
                    }
                    break;
                case Dir.ToLeft:
                    break;
                case Dir.ToRight:
                    break;
    
            }
        }
    }
    private _calculateBoundary(){        
        switch(this.dir){
            case Dir.ToBottom:
                this.behind = this.node.height * (1 - this.node.anchorY);
                this.forward = this.behind - this.node.height;
                break;
            case Dir.ToTop:
                this.forward = this.node.height * (1 - this.node.anchorY);
                this.behind = this.forward - this.node.height;
                break;
            case Dir.ToLeft:
                this.forward = -this.node.width * this.node.anchorX;
                this.behind = this.forward + this.node.width;
                break;
            case Dir.ToRight:
                this.behind = -this.node.width * this.node.anchorX;
                this.forward = this.behind + this.node.width;
                break;
        }
        // cc.log("forward:",this.forward);
        // cc.log("behind:",this.behind);
    }

    private resetIndex(){
        for(let i = 0 ; i < this.list.length ; i++)this.list[i].index = i;
    }
    private logIndex(flag=""){
        cc.log("----------------index:"+flag+"----------------");
        for(let i = 0 ; i < this.list.length ; i++)cc.log(this.list[i].index.toString());
        cc.log("----------------index_end----------------")
    }
    // switch(this.dir){
    //     case Dir.ToBottom:
    //         break;
    //     case Dir.ToTop:
    //         break;
    //     case Dir.ToLeft:
    //         break;
    //     case Dir.ToRight:
    //         break;
    // }

    public beginMove(){
        this.state = 1;
        this.cur_speed = this.begin_speed;
        // this.cur_continue = this.continue_time;
    }
    public forceStop(){
        this.state = 0;
    }
    public stop(data:any){
        this.data = data;
        this.state = 3;
        this.cur_speed = this.stop_speed;
        switch(this.dir){
            case Dir.ToBottom:
                let index = 0;
                for(let i = 0 ; i < this.max_count ; i++){
                    if(this.list[i].node.y < this.behind){
                        index = i;
                        break;
                    }
                }
                let add = 2 - index;
                if(add > 0){
                    // cc.log("fewfw",this.index);
                    this.frontedList(add);
                    index = 2;
                }
                else{
                    add = 0;
                }
                let cur_index = index - 2;
                this.data_index = this.data.length - 1;
                this.first_index = cur_index;
                // cc.log("stop first ",this.index,":",this.first_index);
                let top = 0;
                // cc.log("cur_index:",cur_index);
                if(cur_index > this.data_index)top = cur_index - this.data_index;
                for(let i = cur_index ; i >= top ; i--){
                    this.list[i].init(this.data[this.data_index]);
                    this.data_index--;
                }
                this.resetPos(cur_index + add);
                let node = this.list[cur_index].node;
                this.target_dis = node.y - this.forward - node.height * node.anchorY * node.scaleY;
                break;
            case Dir.ToTop:
                break;
            case Dir.ToLeft:
                break;
            case Dir.ToRight:
                break;
         }
    }
    private last2first(){
        let item = this.list[this.last];
        for(let i = this.max_count - 2; i >= 0; i--){
            this.list[i + 1] = this.list[i];
        }
        this.list[0] = item;
    }
    private frontedList(count:number){
        let first_change = this.max_count - count;
        for(let i = 0 ; i < count ; i++){
            this.temp_arr[i] = this.list[first_change + i];
        }
        for(let i = this.max_count - count; i >= 0; i--){
            this.list[i + count] = this.list[i];
        }
        for(let i = 0 ; i < count ; i++){
            this.list[i] = this.temp_arr[i];
        }
    }
    //消除元素:元素索引，显示元素的个数
    public remove(arr_pos:Array<number>, len:number, new_eles:Array<any>){
        this.state = SlotViewState.Remove;
        let add = this.first_index + 1 - len;
        let tier = 0, cur_index = -1, cur_add = 0;
        for(let i = arr_pos.length - 1 ; i >= 0 ; i--){
            let real_index = arr_pos[i]  + add;
            this.temp_arr[i] = this.list[real_index];
            if(real_index < cur_index - 1){
                tier++;
                for(let j = cur_index - 1 ; j > real_index ; j--){
                    this.list[j].drop_tier = tier;
                    this.list[j + cur_add] = this.list[j];
                }
            }
            cur_add++;
            cur_index = real_index;
        }
        for(let i = cur_index -1 ; i >= 0 ; i--){
            this.list[i].drop_tier = tier + 1;
            this.list[i + cur_add] = this.list[i];
        }
        for(let i = arr_pos.length - 1 ; i >= 0 ; i--){
            this.list[i] =  this.temp_arr[i];
            this.list[i].drop_tier = tier + 1;
        }
        if(this.first_index + 1 < new_eles.length){cc.warn("元素总数不足");}
        add = this.first_index + 1 - new_eles.length;
        for(let i = 0 , l = new_eles.length ; i < l ; i++){
            this.list[add + i].init(new_eles[i]);
        }
        this.resetPos(arr_pos[0] + add - 2 + arr_pos.length);
        // this.logIndex();
        let bottom_count = len - arr_pos[arr_pos.length - 1] - 1;
        this.fall_bottom = this.first_index - bottom_count;
    }
    public fall(){
        if(this.state != SlotViewState.Remove)return;
        // cc.log("fall_bottom ",this.index,":",this.fall_bottom);
        if(this.fall_times.length <= 0){
            this.resetPos(this.fall_bottom);
            return;
        }
        this.state = SlotViewState.Fall;
        let base = this.forward;
        let node:cc.Node = null;
        if(this.fall_bottom < this.first_index){
            node = this.list[this.fall_bottom + 1].node;
            if(this.item_offset > 0)base = node.y + this.item_offset * (1 - node.anchorY);
            else base = node.y + node.height * (1 - node.anchorY) * node.scaleY;
        }
        if(this.item_offset > 0){
            for(let i = this.fall_bottom ; i >= 0 ; i--){
                node = this.list[i].node;
                this.fall_begin_pos[i] = node.y;
                this.fall_dis_arr[i] = node.y - base - this.item_offset * node.anchorY;
                base += this.item_offset;
            }
        }
        else{
            let add = 0;
            for(let i = this.fall_bottom ; i >= 0 ; i--){
                node = this.list[i].node;
                this.fall_begin_pos[i] = node.y;
                add = node.height * node.scaleY;
                this.fall_dis_arr[i] = node.y - base - add * (1 - node.anchorY);
                base += add;
            }
        }
        // cc.log("begin:",this.fall_begin_pos)
        // cc.log("dis:",this.fall_dis_arr);
        this.fall_index = 0;
        this.cur_time = 0;
    }
    private fallBottom(dt:number){
        this.cur_time += dt;
        let over = false;
        if(this.cur_time >= this.fall_times[this.fall_index]){
            this.cur_time = this.fall_times[this.fall_index];
            over = true;
        }
        let node:cc.Node = null;
        let progress = this.cur_time / this.fall_times[this.fall_index];
        if(this.fall_index % 2 == 0){
            for(let i = 0, l = this.fall_bottom ; i <= l ; i++){
                node = this.list[i].node;
                node.y = this.fall_begin_pos[i] - progress * this.fall_dis_arr[i];
            }
        }
        else{
            for(let i = 0, l = this.fall_bottom ; i <= l ; i++){
                node = this.list[i].node;
                node.y = this.fall_begin_pos[i] + progress * this.fall_dis_arr[i];
            }
        }
        if(!over)return;
        this.fall_index++;
        if(this.fall_index >= this.fall_times.length){
            this.state = 0;
            this.func_fall_over && this.func_fall_over.call(this.call_obj, this.index);
        }
        else{
            let max_tier = this.list[0].drop_tier;
            let part = this.fall_beyond / max_tier;
            // cc.log(this.list);
            // cc.log("part:",part);
            for(let i = 0, l = this.fall_bottom ; i <= l ; i++){
                this.fall_begin_pos[i] = this.list[i].node.y;
                this.fall_dis_arr[i] = part * this.list[i].drop_tier;
            }
            this.cur_time = 0;
        }
    }
    private moveBottom(dis:number){
        let not_monitor = this.state > 3;
        if(this.state == 3){
            this.target_dis -= dis;
            if(this.target_dis <= 0){
                dis += this.target_dis;
                this.state = 4;
                this.cur_speed = this.back_speed;
                this.target_dis = this.beyond;
            }
        }
        else if(this.state == 4){
            this.target_dis -= dis;
            if(this.target_dis < 0){
                dis += this.target_dis;
                this.target_dis = this.beyond;
                this.state = 5;
            }
            dis = -dis;
        }
        else if(this.state == 5){
            this.target_dis -= dis;
            if(this.target_dis < 0){
                dis += this.target_dis;
                this.state = 0;
                this.func_move_over && this.func_move_over.call(this.call_obj, this.index);
            }
        }
        for(let i = 0 ; i < this.max_count ; i++){
            this.list[i].node.y -= dis;
        }
        if(not_monitor)return;
        if(this.list[this.last2].node.y > this.forward)return;
        this.last2first();
        if(this.state >= 3){
            this.first_index++;
            if(this.data_index >= 0){
                this.list[0].init(this.data[this.data_index]);
                this.data_index--;
            }
        }
        else this.list[0].random();
        if(this.item_offset > 0)this.list[0].node.y = this.list[1].node.y + this.item_offset;
        else{
            let node0 = this.list[0].node;
            let node1 = this.list[1].node;
            node0.y = node1.y + node1.height * (1 - node1.anchorY) * node1.scaleY + node0.height * node0.anchorY* node0.scaleY;
        }            
    }

    protected update(dt: number) {
        if(this.state == 0 || this.state == SlotViewState.Remove)return;
        if(this.state == SlotViewState.Fall){
            this.fallBottom(dt);
            return;
        }
        let dis = this.cur_speed * dt;
        if(this.state == 1){
            this.cur_speed += this.acc_speed * dt;
            if(this.cur_speed >= this.max_speed){
                this.cur_speed = this.max_speed;
                this.state = 2;
            }
        }
        switch(this.dir){
            case Dir.ToBottom:
                this.moveBottom(dis);
                break;
            case Dir.ToTop:
                break;
            case Dir.ToLeft:
                break;
            case Dir.ToRight:
                break;
        }
    }
}
