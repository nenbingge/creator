import { _decorator, Sprite, assetManager, isValid } from 'cc';
const { ccclass, property, executeInEditMode, disallowMultiple, requireComponent, menu } = _decorator;

import { i18nMgr, i18Type } from "../manager/i18nMgr";
import { i18Base } from './i18Base';
import { ResMgr } from '../manager/ResMgr';

@ccclass("i18nSprite")
@executeInEditMode
@requireComponent(Sprite)
@disallowMultiple
@menu("多语言/i18nSprite")
export class i18nSprite extends i18Base {
    protected i18_type = i18Type.Sprite;
 
    @property
    public use_res_atlas = false;
    @property
    public atlas: string = "";
    private m_sp: Sprite = null;

    onLoad() {
        this.m_sp = this.node.getComponent(Sprite);
    }
    public refresh() {
        if (!this.i18n_index.length) return;
        if (!isValid(this.m_sp)) return;
        if(this.use_res_atlas){
            this.m_sp.spriteFrame = ResMgr.getSfByAtlas(this.atlas, this.i18n_index + "_" + i18nMgr.getLanguage());
            return;
        }
        let sf = null;
        if (this.bundle) {
            sf = i18nMgr.getGameSf(this.i18n_index, this.atlas);
        } else {
            sf = i18nMgr.getSf(this.i18n_index, this.atlas);
        }
        this.m_sp.spriteFrame = sf;
    }
    public async refreshInEditor() {
        if (!this._preview || !this.i18n_index.length) {
            this.m_sp.spriteFrame = null;
            return;
        }
        let bundle = this.bundle;
        if (!bundle.length) bundle = "master";
        if (this.atlas.length) {
            let assetPath = "";
            if (bundle == "master") assetPath = `db://assets/master/i18n/atlas/${this.atlas}_${i18nMgr.getLanguage()}.plist`;
            else assetPath = `db://assets/games/${this.bundle}/i18n/atlas/${this.atlas}_${i18nMgr.getLanguage()}.plist`;
            const info = await Editor.Message.request('asset-db', 'query-assets', { ccType: "cc.SpriteAtlas", pattern: assetPath });
            assetManager.loadAny({ uuid: info[0].uuid }, (err, atlas) => {
                this.m_sp.spriteFrame = atlas.getSpriteFrame(this.i18n_index);
            });
        }
        else {
            let assetPath = "";
            if (bundle == "master") assetPath = `db://assets/master/i18n/sprite/${i18nMgr.getLanguage()}/${this.i18n_index}.png`;
            else assetPath = `db://assets/games/${this.bundle}/i18n/sprite/${i18nMgr.getLanguage()}/${this.i18n_index}.png`;
            const info = await Editor.Message.request('asset-db', 'query-assets', { ccType: "cc.SpriteFrame", pattern: assetPath });
            assetManager.loadAny({ uuid: info[0].uuid }, (err, sprite) => {
                this.m_sp.spriteFrame = sprite;
            });
        }
    }

}
