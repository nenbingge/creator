
import { _decorator, ScrollView, Prefab, instantiate, Size, Node, UITransform, Vec3, NodeEventType } from "cc";
import { ViewItemPro } from "./ViewItemPro"

const { ccclass, property } = _decorator;

const EPSILON = 1e-4;
const _tempVec3 = new Vec3();

class ViewItemInfo {
    public node: Node;
    public ui_tran: UITransform;
    public itemComponent: ViewItemPro;
    public type: number;

    constructor(type: number, node: Node) {
        this.node = node;
        this.type = type;
        this.itemComponent = node.getComponent(ViewItemPro);
        this.ui_tran = node.getComponent(UITransform);
    }
}

@ccclass("ScrollViewMore")
export class ScrollViewMore extends ScrollView {
    @property([Prefab])
    public pres_item: Prefab[] = [];
    @property
    public space = 0;

    private uitrans: UITransform = null;

    private cache_pools: Array<Array<ViewItemInfo>> = [];
    private all_height: number[] = [];
    private item_list: Array<ViewItemInfo> = [];
    private begin_index: number = 0;
    private end_index: number = 0;
    private data: Array<any> = [];
    private p: any = null;
    private func2type: Function = null;

    protected onLoad() {
        this.uitrans = this._content.getComponent(UITransform);
        this.refreshSize();
        for (let i = 0, len = this.pres_item.length; i < len; i++) {
            this.cache_pools.push([]);
            let height = this.pres_item[i].data.getComponent(UITransform).height;
            this.all_height.push(height);
        }
        this.node.on(NodeEventType.SIZE_CHANGED, this.refreshSize, this);
    }
    protected onDestroy(): void {
        let func = function(item: ViewItemInfo){item.node.destroy();}
        for(let i = 0 ; i < this.item_list.length ; i++){
            this.cache_pools[i].forEach(func);
        }
    }
    private refreshSize() {
        this.uitrans.setContentSize(this.node.getComponent(UITransform).contentSize);
    }

    public setTypeFunc(func: Function) {
        this.func2type = func;
    }

    private getItemByType(type: number) {
        if (type < 0 || type >= this.cache_pools.length) return null;
        let pool = this.cache_pools[type];
        if(pool.length > 0){
            let item = pool.pop();
            item.node.active = true;
            return item;
        }
        let node = instantiate(this.pres_item[type]);
        let item = new ViewItemInfo(type, node);
        node.setParent(this.content);
        return item;
    }
    private pushItemByType(item: ViewItemInfo) {
        item.node.active = false;
        this.cache_pools[item.type].push(item);
    }
    private geneOneNode(index: number) {
        let type = this.func2type(this.data[index]);
        let item = this.getItemByType(type);
        item.itemComponent.reset();
        item.itemComponent.init(this.data[index], index, this.p);
        return item;
    }
    private recycleItems() {
        for (let i = 0; i < this.item_list.length; i++) {
            this.pushItemByType(this.item_list[i]);
        }
        this.item_list.length = 0;
    }
    public getInfoByIndex(index: number) {
        if (index >= this.begin_index && index <= this.end_index) {
            let new_index = index - this.begin_index;
            return this.item_list[new_index];
        }
        return null;
    }
    public getItems(){
        return this.item_list;
    }
    public initData(data: Array<any>, p: any = null) {
        this.recycleItems();
        this.data = data;
        this.p = p;
        if(!data || !data.length){
            this.refreshSize();
            return;
        }
        let y = this.refreshContentSize(true);
        this.begin_index = 0;
        this.initItems(y);
    }
    protected refreshContentSize(need_reset:boolean){
        let datas = this.data;
        let height = this.space * (datas.length - 1);
        for(let i = 0, len = datas.length; i < len; i++) {
            height += this.all_height[this.func2type(datas[i])];
        }
        this.uitrans.height = height;
        // if(height < this.view.contentSize.height)height = this.view.contentSize.height;
        let begin_y = (1 - this.uitrans.anchorY) * height;
        if(need_reset){
            this.scrollToTop();
        }
        return begin_y;
    }
    private initItems(begin_y: number) {
        let is_full = false;
        let item = null, ui_tran = null;
        for (let i = 0; i < this.data.length; i++) {
            item = this.geneOneNode(i);
            this.item_list.push(item);
            ui_tran = item.ui_tran;
            item.node.toY(begin_y - ui_tran.height * ui_tran.anchorY);
            begin_y += (ui_tran.height + this.space);
            if (is_full) {
                this.end_index = i;
                break;
            }
            if (begin_y > this.uitrans.height) {
                is_full = true;
            }
        }
        if (!is_full) {
            this.end_index = this.data.length - 1;
        }
    }
    protected _moveContent(deltaMove: Vec3, canStartBounceBack: boolean) {
        let adjustedMove = this._flattenVectorByDirection(deltaMove);

        Vec3.add(_tempVec3, this._content.position, adjustedMove); 
        //@ts-ignore
        this._setContentPosition(_tempVec3);

        if (Math.abs(adjustedMove.y) > EPSILON) {
            this._checkNeedRefresh(adjustedMove);
        }
        if (this.elastic && canStartBounceBack) {
            this._startBounceBackIfNeeded();
        }
    }

    protected _checkNeedRefresh(offset: Vec3) {
        let list = this.item_list;
        if (list.length < 2) return;
        let content_y = this.content.position.y;
        //元素向上滚动
        if (offset.y > 0 && this.end_index < this.data.length - 1) {
            if (list[1].node.position.y + content_y > this._topBoundary) {
                //回收顶部元素
                let item = list.shift();
                this.pushItemByType(item);
                this.begin_index++;
            }
            let len = list.length;
            //倒数第二个进入了view的底部，就创建后面元素
            if (list[len - 2].node.position.y + content_y > this._bottomBoundary) {
                this.end_index++;
                let new_item = this.geneOneNode(this.end_index);
                let end_tran = list[len - 1].ui_tran;
                let y = end_tran.node.position.y - this.space - end_tran.height * end_tran.anchorY - new_item.ui_tran.height * (1 - new_item.ui_tran.anchorY);
                new_item.node.toY(y);
                list.push(new_item);
            }
        }
        else if (offset.y < 0 && this.begin_index > 0) {
            //向下移动，并且上面还有元素
            if (list[1].node.position.y + content_y < this._topBoundary) {
                this.begin_index--;
                let item = this.geneOneNode(this.begin_index);
                let first_uitran = list[0].ui_tran;
                let y = first_uitran.node.position.y + this.space + first_uitran.height * (1 - first_uitran.anchorY) + item.ui_tran.height * item.ui_tran.anchorY;
                item.node.toY(y);
                this.item_list.unshift(item);
            }
            let len = this.item_list.length;
            if (list[len - 2].node.position.y + content_y < this._bottomBoundary) {
                var item = this.item_list.pop();
                this.pushItemByType(item);
                this.end_index--;
            }
        }
    }

}
