import { _decorator,sp } from 'cc';
import { i18nMgr, i18Type } from '../manager/i18nMgr';
import { i18Base } from './i18Base';
const { ccclass, property, executeInEditMode, disallowMultiple, requireComponent, menu } = _decorator;

@ccclass("i18nSkeleton")
@executeInEditMode
@requireComponent(sp.Skeleton)
@disallowMultiple
@menu("多语言/i18nSkeleton")
export class i18nSkeleton extends i18Base {
    protected i18_type = i18Type.Spine;
    
    @property
    public anim: string = "";
    private spine: sp.Skeleton = null;

    onLoad(){
        this.spine = this.node.getComponent(sp.Skeleton);
    }

    public refresh() {
        if(!this.i18n_index.length)return;
        let data = null;
        if (this.bundle) {
            data = i18nMgr.getGameSpine(this.i18n_index)
        } else {
            data = i18nMgr.getSpine(this.i18n_index);
        }
        if(data){
            this.spine.skeletonData = data;
            if(this.anim.length){
                this.spine.setAnimation(0, this.anim, this.spine.loop);
            }
        }
        else{
            this.spine.skeletonData = null;
        }
    }
    public refreshInEditor(){

    }
}
