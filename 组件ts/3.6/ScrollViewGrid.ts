import { _decorator, Component, instantiate, Node, NodeEventType, Prefab, ScrollBar, ScrollView, Sprite, UITransform, Vec3 } from 'cc';
import { CircleArray } from '../extra/ExClass';
import { ViewItemPro } from './ViewItemPro';
import { GameConst } from '../game/GameConst';
const { ccclass, property, menu } = _decorator;

class GridInfo{
    public pos = 0;
    public items: ViewItemPro[] = null;
    // public abstract refresh:(pre:Prefab, data:any[], begin:number)=>void;
    // public init(parent: Node, pre: Prefab, count:number){
    //     this.items = new Array(count);
    //     for(let i = 0; i < count; i++){
    //         let node = instantiate(pre);
    //         node.setParent(parent);
    //         this.items[i] = node.getComponent(ViewItemPro);
    //     }
    // }
    public hide(){
        this.items.forEach(function(item){item.node.active = false;})
    }
}
const _tempVec3 = new Vec3();
const EPSILON = 1e-4;

@ccclass('ScrollViewGrid')
@menu("新组件/ScrollViewGrid")
export class ScrollViewGrid extends ScrollView {
    @property(Prefab)
    public item_prefab: Prefab = null;
    @property({ tooltip: "每一排个数"})
    public every_count = 0;
    @property({ tooltip: "行高"})
    public item_offset = 0;
    @property({ tooltip: "一个item的宽度"})
    public one_offset = 0;
    @property({ tooltip: "最多几行"})
    public item_num = 0;

    protected uitrans: UITransform = null;
    protected all_item: GridInfo[] = [];
    protected circle_items: CircleArray<GridInfo> = null;
    protected begin_index = 0;
    protected end_index = 0;
    protected total_count = 0;
    protected have_init = false;
    protected data = null;
    protected p = null;
    protected pos_every:number[] = null;
    protected can_refresh = false;

    protected onLoad(){
        this.uitrans = this._content.getComponent(UITransform);
    }
    protected initParam(data: Array<any>, p?:any){
        data = data || [];
        this.data = data;
        this.p = p;
        this.total_count = Math.ceil(data.length / this.every_count);
        if(this.have_init)return;
        this.have_init = true;
        // this.uitrans = this._content.getComponent(UITransform);
        this.pos_every = GameConst.getOffsetArr(this.every_count, 0, this.one_offset);
        if(this.item_offset == 0){
            console.error("item_offset不能为0");
            return;
        }
        if(this.item_num == 0){
            this.item_num = Math.ceil(this.view.height / this.item_offset) + 2;
        }
        return
    }
    protected refreshContentSize(len:number, need_reset:boolean){
        let height = len * this.item_offset;
        // if(height < this.view.contentSize.height)height = this.view.contentSize.height;
        this.uitrans.height = height;
        let begin_y = (1 - this.uitrans.anchorY) * height;
        if(need_reset){
            this.stopAutoScroll();
            this.content.toY(this._topBoundary - begin_y);
        }
        return begin_y;
    }
    public autoSize(){
        if(this.view){
            this.view.node.on(NodeEventType.SIZE_CHANGED, function(){
                let height = this.view.height;
                if(this.uitrans.height < height)this.uitrans.height = height;
                if((this.item_num - 1) * this.item_offset < height && this.item_offset > 0){
                    this.item_num = Math.ceil(height / this.item_offset) + 2;
                }
            }, this);
        }
    }

    public initData(data: Array<any>, p?:any) {
        this.can_refresh = false;
        this.initParam(data, p);
        let begin_y = this.refreshContentSize(this.total_count, true);
        this.initItems(begin_y);
        this.can_refresh = true;
    }
    public refreshData(data: Array<any>, p?:any) {
        this.can_refresh = false;
        this.initParam(data, p);
        let begin_y = this.refreshContentSize(this.total_count, false);
        this.refreshItems(begin_y);
        this.can_refresh = true;
    }
    protected geneItems(count: number){
        for(let i = this.all_item.length; i < count; i++){
            let info = new GridInfo();
            this.all_item.push(info);
            let nodes = GameConst.geneNodes(this.item_prefab, this.every_count, this._content);
            info.items = new Array(nodes.length);
            for(let j = 0; j < this.every_count; j++){
                info.items[j] = nodes[j].getComponent(ViewItemPro);
                info.items[j].node.toX(this.pos_every[this.every_count - j - 1]);
            }
        }
    }
    protected initItems(begin_y: number) {
        let num = Math.min(this.total_count, this.item_num);
        this.begin_index = 0;
        this.end_index = num - 1;
        this.geneItems(num);
        let i = 0;
        begin_y -= this.item_offset * 0.5;
        for (; i < num; i++) {
            this.refresh(begin_y, i, this.all_item[i]);
            begin_y -= this.item_offset;
        }
        //超出数据的隐藏掉
        for (; i < this.all_item.length; i++) {
            this.all_item[i].hide();
        }
        this.circle_items = new CircleArray(this.all_item, num);
    }
    protected refreshItems(begin_y: number){
        let len = Math.min(this.item_num, this.total_count);
        if(this.circle_items){
            this.circle_items.resetArrIndex();
            this.end_index = this.begin_index + len - 1;
            let beyond = this.end_index + 1 - this.total_count;
            if(beyond > 0){
                this.end_index -= beyond;
                this.begin_index -= beyond;
                if(this.begin_index < 0)this.begin_index = 0;
                // this.content.toY(begin_y - this.item_offset * this.begin_index);
                this.stopAutoScroll();
                this._adjustContentOutOfBoundary();
            }
        }
        else{
            this.begin_index = 0;
            this.end_index = len - 1;
        }
        this.geneItems(len);
        begin_y -= (this.begin_index + 0.5) * this.item_offset;
        let i = 0;
        for(; i < len; i++){
            this.refresh(begin_y, i + this.begin_index, this.all_item[i]);
            begin_y -= this.item_offset;
        }
        for (; i < this.all_item.length; i++) {
            this.all_item[i].hide();
        }
        this.circle_items = new CircleArray(this.all_item, len);
    }
    protected refresh(pos:number, begin:number, info:GridInfo){
        let data = this.data;
        let base = begin * this.every_count;
        info.pos = pos;
        let i = 0;
        for(; i < this.every_count; i++){
            let index = base + i;
            if(index == data.length)break;
            let item = info.items[i]
            item.node.active = true;
            item.node.toY(pos);
            item.init(data[index], index, this.p);
        }
        for(; i < this.every_count; i++){
            info.items[i].node.active = false;
        }
    }
    // public getItemByIndex(index: number) {
    //     let new_index = index - this.begin_index;
    //     return this.circle_items.get(new_index);
    // }

    protected _moveContent(deltaMove: Vec3, canStartBounceBack: boolean) {
        let adjustedMove = this._flattenVectorByDirection(deltaMove);

        Vec3.add(_tempVec3, this._content.position, adjustedMove); 
        //@ts-ignore
        this._setContentPosition(_tempVec3);

        if (this.all_item.length == this.circle_items?.getLen() && Math.abs(adjustedMove.y) > EPSILON) {
            this._checkNeedRefresh(adjustedMove);
        }
        if (this.elastic && canStartBounceBack) {
            this._startBounceBackIfNeeded();
        }
    }

    protected _checkNeedRefresh(offset: Vec3) {
        if (!this.can_refresh || this.circle_items.getLen() < 2) return;
       // console.log("滚动 检查刷新", offset)
        //元素向上滚动
        if (offset.y > 0 && this.end_index < this.total_count - 1 && this._content.position.y + this.circle_items.get(1).pos > this._topBoundary) {
            this.begin_index++;
            this.end_index++;//数据索引增加
            let first_info = this.circle_items.get(0);
            let node_pos = this.circle_items.getLast(1).pos;
            // log("tobottom:",this.data[this.end_index])
            this.circle_items.addOffset(1);
            this.refresh(node_pos - this.item_offset, this.end_index, first_info);
        }
        if (offset.y < 0 && this.begin_index > 0 && this._content.position.y + this.circle_items.getLast(2).pos < this._bottomBoundary) {
            this.begin_index--;
            this.end_index--;
            let end_info = this.circle_items.getLast(1);
            let pos_first = this.circle_items.get(0).pos;
            // log("tobottom:",this.data[this.end_index])
            this.circle_items.addOffset(-1);
            this.refresh(pos_first + this.item_offset, this.begin_index, end_info);
        }
    }
    public resetInEditor(){
        let sprite = this.node.getComponent(Sprite);
        if(sprite)sprite.destroy();
        if(this.horizontal && this.vertical)this.horizontal = false;
        if(this.node.children[0].getComponent(ScrollBar)){
            this.node.children[0].destroy();
        }
        if(!this._content){
            let size = this.node._uiProps.uiTransformComp.contentSize;
            let node = this.node.getChildByName("view");
            node._uiProps.uiTransformComp.setContentSize(size);
            if(!node)return;
            node = node.children[0];
            node._uiProps.uiTransformComp.setContentSize(size);
            node.setPosition(0, size.height / 2, 0);
            if(node)this._content = node;
        }

    }


}



