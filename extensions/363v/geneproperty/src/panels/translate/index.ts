
const Go_Type_Js = {
    bool: ": boolean",
}
const Go_Class_Js = {
    bool: " = false",
    string: ' = ""',
}
function getWords(str: string, exclude: string[]):string[]{
    let res = [];
    let index = -1;
    for(let i = 0; i < str.length; i++){
        if(index == -1){
            if(exclude.indexOf(str[i]) == -1)index = i;
        }
        else{
            if(exclude.indexOf(str[i]) > -1){
                res.push(str.slice(index, i));
                index = -1;
            }
        }
    }
    if(index > -1){
        res.push(str.slice(index))
    }
    return res;
}
function dealType(str:string, is_type:boolean){
    let count = 0;
    let index = 0;
    for(let i = 0; i < str.length; i++){
        switch(str[i]){
            case "[":
                count++;
            case "]":
            case "*":
                continue;
        }
        index = i;
        break;
    }

    let type = str;
    if(index > -1)type = str.slice(index);
    if(type.startsWith("int") || type.startsWith("float")){
        if(is_type)type = ": number";
        else type = " = 0";
    }
    else{
        if(is_type){
            if(Go_Type_Js[type])type = Go_Type_Js[type];
            else type = ": " + type;
        }
        else{
            if(Go_Class_Js[type])type = Go_Class_Js[type];
            else type = ": " + type + " = null";
        } 
    }
    if(count == 0)return type;
    return type + new Array(count).fill("[]").join("");
}
function getFiledAndDesc(str:string, is_type:boolean){
    let strs = getWords(str, [" ", "\t"]);
    let field = strs[0];
    let f_type = strs[1];
    let desc = "";
    for(let i = 2; i < strs.length; i++){
        if(strs[i].startsWith("/")){
            if(strs[i].length == 2){
                desc = strs[i + 1];
                i++;
            }
            else{
                desc = strs[i].slice(2);
            }
            break;
        }
        if(strs[i].startsWith("`json")){
            let info = getWords(strs[i], ["`", ":",'"']);
            field = info[1];
        }
    }
    return [field + dealType(f_type, is_type), desc];
}
module.exports = Editor.Panel.define({
    listeners: {
        show() { console.log('translate show'); },
        hide() { console.log('translate hide'); },
    },
    template: `
        <ui-textarea id="content"></ui-textarea>
        <ui-button id="btn_go2type">go2type</ui-button>
        <ui-button id="btn_go2class">go2class</ui-button>
        <ui-button id="btn_json2type">json2type</ui-button>
        <ui-button id="clear">clear</ui-button>
    `,
    style: `
        #content{
            height: 150px;
        }
    `,
    $: {
        content: '#content',
        btn_go2type: '#btn_go2type',
        btn_go2class: '#btn_go2class',
        btn_json2type: '#btn_json2type',
        clear: '#clear'
    },
    methods: {
        go2type(str:string, is_type:boolean){
            if(str.length == 0)return;
            let strs = str.split("\n");
            let all_info = new Array(strs.length);
            let max_length = 0;
            for(let i = 0; i < strs.length; i++){
                all_info[i] = getFiledAndDesc(strs[i], is_type);
                if(all_info[i][0].length > max_length)max_length = all_info[i][0].length;
            }
            let res = "";
            let diff = 0;
            let space = "";
            max_length++;
            for(let i = 0 ;i < all_info.length ; i++){
                // console.log(all_info[i]);
                diff = max_length - all_info[i][0].length;
                if(diff > 0){
                    space = new Array(diff).fill(" ").join("");
                }
                else{
                    space = "";
                }
                res += `\t${all_info[i][0]}${space}//${all_info[i][1]}\n`;
            }
            Editor.Clipboard.write("text", res);
            console.log("新结构已复制💪");
        },
        go2type2(str:string, is_type:boolean){
            if(str.length == 0)return;
            let strs = str.split("\n");
            let begin = -1;
            let temp:string[][] = [];
            let max_length = 0;
            for(let i = 0; i < strs.length; i++){
                if(begin == -1){
                    if(strs[i].startsWith("type")){
                        begin = i + 1;
                        let words = strs[i].split(" ");
                        if(is_type)strs[i] = "export type " + words[1] + " = {";
                        else strs[i] = "export class " + words[1] + "{";
                        max_length = 0;
                        temp.length = 0;
                    }
                }
                else{
                    if(strs[i].startsWith("}")){
                        let diff = 0;
                        for(let j = 0; j < temp.length; j++){
                            if(temp[j][1].length == 0){
                                strs[begin + j] = "\t" + temp[j][0];
                                continue;
                            }
                            diff = max_length - temp[j][0].length;
                            if(diff == 0)strs[begin + j] = "\t" + temp[j][0] + "//" + temp[j][1];
                            else{
                                strs[begin + j] = "\t" + temp[j][0] + new Array(diff).fill(" ").join("") + "//" + temp[j][1];
                            }
                        }
                        begin = -1;
                    }
                    else{
                        let info = getFiledAndDesc(strs[i], is_type);
                        temp.push(info)
                        if(info[0].length > max_length)max_length = info[0].length;
                    }
                }
            }
            Editor.Clipboard.write("text", strs.join("\n"));
            console.log("新结构已复制💪");
        },
        json2type(str:string){
            let info = JSON.parse(str);
            let types = [];
            function getObjDesc(obj:any){
                let index = types.length;
                let type = "type" + index;
                let res = "type " + type + " = {\n";
                types.push(res);
                for(let k in obj){
                    if(typeof(obj[k]) == "object"){
                        if(Array.isArray(obj[k])){
                            types[index] += `\t${k}: ${getObjDesc(obj[k][0])}[]\n`;
                        }
                        else{
                            types[index] += `\t${k}: ${getObjDesc(obj[k])}\n`;
                        }
                    }
                    else{
                        types[index] += `\t${k}: ${typeof(obj[k])}\n`;
                    }
                }
                types[index] += "}\n";
                return type;
            }
            let str_arr = "type NewType = type0";
            while(Array.isArray(info)){
                str_arr += "[]";
                info = info[0];
            }
            getObjDesc(info);
            // for(let k in info){
            //     if(typeof(info[k]) == "object"){
            //         if(Array.isArray(info[k])){
            //             res += `\t${k}: ${getObjDesc(info[k][0])}[]\n`;
            //         }
            //         else{
            //             res += `\t${k}: ${getObjDesc(info[k])}\n`;
            //         }
            //     }
            //     else{
            //         res += `\t${k}: ${typeof(info[k])}\n`;
            //     }
            // }
            // res += "}" + str_arr;
            Editor.Clipboard.write("text", types.join("") + str_arr);
            console.log("新结构已复制💪");
        },
    },
    ready() {
        this.$.btn_go2type.addEventListener('confirm', () => {
            this.go2type2(this.$.content.value, true);
        });
        this.$.btn_go2class.addEventListener('confirm', () => {
            this.go2type2(this.$.content.value, false);
        });
        this.$.btn_json2type.addEventListener('confirm', () => {
            this.json2type(this.$.content.value);
        });

        this.$.clear.addEventListener('confirm', ()=>{
            this.$.content.value = "";
        });
    },
    beforeClose() { },
    close() { },
});
