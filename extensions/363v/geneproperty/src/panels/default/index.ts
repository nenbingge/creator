
import fs = require("fs-extra")

module.exports = Editor.Panel.define({
    listeners: {
        show() { console.log('show'); },
        hide() { console.log('hide'); },
    },
    template: `
        <ui-checkbox value="true" id="field">生成字段</ui-checkbox>
        <hr />
        <ui-checkbox value="true" id="func">生成函数</ui-checkbox>
        <hr />
        <ui-button id="btn_save">保存</ui-button>
    `,
    style:"",
    $: {
        field: '#field',
        func: '#func',
        btn_save: '#btn_save'
    },
    methods: {
    },
    ready() {
        let pack_path = Editor.Package.getPath("geneproperty");
        let config_path = Editor.Utils.Path.join(pack_path, "config.txt");
        if(fs.existsSync(config_path)){
            let data=fs.readFileSync(config_path).toString();
            // console.log("data:",data);
            this.$.field.value = data[0] == "1";
            this.$.func.value = data[1] == "1";
        }
        this.$.btn_save.addEventListener('confirm', () => {
            console.log("savconfig")
            let pack_path = Editor.Package.getPath("geneproperty");
            let config_path = Editor.Utils.Path.join(pack_path, "config.txt");
            let str0 = this.$.field.value ? "1" :"0";
            let str1 = this.$.func.value ? "1" :"0";
            let str = str0 + str1;
            fs.writeFileSync(config_path, str);
            Editor.Message.send('geneproperty','save',str);
            console.log("保存配置成功🐇");
        });
    },
    beforeClose() { },
    close() { },
});
