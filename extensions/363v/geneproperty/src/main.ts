// @ts-ignore
import packageJSON from '../package.json';
import fs = require("fs-extra")
// import { ExecuteSceneScriptMethodOptions } from '../@types/packages/scene/@types/public';
/**
 * @en 
 * @zh 为扩展的主进程的注册方法
 */

var config = "11";
let sep = Editor.Utils.Path.sep;

let options = {
    name: packageJSON.name,
    method: '',
    args: []
};

async function jump2path(url: string){
    let info = await Editor.Message.request('asset-db', 'query-asset-info', url);
    if(!info){
        console.log("not found:", url);
        return;
    }
    if(info.isDirectory){
        Editor.Selection.clear('asset');
        Editor.Selection.select("asset", info.uuid);
    }
    else{
        Editor.Message.broadcast("ui-kit:touch-asset", [info.uuid]);
    }
}

export const methods: { [key: string]: (...any: any) => any } = {
    //#region 生成属性
    save(str){
        config = str;
    },
    openPanel() {
        Editor.Panel.open(packageJSON.name);
    },
    single(){
        options.method = "single";
        options.args = [config];
        Editor.Message.request('scene', 'execute-scene-script', options);
    },
    recursion(){
        options.method = "recursion";
        options.args = [config];
        Editor.Message.request('scene', 'execute-scene-script', options);
    },
    //#region 杂项
    translate(){
        Editor.Panel.open(packageJSON.name + ".translate");
    },
    getint(){
        options.method = "getint";
        options.args = [];
        Editor.Message.request('scene', 'execute-scene-script', options);
    },
    genehead(){
        options.method = "genehead";
        options.args = [];
        Editor.Message.request('scene', 'execute-scene-script', options);
    },
    //#region 位置
    one(){
        options.method = "one";
        options.args = ["0"];
        Editor.Message.request('scene', 'execute-scene-script', options);
    },
    two(){
        options.method = "two";
        options.args = ["0"];
        Editor.Message.request('scene', 'execute-scene-script', options);
    },
    oneangle(){
        options.method = "one";
        options.args = ["1"];
        Editor.Message.request('scene', 'execute-scene-script', options);
    },
    twoangle(){
        options.method = "two";
        options.args = ["1"];
        Editor.Message.request('scene', 'execute-scene-script', options);
    },
    multipleOne(){
        options.method = "multipleOne";
        Editor.Message.request('scene', 'execute-scene-script', options);
    },
    multipleTwo(){
        options.method = "multipleTwo";
        Editor.Message.request('scene', 'execute-scene-script', options);
    },
    //#region 组件操作
    delopa(){
        options.method = "delopa";
        options.args = ["cc.UIOpacity"];
        Editor.Message.request('scene', 'execute-scene-script', options);
    },
    clearlb(){
        options.method = "setlb";
        options.args = [""];
        Editor.Message.request('scene', 'execute-scene-script', options);
    },
    setlb(){
        options.method = "setlb";
        options.args = ["test"];
        Editor.Message.request('scene', 'execute-scene-script', options);
    },
    usesamim(){
        options.method = "usefont";
        options.args = ["dca50568-c298-44d7-b76e-b0083d458aaa"];
        Editor.Message.request('scene', 'execute-scene-script', options);
    },
    i18lbfalse(){
        options.method = "setComs";
        options.args = ["i18nLabel", JSON.stringify({enabled:false})];
        Editor.Message.request('scene', 'execute-scene-script', options);
    },
    i18lbtrue(){
        options.method = "setComs";
        options.args = ["i18nLabel", JSON.stringify({enabled:true})];
        Editor.Message.request('scene', 'execute-scene-script', options);
    },
    rmpremul(){
        options.method = "setComs";
        options.args = ["sp.Skeleton", JSON.stringify({premultipliedAlpha:false})];
        Editor.Message.request('scene', 'execute-scene-script', options);
    },
    yonoaddsf(){
        options.method = "yonoaddsf";
        options.args = [];
        Editor.Message.request('scene', 'execute-scene-script', options);
    },
    adjustscrollview(){
        options.method = "adjustscrollview";
        options.args = [];
        Editor.Message.request('scene', 'execute-scene-script', options);
    },
    //#region 选中
    jumpttf(){
        jump2path("db://assets/master/font/ttf/Samim.ttf");
    },
    jumphome(){
        jump2path("db://assets/master/prefab/lobby/home/user.prefab");
    },
    jumpui(){
        jump2path("db://assets/master/prefab/ui/gift");
    },
    jumpuitex(){
        jump2path("db://assets/master/texture/ui/group");
    },
    jumpspine(){
        jump2path("db://assets/master/spine/marquee");
    },
    jumpplist(){
        jump2path("db://assets/master/plist/lobby");
    }

};

/**
 * @en Hooks triggered after extension loading is complete
 * @zh 扩展加载完成后触发的钩子
 */
export function load() {
    let config_path=Editor.Package.getPath(packageJSON.name) + sep + "config.txt";
    if(fs.existsSync(config_path)){
        config = fs.readFileSync(config_path).toString();
    }
}

/**
 * @en Hooks triggered after extension uninstallation is complete
 * @zh 扩展卸载完成后触发的钩子
 */
export function unload() { }
