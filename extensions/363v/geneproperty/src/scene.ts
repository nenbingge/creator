import { join } from 'path';

module.paths.push(join(Editor.App.path, 'node_modules'));
const { js, Node, assetManager } = require('cc');
declare const cce: any

async function getSelectNode(func:Function){
    let uuid = Editor.Selection.getLastSelected("node");
	if(uuid){
		let node = await cce.Node.query(uuid);
		func(node);
	}
	else{
		console.warn("没有选择节点👴");
	}
}
async function getSelectNodes(func:Function){
    let uuids = Editor.Selection.getSelected("node");
	if(uuids){
		let nodes = [];
		for(let i = 0; i < uuids.length; i++){
			let node = await cce.Node.query(uuids[i]);
			nodes.push(node);
		}
		func(nodes);
	}
	else{
		console.warn("没有选择节点👴");
	}
}
async function getRes(type:string, asset_path:string){
	return new Promise(async (func)=>{
		const info = await Editor.Message.request('asset-db', 'query-assets', { ccType: type, pattern: asset_path });
		assetManager.loadAny({ uuid: info[0].uuid }, (err, res) => {
			if(err)console.log(err);
			func(res);
		});
	})
}
async function getResByUuid(uuid:string){
	return new Promise(async (func)=>{
		assetManager.loadAny({ uuid }, (err, res) => {
			if(err)console.log(err);
			func(res);
		});
	})
}        

let root_sc = null;
var temp_str = "";
var func_str = "";

function getOneFiled(name, type){
	if(root_sc && root_sc.hasOwnProperty(name))return;
	temp_str += `\tprivate ${name}: ${type} = null;\n`;
}

function getFunction(p, name, content){
	if(root_sc && root_sc[name])return;
	func_str += `\t//auto\n\t${p} ${name}(){\n${content}\n\t}\n`;
}

function saveClip(config){
	if(config[0] == "0"){
		temp_str = "";
	}
	if(config[1] == "0"){
		func_str = "";
	}
	if(temp_str.length > 0 || func_str.length > 0){
    	Editor.Clipboard.write("text", temp_str + func_str);
    	console.log("新增内容已复制💪");
    }
    else{
    	console.log("没有新增内容🌚");
    }
}
function geneUINode(name){
	let node = new Node(name);
	node.addComponent("cc.UITransform");
	node.addComponent("cc.Sprite");
	return node;
}

function getComName(node){
	let name = node.name;
	switch (name[0]) {
		case "a":
			if (name.startsWith("auto_btn_")){
				let handName=name.replace("auto_btn_","");
    			let tHandleName="on"+handName[0].toUpperCase()+handName.slice(1)+"Click";
				getFunction("public",tHandleName,"\t\t");
			}
			break;
	    case "b":
	    	if (name.startsWith("btn_")) getOneFiled(name, "Button");
	        break;
	    case "e":
	        if (name.startsWith("edit_"))getOneFiled(name, "EditBox") ;
	        break;
		case "i":
			if (name.startsWith("ilb_")) getOneFiled(name, "i18nLabel");
            if (name.startsWith("isk_")) getOneFiled(name, "i18nSkeleton")
			break;
		case "g":
            if (name.startsWith("gene_")) getOneFiled(name, "Generate")
			break;
	    case "l":
	        if (name.startsWith("lb_")) getOneFiled(name, "Label");
	    	break;
	    case "n":
	        if (name.startsWith("node_")) getOneFiled(name, "Node");
	        break;
	    case "r":
	    	if (name.startsWith("rich_")) getOneFiled(name, "RichText");
	    	break;
	    case "s":
	        if (name.startsWith("spine_")) getOneFiled(name, "sp.Skeleton");
	        else if (name.startsWith("sp_")) getOneFiled(name, "Sprite");
	        else if (name.startsWith("sc_")) {
	            name = name.slice(3);
	            let com_name = "Component";
	            let com = node._components[0];
	            if(js.getClassName(com))com_name = js.getClassName(com);
	            getOneFiled(name, com_name);
	        }
	        break;
	    case "t":
	        if (name.startsWith("toggle_")) getOneFiled(name, "Toggle");
	        break;
        case "u":
            if (name.startsWith("uitrans_")) getOneFiled(name, "UITransform");
            if (name.startsWith("uiopca_")) getOneFiled(name, "UIOpacity");
            break;
	}
}

function geneCom(node, recursion){
	let children = node.children;
	if(recursion){
		for (let i = 0, len = children.length; i < len; i++) {
			children[i].walk(getComName);
		}
	}
	else{
		for (let i = 0, len = children.length; i < len; i++) {
			getComName(children[i]);
		}
	}
}

function getStrFromInfo(key, arr, need_sort){
	if(need_sort)arr = arr.sort((a,b)=>{return a.index - b.index});
	key = key[0].toUpperCase() + key.slice(1);
	let str = `const ${key}_Pos = [`;
	for(let i = 0  ; i < arr.length ; i++){
		str += arr[i].pos
		if(i < arr.length - 1)str += ", "
	}
	str += "];\n";
	return str;
}
function getIntVec3(vec){
	vec.x = parseInt(vec.x);
	vec.y = parseInt(vec.y);
	vec.z = parseInt(vec.z);
	return vec;
}
function getNumIndex(str:string){
	for(let i = 0; i < str.length; i++){
		const charCode = str.charCodeAt(i);
		if ((charCode >= 48 && charCode <= 57)) {
			return i - 1;
		}
	}
	return 0;
}
export function load() {};
export function unload() {};
export const methods = {
    single(config){
		console.log("single:");
        getSelectNode(function(node){
			temp_str = "";
			func_str = "";
			root_sc = node._components[0];
			getFunction("protected", "onLoad", "\t\tUtilsPanel.getAllNeedCom(this, this.node, false);");
			geneCom(node, false);
			saveClip(config);
		});
	},

	recursion(config) {
        console.log('recursion');
		getSelectNode(function(node){
			temp_str = "";
			func_str = "";
			root_sc = node._components[0];
			getFunction("protected", "onLoad", "\t\tUtilsPanel.getAllNeedCom(this, this.node, true);");
			geneCom(node, true);
			saveClip(config);
		});

    },
    one(have_angle){
		console.log("获取第一层的坐标:"+have_angle);
		getSelectNode(function(node){
			let children = node.children;
			let map = new Map();
			for(let i = 0 ; i < children.length ; i++){
				let name = children[i].name;
				let num_index = getNumIndex(name);
				let key = name.slice(0, num_index);
				if(!map.has(key))map.set(key, []);
				if(have_angle == "1"){
					map.get(key).push({
						index:parseInt(name.slice(num_index)),
						pos: `[${Math.floor(children[i].position.x)}, ${Math.floor(children[i].position.y)}, ${Math.floor(children[i].angle)}]`
					})
				}
				else{
					map.get(key).push({
						index:parseInt(name.slice(num_index)),
						pos: `[${Math.floor(children[i].position.x)}, ${Math.floor(children[i].position.y)}]`
					})
				}
			}
			let str = "";
			map.forEach((item, key)=>{
				str += getStrFromInfo(key, item, true);
			})
			Editor.Clipboard.write("text", str);
			console.log("位置已放置剪切板👴");
		})
	},
	two (have_angle) {
        console.log("获取第二层的坐标:"+have_angle);
		getSelectNode(function(node){
			let children = node.children;
			let map = new Map();
			for(let i = 0 ; i < children.length ; i++){
				let children2 = children[i].children;
				for(let j = 0 ; j < children2.length ; j++){
					let key = children2[j].name;
					if(i == 0)map.set(key, []);
					if(have_angle == "1"){
						map.get(key).push({
							pos: `[${Math.floor(children2[j].position.x)},${Math.floor(children2[j].position.y)},${Math.floor(children[i].angle)}]`
						})
					}
					else{
						map.get(key).push({
							pos: `[${Math.floor(children2[j].position.x)},${Math.floor(children2[j].position.y)}]`
						})
					}
				}
			}
			let str = "";
			map.forEach((item, key)=>{
				str += getStrFromInfo(key, item, false);
			})
			// console.log(str)
			Editor.Clipboard.write("text",str);
			console.log("位置已放置剪切板👴");
		})
    },
	multipleOne(){
		console.log("获取选中节点坐标(多选)");
		getSelectNodes(function(nodes){
			let str = "[";
			let len = nodes.length - 1;
			for(let i = 0; i < len; i++){
				str += `[${Math.floor(nodes[i].position.x)},${Math.floor(nodes[i].position.y)}],`;
			}
			str += `[${Math.floor(nodes[len].position.x)},${Math.floor(nodes[len].position.y)}]]`;
			Editor.Clipboard.write("text",str);
			console.log("位置已放置剪切板👴");
		})
	},
	multipleTwo(){
		console.log("获取选中节点二层坐标(多选)");
		getSelectNodes(function(nodes){
			let str = "[\n";
			for(let i = 0 ; i < nodes.length ; i++){
				let children2 = nodes[i].children;
				str += "["
				let len = children2.length - 1;
				for(let j = 0 ; j < len ; j++){
					str += `[${Math.floor(children2[j].position.x)},${Math.floor(children2[j].position.y)}],`;
				}
				str += `[${Math.floor(children2[len].position.x)},${Math.floor(children2[len].position.y)}]]`;
				if(i < nodes.length - 1)str += ",\n"
				else str += "\n"
			}
			str += "]";
			Editor.Clipboard.write("text",str);
			console.log("位置已放置剪切板👴");
		})
	},
	delComs(com_name:string){
		console.log("🏹删除组件:"+com_name);
		getSelectNode(function(node:any){
			node.walk((target:any)=>{
				// console.log(target.getComponent(com_name))
				let com = target.getComponent(com_name);
				if(com){
					console.log(target.name);
					com.destroy();
				}
				// target.removeComponent(com_name);
			})
			console.log("👌删除完毕");
		})
	},
	delopa(){
		console.log("🏹删除组件:cc.UIOpacity");
		getSelectNode(function(node:any){
			node.walk((target:any)=>{
				// console.log(target.getComponent(com_name))
				let com = target.getComponent("cc.UIOpacity");
				if(com){
					if(com.opacity != 255){
						let com_ui = target.getComponent("cc.UIRenderer");
						let color = com_ui.color.clone();
						color.a = com.opacity;
						com_ui.color = color;
						console.log(target.name, ".a  ->  ", com.opacity)
					}
					else{
						console.log(target.name);
					}
					com.destroy();
				}
				// target.removeComponent(com_name);
			})
			console.log("👌删除完毕");
		})
	},
	setComs(com_name:string, option_str:string){
		let options = JSON.parse(option_str);
		console.log(com_name, "-->", option_str);
		let keys = Object.keys(options);
		getSelectNode(function(node:any){
			node.walk((target:any)=>{
				let com = target.getComponent(com_name);
				if(com){
					console.log(target.name);
					for(let i = 0; i < keys.length; i++){
						com[keys[i]] = options[keys[i]];
					}
				}
			})
			console.log("👌修改完毕");
		})
	},
	getint(){
		getSelectNode(function(node:any){
			node.walk((target:any)=>{
				target.position = getIntVec3(target.position);
			})
			console.log("👌取整完毕");
		})
	},
	setlb(str:string){
		getSelectNode(function(node:any){
			node.walk((target:any)=>{
				if(target.name == "title")return;
				let com = target.getComponent("cc.Label");
				if(com){
					com.string = str;
				}
			})
			console.log("👌设置文字完毕");
		})
	},
	usefont(uuid: string){
		getSelectNode(async function(node:any){
			let font = await getResByUuid(uuid);
			node.walk((target:any)=>{
				if(target.name == "title")return;
				let com = target.getComponent("cc.Label");
				if(!com)return;
				if(!com.font){
					console.log(target.name);
					com.useSystemFont = false;
					com.font = font
				}
			})
			console.log("👌设置字体完毕");
		})
	},
	genehead(){
		getSelectNode(async function(node:any){
			let head = geneUINode("sp_head");
			head.setParent(node);
			let frame = geneUINode("sp_frame");
			frame.setParent(node);
			
			let assetPath = `db://assets/master/texture/item/deco/type1.png`;
			let sf = await getRes("cc.SpriteFrame", assetPath);
			frame.getComponent("cc.Sprite").spriteFrame = sf;

			assetPath = `db://assets/master/plist/head.plist`;
			let atlas:any = await getRes("cc.SpriteAtlas", assetPath);
			head.getComponent("cc.Sprite").spriteFrame = atlas.getSpriteFrame("head18");
			console.log("👌生成头像节点");
		})
	},
	yonoaddsf(){
		getSelectNode(function(node:any){
			node.walk((target:any)=>{
				let com = target.getComponent("cc.Sprite");
				if(com){
					if(com.spriteFrame)return;
					if(!com.spriteAtlas)return;
					let name:string = target.name;
					let sf = com.spriteAtlas.getSpriteFrame(name);
					while(!sf && name.indexOf("-") > -1){
						name = name.replace("-", "/");
						sf = com.spriteAtlas.getSpriteFrame(name);
					}
					if(sf){
						console.log(target.name + "->" + name);
						com.spriteFrame = sf;
					}
				}
			})
			console.log("👌修改完毕");
		})
	},
	adjustscrollview(){
		getSelectNode(function(node:any){
			let com = node.getComponent("cc.ScrollView");
			if(com){
				if(com.verticalScrollBar){
					com.verticalScrollBar.node.destroy();
					com.verticalScrollBar = null;
				}
				if(com.content.parent != com.node){
					com.content.setParent(com.node);
					let uirender = com.node.getComponent("cc.Sprite");
					if(uirender)uirender.destroy();
					setTimeout(()=>{
						com.node.addComponent("cc.Mask");
					}, 1)
					let view = com.node.getChildByName("view");
					if(view)view.destroy();
				}
			}
		})
	}

};

