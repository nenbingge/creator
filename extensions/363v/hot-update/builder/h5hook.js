
'use strict';

var Fs = require("fs");
var Path = require("path");
const exfs = require("fs-extra");

exports.onAfterBuild = function (options, result) {
    let target_path = options.packages["hot-update"].copypath;
    console.error(target_path);
    if(!target_path || !Fs.existsSync(target_path))return;
    let final_path = Path.join(target_path, options.outputName);
    console.warn("拷贝项目到:", target_path);
    if(Fs.existsSync(final_path)){
        console.warn("删除:", final_path);
        exfs.removeSync(final_path);
    }
    console.warn("拷贝项目");
    console.warn(result.dest);
    console.warn(final_path);
    exfs.copySync(result.dest, final_path);
    // console.error(Fs);  
    // let url = Path.join(Path_Target, options.outputName);
    // if (!Fs.existsSync(url)){
    // }     
    // var url = Path.join(result.dest, 'data', 'main.js');
}

