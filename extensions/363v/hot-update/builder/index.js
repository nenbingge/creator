exports.configs = {
    'android': {
        hooks: './builder/hook.js'
    },
    'ios': {
        hooks: './builder/hook.js'
    },
    "web-desktop":{
        hooks: "./builder/h5hook.js",
        options: {
            copypath: {
                label: 'path',
                description: 'copy to path',
                default: '',
                render: {
                    ui: 'ui-input',
                }
            }
        }
    }
};

