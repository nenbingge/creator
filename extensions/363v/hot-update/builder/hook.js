
'use strict';

var Fs = require("fs");
var Path = require("path");

var inject_script = `
(function () {
    if (typeof window.jsb === 'object') {
        var hotUpdateSearchPaths = localStorage.getItem('HotUpdateSearchPaths');
        if (hotUpdateSearchPaths) {
            var paths = JSON.parse(hotUpdateSearchPaths);
            jsb.fileUtils.setSearchPaths(paths);

            var fileList = [];
            var storagePath = paths[0] || '';
            var tempPath = storagePath + '_temp/';
            var baseOffset = tempPath.length;

            if (jsb.fileUtils.isDirectoryExist(tempPath) && !jsb.fileUtils.isFileExist(tempPath + 'project.manifest.temp')) {
                jsb.fileUtils.listFilesRecursively(tempPath, fileList);
                fileList.forEach(srcPath => {
                    var relativePath = srcPath.substr(baseOffset);
                    var dstPath = storagePath + relativePath;

                    if (srcPath[srcPath.length] == '/') {
                        jsb.fileUtils.createDirectory(dstPath)
                    }
                    else {
                        if (jsb.fileUtils.isFileExist(dstPath)) {
                            jsb.fileUtils.removeFile(dstPath)
                        }
                        jsb.fileUtils.renameFile(srcPath, dstPath);
                    }
                })
                jsb.fileUtils.removeDirectory(tempPath);
            }
        }

        var searchPaths = jsb.fileUtils.getSearchPaths();	
	    searchPaths.unshift(jsb.fileUtils.getWritablePath() + "hotfix_asset/");       
	    jsb.fileUtils.setSearchPaths(searchPaths);
    }
})();
function delHoxfix(cc){
    let version = "";
    if (cc.sys.platform == cc.sys.Platform.IOS) {
        version = cc.native.reflection.callStaticMethod("JavaScripNative", "getVersionCode")
    }
    else{
        version = cc.native.reflection.callStaticMethod("org/cocos2dx/jsb/JsbClass", "getVersionCode", "()Ljava/lang/String;");
    }
    let save_version = localStorage.getItem("install_check");
    console.log("hotfix check:", version, "::", save_version);
    if(!save_version || save_version != version){
        let path = jsb.fileUtils.getWritablePath() + "hotfix_asset/";
        if(jsb.fileUtils.isDirectoryExist(path)){
            console.log("hotfix del:",path);
            jsb.fileUtils.removeDirectory(path);
        }
        localStorage.setItem("install_check", version);
    }
}
`;

const round_vert = "\nprecision highp float;\nlayout(set = 0, binding = 0) uniform CCGlobal {\n  highp   vec4 cc_time;\n  mediump vec4 cc_screenSize;\n  mediump vec4 cc_nativeSize;\n  mediump vec4 cc_debug_view_mode;\n  mediump vec4 cc_debug_view_composite_pack_1;\n  mediump vec4 cc_debug_view_composite_pack_2;\n  mediump vec4 cc_debug_view_composite_pack_3;\n};\nlayout(set = 0, binding = 1) uniform CCCamera {\n  highp   mat4 cc_matView;\n  highp   mat4 cc_matViewInv;\n  highp   mat4 cc_matProj;\n  highp   mat4 cc_matProjInv;\n  highp   mat4 cc_matViewProj;\n  highp   mat4 cc_matViewProjInv;\n  highp   vec4 cc_cameraPos;\n  mediump vec4 cc_surfaceTransform;\n  mediump vec4 cc_screenScale;\n  mediump vec4 cc_exposure;\n  mediump vec4 cc_mainLitDir;\n  mediump vec4 cc_mainLitColor;\n  mediump vec4 cc_ambientSky;\n  mediump vec4 cc_ambientGround;\n  mediump vec4 cc_fogColor;\n  mediump vec4 cc_fogBase;\n  mediump vec4 cc_fogAdd;\n  mediump vec4 cc_nearFar;\n  mediump vec4 cc_viewPort;\n};\n#if USE_LOCAL\n  layout(set = 2, binding = 0) uniform CCLocal {\n    highp mat4 cc_matWorld;\n    highp mat4 cc_matWorldIT;\n    highp vec4 cc_lightingMapUVParam;\n    highp vec4 cc_localShadowBias;\n  };\n#endif\n#if SAMPLE_FROM_RT\n  #define QUATER_PI         0.78539816340\n  #define HALF_PI           1.57079632679\n  #define PI                3.14159265359\n  #define PI2               6.28318530718\n  #define PI4               12.5663706144\n  #define INV_QUATER_PI     1.27323954474\n  #define INV_HALF_PI       0.63661977237\n  #define INV_PI            0.31830988618\n  #define INV_PI2           0.15915494309\n  #define INV_PI4           0.07957747155\n  #define EPSILON           1e-6\n  #define EPSILON_LOWP      1e-4\n  #define LOG2              1.442695\n  #define EXP_VALUE         2.71828183f\n  #define FP_MAX            65504.0\n  #define FP_SCALE          0.0009765625\n  #define FP_SCALE_INV      1024.0\n  #define GRAY_VECTOR       vec3(0.299, 0.587, 0.114)\n      #define CC_LIGHTMAP_DISABLED 0\n  #define CC_LIGHTMAP_ALL_IN_ONE 1\n  #define CC_LIGHTMAP_CC_LIGHTMAP_INDIRECT_OCCLUSION 2\n#endif\nlayout(location = 0) in vec3 a_position;\nlayout(location = 1) in vec2 a_texCoord;\nlayout(location = 2) in vec4 a_color;\nlayout(location = 0) out vec4 color;\nlayout(location = 1) out vec2 uv0;\nvec4 vert () {\n  vec4 pos = vec4(a_position, 1);\n  #if USE_LOCAL\n    pos = cc_matWorld * pos;\n  #endif\n  #if USE_PIXEL_ALIGNMENT\n    pos = cc_matView * pos;\n    pos.xyz = floor(pos.xyz);\n    pos = cc_matProj * pos;\n  #else\n    pos = cc_matViewProj * pos;\n  #endif\n  uv0 = a_texCoord;\n  #if SAMPLE_FROM_RT\n    uv0 = cc_cameraPos.w > 1.0 ? vec2(uv0.x, 1.0 - uv0.y) : uv0;\n  #endif\n  color = a_color;\n  return pos;\n}\nvoid main() { gl_Position = vert(); }";
const round_frag = "\nprecision highp float;\nvec4 CCSampleWithAlphaSeparated(sampler2D tex, vec2 uv) {\n#if CC_USE_EMBEDDED_ALPHA\n  return vec4(texture(tex, uv).rgb, texture(tex, uv + vec2(0.0, 0.5)).r);\n#else\n  return texture(tex, uv);\n#endif\n}\n#if USE_ALPHA_TEST\n  layout(set = 1, binding = 0) uniform ALPHA_TEST_DATA {\n    float alphaThreshold;\n  };\n#endif\nvoid ALPHA_TEST (in vec4 color) {\n  #if USE_ALPHA_TEST\n    if (color.a < alphaThreshold) discard;\n  #endif\n}\nvoid ALPHA_TEST (in float alpha) {\n  #if USE_ALPHA_TEST\n    if (alpha < alphaThreshold) discard;\n  #endif\n}\nlayout(set = 0, binding = 0) uniform CCGlobal {\n  highp   vec4 cc_time;\n  mediump vec4 cc_screenSize;\n  mediump vec4 cc_nativeSize;\n  mediump vec4 cc_debug_view_mode;\n  mediump vec4 cc_debug_view_composite_pack_1;\n  mediump vec4 cc_debug_view_composite_pack_2;\n  mediump vec4 cc_debug_view_composite_pack_3;\n};\nlayout(set = 0, binding = 1) uniform CCCamera {\n  highp   mat4 cc_matView;\n  highp   mat4 cc_matViewInv;\n  highp   mat4 cc_matProj;\n  highp   mat4 cc_matProjInv;\n  highp   mat4 cc_matViewProj;\n  highp   mat4 cc_matViewProjInv;\n  highp   vec4 cc_cameraPos;\n  mediump vec4 cc_surfaceTransform;\n  mediump vec4 cc_screenScale;\n  mediump vec4 cc_exposure;\n  mediump vec4 cc_mainLitDir;\n  mediump vec4 cc_mainLitColor;\n  mediump vec4 cc_ambientSky;\n  mediump vec4 cc_ambientGround;\n  mediump vec4 cc_fogColor;\n  mediump vec4 cc_fogBase;\n  mediump vec4 cc_fogAdd;\n  mediump vec4 cc_nearFar;\n  mediump vec4 cc_viewPort;\n};\nlayout(location = 0) in vec4 color;\n#if USE_TEXTURE\n  layout(location = 1) in vec2 uv0;\n  layout(set = 2, binding = 11) uniform sampler2D cc_spriteTexture;\n#endif\nvec4 frag () {\n  vec4 o = vec4(1, 1, 1, 1);\n  #if USE_TEXTURE\n    o *= CCSampleWithAlphaSeparated(cc_spriteTexture, uv0);\n    #if IS_GRAY\n      float gray  = 0.2126 * o.r + 0.7152 * o.g + 0.0722 * o.b;\n      o.r = o.g = o.b = gray;\n    #endif\n  #endif\n  float d = distance(uv0, vec2(0.5, 0.5));\n  float r = 0.5;\n  float mask = smoothstep(r + 0.01, r - 0.01, d);\n  o.a = mask;\n  ALPHA_TEST(o);\n  return o;\n}\nlayout(location = 0) out vec4 cc_FragColor;\nvoid main() { cc_FragColor = frag(); }";
const corner_vert = "\nprecision highp float;\nlayout(set = 0, binding = 0) uniform CCGlobal {\n  highp   vec4 cc_time;\n  mediump vec4 cc_screenSize;\n  mediump vec4 cc_nativeSize;\n  mediump vec4 cc_debug_view_mode;\n  mediump vec4 cc_debug_view_composite_pack_1;\n  mediump vec4 cc_debug_view_composite_pack_2;\n  mediump vec4 cc_debug_view_composite_pack_3;\n};\nlayout(set = 0, binding = 1) uniform CCCamera {\n  highp   mat4 cc_matView;\n  highp   mat4 cc_matViewInv;\n  highp   mat4 cc_matProj;\n  highp   mat4 cc_matProjInv;\n  highp   mat4 cc_matViewProj;\n  highp   mat4 cc_matViewProjInv;\n  highp   vec4 cc_cameraPos;\n  mediump vec4 cc_surfaceTransform;\n  mediump vec4 cc_screenScale;\n  mediump vec4 cc_exposure;\n  mediump vec4 cc_mainLitDir;\n  mediump vec4 cc_mainLitColor;\n  mediump vec4 cc_ambientSky;\n  mediump vec4 cc_ambientGround;\n  mediump vec4 cc_fogColor;\n  mediump vec4 cc_fogBase;\n  mediump vec4 cc_fogAdd;\n  mediump vec4 cc_nearFar;\n  mediump vec4 cc_viewPort;\n};\n#if USE_LOCAL\n  layout(set = 2, binding = 0) uniform CCLocal {\n    highp mat4 cc_matWorld;\n    highp mat4 cc_matWorldIT;\n    highp vec4 cc_lightingMapUVParam;\n    highp vec4 cc_localShadowBias;\n  };\n#endif\n#if SAMPLE_FROM_RT\n  #define QUATER_PI         0.78539816340\n  #define HALF_PI           1.57079632679\n  #define PI                3.14159265359\n  #define PI2               6.28318530718\n  #define PI4               12.5663706144\n  #define INV_QUATER_PI     1.27323954474\n  #define INV_HALF_PI       0.63661977237\n  #define INV_PI            0.31830988618\n  #define INV_PI2           0.15915494309\n  #define INV_PI4           0.07957747155\n  #define EPSILON           1e-6\n  #define EPSILON_LOWP      1e-4\n  #define LOG2              1.442695\n  #define EXP_VALUE         2.71828183f\n  #define FP_MAX            65504.0\n  #define FP_SCALE          0.0009765625\n  #define FP_SCALE_INV      1024.0\n  #define GRAY_VECTOR       vec3(0.299, 0.587, 0.114)\n      #define CC_LIGHTMAP_DISABLED 0\n  #define CC_LIGHTMAP_ALL_IN_ONE 1\n  #define CC_LIGHTMAP_CC_LIGHTMAP_INDIRECT_OCCLUSION 2\n#endif\nlayout(location = 0) in vec3 a_position;\nlayout(location = 1) in vec2 a_texCoord;\nlayout(location = 2) in vec4 a_color;\nlayout(location = 0) out vec4 color;\nlayout(location = 1) out vec2 uv0;\nvec4 vert () {\n  vec4 pos = vec4(a_position, 1);\n  #if USE_LOCAL\n    pos = cc_matWorld * pos;\n  #endif\n  #if USE_PIXEL_ALIGNMENT\n    pos = cc_matView * pos;\n    pos.xyz = floor(pos.xyz);\n    pos = cc_matProj * pos;\n  #else\n    pos = cc_matViewProj * pos;\n  #endif\n  uv0 = a_texCoord;\n  #if SAMPLE_FROM_RT\n    uv0 = cc_cameraPos.w > 1.0 ? vec2(uv0.x, 1.0 - uv0.y) : uv0;\n  #endif\n  color = a_color;\n  return pos;\n}\nvoid main() { gl_Position = vert(); }";
const corner_frag = "\nprecision highp float;\nvec4 CCSampleWithAlphaSeparated(sampler2D tex, vec2 uv) {\n#if CC_USE_EMBEDDED_ALPHA\n  return vec4(texture(tex, uv).rgb, texture(tex, uv + vec2(0.0, 0.5)).r);\n#else\n  return texture(tex, uv);\n#endif\n}\n#if USE_ALPHA_TEST\n  layout(set = 1, binding = 0) uniform ALPHA_TEST_DATA {\n    float alphaThreshold;\n  };\n#endif\nvoid ALPHA_TEST (in vec4 color) {\n  #if USE_ALPHA_TEST\n    if (color.a < alphaThreshold) discard;\n  #endif\n}\nvoid ALPHA_TEST (in float alpha) {\n  #if USE_ALPHA_TEST\n    if (alpha < alphaThreshold) discard;\n  #endif\n}\nlayout(location = 0) in vec4 color;\nlayout(set = 1, binding = 1) uniform Float {\n  float rounded;\n};\n#if USE_TEXTURE\n  layout(location = 1) in vec2 uv0;\n  layout(set = 2, binding = 11) uniform sampler2D cc_spriteTexture;\n#endif\nvec4 frag () {\n  vec4 o = vec4(1, 1, 1, 1);\n  #if USE_TEXTURE\n    o *= CCSampleWithAlphaSeparated(cc_spriteTexture, uv0);\n    #if IS_GRAY\n      float gray  = 0.2126 * o.r + 0.7152 * o.g + 0.0722 * o.b;\n      o.r = o.g = o.b = gray;\n    #endif\n  #endif\n  vec2 uv1 = vec2(uv0.x - 0.5,uv0.y-0.5);\n  float rx = mod(abs(uv1.x), 0.5-rounded);\n  float ry = mod(abs(uv1.y), 0.5-rounded);\n  float mx = step(0.5-rounded, abs(uv1.x));\n  float my = step(0.5-rounded, abs(uv1.y));\n   float filletAlpha = 1.0-mx*my*smoothstep(rounded-0.01, rounded + 0.01, length(vec2(rx,ry)));\n  o.a=filletAlpha;\n  ALPHA_TEST(o);\n  return o;\n}\nlayout(location = 0) out vec4 cc_FragColor;\nvoid main() { cc_FragColor = frag(); }";

var BASE64_KEYS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
var Base64Values = new Array(123); // max char code in base64Keys
for (let i = 0; i < 123; ++i) Base64Values[i] = 64; // fill with placeholder('=') index
for (let i = 0; i < 64; ++i) Base64Values[BASE64_KEYS.charCodeAt(i)] = i;
var HexChars = '0123456789abcdef'.split('');
let HexMap = {}
{
    for (let i = 0; i < HexChars.length; i++) {
        let char = HexChars[i]
        HexMap[char] = i
    }
}
function readJsonSync(path){
    return JSON.parse(Fs.readFileSync(path));
}
function compressUuid(fullUuid) {
    const strs = fullUuid.split('@');
    const uuid = strs[0];
    if (uuid.length !== 36) {
        return fullUuid;
    }

    let zipUuid = []
    zipUuid[0] = uuid[0];
    zipUuid[1] = uuid[1];
    let cleanUuid = uuid.replace('-', '').replace('-', '').replace('-', '').replace('-', '')

    for (let i = 2, j = 2; i < 32; i += 3) {

        const left = HexMap[String.fromCharCode(cleanUuid.charCodeAt(i))];
        const mid = HexMap[String.fromCharCode(cleanUuid.charCodeAt(i + 1))];
        const right = HexMap[String.fromCharCode(cleanUuid.charCodeAt(i + 2))];

        zipUuid[j++] = BASE64_KEYS[(left << 2) + (mid >> 2)]
        zipUuid[j++] = BASE64_KEYS[((mid & 3) << 4) + right]
    }
    return fullUuid.replace(uuid, zipUuid.join(''));
}
function findUuidByPath(info, path, type){
    let type_index = info.types.findIndex(item=>item == type);
    if(type_index == -1){
        console.log("没有找类型:",type, "  ", path);
        return;
    }
    let index_str = "";
    for(let k in info.paths){
        let item = info.paths[k];
        if(item[1] == type_index && item[0] == path){
            index_str = k;
            break;
        }
    }
    if(index_str.length == 0){
        console.log("没有找路径:",type, "  ", path);
        return;
    }
    for(let k in info.packs){
        let item = info.packs[k];
        for(let i = 0 ; i < item.length ; i++){
            if(item[i] == index_str)return k;
        }
    }
    let index = parseInt(index_str);
    return info.uuids[index];
}
function findInfoByPack(info, index, path){
    let content_info = info[5];
    for(let i= 0 ; i < content_info.length ; i++){
        let item = content_info[i][0][0];
        if(item[index] == path){
            return item;
        }
    }
}
function addGlsl4(asset_path, bundle, path, vert, frag){
    // if(asset_path[asset_path.length - 1] != "/")asset_path += "/";
    let info = readJsonSync(Path.join(asset_path , bundle , "cc.config.json"));
    let uuid = findUuidByPath(info, path, "cc.EffectAsset");
    let res_path = Path.join(asset_path, bundle, "import", uuid.slice(0, 2), uuid + ".json");
    console.warn(path+":"+res_path);
    let res_info = readJsonSync(res_path);
    let eff_info = findInfoByPack(res_info, 1, "../" + bundle + "/" + path);
    if(!eff_info){
        console.warn("没找到对应信息");
        return;
    }
    // console.warn("aa:",eff_info[0])
    for(let i = 0 ; i < eff_info.length ; i++){
        // console.warn(i)
        if(eff_info[i].length && eff_info[i][0].glsl1){
            eff_info[i][0].glsl4 = {
                vert,
                frag
            }
            break;
        }
    }
    
    Fs.writeFileSync(res_path, JSON.stringify(res_info));
    // Fs.writeFile(res_path, JSON.stringify(res_info), function (error) {
    //     if (err) {
    //         console.warn("add glsl4 fail");
    //         throw err;
    //     }
    //     else{
    //         console.log("add glsl4");
    //     }
    // });
}


exports.onAfterBuild = function (options, result) {
    var url = Path.join(result.dest, 'data', 'main.js');

    if (!Fs.existsSync(url)) {
        url = Path.join(result.dest, 'assets', 'main.js');
    }

    Fs.readFile(url, "utf8", function (err, data) {
        if (err) {
            throw err;
        }
        data = data.replace("return application.init(cc);", "delHoxfix(cc);\nreturn application.init(cc);")
        var newStr = inject_script + data;
        Fs.writeFile(url, newStr, function (error) {
            if (err) {
                throw err;
            }
            console.warn("SearchPath updated in built main.js for hot update");
        });
    });
    // if(options.platform == "ios")return;
    
    // let asset_path = Path.join(result.dest,"data", 'assets');
    // console.log(asset_path)
    // addGlsl4(asset_path, "master", "shader/RoundSprite", round_vert, round_frag);
    // addGlsl4(asset_path, "master", "shader/CornerSprite", corner_vert, corner_frag);
}

