'use strict';

type Selector<$> = { $: Record<keyof $, any | null> }
//@ts-ignore
import { updatePropByDump } from '../../prop.js';
// import { updatePropByDump, disconnectGroup } = r('./prop');

export const template = `
<div class="component-container"></div>
<ui-prop type="dump" class="test"></ui-prop>
<ui-prop type="dump" class="test1"></ui-prop>
<ui-label class="label"></ui-label>
<ui-input class="test-input"></ui-input>
<ui-button type="primary" class="btn1">primary</ui-button>
`;

export const $ = {
    componentContainer: '.component-container',
    test: '.test',
    test1: '.test1',
    label: '.label',
    testInput: '.test-input',
    btn1: ".btn1"
};

type PanelThis = Selector<typeof $> & { dump: any };

export function update(this: PanelThis, dump: any) {
    // 使用 ui-porp 自动渲染，设置 prop 的 type 为 dump
    // render 传入一个 dump 数据，能够自动渲染出对应的界面
    // 自动渲染的界面修改后，能够自动提交数据
    updatePropByDump(this, dump);
    this.dump = dump;
    this.$.test.render(dump.value.label);
    this.$.test1.render(dump.value.label1);
    this.$.testInput.value = dump.value.label.value;
    this.$.label.value = dump.value.label.name;
}
export function ready(this: PanelThis) {
    this.$.testInput.addEventListener('confirm', () => {
        this.dump.value.label.value = this.$.testInput.value;
        this.$.test.dispatch('change-dump');
    });

    this.$.btn1.addEventListener('confirm', () => {
        Editor.Message.send("scene", "execute-component-method", { uuid: this.dump.value.uuid.value, name: "test", args: [] });
    });
}