'use strict';

const path = require("path");
const fs = require("fs-extra")
const exec = require("child_process").execSync;
const execFile = require("child_process").execFileSync;

var is_wait = false;
var is_wait2 = false;
var wait_over_str = "";
var wait_over_str2 = "";

var debug = 1;
function log(...arg){
    if(debug)Editor.log(arg);
}

function parsePaths(ori, dest, config, res_arr, add_meta){
    let final_ori = path.join(ori, config.origin);
    let final_dest = path.join(dest, config.dest);
    let path_arr = config.paths;
    let is_cocos = config.dest == "assets";
    add_meta = (add_meta && is_cocos);
    if(config.map_mode == 0){
        for(let i = 0 ; i < path_arr.length ; i++){
            res_arr.push(path.join(final_ori, path_arr[i]));
            res_arr.push(path.join(final_dest, path_arr[i]));
            if(add_meta){
                res_arr.push(path.join(final_ori, path_arr[i] + ".meta"));
                res_arr.push(path.join(final_dest, path_arr[i] + ".meta"));
            }
        }
    }
    else if(config.map_mode == 1){
        for(let i = 0 ; i < path_arr.length ; i += 2){
            res_arr.push(path.join(final_ori, path_arr[i]));
            res_arr.push(path.join(final_dest, path_arr[i + 1]));
            if(add_meta){
                res_arr.push(path.join(final_ori, path_arr[i] + ".meta"));
                res_arr.push(path.join(final_dest, path_arr[i + 1] + ".meta"));
            }
        }
    }
}
function parsePaths2(ori, dest, config, res_arr, cocos_arr){
    let final_ori = path.join(ori, config.origin);
    let final_dest = path.join(dest, config.dest);
    let path_arr = config.paths;
    let is_cocos = config.dest == "assets";
    let arr = is_cocos ? cocos_arr : res_arr;
    if(config.map_mode == 0){
        for(let i = 0 ; i < path_arr.length ; i++){
            arr.push(path.join(final_ori, path_arr[i]));
            arr.push(path.join(final_dest, path_arr[i]));
        }
    }
    else if(config.map_mode == 1){
        for(let i = 0 ; i < path_arr.length ; i += 2){
            arr.push(path.join(final_ori, path_arr[i]));
            arr.push(path.join(final_dest, path_arr[i + 1]));
        }
    }
}
function getDelPaths(dest, configs, res_arr, cocos_arr){
    for(let i = 0 ; i < configs.length ; i++){
        let config = configs[i];
        let final_dest = path.join(dest, config.dest);
        
        let path_arr = config.paths;
        let is_cocos = config.dest == "assets";
        let offset = 0;
        let interval = 1;
        if(config.map_mode == 1){
            offset = 1;
            interval = 2;
        }
        let final_path = "";
        if(is_cocos){
            for(let j = 0 ; j < path_arr.length ; j += interval){
                // if(path_arr[j + offset].indexOf(".") == -1){
                    final_path = "db://assets/" + path_arr[j + offset];
                    if(Editor.assetdb.exists(final_path))cocos_arr.push(final_path); 
                // }
            }
        }
        else{
            for(let j = 0 ; j < path_arr.length ; j += interval){
                final_path = path.join(final_dest, path_arr[j + offset]);
                if(fs.existsSync(final_path))res_arr.push(path.join(final_dest, path_arr[j + offset]));
            }
        }
       
    }
}
function saveDiffs(paths, func){
    let count = 0;
    let over_func = function(err){
        if(err)Editor.log(err);
        count--;
        if(count == 0)func();
    }
    for(let i = 0 ; i < paths.length ; i += 2){
        log("diffsave:" + paths[i + 1]);
        fs.copySync(paths[i + 1], paths[i]);
        let source_str = fs.readFileSync(paths[i + 1], "utf-8");
        let index = 0;
        let diff_str = "";
        while(1){
            let begin = "//diff"+index;
            let end = "//diffend"+index;
            let index0 = source_str.indexOf(begin);
            let index1 = source_str.indexOf(end);

            // log(index0 + "----" + index1);
            if(index0 > -1 && index1 > -1){
                index0 += begin.length;
                diff_str += begin;
                diff_str += source_str.slice(index0, index1);
                diff_str += (end + "\n");
            }
            else{
                log("保存"+paths[i]);
                count++;
                fs.writeFile(paths[i], diff_str, over_func);
                break;
            }
            index++;
        }
    }
}
function copyPaths(paths, func, mode){
    let count = 0;
    let from = 1;
    let to = 0;
    if(mode == 1){
        from = 0;
        to = 1;
    }
    for(let i = 0 ; i < paths.length ; i += 2){
        count++;
        fs.copy(paths[i + from], paths[i + to], function(err){
            if(err)Editor.log(err);
            else log("拷贝:"+paths[i + from]);
            count--;
            if(count == 0){
                func();
            }
        });
    }
}
function copyPathsCocos(paths, func){
    let count = 0;
    for(let i = 0 ; i < paths.length ; i += 2){
        count++;
        if(fs.existsSync(paths[i]+".meta")){
            fs.copySync(paths[i]+".meta", paths[i + 1] + ".meta");
        }
        let to_path = Editor.assetdb.fspathToUrl(paths[i+1]);
        to_path = to_path.slice(0, to_path.lastIndexOf("/"));
        Editor.assetdb.import([paths[i]], to_path, function(err){
            if(err)Editor.log(err);
            else log("导入:"+paths[i]);
            count--;
            if(count == 0){
                func();
            }
        });
    }
}
function removePaths(paths, func){
    let count = 0;
    if(paths.length == 0){
        func();
        return;
    }
    for(let i = 0 ; i < paths.length ; i++){
        count++;
        fs.remove(paths[i],  function(err){
            if(err)Editor.log(err);
            else log("del:"+paths[i]);
            count--;
            if(count == 0){
                func();
            }
        });
    }

}


function replaceTxt(str, source_str, dest_str){
    let re = new RegExp(source_str, 'g');
    return str.replace(re, dest_str);
}
function changeChannel(path, cur_config, to_config){
    let str = fs.readFileSync(path, "utf-8");
    str = replaceTxt(str, cur_config.channel, to_config.channel);
    if(cur_config.bundle_name){
        str = replaceTxt(str, cur_config.bundle_name, to_config.bundle_name);
    }
    let db_path = Editor.assetdb.fspathToUrl(path);
    Editor.assetdb.saveExists(db_path, str);
}
function changePathTxt(path, tartget, to){
    let str = fs.readFileSync(path, "utf-8");
    str = replaceTxt(str, tartget, to);
    fs.writeFileSync(path, str); 
}

function dealDiffOne(diff_path, source_path, cocos){
    log(source_path)
    let source_str = fs.readFileSync(source_path, "utf-8");
    log(diff_path)
    let diff_str = fs.readFileSync(diff_path, "utf-8");
    let cur_diff = "";
    let index = 0;
    while(1){
        let begin = "//diff"+index;
        let end = "//diffend"+index;
        let index0 = diff_str.indexOf(begin);
        let index1 = diff_str.indexOf(end);

        // log(index0 + "--333--" + index1);
        if(index0 > -1 && index1 > -1){
            index0 = index0 + begin.length;
            cur_diff = diff_str.slice(index0, index1);
        }
        else break;
        index0 = source_str.indexOf(begin);
        index1 = source_str.indexOf(end);
        // log(index0 + "--444--" + index1);
        if(index0 > -1 && index1 > -1){
            index0 = index0 + begin.length;
            source_str = source_str.slice(0, index0) + cur_diff + source_str.slice(index1);
        }
        else{
            Editor.warn("diff count 不一致:");
            Editor.warn(diff_path + "-->" + source_path);
            break;
        }
        index++;
    }
    if(cocos){
        let db_path = Editor.assetdb.fspathToUrl(source_path);
        Editor.assetdb.saveExists(db_path, source_str);
    }
    else{
        fs.writeFileSync(source_path, source_str);
    }
}
function dealDiffs(paths, cocos){
    for(let i = 0 ; i < paths.length ; i += 2){
        dealDiffOne(paths[i], paths[i + 1], cocos);
    }
}

module.exports = {
  load () {
  },

  unload () {
  },

  messages: {
    'open' () {
        Editor.Panel.open('changeskin');
    },

    'save' (a, cur_str) {
        if(is_wait){
            Editor.warn(wait_over_str);
            return;
        }
        is_wait = true;
        wait_over_str = "保存文件中..";

        // let skin_path = path.join(Editor.Project.path, "/packages/changeskin/");
        // exec(`chmod -R 777 ${skin_path}`);
        
        let cur_config = JSON.parse(cur_str);
        let cur_res_path = path.join(Editor.Project.path, "/majia/"+cur_config.res_path);
        let cur_diff_path = path.join(cur_res_path, "config.json");
        let cur_diff_config = JSON.parse(fs.readFileSync(cur_diff_path, "utf-8"));
        Editor.log("开始保存:  " + cur_config.name);

        Editor.log("清理文件...");
        let del_paths = new Array();
        del_paths.push(path.join(cur_res_path, "assets"));

        let funcOver = function(){
            is_wait = false;
            Editor.success("保存cocos文件over:        " + cur_config.name);
        }

        let funcSaveDiff = function(){
            Editor.log("保存diff文件")
            let diff_paths = new Array();
            let diffs = cur_diff_config.diffs;
            for(let i = 0 ; i < diffs.length ; i++){
                if(diffs[i].need_save){
                    parsePaths(cur_res_path, Editor.Project.path, diffs[i], diff_paths, false);
                }
            }
            log(diff_paths);
            saveDiffs(diff_paths, funcOver);
        }

        let funcSaveCover = function(){
            Editor.log("拷贝文件")
            let cover_paths = new Array();
            let covers = cur_diff_config.covers;
            for(let i = 0 ; i < covers.length ; i++){
                if(covers[i].need_save){
                    parsePaths(cur_res_path, Editor.Project.path, covers[i], cover_paths, true);
                }
            }
            copyPaths(cover_paths,funcSaveDiff,0);
        }

        removePaths(del_paths, function(){
            setTimeout(function(){
                funcSaveCover();
            }, 300)
        });
    },

    'delete' (a, cur_str){
        let cur_config = JSON.parse(cur_str);
        let cur_res_path = path.join(Editor.Project.path, "/majia/"+cur_config.res_path);
        let cur_diff_path = path.join(cur_res_path, "config.json");
        let cur_diff_config = JSON.parse(fs.readFileSync(cur_diff_path, "utf-8"));
        let del_paths = new Array();
        let cocos_dels = new Array();
        // let covers = cur_diff_config.delete;
        Editor.log("开始清理文件...",cur_diff_config);
        getDelPaths(Editor.Project.path, cur_diff_config.delete, del_paths, cocos_dels);
        Editor.log("开始清理文件...",cocos_dels);
        Editor.assetdb.delete(cocos_dels, function(){
            Editor.log("清理文件完成...");
        });
    },

    'load' (a, cur_str, to_str) {
        if(is_wait){
            Editor.warn(wait_over_str);
            return;
        }
        is_wait = true;
        wait_over_str = "载入中..";
        
        let cur_config = JSON.parse(cur_str);
        let to_config = JSON.parse(to_str);
        Editor.log("开始载入:" + to_config.name);

        let cur_res_path = path.join(Editor.Project.path, "/majia/"+cur_config.res_path);
        let cur_diff_path = path.join(cur_res_path, "config.json");
        let cur_diff_config = JSON.parse(fs.readFileSync(cur_diff_path, "utf-8"));

        let res_path = path.join(Editor.Project.path, "/majia/"+to_config.res_path);
        let diff_config_path = path.join(res_path, "config.json");
        let diff_config = JSON.parse(fs.readFileSync(diff_config_path, "utf-8"));
       
        // for(let i = 0 ; i < cocos_dels.length ; i++){
        //     Editor.assetdb.refresh(cocos_dels[i],function (err, results) {
        //         log(err);
        //         log(results);
        //     });
        // }

        let funcOver = function(){
            // Editor.assetdb.refresh('db://assets');
            // Editor.Ipc.sendToPanel('changeskin', 'loadSuccess');
            is_wait = false;
            Editor.Ipc.sendToPanel('changeskin', 'loadover', to_config.name);
            Editor.success("载入over:" + to_config.name);
        }

        let funcChangeName = function(){
            Editor.log("更换渠道:" + cur_config.channel + "-->" + to_config.channel);
            Editor.log("bundle名:" + cur_config.bundle_name + "-->" + to_config.bundle_name);
            let path_arr = diff_config.change_channel;
            for(let i = 0 ; i < path_arr.length ; i++){
                let final_path = path.join(Editor.Project.path, path_arr[i]);
                changeChannel(final_path, cur_config, to_config);
            }
            //改变修改uuid的渠道号
            let upath = path.join(Editor.Project.path, "changePngUuid.js");
            let ustr = fs.readFileSync(upath, "utf-8");
            ustr = replaceTxt(ustr, cur_config.channel, to_config.channel);
            fs.writeFileSync(upath,ustr)
            Editor.log("替换包名:" + cur_config.pack_name + "-->" + to_config.pack_name);

            let android_path = path.join(Editor.Project.path, "build/jsb-default/frameworks/runtime-src/proj.android-studio");
            //替换包名
            path_arr = diff_config.android_ex_packname;
            for(let i = 0 ; i < path_arr.length ; i++){
                let final_path = path.join(android_path, path_arr[i]);
                changePathTxt(final_path, cur_config.pack_name, to_config.pack_name);
            }
            funcOver();
        }

        let funcSaveDiff = function(){
            Editor.log("修改差异文件");
            let diff_paths = new Array();
            let cocos_diff = new Array();
            let diffs = diff_config.diffs;
            for(let i = 0 ; i < diffs.length; i++){
                 parsePaths2(res_path, Editor.Project.path, diffs[i], diff_paths, cocos_diff);
            }
            log(diff_paths);
            log(cocos_diff);
            Editor.log("修改普通文件")
            dealDiffs(diff_paths, false);
            Editor.log("修改cocos文件");
            dealDiffs(cocos_diff, true);
            funcChangeName();
        }

        let funcCover = function(){
            let cover_paths = new Array();
            let cocos_cover = new Array();
            let covers = diff_config.covers;
            for(let i = 0 ; i < covers.length; i++){
                 parsePaths2(res_path, Editor.Project.path, covers[i], cover_paths, cocos_cover);
            }
            Editor.log("导入cocos资源...");
            copyPathsCocos(cocos_cover, function(){
                Editor.log("拷贝普通资源...")
                copyPaths(cover_paths,function(){
                    funcSaveDiff();
                },1);
            })
        }
        
        let funcDel = function(func){
            Editor.log("清理文件...");
            let count = 2;
            let del_paths = new Array();
            let cocos_dels = new Array();
            let covers = cur_diff_config.covers;

            getDelPaths(Editor.Project.path, cur_diff_config.covers, del_paths, cocos_dels);
            log(del_paths);
            log(cocos_dels);
            Editor.assetdb.delete(cocos_dels, function(){
                count--;
                if(count == 0){
                    func();
                }
            });
            removePaths(del_paths, function(){
                count--;
                if(count == 0){
                    func();
                }
            });
        }
        funcDel(funcCover);
    },

    'save_version' (a, cur_str) {
        if(is_wait2){
            Editor.warn(wait_over_str2);
            return;
        }
        is_wait = true;
        wait_over_str2 = "载入版本中..";

        Editor.log(cur_str);
        let cur_config = JSON.parse(cur_str);
        let cur_res_path = path.join(Editor.Project.path, "/majia/"+cur_config.res_path);
        let cur_diff_path = path.join(cur_res_path, "config.json");
        let cur_diff_config = JSON.parse(fs.readFileSync(cur_diff_path, "utf-8"));

        let common_path = path.join(Editor.Project.path, "/majia/common/all_have_ver.json");
        let common_config = JSON.parse(fs.readFileSync(common_path, "utf-8"));
        Editor.log("开始保存版本:  " + cur_config.name);

        let funcOver = function(){
            is_wait2 = false;
            Editor.success("保存版本over:        " + cur_config.name);
        }
        let funcSaveCover = function(){
            Editor.log("拷贝文件")
            let cover_paths = new Array();
            let covers = cur_diff_config.covers;

            parsePaths(cur_res_path, Editor.Project.path, common_config, cover_paths, true);
            for(let i = 0 ; i < covers.length ; i++){
                if(covers[i].need_save){
                    parsePaths(cur_res_path, Editor.Project.path, covers[i], cover_paths, true);
                }
            }
            copyPaths(cover_paths,funcOver,0);
        }
        funcSaveCover();
    },

    'load_version' (a, cur_str, to_str) {
        if(is_wait2){
            Editor.warn(wait_over_str2);
            return;
        }
        is_wait2 = true;
        wait_over_str2 = "载入版本中..";

        let cur_config = JSON.parse(cur_str);
        let to_config = JSON.parse(to_str);
        Editor.log("开始载入版本:" + to_config.name);

        let cur_res_path = path.join(Editor.Project.path, "/majia/"+cur_config.res_path);
        let cur_diff_path = path.join(cur_res_path, "config.json");
        let cur_diff_config = JSON.parse(fs.readFileSync(cur_diff_path, "utf-8"));

        let res_path = path.join(Editor.Project.path, "/majia/"+to_config.res_path);
        let diff_config_path = path.join(res_path, "config.json");
        let diff_config = JSON.parse(fs.readFileSync(diff_config_path, "utf-8"));

        let common_path = path.join(Editor.Project.path, "/majia/common/all_have_ver.json");
        let common_config = JSON.parse(fs.readFileSync(common_path, "utf-8"));


        let writeConfig = function(){
            Editor.log("改变11马甲配置")
            let bak_path = path.join(res_path, "config_majia.json");
            let str = fs.readFileSync(bak_path, "utf-8");
            let majia11 = path.join(Editor.Project.path, "/majia/11/config.json");
            fs.writeFileSync(majia11, str); 
        }

        let funcOver = function(){
            writeConfig();
            is_wait2 = false;
            Editor.Ipc.sendToPanel('changeskin', 'loadover2', to_config.name);
            Editor.success("载入over:" + to_config.name);
        }

        let funcChangeVersion = function(){
            Editor.log("更换version:" + cur_config.version + "-->" + to_config.version);
            let path_arr = diff_config.change_channel;
            for(let i = 0 ; i < path_arr.length ; i++){
                let final_path = path.join(Editor.Project.path, path_arr[i]);
                changePathTxt(final_path, cur_config.version, to_config.version);
            }
            funcOver();
        }

        let funcCover = function(){
            let cover_paths = new Array();
            let cocos_cover = new Array();
            let covers = diff_config.covers;
            parsePaths2(res_path, Editor.Project.path, common_config, cover_paths, cocos_cover);
            for(let i = 0 ; i < covers.length; i++){
                 parsePaths2(res_path, Editor.Project.path, covers[i], cover_paths, cocos_cover);
            }
            Editor.log("导入cocos资源...");
            copyPathsCocos(cocos_cover, funcChangeVersion);
        }
        
        let funcDel = function(func){
            Editor.log("清理文件...");
            let del_paths = new Array();
            let cocos_dels = new Array();
            let covers = cur_diff_config.covers;

            getDelPaths(Editor.Project.path, cur_diff_config.covers, del_paths, cocos_dels);
            log(cocos_dels);
            Editor.assetdb.delete(cocos_dels, func);
        }
        
        funcDel(funcCover);
    }

  },
};