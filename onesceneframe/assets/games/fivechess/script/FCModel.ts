import { BaseModel } from "master/base/BaseModel";
import { Stype } from "master/web2/Cmd";
import { protoMan } from "master/web2/ProtoMan";
import { web_conn } from "master/web2/WebConn";


export const FCCmd = {
    Get_Rooms: 1,
    USER_QUIT: 2,
    ENTER_ROOM: 3,
    EXIT_ROOM: 4,
    SITDOWN: 5,
    STANDUP: 6,
    USER_ARRIVED: 7,
    SEND_PROP: 8,
    SEND_DO_READY: 9,
    ROUND_START: 10,
    TURN_TO_PLAYER: 11,
    PUT_CHESS: 12,
    CHECKOUT: 13,
    CHECKOUT_OVER: 14,
    RECONNECT: 15,
}
class Model extends BaseModel {
    public getRooms(func: Function, target: any){
        this.map_callinfo.set(FCCmd.Get_Rooms, [func, target]);
        web_conn.sendCmd(Stype.Game5Chess, FCCmd.Get_Rooms, null);
    }
    public enterRoom(zid: number){
        web_conn.sendCmd(Stype.Game5Chess, FCCmd.ENTER_ROOM, {zid});
    }
    public onRecv(ctype: number, buf: DataView): void {
        let data = protoMan.decodeJson(buf);
        console.log(ctype, " :",data);
        switch(ctype){
            case FCCmd.Get_Rooms:
                let info = this.map_callinfo.get(ctype);
                info[0].call(info[1], data);
        }
    }
}


export const fcModel = new Model();