import { _decorator, Component, Label, Node } from 'cc';
import { fcModel } from '../script/FCModel';
import { sceneMgr } from 'master/mgr/SceneMgr';
import { Scenes } from 'master/Urls';
import { UtilsPanel } from 'master/utils/UtilsPanel';
import { Generate } from 'master/components/Generate';
import { Stype } from 'master/web2/Cmd';
import { web_conn } from 'master/web2/WebConn';
import { q } from 'master/Q';
import { resMgr } from 'master/mgr/ResMgr';
const { ccclass, property } = _decorator;

@ccclass('FiveChess')
export class FiveChess extends Component {
    private gene_rooms: Generate = null;
	//auto
	protected onLoad(){
		UtilsPanel.getAllNeedCom(this, this.node, false);
        web_conn.addModel(Stype.Game5Chess, fcModel);
        fcModel.getRooms(this.initRooms, this);
	}
	//auto
	private onLobbyClick(){
		sceneMgr.loadScene(Scenes.Lobby);
	}

    private initRooms(rooms: Array<any>){
        q.removeLoading();
        this.gene_rooms.initData(rooms, (node:Node, index:number)=>{
            node.getChildByName("lb_name").getComponent(Label).string = rooms[index].name;
            UtilsPanel.addBtnEvent2(node, "onClickRoom", this, rooms[index].zid);
        })
    }
    private onClickRoom(zid:number){
        q.showLoading();
        sceneMgr.loadScene("fivechessView", resMgr.getGameBundle(), zid);
    }
}


