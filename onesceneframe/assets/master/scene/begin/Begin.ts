import { _decorator, Label, Node } from 'cc';
import { BaseScene } from '../../script/base/BaseScene';
import { sceneMgr } from '../../script/mgr/SceneMgr';
import { Scenes } from '../../script/Urls';
import { UtilsPanel } from '../../script/utils/UtilsPanel';
import { web_conn } from '../../script/web2/WebConn';
import { Stype } from '../../script/web2/Cmd';
import { authModel } from '../../script/web2/AuthModel';
import { q } from 'master/Q';

const { ccclass, property } = _decorator;

@ccclass('Begin')
export class Begin extends BaseScene {

    private lb_test: Label = null;

    protected onLoad(): void {
        this.fitScreen();
        UtilsPanel.getAllNeedCom(this, this.node);
        web_conn.addModel(Stype.Auth, authModel);
    }

    protected start(): void {
        this.lb_test.string = "30 : عدد اللاعبين المتصلين الان "
    }
    //auto
	public onBatchClick(){
		sceneMgr.loadScene(Scenes.Batch);
	}
	//auto
	public onLoginClick(){
        sceneMgr.loadScene(Scenes.Login);
	}

}


