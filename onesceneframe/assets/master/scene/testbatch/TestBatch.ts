import { _decorator, Component, EditBox, Label, Node } from 'cc';
import { Scenes } from '../../script/Urls';
import { BaseScene, SceneAction } from '../../script/base/BaseScene';
import { Generate } from '../../script/components/Generate';
import { sceneMgr } from '../../script/mgr/SceneMgr';
import { UtilsPanel } from '../../script/utils/UtilsPanel';

const { ccclass, property } = _decorator;

@ccclass('TestBatch')
export class TestBatch extends BaseScene {
    protected enter_index = SceneAction.R2L;

    private gene_test: Generate = null;
    private edit_count: EditBox = null;
    private lb_cur_static: Label = null;

    protected onLoad(): void {
        UtilsPanel.getAllNeedCom(this, this.node, true);
    }
    private onLobbyClick(){
        sceneMgr.loadScene(Scenes.Lobby);
    }
    private onSwitchClick(){
        this.gene_test.node._static = !this.gene_test.node._static;
        this.lb_cur_static.string = "" + this.gene_test.node._static;
    }
    private onGeneClick(){
        this.gene_test.initData(new Array(parseInt(this.edit_count.string)), function(){});
    }
}


