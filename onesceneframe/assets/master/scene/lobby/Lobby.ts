import { _decorator, Component, Label, Node } from 'cc';
import { Scenes } from '../../script/Urls';
import { BaseScene, SceneAction } from '../../script/base/BaseScene';
import { sceneMgr } from '../../script/mgr/SceneMgr';
import { UtilsPanel } from '../../script/utils/UtilsPanel';
import { my_info } from '../../script/data/MyInfo';
import { hall } from './Hall';

const { ccclass, property } = _decorator;

@ccclass('Lobby')
export class Lobby extends BaseScene {
    protected enter_index = SceneAction.R2L;

    private lb_name: Label = null;
	private lb_id: Label = null;
    
    protected onLoad(): void {
        UtilsPanel.getAllNeedCom(this, this.node);
    }
    protected start(): void {
        this.lb_name.string = my_info.name;
        this.lb_id.string = "" + my_info.uid;
    }
	//auto
	public onLoginClick(){
        sceneMgr.loadScene(Scenes.Login);
	}
	public onFcClick(){
        hall.enterGame("fivechess")
	}
}


