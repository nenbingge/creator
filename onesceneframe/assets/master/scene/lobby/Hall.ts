import { _decorator, AssetManager, assetManager, Component, director, Node, SceneAsset } from 'cc';
import { GameName } from 'master/enum/CommonEnum';
import { resMgr } from 'master/mgr/ResMgr';
import { sceneMgr } from 'master/mgr/SceneMgr';
import { q } from 'master/Q';

class Hall{
    private is_load_game = false;
    public enterGame(game_name: GameName){
        if(this.is_load_game)return
        this.is_load_game = true;
        q.showLoading(0);
        assetManager.loadBundle(game_name, (err: Error, bundle: AssetManager.Bundle)=>{
            if(err){
                q.removeLoading();
                this.is_load_game = true;
                return;
            }
            resMgr.setGameBundle(bundle);
            this.is_load_game = false;
            sceneMgr.loadScene(game_name, bundle)
        })
    }
}
export const hall = new Hall();

