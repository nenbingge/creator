import { _decorator, Enum, Component, Prefab, instantiate, log, UITransform, Node } from "cc";
import { SlotItem } from "./SlotItem";
import { CircleArray } from "../../script/extra/ExClass";

const {ccclass, property} = _decorator;

const Dir = Enum({
    Bottom: 0,
    Top: 1,
    Left: 2,
    Right: 3,
})
// switch(this.dir){
//     case Dir.Bottom:
//         break;
//     case Dir.Top:
//         break;
//     case Dir.Left:
//         break;
//     case Dir.Right:
//         break;
// }
 //0静止 1加速中 2最大速度 3停止中 4 回弹1 5 回弹2
export const SlotViewState = {
    Normal: 0,
    AddSpeed: 1,
    MaxSpeed: 2,
    Stoping: 3,
    Beyond1: 4,
    Beyond2: 5,
}

class SlotInfo{
    item: SlotItem
    uitrans: UITransform
    constructor(node: Node){
        this.item = node.getComponent(SlotItem);
        this.uitrans = node.getComponent(UITransform);
    }
}

@ccclass("SlotView")
export class SlotView extends Component {
    @property(Prefab)
    public pre_item: Prefab = null;
    
    @property({type:Dir})
    public dir = Dir.Bottom;
    @property({tooltip:"速率"})
    public rate = 1;
    @property({tooltip:"起始速度"})
    public begin_speed = 600;
    @property({tooltip:"最大增加速度"})
    public max_add_speed = 2400;
    @property({tooltip:"加速时间(秒)"})
    public acc_time = 1;
    @property({tooltip:"停止时速度"})
    public stop_speed = 4000;
    @property({tooltip:"回弹距离"})
    public beyond = 0;
    @property({tooltip:"回弹时间"})
    public back_speed = 0.1;
    @property({tooltip:"最大元素数量"})
    public max_count = 6;
    @property({tooltip:"元素间距"})
    public item_offset = 0;
    
    private list: CircleArray<SlotInfo> = null;
    private index = 0;
    private state = 0;
    private begin_pos = 0;
    private end_pos = 0;
    private first_index = 0;

    private cur_speed = 0;
    private cur_time = 0;
    private data:Array<any> = null;
    private data_index = 0;
    private target_dis = 0;
    private temp_arr: Array<any>;

    private call_obj: any = null;
    private func_move_over: Function = null;

    public init(index:number){
        this.index = index;
        // this.temp_arr = new Array(this.last);
        // this.last = this.max_count - 1;
        // this.last2 = this.last - 1;
        this._calculateBoundary();
        let arr = new Array(this.max_count);
        for(let i = 0; i < this.max_count; i++){
            let node = instantiate(this.pre_item);
            node.setParent(this.node);
            arr[i] = new SlotInfo(node);
            arr[i].item.random();
        }
        this.list = new CircleArray(arr, arr.length);
        this.resetPos();
    }
    public setCallBack(obj:any, func_move_over:Function){
        this.call_obj = obj;
        this.func_move_over = func_move_over;
    }
    public getPos(index:number){
        return this.list.get(this.first_index + index).item.node.position;
    }
    public resetPos(){
        let cur = this.begin_pos;
        let uitrans: UITransform = null;
        switch(this.dir){
            case Dir.Bottom:
                for(let i = this.max_count - 1; i >= 0; i--){
                    uitrans = this.list.get(i).uitrans;
                    uitrans.node.toY(cur + uitrans.height * uitrans.anchorY);
                    cur = cur + uitrans.height + this.item_offset;
                }
                break;
            case Dir.Top:
                break;
            case Dir.Left:
                break;
            case Dir.Right:
                break;
            }
    }
    private _calculateBoundary(){
        let uitrans = this.node.getComponent(UITransform);
        switch(this.dir){
            case Dir.Bottom:
                this.begin_pos = -uitrans.height * uitrans.anchorY;
                this.end_pos = this.begin_pos + uitrans.height;
                break;
            case Dir.Top:
                this.end_pos = -uitrans.height * uitrans.anchorY;
                this.begin_pos = this.begin_pos + uitrans.height;
                break;
        }
        // log("begin_pos:",this.begin_pos);
        // log("end_pos:",this.end_pos);
    }

    private resetIndex(){
        for(let i = 0 ; i < this.list.getLen() ; i++)this.list[i].index = i;
    }
    private logIndex(flag=""){
        log("----------------index:"+flag+"----------------");
        for(let i = 0 ; i < this.list.getLen() ; i++)log(this.list[i].index.toString());
        log("----------------index_end----------------")
    }

    public beginMove(){
        this.state = 1;
        this.cur_speed = this.begin_speed;
        // this.cur_continue = this.continue_time;
    }
    public forceStop(){
        this.state = 0;
    }
    public stop(data:any[]){
        this.data = data;
        this.state = 3;
        this.cur_speed = this.stop_speed;
        switch(this.dir){
            case Dir.Bottom:
                this.stopBottom(data);
                break;
            case Dir.Top:
                break;
            case Dir.Left:
                break;
            case Dir.Right:
                break;
         }
    }

    private frontedList(count:number){
        let first_change = this.max_count - count;
        for(let i = 0 ; i < count ; i++){
            this.temp_arr[i] = this.list[first_change + i];
        }
        for(let i = this.max_count - count; i >= 0; i--){
            this.list[i + count] = this.list[i];
        }
        for(let i = 0 ; i < count ; i++){
            this.list[i] = this.temp_arr[i];
        }
    }
    
    private stopBottom(data: any[]){
        let index = 0;
        for(let i = 0 ; i < this.max_count ; i++){
            if(this.list.get(i).item.node.position.y < this.end_pos){
                index = i - 1;
                break;
            }
        }
        if(index < 0){
            this.data_index = data.length - 1;
            let uitrans = this.list.get(0).uitrans;
            this.target_dis = uitrans.node.position.y - this.begin_pos + uitrans.height * (1 - uitrans.anchorY) + this.item_offset;
            return;
        }
        let uitrans = this.list.get(index).uitrans;
        this.target_dis = uitrans.node.position.y - this.begin_pos - uitrans.height * uitrans.anchorY;

        for(let i = index; i >= 0; i--){
            this.list.get(i).item.init(data[this.data_index--]);
        }
        let add = 2 - index;
        if(add > 0){
            // log("fewfw",this.index);
            this.frontedList(add);
            index = 2;
        }
        else{
            add = 0;
        }
        let cur_index = index - 2;
        this.data_index = this.data.length - 1;
        this.first_index = cur_index;
        // log("stop first ",this.index,":",this.first_index);
        let top = 0;
        // log("cur_index:",cur_index);
        if(cur_index > this.data_index)top = cur_index - this.data_index;
        for(let i = cur_index ; i >= top ; i--){
            this.list[i].init(this.data[this.data_index]);
            this.data_index--;
        }
        // this.resetPos(cur_index + add);
        let node = this.list[cur_index].node;
        this.target_dis = node.y - this.begin_pos - node.height * node.anchorY * node.scaleY;
    }
    private moveBottom(dis:number){
        let not_monitor = this.state > 3;
        if(this.state == 3){
            this.target_dis -= dis;
            if(this.target_dis <= 0){
                dis += this.target_dis;
                this.state = 4;
                this.cur_speed = this.back_speed;
                this.target_dis = this.beyond;
            }
        }
        else if(this.state == 4){
            this.target_dis -= dis;
            if(this.target_dis < 0){
                dis += this.target_dis;
                this.target_dis = this.beyond;
                this.state = 5;
            }
            dis = -dis;
        }
        else if(this.state == 5){
            this.target_dis -= dis;
            if(this.target_dis < 0){
                dis += this.target_dis;
                this.state = 0;
                this.func_move_over && this.func_move_over.call(this.call_obj, this.index);
            }
        }
        for(let i = 0 ; i < this.max_count ; i++){
            this.list.get(i).item.node.addY(-dis);
        }
        if(not_monitor)return;
        if(this.list.getLast(1).item.node.position.y > this.begin_pos)return;
        this.list.addOffset(-1);
        if(this.state >= 3){
            this.first_index++;
            if(this.data_index >= 0){
                this.list[0].init(this.data[this.data_index]);
                this.data_index--;
            }
        }
        else this.list[0].random();
        let uitrans1 = this.list.get(1).uitrans;
        this.list.get(0).item.node.toY(uitrans1.node.position.y + (uitrans1.height * (1 - uitrans1.anchorY) + this.item_offset));
    }

    protected update(dt: number) {
        if(this.state == 0)return;
        let dis = this.cur_speed * dt;
        if(this.state == 1){
            this.cur_time += dt;
            this.cur_speed = this.begin_speed + (this.cur_time / this.acc_time) * this.max_add_speed;
            if(this.cur_time >= this.acc_time){
                this.cur_speed = this.begin_speed + this.max_add_speed;
                this.state = 2;
            }
        }
        switch(this.dir){
            case Dir.Bottom:
                this.moveBottom(dis);
                break;
            case Dir.Top:
                break;
            case Dir.Left:
                break;
            case Dir.Right:
                break;
        }
    }
}
