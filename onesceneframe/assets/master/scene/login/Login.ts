import { _decorator, Component, Node } from 'cc';
import { BaseScene, SceneAction } from '../../script/base/BaseScene';
import { UtilsPanel } from '../../script/utils/UtilsPanel';
import { authModel } from '../../script/web2/AuthModel';
import { NativeHelper } from '../../script/utils/NativeHelper';
import { web_conn } from '../../script/web2/WebConn';
import { my_info } from '../../script/data/MyInfo';
import { User } from '../../script/web2/Auth';
import { RspParam } from '../../script/web2/Cmd';
import { sceneMgr } from '../../script/mgr/SceneMgr';
import { Scenes } from '../../script/Urls';
import { C } from '../../script/Const';
const { ccclass, property } = _decorator;

@ccclass('Login')
export class Login extends BaseScene {
    protected enter_index = SceneAction.R2L;

    protected onLoad(): void {
        UtilsPanel.getAllNeedCom(this, this.node);
        web_conn.connect(C.getLoginUrl());
    }
    private onLoginClick(){
        authModel.login(NativeHelper.getImei(), this.onLogin, this);
    }
    private onLogin(data: User){
        if(data.status == RspParam.OK){
            console.log("登录成功:", data);
            my_info.init(data);
            sceneMgr.loadScene(Scenes.Lobby);
        }
        else{
            console.log("登录失败")
        }
    }

}


