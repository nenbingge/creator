import { js } from "cc"
import { User } from "../web2/Auth"

class MyInfo {
    uid: number
    name: string
    sex: number
    face: string
    status: number
    exp: number
    gold: number
    vip: number
    vip_end: number

    public init(user: User){
        js.mixin(this, user);
    }
}

export const my_info = new MyInfo();

