
export abstract class BaseModel {
    protected map_callinfo: Map<number, CallInfo> = new Map();
    public abstract onRecv(ctype:number, buf: DataView):void;
}
