import { _decorator, Component, Node, Prefab, tween, view } from 'cc';

export const SceneAction = {
    None: 0,
    R2L: 1,
}

export class BaseScene extends Component {
    protected enter_index = SceneAction.R2L;
    public scene_cache = false;

    public onEnter(opts?: Function){
        this.fitScreen();
        this.enterAction();
        return this.enter_index;
    }
    protected fitScreen(){
        this.node._uiProps.uiTransformComp.setContentSize(view.getVisibleSize());
    }
    protected enterAction(){
        switch(this.enter_index){
            case SceneAction.None:
                this.afterEnter();
                break;
            case SceneAction.R2L:
                this.node.setPosition(view.getVisibleSize().width, 0);
                tween(this.node).toPro(0.2, {x: 0}).call(()=>{this.afterEnter();}).start();
                break;
        }
    }
    public leaveAction(index:number, func_release:Function){
        switch(index){
            case SceneAction.None:
                this.atferLeave(func_release);
                break;
            case SceneAction.R2L:
                tween(this.node).toPro(0.2, {x: -view.getVisibleSize().width}).call(()=>{this.atferLeave(func_release);}).start();
                break;
        }
    }
    protected afterEnter(){}
    protected atferLeave(func:Function){
        this.node.destroy();
        if(!this.scene_cache)func();
    }
}


