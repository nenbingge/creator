import { Node, Vec3 } from "cc";

//#region CircleArray
export class CircleArray<T>{
    private arr: Array<T> = null;
    private begin = 0;
    private len = 0;
    constructor(arr:Array<T>, len:number){
        this.arr = arr;
        this.len = len;
    }
    public get(index:number):T{
        if(index > -1 && index < this.len){
            let real_index = (this.begin + index) % this.len;
            return this.arr[real_index];
        }
        return null;
    }
    public getLast(offset:number):T{
        let index = this.begin - offset;
        if(index < 0)index += this.len;
        return this.arr[index];
    }
    public addOffset(add:number){
        this.begin = (this.begin + add) % this.len;
        if(this.begin < 0)this.begin += this.len;
    }
    public getLen(){
        return this.len;
    }
    public resetArrIndex(){
        if(this.begin == 0)return;
        let arr_temp = this.arr.slice();
        let map = new Array(this.len);
        for(let i = 0; i < this.len; i++){
            map[i] = (this.begin + i) % this.len;
        }
        for(let i = 0; i < this.len; i++){
            this.arr[i] = arr_temp[map[i]];
        }
    }
}

//#region SplitInfo
export class SplitInfo{
    public static getOffsetByNode(node:Node){
        let children = node.children;
        let arr = new Array(children.length);
        for(let i = 0 ; i < children.length ; i++){
            arr[i] = [children[i].position.x, children[i].position.y];
        }
        return arr;
    }
    public offset = 0;
    public parent: Node = null;
    public children: Array<Node> = null;
    private status: Array<boolean> = null;
    private state = false;
    constructor(parent:Node){
        this.parent = parent;
        this.children = parent.children.concat();
        let grand = parent.parent;
        this.status = new Array(this.children.length);
        for(let i = 0 ; i < this.children.length ; i++){
            this.children[i].setParent(grand);
            this.status[i] = this.children[i].active;
        }
        this.state = parent.active;
    }
    public setSlisByIndex(index:number){
        let len = this.children.length;
        for(let i = 0 ; i < len ; i++){
            this.children[i].setSiblingIndex((index + 1) * (i + 1) + index + this.offset);
        }
    }
    public setActive(state:boolean){
        if(this.state == state)return;
        this.state = state;
        if(state){
            for(let i = 0 , l = this.children.length ; i < l ; i++)this.children[i].active = this.status[i];
        }
        else{
            for(let i = 0 , l = this.children.length ; i < l ; i++){
                this.status[i] = this.children[i].active;
                this.children[i].active = false;
            }
        }
    }
    public moveByOffset(offset_arr:Array<Array<number>>){
        let base_x = this.parent.position.x;
        let base_y = this.parent.position.y;
        for(let i = 0 ; i < offset_arr.length ; i++){
            this.children[i].setPosition(base_x + offset_arr[i][0], base_y + offset_arr[i][1]);
        }
    }
}

//#region LRUArr
export class LRUArr{
    private arr: string[] = null;
    private max = 0;

    constructor(max:number){
        this.max = max;
        this.arr = [];
    }
    public addKey(key: string){
        if (this.arr.length < this.max) {
            this.arr.push(key);
        }
        else {
            let index = this.arr.indexOf(key);
            let last = this.arr.length - 1;
            let out_key = null;
            if (index == -1) {
                index = 0;
                out_key = this.arr[0];
            }
            for (let i = index + 1; i < last; i++) {
                this.arr[i - 1] = this.arr[i];
            }
            this.arr[last] = key;
            return out_key;
        }
        return null;
    }
    public delKey(key: string){
        let index = this.arr.indexOf(key);
        if(index > -1)this.arr.splice(index, 1);
    }
    public log(){
        console.log("lru info: max:", this.max);
        console.log(this.arr);
    }
}