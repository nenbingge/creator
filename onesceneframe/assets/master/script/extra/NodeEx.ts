
import { Director, director, Node, NodeEventType, sys, UIOpacity, UIRenderer, Vec3 } from 'cc';
import { JSB } from 'cc/env';

const nodeProto: any = Node.prototype;

nodeProto._delaySort = function(){
    director.on(Director.EVENT_AFTER_UPDATE, this.sortByPosZ, this);
}
nodeProto.autoSort = function(){
    this.on(NodeEventType.CHILD_ADDED, this._delaySort, this);
}
nodeProto.sortByPosZ = function(){
    let _children = this._children;
    if (_children.length > 1) {
        // insertion sort
        let child, child2;
        for (let i = 1, count = _children.length; i < count; i++) {
            child = _children[i];
            let j = i;
            for (; j > 0 &&
                (child2 = _children[j - 1])._lpos.z > child._lpos.z; j--) {
                    _children[j] = child2;
                }
            _children[j] = child;
        }
    }
    this._updateSiblingIndex();
    if(JSB){
        this._setChildren(_children);
    }
    director.off(Director.EVENT_AFTER_UPDATE, this._delaySort, this);
}
nodeProto.toX = function(x: number){
    this.setPosition(x, this._lpos.y);
}
nodeProto.addX = function(add: number){
    this.setPosition(this._lpos.x + add, this._lpos.y);
}
nodeProto.toY = function(y: number){
    this.setPosition(this._lpos.x, y);
}
nodeProto.addY = function(add: number){
    this.setPosition(this._lpos.x, this._lpos.y + add);
}
nodeProto.toZ = function(z: number, need_sort:boolean){
    this._lpos.z = z;
    if(need_sort && this.parent){
        this.parent._delaySort();
    }
}
nodeProto.toXYByArr = function(pos:Array<number>){
    this.setPosition(pos[0], pos[1]);
}

nodeProto.toScaleX = function(x: number){
    this.setScale(x, this._lscale.y);
}
nodeProto.toScaleY = function(y: number){
    this.setScale(this._lscale.x, y);
}
nodeProto.toScaleXY = function(scale: number){
    this.setScale(scale, scale);
}
function setLocalOpacityDirty (node: Node, opacity: number) {
    if (!node.isValid) {
        return;
    }
    const render = node._uiProps.uiComp as UIRenderer;
    let final_opacity = node._uiProps.localOpacity * opacity;
    if (render && render.color) { // exclude UIMeshRenderer which has not color
        render.renderEntity.colorDirty = true;
        render.renderEntity.localOpacity = final_opacity
    }
    node.children.forEach(function(child: Node){
        setLocalOpacityDirty(child, final_opacity);
    })
}
nodeProto.toOpacity = function(opacity: number){
    // console.log(this.name, opacity)
    this._uiProps.localOpacity = opacity;
    if (JSB) {
        setLocalOpacityDirty(this, 1);
    }
}
