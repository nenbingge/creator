
import { AudioClip, Button, EventHandler, Vec3, isValid } from "cc";
import { audioMgr } from "../mgr/AudioMgr";
import { resMgr } from "../mgr/ResMgr";

function _onTouchEnded(event:any){
    if (!this._interactable || !this.enabledInHierarchy)return;
    if (this._pressed) {
        EventHandler.emitEvents(this.clickEvents, event);
        this.node.emit("click", this);
    }
    this._pressed = false;
    this._updateState();
    audio_c.playBtnSound();
    if (event)event.propagationStopped = true;
}
class AudioCommon{
    private btn_clip: AudioClip = null;
    private func_btn_end = null;

    public setBtnClip(clip: AudioClip){
        if(isValid(this.btn_clip ))return;
        clip.addRef();
        this.btn_clip = clip;
    }

    public playBtnSound(){
        if(isValid(this.btn_clip))audioMgr.playEffect(this.btn_clip);
    }

    public addBtnSound(){
        if(this.func_btn_end)return;
        //@ts-ignore
        this.func_btn_end = Button.prototype._onTouchEnded;
        //@ts-ignore
        Button.prototype._onTouchEnded = _onTouchEnded;
        resMgr.load("cache",'sound/button',AudioClip,(e,a)=>{
            if(e)return;
            this.setBtnClip(a)
        })
    }
    public recoverBtnSound(btn: Button) {
        //@ts-ignore
        btn._onTouchEnded = this.func_btn_end;
    }
    public changeBtnClip(clip:AudioClip){
        let old = this.btn_clip
        this.btn_clip = clip
        return old
    }
}
export const audio_c = new AudioCommon();