import { Node } from "cc";
import { dialogMgr } from "../mgr/DialogMgr";
import { Dialogs } from "../Urls";
import { MsgBox, MsgInfo } from "../../ui/common/MsgBox";
import { Tip } from "../../ui/common/Tip";

class DialogCommon {
    public showTip(desc: string) {
        dialogMgr.showByResUrl<Tip>(["Tip", "common/tip"], desc);
    }
    public showMsgBox1(desc: string, func: Function = null, btn_desc: string = "") {
        let data = new MsgInfo(desc, 1);
        data.func_sure = func;
        data.txt_sure = btn_desc;
        data.show_close = true;
        dialogMgr.showByResUrl<MsgBox>(Dialogs.MsgBox, data)
    }
    public showMsgBox2(desc: string, func_sure: Function, func_cancel: Function = null, btn_desc: string = "") {
        let data = new MsgInfo(desc, 2);
        data.func_sure = func_sure;
        data.func_cancel = func_cancel;
        data.txt_sure = btn_desc;
        data.show_close = true;
        dialogMgr.showByResUrl<MsgBox>(Dialogs.MsgBox, data)
    }
    public log(data: any) {
        dialogMgr.showByResUrl(["LogPanel", "common/log"], data);
    }


}
export const dialog_c = new DialogCommon();