const buildDialog = <T extends {[key:string]: [string, string]}>(library: T): T => library;

export const Dialogs = buildDialog({
    MsgBox: ["MsgBox", "common/msgbox"]
} as const)

export const Scenes = {
    Begin: "begin/begin",
    Lobby: "lobby/lobby",
    Batch: "testbatch/testbatch",
    Login: "login/login"
}

type key = keyof typeof Dialogs
export type ResTag = keyof typeof Scenes | (typeof Dialogs)[key][0] | "cache" | "game"
