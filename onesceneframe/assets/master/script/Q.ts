import { _decorator, AssetManager, assetManager, find, Node } from 'cc';
import { Main } from '../../begin/Main';
import { Loading } from '../../begin/Loading';
import { UtilsPanel } from './utils/UtilsPanel';

class Q {
    public obj_empty = Object.create(null);
    public funcEmpty(){}
    public bundle_main: AssetManager.Bundle = null;

    public node_canvas: Node = null;
    public node_dialog: Node = null;
    private node_resi: Node = null;
    private loading: Loading = null;

    public init() {
        this.bundle_main = assetManager.getBundle("master");
        let node_canvas = this.node_canvas = find("Canvas");
        UtilsPanel.getAllNeedCom(this, node_canvas, true);
        this.loading.init();
        return this.node_resi;
    }
    public showLoading(s_time = 0.2, c_time = 10) {
        // console.trace();
        this.loading.node.active = true;
        this.loading.show(s_time, c_time);
    }
    public removeLoading() {
        // console.trace();
        this.loading.node.active = false;
    }
    public addPersistNode(node: Node) {
        // console.log("addPersistNode", node)
        node.setParent(this.node_resi);
        this.loading.node.setSiblingIndex(-1);
    }
    public getPersistNode(nodeName: string): Node {
        return this.node_resi.getChildByName(nodeName);
    }
}

export const q = new Q();
Main.info[0] = [q.init, q];

