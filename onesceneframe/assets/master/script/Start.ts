import { Node } from "cc";
import { Main } from "../../begin/Main";
import { audio_c } from "./common/AudioCommon";
import { audioMgr } from "./mgr/AudioMgr";
import { dialogMgr } from "./mgr/DialogMgr";
import { extraMgr } from "./mgr/ExtraMgr";
import { resMgr } from "./mgr/ResMgr";
import { sceneMgr } from "./mgr/SceneMgr";
import { setMgr } from "./mgr/SetMgr";
import { timeMgr } from "./mgr/TimeMgr";
import { Scenes } from "./Urls";
import { store } from "./mgr/StorageMgr";

function start(this: undefined, node_resi: Node){
    store.init("");
    timeMgr.init();
    resMgr.init();
    // i18nMgr.init();
    setMgr.init();
    audioMgr.init(node_resi);
    extraMgr.init();
    dialogMgr.init();
    audio_c.addBtnSound();
    sceneMgr.loadScene(Scenes.Begin);
}
Main.info[1] = [start, null];