
export class C {
    public static IsTest = false;
    public static LoginUrl = "ws://127.0.0.1:8687";
    public static LoginUrl_Test = "ws://127.0.0.1:8687";

    public static getLoginUrl(): string {
        return this.IsTest ? this.LoginUrl_Test : this.LoginUrl;
    }

}
