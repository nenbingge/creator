import { ProtoTool } from "./ProtoTool";

class ProtoMan{
	//加密 
	// public encrypt_cmd(cmd){
	// 	return cmd;
	// }
	// //解密
	// public decrypt_cmd(str_or_buf){
	// 	return str_or_buf;
	// }

	public decodeJson(cmd_buf: DataView){
		if(cmd_buf.byteLength <= ProtoTool.Header_Size){
			return null;
		}
		let str_info = ProtoTool.readStrByOffset(cmd_buf, ProtoTool.Header_Size);
		let info = null;
		try{
			info = JSON.parse(str_info[0])
		}
		catch(e){
			return null;
		}
		return info;
	}

	public encodeCmd(stype:number, ctype:number, body:Object,  proto_type:number){
		if(!body){
			return ProtoTool.allocEmptyCmd(stype, ctype);
		}
		if(proto_type){
			return ProtoTool.allocStrCmd(stype, ctype, JSON.stringify(body));
		}
		else{
			// let key = get_key(stype , ctype);
			// if(!encoders[key]){
			// 	return null;
			// }
			// buf = encoders[key](stype , ctype , body);
		}
		return null;
	}

	public decodeHeader(cmd_buf: DataView){
		if(cmd_buf.byteLength < ProtoTool.Header_Size){
			return null;
		}
		return ProtoTool.readHeader(cmd_buf);
	}

	public decodeCmd(stype:number, ctype:number, cmd_buf: DataView, proto_type: number){
		if(proto_type){
			return this.decodeJson(cmd_buf);
		}
		if(cmd_buf.byteLength <= ProtoTool.Header_Size){
			return null;
		}
		let cmd = null;
		// let stype = proto_tools.read_int16(cmd_buf , 0);
		// let ctype = proto_tools.read_int16(cmd_buf , 2);
		// let key = get_key(stype , ctype);
		// if(!decoders[key]){
		// 	return null;
		// }
		// cmd = decoders[key](cmd_buf);
		return cmd;
	}

	// let encoders = {};
	// let decoders = {};

	// public get_key(stype , ctype){
	// 	return stype * 65536 + ctype;
	// }

	// public reg_encoder(stype , ctype , encode_func){
	// 	let key = get_key(stype , ctype);
	// 	if(encoders[key]){
	// 		log.warn("encode:" + "stype:" + stype + "ctype:" + ctype + "have reg");
	// 	}
	// 	encoders[key] = encode_func;
	// }

	// public reg_decoder(stype , ctype , decode_func){
	// 	let key = get_key(stype , ctype);
	// 	if(decoders[key]){
	// 		log.warn("decode:" + "stype:" + stype + "ctype:" + ctype + "have reg");
	// 	}
	// 	decoders[key] = decode_func;
	// }

}

export const protoMan = new ProtoMan();