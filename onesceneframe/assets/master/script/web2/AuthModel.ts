import { my_info } from "../data/MyInfo";
import { BaseModel } from "../base/BaseModel";
import { User } from "./Auth";
import { CmdAuth, RspParam, Stype } from "./Cmd";
import { protoMan } from "./ProtoMan";
import { web_conn } from "./WebConn";

class AuthModel extends BaseModel{
    public login(imei: string, func: Function, target: any){
        this.map_callinfo.set(CmdAuth.LOGIN, [func, target]);
        web_conn.sendCmd(Stype.Auth, CmdAuth.LOGIN, {imei});
    }

    public onRecv(ctype:number, buf: DataView){
        let data = protoMan.decodeJson(buf);
        switch(ctype){
            case CmdAuth.LOGIN:
                let info = this.map_callinfo.get(ctype);
                if(info)info[0].call(info[1], data);
                break;
            case CmdAuth.RELOGIN:
                this.onReLogin();
                break;
        }
    }
    private onReLogin(){
        console.log("账号在其他地方登录");
    }

}

export const authModel = new AuthModel();

