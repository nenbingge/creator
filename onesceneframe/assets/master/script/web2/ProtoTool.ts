import { UtilsCommon } from "../utils/UtilsCommon";


const Header_Size = 8;
export class ProtoTool{
	public static Header_Size = Header_Size;
	//原子操作
	public static readInt8(cmd_buf: DataView, offset:number){
		return cmd_buf.getInt8(offset);
	}
	public static writeInt8(cmd_buf: DataView, offset:number , value: number){
		cmd_buf.setInt8(offset, value);
	}
	public static readInt16(cmd_buf: DataView, offset:number){
		return cmd_buf.getInt16(offset, true);
	}
	public static writeInt16(cmd_buf: DataView, offset:number , value: number){
		cmd_buf.setInt16(offset, value, true);
	}
	public static readInt32(cmd_buf: DataView, offset:number){
		return cmd_buf.getInt32(offset, true);
	}
	public static writeInt32(cmd_buf: DataView, offset:number , value: number){
		cmd_buf.getInt32(offset, true);
	}
	public static readUint32(cmd_buf: DataView, offset:number){
		return cmd_buf.getUint32(offset, true);
	}
	public static writeUint32(cmd_buf: DataView, offset:number , value: number){
		cmd_buf.setUint32(offset, value, true);
	}
	public static readStr(cmd_buf: DataView, offset:number , byte_len:number){
		return cmd_buf.getUtf8(offset , byte_len);
	}
	public static writeStr(cmd_buf: DataView, offset:number , str: string){
		cmd_buf.setUtf8(offset , str);
	}
	public static readFloat(cmd_buf: DataView, offset:number){
		return cmd_buf.getFloat32(offset , true);
	}
	public static writeFloat(cmd_buf: DataView, offset:number , value: number){
		cmd_buf.setFloat32(offset , value , true);
	}
	public static allocBuffer(total_len:number){
		return new DataView(new ArrayBuffer(total_len));
	}

	//通用操作
	public static writeHeader(cmd_buf: DataView, stype:number, ctype:number){
		cmd_buf.setInt16(0 , stype , true);
		cmd_buf.setInt16(2 , ctype , true);
		return Header_Size;
	}
	public static readHeader(cmd_buf: DataView){
		let stype = cmd_buf.getInt16(0 , true);
		let ctype = cmd_buf.getInt16(2 , true);
		return [stype, ctype];
	}

	public static readStrByOffset(cmd_buf: DataView, offset:number):[string, number]{
		let byte_len = cmd_buf.getInt16(offset, true);
		offset += 2;
		let str = cmd_buf.getUtf8(offset, byte_len);
		offset += byte_len;
		return [str, offset];
	}
	public static writeStrByOffset(cmd_buf: DataView, offset:number, str:string, byte_len:number){
		cmd_buf.setInt16(offset, byte_len, true);
		offset += 2;
		cmd_buf.setUtf8(offset, str);
		offset += byte_len;
		return offset;
	}

	//常用操作
	public static allocEmptyCmd(stype:number, ctype:number){
		let cmd_buf = new DataView(new ArrayBuffer(Header_Size));
		this.writeHeader(cmd_buf, stype, ctype);
		return cmd_buf
	}

	public static allocStatusCmd(stype:number, ctype:number, status:number){
		let cmd_buf = new DataView(new ArrayBuffer(Header_Size + 2));
		this.writeHeader(cmd_buf, stype, ctype);
		cmd_buf.setInt16(Header_Size, status, true);
		return cmd_buf
	}
	public static decodeStatusCmd(cmd_buf: DataView){
		let stype = cmd_buf.getInt16(0 , true);
		let ctype = cmd_buf.getInt16(2 , true);
		let value = cmd_buf.getInt16(Header_Size, true);
		return [stype, ctype, value];
	}

	public static allocStrCmd(stype:number, ctype:number, str:string){
		let str_len = UtilsCommon.getUtf8Len(str);
		let total_len = Header_Size + str_len + 2;
		let cmd_buf = new DataView(new ArrayBuffer(total_len));
		cmd_buf.setInt16(0 , stype , true);
		cmd_buf.setInt16(2 , ctype , true);
		cmd_buf.setInt16(Header_Size , str_len , true);
		cmd_buf.setUtf8(Header_Size + 2, str)
		return cmd_buf;
	}
	public static decodeStrCmd(cmd_buf: DataView):[number, number, string]{
		let stype = cmd_buf.getInt16(0 , true);
		let ctype = cmd_buf.getInt16(2 , true);
		let len = cmd_buf.getInt16(Header_Size, true);
		let value = cmd_buf.getUtf8(Header_Size + 2, len);
		return [stype, ctype, value];
	}

}