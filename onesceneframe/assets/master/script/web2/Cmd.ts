
export const Stype = {
	Broadcast: 10000,
	User_Disconnect: 10001,
	Auth: 1,
	TalkRoom: 2,
	GameSystem: 3,	//系统服务
	Game5Chess: 4,	//五子棋的游戏服务
};
export const RspParam = {
	OK: 1,
	INVALID_PARAMS: -100,   //用户传递参数错误
	SYSTEM_ERR: -101,	//系统错误
	ILLEGAL_ACCOUNT: -102,	//非法账号
	INVALID_OPT: -103,	//非法操作
	PHONE_IS_REG: -104,	//手机号已被注册
	PHONE_CODE_ERR: -105,	//手机验证码错误
	UNAME_OR_UPWD_ERR: -106,	//用户名或密码错误
	RANK_IS_EMPTY: -107, 	//排行榜为空
	INVALID_ZONE: -109,		//非法得游戏空间
	CHIP_IS_NOT_ENOUGH: -110,	//金币不足
	VIP_IS_NOT_ENOUGH: -111,	//vip不足
}
export const CmdAuth = {
	LOGIN: 1, //登录
	RELOGIN: 2,	//账号在其他地方登录
	EDIT_PROFILE: 3,	//修改用户资料
}

export const QuitReason = {
	UserQuit: 0,
	UserLostConn: 1,
	VipKick: 2,
	SystemKick: 3,
	CHIP_IS_NOT_ENOUGH: 4,
}
var cmd = {
	User_Disconnect: 10000,
	BROADCAST: 10001,
	Auth: {
		GUEST_LOGIN: 1, //游客登录
		RELOGIN: 2,	//账号在其他地方登录
		EDIT_PROFILE: 3,	//修改用户资料
		GUEST_UPGRADE_INDENTIFY: 4,	//游客升级验证码拉取
		BIND_PHONE_NUM: 5,	//游客绑定手机
		UNAME_LOGIN: 6,	//账号密码登录
		GET_PHONE_REG_VERIFY: 7,	//获取手机注册的验证码
		PHONE_REG_ACCOUNT: 8,	//手机注册账号
		GET_FORGET_PWD_VERIFY: 9, // 获取修改密码的手机验证码
		RESET_USER_PWD: 10, // 重置用户密码
	},

	GameSystem: {
		GET_GAME_INFO: 1,
		LOGIN_BONUES_INFO: 2,
		RECV_LOGIN_BUNUES: 3,
		GET_WORLD_RANK_INFO: 4,
	},

	Game5Chess: {
		ENTER_ZONE: 1,
		USER_QUIT: 2,
		ENTER_ROOM: 3,
		EXIT_ROOM: 4,
		SITDOWN: 5,
		STANDUP: 6,
		USER_ARRIVED: 7,
		SEND_PROP: 8,
		SEND_DO_READY: 9,
		ROUND_START: 10,
		TURN_TO_PLAYER: 11,
		PUT_CHESS: 12,
		CHECKOUT: 13,
		CHECKOUT_OVER: 14,
		RECONNECT: 15,
	}
}
