
export type Req_Login = {
    imei: string
}
export type UserCenter = {
    uid: number
    name: string
    sex: number
    face: string
    status: number
}
export type UserGame = {
    exp: number
    gold: number
    vip: number
    vip_end: number
}
export type User = UserCenter & UserGame
