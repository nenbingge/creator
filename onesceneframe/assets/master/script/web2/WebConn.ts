

import { Color_C } from "master/enum/CommonEnum";
import { BaseModel } from "../base/BaseModel";
import { Stype } from "./Cmd";
import { protoMan } from "./ProtoMan";
import { ProtoTool } from "./ProtoTool";

class WebConn{
    private socket: WebSocket = null
    private is_connected = false
    private url = "";
    private map_model: Map<number, BaseModel> = new Map();

    private onclose (e: CloseEvent){
        console.log("connect server close");
        if(this.socket){
            this.socket.close();
            this.socket = null;
        }
    }
    private onmessage (e: MessageEvent){
        let buf = new DataView(e.data);
        let cmd = protoMan.decodeHeader(buf);
        console.log("%cws recv:", Color_C.netrecv, cmd);        
        if(!cmd)return;
        let model = this.map_model.get(cmd[0]);
        if(model)model.onRecv(cmd[1], buf);
    }
    private onopen (e: Event){
        console.log("connect server success");
        this.is_connected = true;
    }

    public connect(url: string) {
        console.log("connent:", url);
        if(this.socket){
            console.warn("已经有socket");
            this.close();
        }
        this.url = url;
        this.socket = new WebSocket(url);
        this.socket.binaryType = "arraybuffer";
        
        this.socket.onopen = this.onopen.bind(this);
        this.socket.onmessage = this.onmessage.bind(this);
        this.socket.onerror = this.socket.onclose = this.onclose.bind(this);
    }
       
    public sendCmd(stype:number, ctype:number, body:any) {
        console.log("%c"+stype +":"+ctype, Color_C.netsend, body);
        if (!this.socket || !this.is_connected) {
            return;
        }
        let buf = protoMan.encodeCmd(stype, ctype, body, 1);
        this.socket.send(buf);
    }
    public sendStr(stype:number, ctype:number, str:string) {
        console.log("%c"+stype +":"+ctype, Color_C.netsend, str);
        if (!this.socket || !this.is_connected) {
            return;
        }
        let buf = ProtoTool.allocStrCmd(stype, ctype, str);
        this.socket.send(buf);
    }
    public close() {
        this.is_connected = false;
        if (this.socket) {
            this.socket.close();
            this.socket = null;
        }
    }
    public addModel(stye:number, model: BaseModel){
        this.map_model.set(stye, model);
    }
    public removeModel(stye:number){
        this.map_model.delete(stye);
    }
    
}

export const web_conn = new WebConn();


