import { _decorator, Component, Tween, Node, tween, sp, AudioClip, Vec3, v3 } from 'cc';
import { resMgr } from '../../mgr/ResMgr';
import { audioMgr } from '../../mgr/AudioMgr';


const { ccclass, property } = _decorator;

const Names = ["dog", "wen", "jidan", "zhadan", "bingtong", "xihongshi", "bixin", "HUA", "jiubei", "qian", "tuoxie", "zhituan"];
@ccclass('Interaction')
export class Interaction extends Component {

    public spine: sp.Skeleton = null;

    protected action_tween: Tween<Node> = null;

    private pool = null;
    private offset_pos = new Vec3();
    private cur_count = 0;

    private ready_spine = null;
    private ready_sound = null;
    public setPool(pool: any) {
        this.pool = pool;
    }

    protected onLoad(): void {
        this.spine = this.node.children[0].getComponent(sp.Skeleton);
    }
    public showAction(index: number, begin_pos: Vec3, to_pos: Vec3, count: number) {
        //TOdo 动画没有了
        this.node.setPosition(begin_pos);
        Vec3.subtract(this.offset_pos, to_pos, begin_pos);
        this.spine.node.active = false;
        this.cur_count = count;
        this.ready_spine = this.ready_sound = null;
        let self = this;
        resMgr.loadWithTag("game", "spine/prop/" + Names[index], sp.SkeletonData, (err: string, spine: any) => {
            if (err) {
                console.log(err);
                return;
            }
            self.ready_spine = spine;
            self.show(index);
        })
        resMgr.loadWithTag("game", "sound/prop/p" + index, AudioClip, (err: string, clip: any) => {
            if (err) {
                console.log(err);
                return;
            }
            self.ready_sound = clip;
            self.show(index);
        })
    }

    public show(index: number) {
        if (this.ready_spine && this.ready_sound) {
            this.spine.node.active = true;
            this.spine.skeletonData = this.ready_spine;
            this.dealAngle(index, this.offset_pos);
            this.playAction1(index);
            if (this.cur_count == 0) {
                audioMgr.playEffect(this.ready_sound);
            }
        }
    }

    public dealAngle(index: number, offset: Vec3) {
        let scale_x = 1, angle = 0;
        if (index == 0 && offset.x < 0) {
            scale_x = -1;
        }
        else if (index == 3) {
            angle = Math.atan(offset.y / offset.x);
            angle = angle * 180 / Math.PI - 90;
            if (offset.x < 0) {
                angle = angle - 180;
            }
        } else if (index == 9) {
            angle = Math.atan(offset.y / offset.x);
            angle = angle * 180 / Math.PI - 90;
            if (offset.x < 0) {
                angle = angle;
            }
        }
        this.node.toScaleX(scale_x);
        this.node.angle = angle;
    }

    public playAction1(index: number) {
        this.spine.setAnimation(0, "A2", true);
        if (this.action_tween)
            this.action_tween.stop();
        if (index == 10) {
            this.action_tween = tween(this.node)
                .byPro(0.7, { x: this.offset_pos.x, y:this.offset_pos.y, angle: -360 })
                .call(this.playAction2.bind(this))
                .start();
        } else if (index == 9) {
            let angle = -this.node.angle;
            if (this.cur_count < 5) {
                let self = this;
                this.action_tween = tween(this.node)
                    .byPro(0.5, { x: this.offset_pos.x, y:this.offset_pos.y, angle: angle })
                    .call(function () {
                        self.spine.skeletonData = null;
                        self.node.parent = null;
                        self.pool.push(self);
                    })
                    .start();
            } else {
                this.action_tween = tween(this.node)
                    .byPro(0.5, { x: this.offset_pos.x, y:this.offset_pos.y, angle: angle })
                    .call(this.playAction2.bind(this))
                    .start();
            }
        } else {
            this.action_tween = tween(this.node)
                .byPro(0.5, { x: this.offset_pos.x, y:this.offset_pos.y })
                .call(this.playAction2.bind(this))
                .start();
        }
    }

    public playAction2() {
        this.node.angle = 0;
        this.node.toScaleXY(1);
        this.spine.setAnimation(0, "A1", false);
        let self = this;
        this.spine.setCompleteListener(function () {
            self.spine.setCompleteListener(null);
            self.spine.skeletonData = null;
            self.node.parent = null;
            // if(self.pool){
            self.pool.push(self);
            // }else{
            //     self.node.destroy();
            // }
        });
    }

}
