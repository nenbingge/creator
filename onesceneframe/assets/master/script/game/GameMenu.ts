import { _decorator, Component, Node, tween, v3, instantiate, Sprite, UITransform } from 'cc';

import { UtilsPanel } from '../utils/UtilsPanel';
import { resMgr } from '../mgr/ResMgr';

const { ccclass, property } = _decorator;

export const GameMenuBtn = {
    Set: "btn_set",
    Quit: "btn_quit",
    Help: "btn_help",
}
export class GameMenuData {
    public target: any;
    public btns: Array<string>;
    public funcs: Array<Function>;
    constructor(target: any) {
        this.target = target;
    }
}

@ccclass('GameMenu')
export class GameMenu extends Component {

    private node_menu: Node = null;
    private node_bg: Node = null;
    private node_btn: Node = null;

    //components
    private node_mask: Node = null;

    private state: number = 1;
    private tween = null;
    private data: GameMenuData = null;
    private btnArr = []


    public onLoad() {
        UtilsPanel.getAllNeedCom(this, this.node, true);
        this.node_menu.active = false;
        this.node_menu.toScaleY(0.1);
    }
    public init(data: GameMenuData) {
        this.data = data;
        let len = data.btns.length;
        if (len < 1) return;
        let arr = this.btnArr//new Array(len);
        arr[0] = this.node_btn;
        for (let i = 1; i < len; i++) {
            let node_btn = instantiate(this.node_btn);
            node_btn.setParent(this.node_menu);
            arr[i] = node_btn;
        }
        for (let i = 0; i < len; i++) {
            UtilsPanel.addBtnEvent2(arr[i], "onClick", this, i.toString());
            arr[i].getComponent(Sprite).spriteFrame = resMgr.getSfByAtlas("gamecommon", data.btns[i]);
            arr[i].toY(-40 - i * 60);
        }
        this.node_bg.getComponent(UITransform).height = 15 + len * 60;
    }
    public onMoreClick() {
        if (this.tween) this.tween.stop();
        if (this.state == 1) {
            this.state = 2;
            this.node_menu.active = true;
            this.node_bg.active = true;
            this.tween = tween(this.node_menu)
                .toPro(0.2, { scaleY: 1 }, {
                    onComplete: () => { this.tween = null; }
                })
                .start();
        }
        else if (this.state == 2) {
            this.state = 1;
            this.tween = tween(this.node_menu)
                .toPro(0.2, { scaleY: 0.1 }, {
                    onComplete: () => {
                        this.node_menu.active = false;
                        this.node_bg.active = false;
                        this.tween = null;
                    }
                })
                .start();
        }
        if (this.node_mask) this.node_mask.active = this.state == 2;
    }
    public setMask(node: Node) {
        this.node_mask = node;
    }
    public close() {
        if (this.state == 2) this.onMoreClick();
    }
    public onClick(e: any, i: string) {
        this.close();
        if (!this.data) return;
        let index = parseInt(i);
        if (this.data.funcs[index]) this.data.funcs[index].call(this.data.target);
    }

    public showItem(index, show) {
        this.btnArr[index].active = show
        let count=0
        this.btnArr.forEach(n=>{
            count+=n.active?1:0
        })
        //node[0].destroy()
        this.node_bg.getComponent(UITransform).height = 15 + count * 60;

    }
}
