import { _decorator, Component, AudioClip, isValid } from "cc";
import { audioMgr } from "../mgr/AudioMgr";
import { audio_c } from "../common/AudioCommon";
import { resMgr } from "../mgr/ResMgr";


const { ccclass, property } = _decorator;

@ccclass("GameSound")
export class GameSound extends Component {

    public static instance: GameSound = null;

    @property([AudioClip])
    public sounds: AudioClip[] = [];

    public sounds_extra = [];

    private lobby_click = null;

    public playMusic(index: number = 0) {
        if(isValid(this.sounds[index]))audioMgr.playMusic(this.sounds[index], true);
    }
    public playSound(index: number, loop = false, use_extra = false) {
        let sounds = use_extra ? this.sounds_extra : this.sounds;
        if(!isValid(sounds[index]))return;
        console.log(`effect:${index}::${use_extra}`)
        audioMgr.playEffect(sounds[index], loop);
    }
    public playSoundWithFunc(index: number, func: Function, use_extra: boolean){
        let sounds = use_extra ? this.sounds_extra : this.sounds;
        if(!isValid(sounds[index]))return;
        audioMgr.playEffectWithFunc(sounds[index], func);
    }
    public playStopSound(index:number){
        let sound = this.sounds[index];
        if(!isValid(sound))return -1;
        audioMgr.playEffect(sound, false, true);
    }
    public stopSound(index:number){
        audioMgr.stopSound(this.sounds[index]);
    }
    public setGameClick(index: number) {
        this.lobby_click = audio_c.changeBtnClip(this.sounds[index]);
    }
    public loadGameClick(path:string){
        resMgr.gameLoad("gamesound",path,AudioClip,(err,clip:AudioClip)=>{
            if(!isValid(this) || err)return;
            this.lobby_click = audio_c.changeBtnClip(clip);
        })
    }
    public loadMusic( path:string){
        resMgr.gameLoad("gamesound",path,AudioClip,(err,clip:AudioClip)=>{
            if(!isValid(this) || err)return;
            audioMgr.playMusic(clip, true);
        })
    }
    public loadSounds(prefix:string, paths:Array<string>){
        this.sounds.length = paths.length;
        for(let i = 0 ; i < paths.length ; i++){
            if(isValid(this.sounds[i]) || !paths[i])continue;
            resMgr.gameLoad("gamesound",prefix+paths[i],AudioClip,(err,clip:AudioClip)=>{
                if(!isValid(this) || err)return;
                this.sounds[i] = clip;
            })
        }
    }
    public loadSoundsExtra(prefix:string, paths:Array<string>){
        this.sounds_extra.length = paths.length;
        for(let i = 0 ; i < paths.length ; i++){
            if(!paths[i]?.length)continue;
            resMgr.gameLoad("gamesound",prefix+paths[i],AudioClip,(err,clip:AudioClip)=>{
                if(!isValid(this))return;
                if(err){
                    console.log(err)
                    return;
                }
                this.sounds_extra[i] = clip;
            })
        }
    }
    public getSound(index:number, use_extra: boolean){
        let sounds = use_extra ? this.sounds_extra : this.sounds;
        return sounds[index];
    }

    public onLoad() {
        GameSound.instance = this;
    }
    public onDestroy() {
        if (this.lobby_click)audio_c.changeBtnClip(this.lobby_click);
        resMgr.gameRealease("gamesound");
        GameSound.instance = null;
    }

}
