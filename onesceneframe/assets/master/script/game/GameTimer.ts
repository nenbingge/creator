

export class GameTimer {
    public is_running = false;

    private interval = 0;
    private elapsed = 0;
    private func: Function = null;
    private target = null;

    public init(interval: number, func: Function, target:any) {
        this.is_running = true;
        this.interval = interval;
        this.elapsed = 0;
        this.func = func;
        this.target = target;
    }

    public add(time: number) {
        if (!this.is_running) return;
        this.elapsed += time;
        while (this.elapsed > this.interval) {
            this.func.call(this.target);
            this.elapsed -= this.interval;
        }
    }

    public stop(force: boolean, func: Function) {
        if (force || this.func == func) {
            this.elapsed = 0
            this.is_running = false;
        }
    }
}
