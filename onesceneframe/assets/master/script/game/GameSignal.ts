import { _decorator, Component, Sprite, Label, Color, isValid, SpriteFrame, UIRenderer } from 'cc';
import { resMgr } from '../mgr/ResMgr';

const { ccclass, property } = _decorator;

@ccclass('GameSignal')
export class GameSignal extends Component {
    public sp_sm: Sprite = null;
    public lb_sm: Label = null;

    private cur_index = -1;

    public onLoad() {
        this.sp_sm = this.node.getChildByName("sp_signal").getComponent(Sprite);
        this.lb_sm = this.node.getChildByName("lb_ms").getComponent(Label);
    }

    public start() {
        // if (GameFrame.getInstance().getMyNetLag() > 0) {
        //     this.showSigned(GameFrame.getInstance().getMyNetLag());
        // }
        // else {
        //     this.node.active = false;
        // }
    }

    public showSigned(signal: number) {
        if (signal > 999) signal = 999;
        else signal = Math.floor(signal);
        this.lb_sm.string = signal + "ms";
        let index = 0;
        if (signal > 300) index = 2;
        else if (signal > 100) index = 1;
        if (index != this.cur_index) {
            this.cur_index = index;
            if (index == 0) {
                this.lb_sm.node.getComponent(UIRenderer).color = Color.GREEN;
            } else if (index == 1) {
                this.lb_sm.node.getComponent(UIRenderer).color = Color.YELLOW;
            } else {
                this.lb_sm.node.getComponent(UIRenderer).color = Color.RED;
            }
            resMgr.load("game", "texture/game/ui/signal" + index + "/spriteFrame", SpriteFrame, (err: string, sp: any) => {
                if (err) {
                    console.log(err);
                    return;
                }
                if (isValid(this.sp_sm)) {
                    this.sp_sm.spriteFrame = sp;
                }
            })
        }
    }

    // update (dt) {}
}
