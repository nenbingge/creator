
import { _decorator, ScrollView, Prefab, instantiate, Size, Node, UITransform, Vec3, NodeEventType } from "cc";
import { ViewItemPro } from "./ViewItemPro"

const { ccclass, property } = _decorator;
const EPSILON = 1e-4;

class ViewItemInfo {
    public node: Node;
    public ui_tran: UITransform;
    public itemComponent: any;
    public type: number;

    constructor(type: number, node: Node) {
        this.node = node;
        this.type = type;
        this.itemComponent = node.getComponent(ViewItemPro);
        this.ui_tran = node.getComponent(UITransform);
    }
}

const _tempVec3 = new Vec3();

@ccclass
export class ChatView extends ScrollView {
    @property([Prefab])
    public pres_item: Prefab[] = [];
    @property
    public align_bottom = true;

    private uitrans: UITransform = null;

    private cache_pools: Array<Array<ViewItemInfo>> = [];
    private begin_index: number = 0;
    private end_index: number = 0;
    private item_list: Array<ViewItemInfo> = [];//所有添加到场景的节点都放这里
    private data: Array<any> = [];
    private p: any = null;
    private func2type: Function = null;

    // private state_flag:number = DO_NO;
    private bottom = 0;

    protected onLoad() {
        this.uitrans = this._content.getComponent(UITransform);
        this.refreshSize();
        this.bottom = -this.uitrans.anchorY * this.uitrans.height;
        this._calculateBoundary();//计算边界值，并保存到变量，以view的尺寸作为边界
        for (let i = 0, len = this.pres_item.length; i < len; i++) {
            this.cache_pools.push([]);
        }
        this.node.on(NodeEventType.SIZE_CHANGED, this.refreshSize, this);
    }
    private refreshSize() {
        this.uitrans.setContentSize(this.node.getComponent(UITransform).contentSize);
    }

    public setTypeFunc(func: Function) {
        this.func2type = func;
    }

    private getItemByType(type: number) {
        if (type < 0 || type >= this.cache_pools.length) return null;
        let pool = this.cache_pools[type];
        if(pool.length > 0){
            let item = pool.pop();
            item.node.active = true;
            return item;
        }
        let node = instantiate(this.pres_item[type]);
        let item = new ViewItemInfo(type, node);
        node.setParent(this.content);
        // var self = this;
        // node.on(Node.EventType.SIZE_CHANGED, () => {
        //     self._onItemSizeChange(item._data_index);
        // });
        return item;
    }
    private pushItemByType(item: ViewItemInfo) {
        item.node.active = false;
        this.cache_pools[item.type].push(item);
    }
    private geneOneNode(index: number) {
        let type = this.func2type ? this.func2type(this.data[index]) : 0;
        let item = this.getItemByType(type);
        item.itemComponent.reset();
        item.itemComponent.init(this.data[index], index, this.p);
        return item;
    }
    private recycleItems() {
        for (let i = 0; i < this.item_list.length; i++) {
            this.pushItemByType(this.item_list[i]);
        }
        this.item_list = [];
    }
    public getChildInfoByIndex(index: number) {
        if (index >= this.begin_index && index <= this.end_index) {
            var new_index = index - this.begin_index;
            return this.item_list[new_index];
        }
        return null;
    }
    public initData(data: Array<any>, p: any = null) {
        this.recycleItems();
        this.data = data;
        this.p = p;
        if(!data || !data.length)return;
        this.begin_index = this.end_index = data.length - 1;
        this.initItems();
        if(!this.align_bottom)this.align2Top();
    }
    public changeData(data: any, p:any = null) {
        if (this.end_index < this.data.length - 1) {
            this.data = data;
        } else {
            this.initData(data, p);
        }
    }
    private initItems() {
        this.bottom = -this.uitrans.anchorY * this.uitrans.height;
        this.content.toY(this._topBoundary + this.bottom * (1 - this.uitrans.anchorY));

        let total_height = 0;
        let is_full = false;
        let begin_y = this.bottom;
        //从下往上生成
        let item = null, ui_tran = null;
        for (let i = this.data.length - 1; i >= 0; i--) {
            item = this.geneOneNode(i);
            ui_tran = item.ui_tran;
            this.item_list.unshift(item);//所有实例化的节点都放在item_list，后面的消息排后面。
            // item._data_index = i;
            item.node.toY(begin_y + ui_tran.height * ui_tran.anchorY);
            begin_y += ui_tran.height;
            if (is_full) break;
            total_height += ui_tran.height;
            this.begin_index--;
            if (total_height > this.uitrans.height) {
                is_full = true;
            }
        }
    }
    private align2Top() {
        let ui_tran = this.item_list[0].ui_tran;
        let y = this.content.position.y + ui_tran.node.position.y + ui_tran.height * (1 - ui_tran.anchorY);
        let offset = this._topBoundary - y;
        if(offset > 0){
            this.item_list.forEach((item: ViewItemInfo) => {
                item.node.addY(offset);
            })
        }
    }
    // private _onItemSizeChange(data_index:number){
    //     if(this.state != 1)return;
    //     let index = 0;
    //     for (let i = 0; i < this.item_list.length; ++i) {
    //         if (this.item_list[i]._data_index == data_index) {
    //             //找到元素在list孩子的索引
    //             index = i;
    //             break
    //         }
    //     }

    //     if(index > 0){
    //         index--;
    //     }
    //     let node = this.item_list[index].node;
    //     let empty_y = node.y - node.height * node.anchorY;
    //     for(let i = index + 1 ; i < this.item_list.length ; i++){
    //         node = this.item_list[i].node;
    //         node.y = empty_y - node.height * (1 - node.anchorY);
    //         empty_y -= node.height;
    //     }

    //     // this._quickAllayOuterBoundary();
    // }

    protected _moveContent(deltaMove: Vec3, canStartBounceBack: boolean) {

        let adjustedMove = this._flattenVectorByDirection(deltaMove);

        Vec3.add(_tempVec3, this._content.position, adjustedMove);
        //@ts-ignore
        this._setContentPosition(_tempVec3);

        if (Math.abs(adjustedMove.y) > EPSILON) {
            this._checkNeedRefresh(adjustedMove);
        }
        if (this.elastic && canStartBounceBack) {
            this._startBounceBackIfNeeded();
        }
    }
    private _checkNeedRefresh(offset: Vec3) {
        let list = this.item_list;
        if(list.length < 3)return;
        let content_y = this.content.position.y;
        if (offset.y > 0 && this.end_index < this.data.length - 1) {
            if (list[1].node.position.y + content_y > this._topBoundary) {
                //回收顶部元素
                let item = list.shift();
                this.pushItemByType(item);
                this.begin_index++;
            }
            let len = list.length;
            //倒数第二个进入了view的底部，就创建后面元素
            if (list[len - 2].node.position.y + content_y > this._bottomBoundary) {
                this.end_index++;
                let new_item = this.geneOneNode(this.end_index);
                let end_tran = list[len - 1].ui_tran;
                let y = end_tran.node.position.y - end_tran.height * end_tran.anchorY - new_item.ui_tran.height * (1 - new_item.ui_tran.anchorY);
                new_item.node.toY(y);
                list.push(new_item);
                if(this.end_index == this.data.length - 1){
                    this.bottom = y - new_item.ui_tran.height * new_item.ui_tran.anchorY;
                }
            }
        }
        else if (offset.y < 0 && this.begin_index > 0) {
            //向下移动，并且上面还有元素
            if (list[1].node.position.y + content_y < this._topBoundary) {
                this.begin_index--;
                let item = this.geneOneNode(this.begin_index);
                let first_uitran = list[0].ui_tran;
                let y = first_uitran.node.position.y + first_uitran.height * (1 - first_uitran.anchorY) + item.ui_tran.height * item.ui_tran.anchorY;
                item.node.toY(y);
                this.item_list.unshift(item);
            }
            let len = this.item_list.length;
            if (list[len - 2].node.position.y + content_y < this._bottomBoundary) {
                var item = this.item_list.pop();
                this.pushItemByType(item);
                this.end_index--;
            }
        }
    }

    public clear() {
        this.stopAutoScroll()
        this.content.toY(0);
        this.initData([])
    }

    protected _getContentTopBoundary() {
        let height = this.uitrans.height;
        height += this._getContentBottomBoundary();
        if (this.item_list.length > 0) {
            let ui_tran = this.item_list[0].ui_tran;
            let y = this.content.position.y + ui_tran.node.position.y + ui_tran.height * (1 - ui_tran.anchorY);
            if(y > height)height = y;
        }
        return height;
    }
    protected _getContentBottomBoundary() {
        return this.content.position.y + this.bottom;
    }

    protected _moveContentToTopLeft(viewSize: Size) {
        // if(this.index_arr.length == 0){
        //     ScrollView.prototype._moveContentToTopLeft.call(this,viewSize);
        // }
    }

}
