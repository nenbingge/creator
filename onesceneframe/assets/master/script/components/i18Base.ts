
import { _decorator, Component } from 'cc';
import { EDITOR } from 'cc/env';
import { i18nMgr } from '../mgr/i18nMgr';
const { ccclass, property } = _decorator;

export abstract class i18Base extends Component {
    protected i18_type = -1;

    @property({visible: false})
    protected i18n_index: string = "";
    @property({serializable: true})
    protected _preview = false;

    @property
    public bundle = "";

    @property
    get index() {
        return this.i18n_index
    }
    set index(value:string) {
        if(!value || this.i18n_index == value)return;
        this.i18n_index = "" + value;
        if(EDITOR)this.refreshInEditor();
        else this.refresh()
    }
    
    @property
    get preview(){
        return this._preview
    }
    set preview(value:boolean){
        if(this._preview == value)return;
        this._preview = value;
        this.refreshInEditor();
    }
    
    onEnable(){
        if(this.i18_type == -1)return;
        if(EDITOR){
            this.refreshInEditor();
        }
        else{
            i18nMgr.addCom(this.bundle, this.i18_type, this);
            this.refresh();
        }
    }
    onDisable(){
        if(this.i18_type == -1)return;
        if(!EDITOR)i18nMgr.removeCom(this.bundle, this.i18_type, this);
    }
    public abstract refresh(): void;
    public abstract refreshInEditor():void;
}
