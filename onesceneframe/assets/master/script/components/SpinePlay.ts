
import { _decorator, Component, Node, sp } from 'cc';
const { ccclass } = _decorator;

@ccclass('SpinePlay')
export class SpinePlay extends Component {

    private spine: sp.Skeleton = null;
    public ani_name = "";

    protected onLoad(): void {
        this.spine = this.node.getComponent(sp.Skeleton);
    }

    protected onEnable(): void {
        if(this.ani_name.length > 0 && this.spine){
            this.spine.setAnimation(0, this.ani_name, true);
        }
    }
    public play(ani:string){
        this.ani_name = ani;
        this.spine && this.spine.setAnimation(0, ani, true);
    }
}
