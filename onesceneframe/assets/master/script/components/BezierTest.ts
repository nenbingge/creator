
import { _decorator, Component, Graphics, instantiate, Node, NodeEventType } from 'cc';
import { GameConst } from '../game/GameConst';
const { ccclass, executeInEditMode, disallowMultiple, property } = _decorator;

/*
重置组件：节点上有Graphics:删掉Graphics，并把子节点删了，只剩第一个节点
            没Graphics:添加Graphics,并用第一个子节点为模板生成3个子节点
拖动任意节点即可显示对应的 Bezier 曲线
*/
@ccclass('BezierTest')
@executeInEditMode
@disallowMultiple
export class BezierTest extends Component {

    private _show_point = false;
    @property
    get show_point(){
        return this._show_point;
    }
    set show_point(v:boolean){
        this._show_point = v;
        if(!this.node_c1)return;
        let pos = this.node_begin.position;
        console.log(`begin:  [${Math.floor(pos.x)},${Math.floor(pos.y)}]`)
        pos = this.node_end.position;
        console.log(`end:  [${Math.floor(pos.x)},${Math.floor(pos.y)}]`)
        let pos1 = this.node_c1.position;
        let pos2 = this.node_c2.position;
        console.log(`参数:  [${Math.floor(pos1.x)},${Math.floor(pos1.y)},${Math.floor(pos2.x)},${Math.floor(pos2.y)}]`);
    }
    
    private grap: Graphics = null;
    private node_begin: Node = null;
    private node_end: Node = null;
    private node_c1: Node = null;
    private node_c2: Node = null;

    protected onLoad(): void {
        let children = this.node.children;
        if(children.length > 3){
            this.grap = this.node.getComponent(Graphics);
            this.node_begin = children[0];
            this.node_end = children[1];
            this.node_c1 = children[2];
            this.node_c2 = children[3];
            this.draw(Node.TransformBit.POSITION);
        }
    }
    public resetInEditor(){
        this.grap = this.node.getComponent(Graphics);
        if(this.grap){
            this.grap.destroy();
            let children = this.node.children;
            for(let i = children.length - 1 ; i > 0 ; i--)this.node.removeChild(children[i]);
        }
        else{
            this.grap = this.node.addComponent(Graphics);
            this.grap.lineWidth = 5;
            this.grap.strokeColor.fromHEX('#ff0000');
            let children = this.node.children;
            if(children.length == 0){
                let node = new Node("begin");
                node.setParent(this.node);
            }
            let nodes = GameConst.geneNodes(this.node.children[0], 4);
            let names = ["begin", "end", "c1", "c2"];
            let pos = [-200, 200, -100, 100];
            for(let i = 0 ; i < 4 ; i++){
                nodes[i].name = names[i];
                nodes[i].on(NodeEventType.TRANSFORM_CHANGED, this.draw, this);
                let y = i > 1 ? 60 : 0;
                nodes[i].setPosition(pos[i], y, 0);
            }
            this.node_begin = nodes[0];
            this.node_end = nodes[1];
            this.node_c1 = nodes[2];
            this.node_c2 = nodes[3];
            this.draw(Node.TransformBit.POSITION);
        }
    }
    private draw(bit:number){
        if(!this.node_c2)return;
        if(bit != Node.TransformBit.POSITION)return;
        this.grap.clear();
        let pos = this.node_end.position;
        this.grap.moveTo(pos.x, pos.y);
        this.grap.stroke();
        pos = this.node_begin.position;
        this.grap.lineTo(pos.x, pos.y);
        pos = this.node_end.position;
        let c1 = this.node_c1.position;
        let c2 = this.node_c2.position;
        this.grap.bezierCurveTo(c1.x,c1.y,c2.x,c2.y,pos.x,pos.y);
        this.grap.close();
        this.grap.stroke();
    }

}
