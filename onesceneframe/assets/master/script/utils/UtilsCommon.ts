import { _decorator, Node, Button, Component, EventHandler, tween, js } from 'cc';

export class UtilsCommon {

    public static deepClone(obj: any): any {
        if(typeof obj == "object"){
            if(Array.isArray(obj)){
                let arr = new Array(obj.length)
                for(let i = 0; i < obj.length; i++)arr[i] = typeof obj[i] === 'object' ? this.deepClone(obj[i]) : obj[i];
                return arr;
            }
            let o = {};
            for (let k in obj)o[k] = typeof obj[k] === 'object' ? this.deepClone(obj[k]) : obj[k];
            return o;
        }
        return obj;
    }
    public static createEventHandler(node: Node, com: Component, func_name: string, data: string | number) {
        let handler = new Component.EventHandler();
        handler.target = node;
        handler.component = js.getClassName(com);
        handler.handler = func_name;
        //@ts-ignore
        handler.customEventData = data;
        return handler;
    }
    public static getUtf8Len(str: string) {
        let total = 0;
        let code;
        for (let i = 0, l = str.length; i < l; i++) {
            code = str.charCodeAt(i);
            if (code < 0x007f) {
                total ++
            } 
            else if ((0x0080 <= code) && (code <= 0x07ff)) {
                total += 2;
            } 
            else if ((0x0800 <= code) && (code <= 0xffff)) {
                total += 3;
            }
        }
        return total; 
    }
}

