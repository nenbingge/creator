
export class DateHelper{

    public static isToday(timestamp:number) {
        let date = new Date(timestamp);
        let cur = new Date();
        return date.getFullYear() == cur.getFullYear() 
            && date.getMonth() == cur.getMonth() 
            && date.getDate() == cur.getDate();
    }
}