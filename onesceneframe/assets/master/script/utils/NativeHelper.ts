import { native, sys } from "cc";
import { eventMgr } from "../mgr/EventMgr";
import { JSB } from "cc/env";
import { UtilsCommon } from "./UtilsCommon";
import { store } from "../mgr/StorageMgr";

if(JSB){
    native.bridge.onNative = (arg0:string, arg1: string):void=>{
        // eventMgr.emit(arg0, arg1);
    }
}

export class NativeHelper{
    // native.bridge.sendToNative('open_ad', defaultAdUrl);
    public static getImei(){
        // if(JSB)
        let imei = store.getStr("imei");
        if(imei)return imei;
        imei = Math.floor(Math.random() * 100000) + "" + new Date().getTime();
        store.setStr("imei", imei);
        return imei;
    }
}


