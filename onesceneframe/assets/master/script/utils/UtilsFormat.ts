import { Label, tween, Tween, UITransform, Vec3, _decorator } from 'cc';

const limit = [1000000000000, 1000000000, 1000000, 1000];
const chars = ["T", "B", "M", "K"];
const Time_Desc = ["d","h","m","s"];

export class UtilsFormat {
    public static toThousands(v: number) {
        let prefix = "";
        if (v < 0) {
            prefix = "-";
            v = -v;
        }
        let s = "" + v;
        let p = s.indexOf(".");
        let i = p >= 0 ? p : s.length;
        let str = "";
        while (i > 0) {
            if (!!str) {
                str = "," + str;
            }
            str = s.substring(i > 3 ? i - 3 : 0, i) + str;
            i -= 3;
        }
        if (p >= 0) {
            str = str + s.substring(p, s.length);
        }
        return !!prefix ? prefix + str : str;
    }

    public static getShortName(name: string, num: number = 10) {
        return name.length > num ? name.slice(0, num) + '...' : name;
    }

    public static formatRate(rate: number) {
        return Math.floor(rate * 10000) / 100 + "%";
    }

    public static formatMoney(num: number, limit:number) {
        //避免低于10000的金额被格式化
        if (limit > 0 && Math.abs(num) < limit) {
            return "" + num;
        }
        num = Math.floor(num / 1000) * 1000;
        let is_minus = num < 0;
        if (is_minus) {
            num = -num;
        }
        for (let i = 0; i < 4; i++) {
            if (num >= limit[i]) {
                num = (num * 1) / limit[i];
                let remain = num % 1;
                let str = "";
                if (remain > 0) {
                    str = remain >= 0.01 ? num.toFixed(2) : Math.floor(num) + "";
                } else {
                    str += num;
                }
                return is_minus ? "-" + str + chars[i] : str + chars[i];
            }
        }
        return is_minus ? "-" + this.toThousands(num) : this.toThousands(num);
    }

    public static addZero(n:number) {
        if (n < 10) return "0" + n;
        return "" + n;
    }
    // n  y  r  s  f  m
    // 年 月 日 时 分 秒
    public static getDateStr(date: Date, format_str:string) {
        let str = "";
        let temp = "";
        for (let i = 0, len = format_str.length; i < len; i++) {
            temp = "";
            switch (format_str[i]) {
                case "n":
                    temp = date.getFullYear() + "";
                    temp = temp.slice(2, 4);
                    break;
                case "N":
                    temp = date.getFullYear() + "";
                    break;
                case "Y":
                case "y":
                    temp = this.addZero(date.getMonth() + 1);
                    break;
                case "R":
                case "r":
                    temp = this.addZero(date.getDate());
                    break;
                case "S":
                case "s":
                    temp = this.addZero(date.getHours());
                    break;
                case "F":
                case "f":
                    temp = this.addZero(date.getMinutes());
                    break;
                case "M":
                case "m":
                    temp = this.addZero(date.getSeconds());
                    break;
            }
            if (temp != "") {
                str = str + temp;
            } else {
                str = str + format_str[i];
            }
        }
        return str;
    }
    public static getCountdown(s: number, strs: Array<string>) {
        if (!strs) strs = ["d","h","m"];
        if (s > 86400) {
            let all_hour = Math.floor(s / 3600);
            let hour = all_hour % 24;
            let day = (all_hour - hour) / 24;
            return `${day}${strs[0]}:${hour}${strs[1]}`;
        } else {
            let all_minute = Math.floor(s / 60);
            let minute = all_minute % 60;
            let hour = (all_minute - minute) / 60;
            return `${hour}${strs[1]}:${minute}${strs[2]}`;
        }
    }
    public static getCountdown2(s:number, format_str:string, desc:Array<string>, limit:number){
        let str = "";
        let suffix = "";
        if(!desc)desc = Time_Desc;
        let count = 0;
        for (let i = 0, len = format_str.length; i < len; i++) {
            switch (format_str[i]) {
                case "R":
                case "r":
                    count = Math.floor(s / 86400);
                    if(count > 0){
                        if(format_str[i] == "R")suffix += desc[0] + count;
                        else str += (count + desc[0]);
                        s -= count * 86400;
                        limit--;
                    }
                    break;
                case "S":
                case "s":
                    count = Math.floor(s / 3600);
                    if(count > 0){
                        str += (count + desc[1]);
                        s -= count * 3600;
                        limit--;
                    }
                    break;
                case "F":
                case "f":
                    count = Math.floor(s / 60);
                    str += (count + desc[2]);
                    s -= count * 60;
                    limit--;
                    break;
                case "M":
                case "m":
                    str += (s + desc[3]);
                    limit--;
                    break;
            }
            if(limit == 0)break;
        }
        return str + suffix;
    }

    public static firstUpper(str: string) {
        return str[0].toUpperCase() + str.slice(1);
    }
}
