
import { _decorator, SpriteFrame, Node, Sprite, Button, Label, Toggle, EditBox, RichText, sp, isValid, Prefab, instantiate, assetManager, UITransform, UIOpacity, ImageAsset, Component, js, builtinResMgr, UIRenderer, Material, ProgressBar, Layers, view, NodeEventType, Color, tween, Tween } from 'cc';
import { UtilsFormat } from './UtilsFormat'
import { i18nLabel } from '../components/i18nLabel';
import { i18nSkeleton } from '../components/i18nSkeleton';
import { Generate } from '../components/Generate';
import { BUILD } from 'cc/env';
import { SpinePlay } from '../components/SpinePlay';
import { resMgr } from '../mgr/ResMgr';
import { UtilsCommon } from './UtilsCommon';
import { ResTag } from '../Urls';

export class UtilsPanel {
    public static showNodeByIndex(arr: Array<Node>, index: number) {
        arr.forEach((node, i) => {node.active = i == index;})
    }
    //#region set com
    public static setItemIcon(flag: ResTag, url: string, sp_icon: Sprite) {
        resMgr.loadImg(flag, url, (err: string, sf: SpriteFrame) => {
            if (err || sp_icon.isValid) return;
            sp_icon.spriteFrame = sf;
        })
    }
    public static setRemoteImg(url: string, sp_icon: Sprite) {
        if (!BUILD) return;
        assetManager.loadRemote(url, { maxRetryCount: 0, ext: '.png' }, function (err, tex: ImageAsset) {
            if (err || !isValid(sp_icon)) return;
            sp_icon.spriteFrame = SpriteFrame.createWithImage(tex);
        });
    }
    public static limitNode(node: Node, width: number, height: number) {
        if (width == 0 || height == 0) return;
        let uitrans = node.getComponent(UITransform);
        let min: any = Math.min(width / uitrans.width, height / uitrans.height);
        node.toScaleXY(min);
        return min
    }
    //#region spine
    public static geneSpineNode() {
        let new_node = new Node("spine");
        let spine = new_node.addComponent(sp.Skeleton);
        spine.premultipliedAlpha = false;
        return spine;
    }
    public static addSpinePlay(node: Node, name: string) {
        let spine_play = node.getComponent(SpinePlay);
        if (!spine_play) spine_play = node.addComponent(SpinePlay);
        if (spine_play.ani_name != name) spine_play.play(name);
    }
    public static autoHideSpine(spine: sp.Skeleton) {
        spine.setCompleteListener(function () {
            spine.node.active = false;
        })
    }

    //#region get com
    public static getAllNeedCom(sc: any, node: Node, recursive: boolean = false) {
        SC = sc;
        let children = node.children;
        if (recursive) {
            for (let i = 0, len = children.length; i < len; i++) {
                children[i].walk(getComByName);
            }
        }
        else {
            for (let i = 0, len = children.length; i < len; i++) {
                getComByName(children[i]);
            }
        }
    }
    public static getCom(node: Node, path: string[], type = null): any {
        for (let i = 0, len = path.length; i < len; i++) {
            node = node.getChildByName(path[i]);
        }
        return type ? node.getComponent(type) : node;
    }

    public static getArrCom(node: Node, name: string, count: number, type = null) {
        let arr = new Array(count);
        let children = node.children;
        let cur_connt = 0;
        for (let i = 0; i < children.length; i++) {
            if (children[i].name.startsWith(name)) {
                cur_connt++;
                let index = parseInt(children[i].name.slice(name.length));
                arr[index] = type ? children[i].getComponent(type) : children[i];
                if (cur_connt >= count) break;
            }
        }
        return arr;
    }
    public static getChildMap(node: Node) {
        let map = new Map<string, Node>()
        let children = node.children;
        for (let i = 0; i < children.length; i++) {
            map.set(children[i].name, children[i]);
        }
        return map;
    }
    //#region btn
    public static addBtnEvent(btn_node: Node, func: Function, target?: any) {
        let btn = btn_node.getComponent(Button);
        if (!btn) {
            btn = btn_node.addComponent(Button);
            btn.transition = Button.Transition.SCALE;
            btn.target = btn_node;
            btn.zoomScale = 0.9;
            btn.duration = 0.1;
        }
        btn_node.on("click", func, target);
        return btn;
    }
    public static addBtnEvent2(btn_node: Node, func: string, target: any, param: string | number) {
        let btn = btn_node.getComponent(Button);
        if (!btn) {
            btn = btn_node.addComponent(Button);
            btn.transition = Button.Transition.SCALE;
            btn.target = btn_node;
            btn.zoomScale = 0.9;
            btn.duration = 0.1;
        }
        let handler = UtilsCommon.createEventHandler(btn_node, target, func, param);
        btn.clickEvents.push(handler);
        return btn;
    }
    public static clearBtnEvent(node: Node, remove_btn: boolean) {
        node.off("click");
        let btn = node.getComponent(Button);
        if (btn) {
            btn.clickEvents.length = 0;
            if (remove_btn) btn.destroy();
        }
    }
    public static setNodeGray(node_or_btn: Node | Button, state: boolean, recursive = false) {
        let node = node_or_btn;
        let btn = null;
        if (node_or_btn instanceof Button) {
            btn = node_or_btn;
            node = node_or_btn.node;
        }
        else {
            btn = (node as Node).getComponent(Button);
        }
        if (btn) btn.interactable = state;
        let mat = state ? null : builtinResMgr.get("ui-sprite-gray-material");
        let func = function (node0: any) {
            let btn = node0.getComponent(Button)
            btn && (btn.interactable = state);
            let com_rendener = node0.getComponent(UIRenderer);
            if (com_rendener && !com_rendener.setAnimation) com_rendener.customMaterial = mat;
        }
        if (recursive) {
            (node as Node).walk(func);
        }
        else {
            func(node);
        }
    }
}

var SC = null;
function getComByName(node: Node) {
    let name = node.name;
    let com = null;
    switch (name[0]) {
        case "a":
            if (name.startsWith("auto_btn_")) {
                let click_name = name.slice(10);
                let func_name = "on" + name[9].toUpperCase() + click_name + "Click";
                if (SC[func_name]) UtilsPanel.addBtnEvent(node, SC[func_name], SC);
            }
            break;
        case "b":
            if (name.startsWith("btn_")) com = node.getComponent(Button);
            break;
        case "e":
            if (name.startsWith("edit_")) com = node.getComponent(EditBox);
            break;
        case "g":
            if (name.startsWith("gene_")) com = node.getComponent(Generate);
            break;
        case "i":
            if (name.startsWith("ilb_")) com = node.getComponent(i18nLabel);
            else if (name.startsWith("isk_")) com = node.getComponent(i18nSkeleton);
            break;
        case "l":
            if (name.startsWith("lb_")) com = node.getComponent(Label);
            break;
        case "n":
            if (name.startsWith("node_")) com = node;
            break;
        case "r":
            if (name.startsWith("rich_")) com = node.getComponent(RichText);
            break;
        case "s":
            if (name.startsWith("spine_")) com = node.getComponent(sp.Skeleton);
            else if (name.startsWith("sp_")) com = node.getComponent(Sprite);
            else if (name.startsWith("sc_")) {
                name = name.slice(3);
                //@ts-ignore
                com = node._components[0];//cocos creator 中组件排序 如果取不到留意顺序是否出错
            }
            break;
        case "t":
            if (name.startsWith("toggle_")) com = node.getComponent(Toggle);
            break;
        case "u":
            if (name.startsWith("uitrans_")) com = node.getComponent(UITransform);
            break;
    }
    if (com && SC.hasOwnProperty(name)) {
        if (SC[name]) console.warn("重复调用?", name);
        SC[name] = com;
    }
}
