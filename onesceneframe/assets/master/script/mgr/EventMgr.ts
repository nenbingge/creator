
type NoParam = ()=>void;
interface protocol {
    test(a: number, b:string, c:string): void
    on_reconn: NoParam
    on_reconn_fail: NoParam
    on_login(data:string):void
}
type K = keyof protocol;
type F<k extends K> = (...params:  Parameters<protocol[k]>) => ReturnType<protocol[k]>;
type P<k extends K> = Parameters<protocol[k]>;

class EventMgr{
    private map_events: Map<string, Array<CallInfo>> = new Map();
    private arr_info: CallInfo[] = [];
    public addEvent<k extends K>(name:k, func:F<k>, target:any){
        let arr = this.map_events.get(name);
        if(!arr){
            arr = [];
            this.map_events.set(name, arr);
        }
        for(let i = 0; i < arr.length; i++){
            if(arr[i][0] == func)return;
        }
        arr.push([func, target]);
    }
    public removeEvent<k extends K>(name:k, func:Function){
        let arr = this.map_events.get(name);
        if(!arr)return;
        for(let i = 0; i < arr.length; i++){
            if(arr[i][0] == func){
                arr.splice(i, 1);
                break;
            }
        }
    }
    public removeName<k extends K>(name:k){
        this.map_events.delete(name);
    }
    public emit<k extends K>(name:k, p0?:P<k>[0], p1?:P<k>[1], p2?:P<k>[2]){
        let arr = this.map_events.get(name);
        if(!arr)return;
        for(let i = 0; i < arr.length; i++){
            arr[i][0].call(arr[i][1], p0, p1, p2);
        }
    }
    public invoke<k extends K>(name:k, p0?:P<k>[0], p1?:P<k>[1], p2?:P<k>[2]){
        let arr = this.map_events.get(name);
        if(!arr)return null;
        return arr[0][0].call(arr[0][1], p0, p1, p2);
    }

    public setInfoLen(len: number) {
        this.arr_info.length = len;
    }
    public addInfo(index: number, target: any, func: Function) {
        this.arr_info[index] = [func, target];
    }
    public call(index: number, p0?:any, p1?:any, p2?:any) {
        let info = this.arr_info[index];
        if (info) return info[0].call(info[1], p0, p1, p2);
        return null;
    }
}
export const eventMgr = new EventMgr();
