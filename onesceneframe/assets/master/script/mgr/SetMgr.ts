
import { _decorator, game, input, Input, KeyCode, log, macro, sys, screen, assetManager, Node, Label, find, UITransform, view, Sprite, SpriteFrame, Vec3, isValid, Layers, Prefab, Texture2D, AudioClip, Asset, AssetManager, director, Font } from 'cc';
import { dialogMgr } from './DialogMgr';
import { resMgr } from './ResMgr';
import { timeMgr } from './TimeMgr';
import { dialog_c } from '../common/DialogCommon';
import { i18nMgr } from './i18nMgr';
import { Color_C } from 'master/enum/CommonEnum';

export class SetMgr {
    public init() {
        //关掉多点触摸
        macro.ENABLE_MULTI_TOUCH = false;
        if (sys.isBrowser) {
            input.on(Input.EventType.KEY_DOWN, onKeyDownTest, this);
        }
        else {
            input.on(Input.EventType.KEY_DOWN, onKeyDown, this);
        }
    }
}
export const setMgr = new SetMgr();

function onExit() {
    let ret: any = dialogMgr.hideTop();
    if (ret)return;
    dialog_c.showMsgBox2("退出?", function () {
        game.end();
    });
}
function onKeyDown(e) {
    switch (e.keyCode) {
        case KeyCode.MOBILE_BACK:
            onExit();
            break;
    }
}
/*
a 资源数
s resmanager
d dialogmgr
f 资源引用打印
g 界面打印
c 关闭z或x
b 打印eventManager的监听
*/
function onKeyDownTest(e) {
    switch (e.keyCode) {
        case KeyCode.KEY_Q:
            onExit();
            break;
        case KeyCode.KEY_A:
            logCache();
            break;
        case KeyCode.KEY_S:
            resMgr.log();
            break;
        case KeyCode.KEY_D:
            dialogMgr.log();
            break;
        case KeyCode.KEY_F:
            i18nMgr.log();
            break;
        case KeyCode.KEY_G:
            timeMgr.log();
            logTimeInfo();
            break;
        case KeyCode.KEY_Z:
            showLbAtlas();
            break;
        case KeyCode.KEY_X:
            closeDebugNode();
            break;

    }
}

var node_debug = null;
function closeDebugNode() {
    if (isValid(node_debug)) node_debug.setParent(null);
}
function showLbAtlas() {
    if (!isValid(node_debug)) {
        node_debug = new Node("debug");
        node_debug._layer = Layers.Enum.UI_2D;
        // node_debug.addComponent(Sprite).spriteFrame = ResMgr.atlas_common.getSpriteFrame("white");
        node_debug.getComponent(UITransform).setContentSize(view.getVisibleSize());
        let new_node = new Node("tex");
        new_node.setParent(node_debug);
        new_node.addComponent(Sprite);
        new_node.setScale(new Vec3(0.5, 0.5, 1))
    }
    node_debug.setParent(find("node_persist"));
    //@ts-ignore
    let tex = Label.Assembler.getAssembler({ cacheMode: Label.CacheMode.CHAR }).getAssemblerData();
    console.log(tex);
    let sf = new SpriteFrame();
    sf.texture = tex;
    node_debug.children[0].getComponent(Sprite).spriteFrame = sf;
    console.log(node_debug)
}
class ResInfo {
    public bundle: AssetManager.Bundle = null;
    public arr_pre = [];
    public arr_tex = [];
    public arr_sound = [];
    public arr_font = [];
    constructor(bundle: AssetManager.Bundle) {
        this.bundle = bundle;
    }
    public addRes(res: Asset) {
        let info = this.bundle.getAssetInfo(res._uuid);
        if (!info) return false;
        //@ts-ignore
        let path = info.path;
        if (!path) {
            if (info.packs) path = "|||" + info.uuid;
            else path = "___" + res.name;
        }
        if (res instanceof Prefab) this.arr_pre.push(path);
        else if (res instanceof Texture2D) {
            this.arr_tex.push(path);
        }
        else if (res instanceof AudioClip) this.arr_sound.push(path);
        else if (res instanceof Font) this.arr_font.push(path);
        return true;
    }
    public log() {
        if (this.arr_pre.length + this.arr_sound.length + this.arr_tex.length + this.arr_font.length == 0) return;
        console.log("%c" + this.bundle.name + ":", Color_C.mgr);
        if (this.arr_pre.length > 0) console.log("Prefab:", this.arr_pre);
        if (this.arr_tex.length > 0) {
            this.arr_tex.sort(function (a: string, b: string) { return a.charCodeAt(0) - b.charCodeAt(0) });
            console.log("Texture2D:", this.arr_tex);
        }
        if (this.arr_sound.length > 0) console.log("AudioClip:", this.arr_sound);
        if (this.arr_font.length) console.log("font:", this.arr_font);
    }
}
function logCache() {
    // let assets = assetManager.assets;//._count);
    let infos: Array<ResInfo> = [];
    assetManager.bundles.forEach((item) => {
        infos.push(new ResInfo(item));
    });
    assetManager.assets.forEach((item, key) => {
        for (let i = 0; i < infos.length; i++) {
            if (infos[i].addRes(item)) break;
        };
    })
    for (let i = 0; i < infos.length; i++) {
        infos[i].log();
    }
}
function logTimeInfo() {
    console.log("action magager:");
    //@ts-ignore
    console.log(TweenSystem.instance.ActionManager._arrayTargets);
    let schedule = director.getScheduler();
    let update_str = "update schedule:\n";
    let deal_arr = function (arr: any[], prefix: string) {
        update_str += "  " + prefix + "\n";
        for (let i = 0; i < arr.length; i++) {
            update_str += arr[i].target.name || arr[i].target._name + "\n";
        }
    }
    deal_arr(schedule["_updatesNegList"], "neg");
    deal_arr(schedule["_updates0List"], "0");
    deal_arr(schedule["_updatesPosList"], "pos");

    let str = "schedule task:\n";
    let arr = schedule["_arrayForTimers"];
    for (let i = 0; i < arr.length; i++) {
        str += arr[i].target.constructor.name + "\n";
        let arr1 = arr[i].timers;
        for (let j = 0; j < arr1.length; j++) {
            str += "  " + arr1[j]._callback.name;
            // console.log(arr1[j]._callback.name)
        }
        str += "\n";
    }
    console.log(update_str);
    console.log(str);
}