import { Asset, AssetManager, SpriteFrame, assetManager, isValid } from "cc";
import { ResTag } from "../Urls";

class ResPack {
    public m_bundle: AssetManager.Bundle;
    private all_res: Map<string, Asset>;//所有路径-》资源
    private all_tag: Map<string, Set<string>>;//用tag保存，路径-》资源的集合
    constructor(bundle: AssetManager.Bundle) {
        this.m_bundle = bundle;
        this.all_res = new Map<string, any>();
        this.all_tag = new Map<string, Set<string>>();
    }
    public load(tag: string, path: string, type: any, callback: Function) {
        if(type == SpriteFrame)path += "/spriteFrame";
        let set = this.all_tag.get(tag);
        if (!set) {
            callback("no tag", null);
            return;
        }
        let add_ref = false;
        if (!set.has(path)) {
            add_ref = true;
            set.add(path);
        }
        let old_res = this.all_res.get(path);
        if (old_res) {
            if (isValid(old_res)) {
                if (add_ref)old_res.addRef();
                callback(null, old_res);
                return;
            } else {
                this.all_res.delete(path);
            }
        }
        this.m_bundle.load(path, type, (err, res: Asset) => {
            if (err) {
                console.log(err);
                callback(err, null);
                return;
            }
            if (this.all_tag.has(tag)) {
                //正常加载
                if (add_ref)res.addRef();
                this.all_res.set(path, res);
                callback(err, res);
            } 
            //加载成功时已经关掉界面
            else if (res.refCount <= 0)assetManager.releaseAsset(res);
        });
    }
    public addTag(tag: string){
        if(this.all_tag.has(tag))return;
        this.all_tag.set(tag, new Set());
    }
    public release(tag: string) {
        let paths = this.all_tag.get(tag);
        if (paths) {
            let self = this;
            paths.forEach(element => {
                let res = self.all_res.get(element);
                if (isValid(res)) {
                    res.decRef();
                    if (res.refCount <= 0) this.all_res.delete(element);
                }
            });
            this.all_tag.delete(tag);
        }
    }
    public releaseAll() {
        if (this.m_bundle) {
            this.all_res.forEach((res)=>{
                assetManager.releaseAsset(res);
            })
        }
        this.all_tag.clear();
        this.all_res.clear();
    }
    public logRes() {
        if(this.m_bundle)console.log(this.m_bundle.name + " log::");
        else console.log("bundle无效");
        console.log(this.all_tag);
        console.log(this.all_res);
    }
}

class ResMgr{
    private main_pack: ResPack = null;
    private texture_pack: ResPack = null;
    private game_pack: ResPack = null;
    private all_res = new Map<string, any>();

    public init() {
        this.main_pack = new ResPack(assetManager.getBundle("master"));
        this.texture_pack = new ResPack(assetManager.getBundle("texture"));
    }
    public addTag(tag: ResTag){
        this.main_pack.addTag(tag);
        this.texture_pack.addTag(tag);
    }
    public load(tag: ResTag, path: string, type: any, callback: Function) {
        this.main_pack.load(tag, path, type, callback);
    }
    public loadImg(tag: ResTag, path: string, callback: Function){
        this.texture_pack.load(tag, path, SpriteFrame, callback);
    }
    public release(tag: ResTag) {
        console.log("---------------------------release:", tag);
        this.main_pack.release(tag);
        this.texture_pack.release(tag);
    }
    public getBundle(is_tex:boolean){
        return is_tex ? this.texture_pack.m_bundle : this.main_pack.m_bundle;
    }

    public gameAddTag(tag:string){
        this.game_pack.addTag(tag);
    }
    public gameLoad(tag: string, path: string, type: any, callback: Function) {
        this.game_pack.load(tag, path, type, callback);
    }
    public gameRealease(tag: string) {
        this.game_pack.release(tag);
    }

    public setGameBundle(bundle: any) {
        if(this.game_pack?.m_bundle == bundle)return;
        if(this.game_pack)this.game_pack.releaseAll();
        this.game_pack = new ResPack(bundle);
    }
    public getGameBundle() {
        return this.game_pack.m_bundle;
    }

    public addRes(name: string, res: any) {
        this.all_res.set(name, res);
    }
    public getRes(name: string) {
        return this.all_res.get(name);
    }
    public deleteRes(name: string) {
        this.all_res.delete(name);
    }
    public getSfByAtlas(atlas_name: string, sp_name: string) {
        let atlas = this.all_res.get(atlas_name);
        return atlas ? atlas.getSpriteFrame(sp_name) : null;
    }
    public log() {
        this.main_pack.logRes();
        this.game_pack?.logRes();
        console.log(this.all_res);
    }

}
export const resMgr = new ResMgr();