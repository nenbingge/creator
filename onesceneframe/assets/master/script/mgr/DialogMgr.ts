
import { Node, instantiate, isValid, Prefab, Sprite, UITransform, tween, view, NodeEventType, BlockInputEvents, Layers, Color, builtinResMgr, SpriteFrame, Texture2D } from 'cc';

import { resMgr } from './ResMgr';
import { q } from '../Q';
import { Dialog } from '../base/Dialog';
import { LRUArr } from '../extra/ExClass';
import { ResTag } from '../Urls';
import { Color_C } from 'master/enum/CommonEnum';

const HoldLen = 2;

type DialogInfo = [string, string];

class DialogMgr {
    private cache_keys: LRUArr = null;
    //目前显示的页面代码
    private loading_name = "";
    private wait_queue:[DialogInfo, any, boolean, Node][] = [];
    private showing_arr: [string, Dialog, Node][] = [];

    //缓存
    private cache_info: Map<string, [Prefab, Node]> = new Map();

    private sf_bg = null;
    private nodes_bg:Node[] = [];

    private cur_holds:string[] = [];
    public hold_info:[string, Dialog, any, Node][] = [];

    public init() {
        this.cache_keys = new LRUArr(3);
        this.sf_bg = new SpriteFrame();
        this.sf_bg.texture = builtinResMgr.get("black-texture") as Texture2D;
        for(let i = 0 ; i < HoldLen ; i++){
            this.cur_holds.push("");
        }
    }
    public onChangeScene(){
        q.node_dialog.destroyAllChildren();
        this.reset();
    }
    public reset(){
        for(let i = 0 ; i < HoldLen ; i++){
            this.cur_holds[i] = "";
        }
        this.hold_info.length = 0;
        this.showing_arr.length = 0;
        this.wait_queue.length = 0;
        this.loading_name = "";
    }
    private getNodeMask() {
        if (this.nodes_bg.length > 0) {
            let bg = this.nodes_bg.pop();
            bg.off(NodeEventType.TOUCH_END);
            return bg;
        }
        let node = new Node("BG");
        node.layer = Layers.Enum.DEFAULT;
        node.addComponent(UITransform);
        let uitrans_bg = node.getComponent(UITransform);
        uitrans_bg.setContentSize(view.getVisibleSize());
        let sp = node.addComponent(Sprite);
        sp.sizeMode = Sprite.SizeMode.CUSTOM;
        sp.spriteFrame = this.sf_bg;
        sp.color = Color.BLACK;
        return node;
    }
    //#region show
    private showNode(script_name: string, node: Node, data: any, parent: Node) {
        this.cache_keys.delKey(script_name);
        parent = parent || q.node_dialog;
        let script = node.getComponent(script_name) as Dialog;
        let hold_index = script.hold_index;
        if(hold_index > -1){
            if(this.cur_holds[hold_index].length > 0){
                if(script_name != this.cur_holds[hold_index]){
                    this.hold_info[hold_index].push([script_name, script, data, parent]);
                }
                return;
            }
            else{
                this.cur_holds[hold_index] = script_name;
            }
        }
        let bg = null;
        if (script.fill_screen) {
            script.node.getComponent(UITransform).height = view.getVisibleSize().height;
            //(view.getVisibleSize());
        }
        else if (script.auto_mask > 0) {
            bg = this.getNodeMask();
            bg.setParent(parent);
            bg.toOpacity(0);
            tween(bg).toPro(0.2, { opacity: script.mask_opacity }).start();
            
            let block = bg.getComponent(BlockInputEvents);
            if (script.auto_mask == 2) {
                if(block)block.destroy();
                bg.on(NodeEventType.TOUCH_END, function () {
                    script.hide();
                }, script);
            }
            else{
                if(!block)bg.addComponent(BlockInputEvents);
            }
        }
        node.setParent(parent);
        node.addComponent(BlockInputEvents);
        this.showing_arr.push([script_name, script, bg]);
        if(script)resMgr.addTag(script_name as ResTag);
        script.show(data);
    }
    // public show<UI extends BaseView>({ name, data, queue, onShow, onHide, onError, top = true, attr = null }
    //     : IShowParams<UIName, Parameters<UI['onShow']>[0], ReturnType<UI['onShow']>, ReturnType<UI['onHide']>>): boolean {

    public showByResUrl<T extends Dialog>(info: DialogInfo, data?: Parameters<T["init"]>[0], is_game = false, parent: Node = null) {
        console.log("%cshowByResUrl " + info[0], Color_C.mgr, this.loading_name);
        if (this.loading_name == info[0]) return;
        if (this.loading_name.length) {
            this.wait_queue.push([info, data, is_game, parent]);
            return;
        }

        let dialog = this.getDialogByName(info[0]);
        if (dialog) {
            dialog.updateData(data);
            this.next();
            return;
        }
        let cache_info = this.cache_info.get(info[0]);
        if(cache_info){
            let node = cache_info[1];
            if (node) {
                if (node.isValid) {
                    this.showNode(info[0], node, data, parent);
                    this.next();
                    return;
                } 
                else {
                    cache_info[1] = null;
                }
            }
            let pre = cache_info[0];
            if (pre.isValid) {
                let node = instantiate(pre);
                this.showNode(info[0], node, data, parent);
                this.next();
                return;
            }
        }
        this.loading_name = info[0];
        let bundle = is_game ?  resMgr.getGameBundle() : resMgr.getBundle(false);
        let s = new Date().getTime();
        bundle.load("prefab/ui/" + info[1], Prefab, (err: any, prefab: Prefab) => {
            console.log("%cloaddialog time(ms):" + info[0] , Color_C.mgr, (new Date().getTime() - s));
            if (!err) {
                if(this.loading_name == info[0]){
                    prefab.addRef();
                    this.cache_info.set(info[0], [prefab, null]);
                    let node = instantiate(prefab);
                    this.showNode(info[0], node, data, parent);
                }
            } 
            else {
                console.log(err);
            }
            this.loading_name = "";
            this.next();
        });
    }

    public getDialogByName(name: string) {
        for (let i = this.showing_arr.length - 1; i >= 0; i--) {
            let info = this.showing_arr[i];
            if (info[0] != name)continue;
            if(info[1].isValid)return info[1];
            else this.showing_arr.splice(i, 1);
        }
        return null;
    }
    public script_can_load(script_name: string) {
        if(this.loading_name == script_name) return false;
        if(this.getDialogByName(script_name))return false;
        if(this.wait_queue.find(function(item){return item[0][0] == script_name}))return false;
        return !this.hold_info.find(function(item){return item[0] == script_name});
    }
    public next() {
        if (this.wait_queue.length == 0) return;
        let data = this.wait_queue.shift();
        this.showByResUrl(data[0], data[1], data[2], data[3]);
    }
    //#region hide
    public hideByName(script_name: string) {
        if(script_name == this.loading_name)this.loading_name = "";
        for (let i = this.showing_arr.length - 1; i >= 0; i--) {
            if (this.showing_arr[i][0] == script_name) {
                this.hideNode(i);
                return;
            }
        }
    }
    public hide(script: any) {
        for (let i = this.showing_arr.length - 1; i >= 0; i--) {
            if (this.showing_arr[i][1] == script) {
                this.hideNode(i);
                return;
            }
        }
    }
    public hideNode(index: number) {
        let info = this.showing_arr[index];
        console.log("%chideNode: " + info[0], Color_C.mgr);
        this.showing_arr.splice(index, 1);
        if (isValid(info[1])) info[1].hideAnim(info);

        let script = info[1];
        if(script.hold_index == -1)return;
        let hold_index = script.hold_index;
        this.cur_holds[hold_index] = "";
        let len = this.hold_info.length
        if(len == 0)return;
        for(let i = 0; i < len; i++){
            let info = this.hold_info[i];
            if(info[1].hold_index != hold_index)continue
            this.showNode(info[0], info[1].node, info[2], info[3]);
        }    
    }
    private forceHide(index: number){
        let info = this.showing_arr[index];
        this.showing_arr.splice(index, 1);
        if (isValid(info[1])){
            info[1].forceHide();
            this.afterHide(info);
        }
    }
    public afterHide(info: [string, Dialog, Node]) {
        let script = info[1];
        let node = script.node;
        if (script.cache_mode == 0) {
            let key = this.cache_keys.addKey(info[0])
            if(key){
                let cache_info = this.cache_info.get(key);
                cache_info[0].decRef();
                this.cache_info.delete(key);
                if(script.add_tag)resMgr.release(info[0] as ResTag);
            }
        }
        if (!isValid(node)) return;
        if (script.cache_mode == 2) {
            node.removeFromParent();
            this.cache_info.get(info[0])[1] = node;
        }
        else {
            node.destroy();
        }
        if (isValid(info[2])) {
            info[2].removeFromParent();
            this.nodes_bg.push(info[2]);
        }
    }

    public hideTop() {
        let len = this.showing_arr.length;
        if(len > 0 && this.showing_arr[len - 1][1].auto_hide){
            this.hideNode(len - 1);
            return true;
        }
        return false;
    }
    public closeAll(clear_cache: boolean) {
        for (let i = this.showing_arr.length - 1; i >= 0; i--) {
            this.hideNode(i);
        }
        this.reset();
        if (!clear_cache) return;
        this.cache_info.forEach(function (v) {
            v[0].decRef();
            v[1]?.destroy();
        })
    }
    public closeByIgnore(func: Function){
        let arr = this.showing_arr;
        for (let i = arr.length - 1; i >= 0; i--) {
            if(func && func(arr[i]))continue;
            this.forceHide(i);
        }
        this.wait_queue.length = 0;
        this.loading_name = "";
    }
    public log() {
        console.log("显示的界面", this.showing_arr);
        console.log("缓存", this.cache_info);
        console.log("等待的界面", this.wait_queue);
        console.log("缓存key", this.cache_keys);
    }
}
export const dialogMgr = new DialogMgr();