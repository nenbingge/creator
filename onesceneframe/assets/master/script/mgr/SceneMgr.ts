import { _decorator, AssetManager, Component, find, instantiate, isValid, Node, Prefab } from 'cc';
import { BaseScene } from '../base/BaseScene';
import { q } from '../Q';
import { Color_C } from 'master/enum/CommonEnum';

class SceneMgr{
    private pre_map: Map<string, Prefab> = new Map();
    private load_name = "";
    
    private cur_game: BaseScene = null;
    public get node_game(){ return this.cur_game.node;}
    
    private _name = "";
    public get name(){ return this._name; }

    public loadScene(name:string, bundle?:AssetManager.Bundle, opts?: any){
        console.log("%cloadScene " + name, Color_C.mgr, this._name);
        if(this.load_name == name || this._name == name)return;
        if(!bundle)bundle = q.bundle_main;
        let pre_cache = this.pre_map.get(name);
        if(pre_cache && pre_cache.isValid){
            this.showScene(pre_cache, opts);
            this._name = name;
            return;
        }
        this.load_name = name;
        let s = new Date().getTime();
        bundle.load("scene/" + name, Prefab, (err:any, pre:Prefab)=>{
            console.log("%cloadscene time(ms):" + name , Color_C.mgr, (new Date().getTime() - s));
            if(err){console.log(err); return}
            if(this.load_name != name)return;
            this.load_name = "";
            this.pre_map.set(name, pre);
            this.showScene(pre, opts);
            this._name = name;
        })
    }
    private showScene(pre: Prefab, opts: any){
        pre.addRef();
        let node = instantiate(pre);
        node.setParent(q.node_canvas);
        if(this.cur_game){
            node.setSiblingIndex(this.cur_game.node.getSiblingIndex() + 1);
            let scene = node.getComponent(BaseScene);
            let enter_index = scene.onEnter(opts);
            let old_name = this._name;
            this.cur_game.leaveAction(enter_index, ()=>{
                this.releaseName(old_name)
            })
            this.cur_game = scene;
        }
        else{
            node.setSiblingIndex(1);
            this.cur_game = node.getComponent(BaseScene);
        }
    }
    private releaseName(scene_name: string){
        let pre = this.pre_map.get(scene_name);
        if(!isValid(pre))return;
        pre.decRef();
        if(pre.refCount <= 0)this.pre_map.delete(scene_name);
    }
    public log(){
        console.log(this.pre_map);
    }
}

export const sceneMgr = new SceneMgr();

