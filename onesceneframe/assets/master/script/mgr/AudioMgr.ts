import { AudioClip, AudioSource, Node } from "cc";
import { store } from "./StorageMgr";

const Extra_Count = 2;
const Effect_Count = 5;
const Use_Index = [0, 0, 0];
class AudioMgr {
    private music_on = true;
    private effect_on = true;
    private audio: AudioSource = null;
    private audios_loop: Array<AudioSource> = null;
    private audios_func: Array<AudioSource> = null;
    private audios_effect: Array<AudioSource> = null;

    private cache = [false, false];

    public init(node: Node) {
        this.audio = node.addComponent(AudioSource);
        this.audio.playOnAwake = false;
        let node_func = new Node("node_audio_func");
        node_func.setParent(node);
        this.audios_loop = new Array(Extra_Count);
        this.audios_func = new Array(Extra_Count);
        let audio = null;
        for (let i = 0; i < Extra_Count; i++) {
            audio = this.audios_loop[i] = node.addComponent(AudioSource);
            audio.loop = true;
            audio.playOnAwake = false;
            audio = this.audios_func[i] = node_func.addComponent(AudioSource);
            audio.loop = false;
            audio.playOnAwake = false;
        }
        this.audios_effect = new Array(Effect_Count);
        for (let i = 0; i < Effect_Count; i++) {
            audio = this.audios_effect[i] = node.addComponent(AudioSource);
            audio.loop = false;
            audio.playOnAwake = false;
        }
        node_func.on("ended", function (source: AudioSource) {
            source.clip.emit("ended");
        })
        this.getSaveSet();
    }
    public onHide() {
        this.cache[0] = this.music_on;
        this.cache[1] = this.effect_on;
        this.music_on = this.effect_on = false;
        this.pause();
    }
    public onShow() {
        this.music_on = this.cache[0];
        this.effect_on = this.cache[1];
        if(this.music_on)this.audio.play();
    }

    public playMusic(clip: AudioClip, loop: boolean) {
        this.audio.stop();
        this.audio.loop = loop;
        this.audio.clip = clip;
        if (this.music_on) {
            this.audio.play();
        }
    }

    public playEffect(clip: AudioClip, loop = false, need_stop = false) {
        if (this.effect_on) {
            if (loop) {
                let audio_source = this.audios_loop[Use_Index[0]];
                audio_source.stop();
                audio_source.clip = clip;
                audio_source.play();
                Use_Index[0] = (Use_Index[0] + 1) % Extra_Count;
            }
            else if (need_stop) {
                let index = Use_Index[2];
                let audio_source = this.audios_effect[index];
                audio_source.stop();
                audio_source.clip = clip;
                audio_source.play();
                Use_Index[2] = (index + 1) % 4;
            }
            else {
                this.audio.playOneShot(clip, 1);
            }
        }
    }
    public playEffectWithFunc(clip: AudioClip, func: Function) {
        if (!func) {
            this.playEffect(clip);
            return;
        }
        let audio_source = this.audios_func[Use_Index[1]];
        audio_source.stop();
        audio_source.clip = clip;
        audio_source.play();
        clip.once("ended", func as any);
        Use_Index[1] = (Use_Index[1] + 1) % Extra_Count;
    }
    public setVolume(vol: number) {
        this.audio.volume = vol;
    }
    public getVolume() {
        return this.audio.volume
    }
    public getMusicOn() {
        return this.music_on;
    }

    public setMusicOn(state: boolean) {
        if (this.music_on == state) return;
        this.music_on = state;
        if (state) {
            this.audio.play();
        } else {
            this.pause();
        }
        this.saveSet();
    }

    public getEffectOn() {
        return this.effect_on;
    }

    public setEffectOn(state: boolean) {
        if (this.effect_on == state) return;
        this.effect_on = state;
        this.saveSet();
    }
    public pause() {
        this.audio.pause();
        for (let i = 0; i < Extra_Count; i++) {
            this.audios_loop[i].pause();
            this.audios_func[i].pause();
        }
        for (let i = 0; i < Effect_Count; i++) {
            this.audios_effect[i].stop();
            this.audios_effect[i].clip = null;
        }
    }
    public stopSound(clip: AudioClip) {
        for (let i = 0; i < Effect_Count; i++) {
            if (this.audios_effect[i].clip == clip) {
                this.audios_effect[i].stop();
                this.audios_effect[i].clip = null;
            }
        }
    }
    public stop() {
        this.audio.stop();
        this.audio.clip = null;
        for (let i = 0; i < Extra_Count; i++) {
            this.audios_loop[i].stop();
            this.audios_loop[i].clip = null;
            this.audios_func[i].stop();
            this.audios_func[i].clip = null;
        }
        for (let i = 0; i < Effect_Count; i++) {
            this.audios_effect[i].stop();
            this.audios_effect[i].clip = null;
        }
    }
    public saveSet() {
        let str = this.music_on ? "1" : "0";
        let str1 = this.effect_on ? "1" : "0";
        store.setStr("music_switch", str + str1);
    }
    public getSaveSet() {
        let str = store.getStr("music_switch");
        if (!str) return;
        this.music_on = str[0] == "1";
        this.effect_on = str[1] == "1";
    }
}
export const audioMgr = new AudioMgr();