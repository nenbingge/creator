
const Key_Common = "storage";
class StoreMgr<T extends string, T2 extends string>{
    private suffix = "";
    private map: Map<string, string> = new Map();
    private map2: Map<string, string> = new Map();

    public init(suffix:string | number){
        this.suffix = suffix + "";
        this.map.clear();
        this.map2.clear();
        let str = localStorage.getItem(Key_Common + suffix);
        if(str)this.parse(str, this.map);
    }
    //#region 短文本
    public setStr(key: T, str: string){
        this.map.set(key, str);
        localStorage.setItem(Key_Common + this.suffix, this.serialize(this.map));
    }
    public getStr(key: T){
        return this.map.get(key);
    }
    public delKey(key: T){
        if(!this.map.has(key))return;
        this.map.delete(key);
        localStorage.setItem(Key_Common + this.suffix, this.serialize(this.map));
    }
    //#region 长文本
    public setStr2(key: T2, str: string, cache: boolean){
        let final_key = key + this.suffix;
        localStorage.setItem(final_key, str);
        if(cache)this.map2.set(final_key, str);
    }
    public getStr2(key: T2){
        let final_key = key + this.suffix;
        if(this.map2.has(final_key))return this.map2.get(final_key);
        return localStorage.getItem(final_key);
    }
    public delKey2(key: T2){
        let final_key = key + this.suffix;
        localStorage.removeItem(final_key);
        this.map2.delete(final_key);
    }

    public parse(str:string, map: Map<string, string>){
        if(!str.length)return;
        let strs = str.split(",");
        console.log(strs);
        let len = strs.length;
        if(len % 2 != 0)len -= 1;
        for(let i = 0; i < len; i += 2){
            map.set(strs[i], strs[i + 1]);
        }
    }
    public serialize(map: Map<string, string>){
        let str = "";
        if(map.size == 0)return str;
        map.forEach(function(val:string, key:string){
            str += key + "," + val + ",";
        })
        return str;
    }
}

export const store = new StoreMgr<KeyType, StrType>();
//一些需要区分后缀的情况  例如用userid区分玩家
export const store2 = new StoreMgr<KeyType2, StrType2>();
type KeyType = "music_switch"
    | "imei"
    | "language"
type StrType = "key0"
    "key1"

type KeyType2 = "key0"
    | "key1"
type StrType2 = "key0"
    "key1"