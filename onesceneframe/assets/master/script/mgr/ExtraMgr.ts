import { sys , Node, ScrollView, Vec2, Vec3, Intersection2D, PolygonCollider2D, view, Toggle, UIRenderer} from "cc";

const Interval_Time = 200;
var old_dispatch = null;
var start_date=0;

function changeDispatch(){
    old_dispatch = Node.prototype.dispatchEvent;
    Node.prototype.dispatchEvent = function (event) {
        if(event.type == "touch-start"){
            let now_time = sys.now();
            if(start_date > 0 && (now_time-start_date) < Interval_Time){
                console.warn("click fast:",this);
                return;
            }
            start_date = now_time;
        }
        old_dispatch.call(this, event);
    };
}

function changeSafeRect(){
    let old_func = sys.getSafeAreaRect;
    // console.log(old_func());
    sys.getSafeAreaRect = function(){
        let rect = old_func();
        // rect.height -= 200;
        // rect.y += 100;
        let y = rect.y;
        if(y > 0){
            rect.height += y;
            rect.y = 0;
        }
        this.fill_height = rect.height;
        let all_height = view.getVisibleSize().height;
        this.safe_top = all_height - rect.height;
        this.fill_bottom = rect.y - this.safe_top / 2;
        return rect;
    }
}
const V3_World = new Vec3(0);
const V3_Local = new Vec3(0);
const V2_Local = new Vec2(0);
function hitTest(screenPoint: Vec2){
    V3_World.x = screenPoint.x;
    V3_World.y = screenPoint.y;
    const cameras = this._getRenderScene().cameras;
    for (let i = 0; i < cameras.length; i++) {
        const camera = cameras[i];
        if (!(camera.visibility & this.node.layer) || (camera.window && !camera.window.swapchain)) continue;
        Vec3.set(V3_World, screenPoint.x, screenPoint.y, 0);  // vec3 screen pos
        camera.screenToWorld(V3_World, V3_World);
        this.convertToNodeSpaceAR(V3_World, V3_Local);
        V2_Local.x = V3_Local.x;
        V2_Local.y = V3_Local.y;
        return Intersection2D.pointInPolygon(V2_Local, this.node.getComponent(PolygonCollider2D).points);
    }
    return false;
}

function toggleEffect () {
    if(this.node.children[0] != this._checkMark.node)this.node.children[0].active = !this._isChecked;
    if (this._checkMark) {
        this._checkMark.node.active = this._isChecked;
    }
}
class ExtraMgr{
    public fill_height = 1280;
    public fill_bottom = 0;
    public safe_top = 0;
    public init(){
        this.fill_height = view.getVisibleSize().height;
        changeDispatch();
        // changeSafeRect();
    }
    public resetDispatch(node:Node){
        node.dispatchEvent = old_dispatch;
    }
    public resetMoveTopLeft(scroll:ScrollView){
        //@ts-ignore
        scroll._moveContentToTopLeft = Empty_Func;
    }
    public changeHitTest(node:Node){
        node._uiProps.uiTransformComp.hitTest = hitTest;
    }
    public changeTogglesFunc(toggles:Array<Toggle>, func?:Function){
        if(!func)func = toggleEffect;
        toggles.forEach(function(toggle:Toggle){toggle.playEffect = func as any})
    }
}
export const extraMgr = new ExtraMgr();