import { Game, System, director, game, isValid } from "cc";

export abstract class TimeInfo{
    public cur_time = 0;
    public calc_back = true;
    public func: Function;
    public target: any;
    constructor(time:number, func:Function, target:any){
        this.cur_time = time;
        this.func = func;
        this.target = target;
    }
    public abstract minusDt(dt:number): boolean;
    public updateTime(time:number){
        this.cur_time = time;
        return this.func.call(this.target, this.cur_time);
    }
}
class CountDownInfo extends TimeInfo{
    public int_time = 0;
    constructor(time:number, func:Function, target:any){
        super(time, func, target);
        this.int_time = time;
    }
    public minusDt(dt:number){
        if(!isValid(this.target))return true;
        this.cur_time -= dt;
        if(this.int_time > 1 + this.cur_time){
            this.int_time = Math.ceil(this.cur_time);
            return this.func.call(this.target, this.int_time);
        }
        return false;
    }
}
class PreciseInfo extends TimeInfo{
    public minusDt(dt:number){
        if(!isValid(this.target))return true;
        this.cur_time -= dt;
        return this.func.call(this.target, this.cur_time);
    }
}
class TaskInfo extends TimeInfo{
    public repeat = 0;
    public one_time = 0;
    constructor(time:number, repeat:number, func:Function, target:any){
        super(time, func, target);
        this.one_time = time;
        this.repeat = repeat;
        this.calc_back = false;
    }
    public minusDt(dt: number) {
        this.cur_time -= dt;
        if(this.cur_time <= 0){
            this.func.call(this.target, this.cur_time);
            if(this.repeat > 0)this.repeat--;
            if(this.repeat == 0)return true;
            this.cur_time += this.one_time;
        }
        return false;
    }
}
class TimeMgr extends System {
    private hide_date = 0;
    private cur_time = 0;
    private infos: TimeInfo[] = [];
    private infos_precise: TimeInfo[] = [];

    init(){
        game.on(Game.EVENT_HIDE, this.onHide, this);
        game.on(Game.EVENT_SHOW, this.onShow, this);
        director.registerSystem("timemgr", this, 0);
    }
    //#region add
    public addCountDown(time:number, func:Function, target:any){
        let info = this.getInfo(false, func);
        if(info){
            info.updateTime(time);
        }
        else{
            info = new CountDownInfo(time, func, target);
            this.infos.push(info);
        }
        return info;
    }
    public addPrecise(func:Function, target:any){
        if(this.getInfo(true, func))return;
        let info = new PreciseInfo(0, func, target);
        this.infos_precise.push(info);
        return info;
    }
    public addTask(time:number, repeat:number, func: Function, obj: any){
        let info = new TaskInfo(time, repeat, func, obj);
        this.infos_precise.push(info);
        return info;
    }
    public nextTick(func: Function, obj: any){
        let info = new TaskInfo(0, 1, func, obj);
        this.infos_precise.push(info);
    }
    public addInfo(info:TimeInfo, is_precise: boolean){
        let arr = is_precise ? this.infos_precise : this.infos;
        if(arr.indexOf(info) > -1)return;
        arr.push(info);
    }
    public getInfo(is_precise: boolean, func:Function){
        let arr = is_precise ? this.infos_precise : this.infos;
        for(let i = 0; i < arr.length; i++){
            if(arr[i].func == func)return arr[i];
        }
        return null;
    }
    //#region remove
    public removeByTarget(target:any, is_precise: boolean){
        let arr = is_precise ? this.infos_precise : this.infos;
        for(let i = arr.length - 1; i >= 0 ; i--){
            if(arr[i].target == target)arr.splice(i, 1);
        }
    }
    public removeFunc(func: Function, is_precise: boolean){
        let arr = is_precise ? this.infos_precise : this.infos;
        for(let i = arr.length - 1; i >= 0 ; i--){
            if(arr[i].func == func)arr.splice(i, 1);
            break;
        }
    }
    public removeInfo(info:TimeInfo, is_precise: boolean){
        let arr = is_precise ? this.infos_precise : this.infos;
        let index = arr.indexOf(info);
        if(index > -1)arr.splice(index, 1);
    }
    public update(dt: number): void {
        if(this.infos.length + this.infos_precise.length == 0)return;
        this.cur_time += dt;
        for(let i = 0; i < this.infos_precise.length; i++){
            if(this.infos_precise[i].minusDt(dt))this.infos_precise.splice(i, 1);
        }
        if(this.cur_time < 1)return;
        let mul = Math.floor(this.cur_time);
        this.cur_time -= mul;
        for(let i = this.infos.length - 1; i >= 0 ; i--){
            if(this.infos[i].minusDt(mul))this.infos.splice(i, 1);
        }
    }

    private onHide(){
        this.hide_date = new Date().getTime();
    }
    private onShow(){
        if(this.hide_date == 0)return;
        let offset = (new Date().getTime() - this.hide_date) / 1000;
        if(offset > 0){
            console.log("进入后台时长(s):", offset);
            for(let i = this.infos_precise.length - 1; i >= 0 ; i--){
                let info = this.infos_precise[i];
                if(!info.calc_back)continue;
                if(info.minusDt(offset))this.infos_precise.splice(i, 1);
            }
            offset = Math.floor(offset);
            for(let i = this.infos.length - 1; i >= 0 ; i--){
                let info = this.infos[i];
                if(!info.calc_back)continue;
                if(info.minusDt(offset))this.infos.splice(i, 1);
            }
        }
        this.hide_date = 0;
    }
    public log(){
        console.log("times:",this.infos);
        console.log(this.infos_precise);
    }
    
}

export const timeMgr = new TimeMgr();
