
export type GameName = "fivechess";

export const Color_C = {
    mgr: "color:#c62",
    netsend: "color:green",
    netrecv: "color:blue",
}
