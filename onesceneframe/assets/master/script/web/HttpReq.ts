import {  sys, _decorator } from 'cc';

type CallBack = (param0?:boolean, param1?:string)=>void;

export class HttpReq {
    public static req(url: string, data: any, callback?: CallBack, post?: boolean) {
        let xhr = new XMLHttpRequest();
        xhr.timeout = 5000;
        if(callback){
            let func = function(){callback(false, "")}
            xhr.onabort = xhr.onerror = xhr.ontimeout = func;
            xhr.onreadystatechange = function () {
                //log("onreadystatechange code = " + xhr.readyState + ",status = " + xhr.status);
                if (xhr.readyState === 4 && (xhr.status >= 200 && xhr.status < 300))callback(true, xhr.response);
            };
        }
        // xhr.onloadstart
        // xhr.onload
        // xhr.onloadend
        if (post) {
            console.log("post:", url);
            xhr.open("POST", url, true);
            xhr.send(data);
            //xhr.send(new Uint8Array([1,2,3,4,5]));
        }
        else {
            if(data)url += "?" + data;
            console.log("get:", url);
            xhr.open("GET", url, true);
            if (sys.isNative)xhr.setRequestHeader("Accept-Encoding", "gzip,deflate");
            xhr.send();
        }

    }

}
