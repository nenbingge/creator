import { _decorator, sys } from 'cc';
import { ByteArray } from "../3rd/ByteArray";
import { WebData } from './WebData';

export class WebBase{
    protected socket: WebSocket = null;
    protected callback_once: Map<string, CallInfo> = new Map();
    protected callback: Map<string, CallInfo> = new Map();
    protected data_cache: string[] = [];
    
    protected url = "";
    protected loginData = "";
    //0 默认  1 重连中  2 不再连了
    public reconnect_state = 0;
    public reconnect_count = 0;

    public initWebSocket(url: string): void {
        if (this.socket) {
            console.log("initWebSocket closing exist socket");
            this.forceClose();
        }
        this.url = url;
        this.socket = new WebSocket(url);
        let self = this;
        this.socket.onopen = function(){
            self.onConnected();
        }
        this.socket.onmessage = function (event) {
            if (sys.isNative) {
                let byte: ByteArray = new ByteArray(event.data);
                self.onData(byte.readUTFBytes(byte.length));
                return;
            }
            let reader = new FileReader();
            reader.readAsText(event.data, 'utf-8');
            reader.onload = function (e) {
                self.onData(reader.result as string);
            }
        };
        this.socket.onerror = this.socket.onclose = function(){
            self.onClose();
        }
    }
    protected onConnected() {
        console.log("connected.", this.url);
        this.reconnect_state = 0;
        this.sendPing();
        if (this.loginData.length) {
            console.log("WebBase.onSocketConnected sending loginData", this.loginData)
            this.socket.send(this.loginData);
        }
    }
    protected onClose(){
        console.log("onclose");
        this.socket = null;
    }
    protected onData(data:string): void {}

    public isConnected(): boolean {
        if (this.socket) {
            return this.socket.readyState == WebSocket.OPEN ||
            this.socket.readyState == WebSocket.CONNECTING;
        }
        return false;
    }
    public canSendData(): boolean {
        return this.socket && this.socket.readyState == WebSocket.OPEN;
    }
    public sendData(data: string): boolean {
        if (this.canSendData()) {
            this.socket.send(data);
            return true;
        } 
        else {
            this.data_cache.push(data);
            return false;
        }
    }
    public sendPing(): boolean {
        if (!this.canSendData()) return false;
        let wd: WebData = new WebData("ping", "");
        this.socket.send(JSON.stringify(wd));
        return true;
    }
    public sendCacheData() {
        if (this.data_cache.length == 0)return;
        console.log("sending cache data begin");
        for (let i = 0, len = this.data_cache[i].length; i < len; i++) {
            if (!this.data_cache[i])continue;
            console.log("send cache:", this.data_cache[i]);
            this.socket.send(this.data_cache[i]);
        }
        console.log("sending cache data end");
        this.data_cache.length = 0;
    }
    public clearCacheData() {
        this.data_cache.length = 0;
    }

    public closeSocket(): void {
        this.reconnect_state = 2;
        this.loginData = "";
        this.socket && this.socket.close();
    }
    public forceClose(){
        if(!this.socket)return;
        this.data_cache.length = 0;
        this.socket.onmessage = this.socket.onclose = this.socket.onerror = this.socket.onopen = null;
        this.socket.close();
        this.socket = null;
    }
    public doConnect() {
        if (this.isConnected()) return;
        this.initWebSocket(this.url);
    }
    public registerCallback(msg: string, listener: Function, obj: any) {
        if (listener == null)return;
        this.callback.set(msg, [listener, obj]);
    }
    public unRegisterCallback(msg: string) {
        this.callback.delete(msg);
    }
    public addCallback(msg: string, listener: Function, obj: any) {
        if (listener == null)return;
        this.callback_once.set(msg, [listener, obj]);
    }

}
