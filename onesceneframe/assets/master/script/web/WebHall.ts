import { _decorator, game, Game } from 'cc';
import { WebData } from "./WebData";
import { WebBase } from "./WebBase";
import { UtilsCommon } from '../utils/UtilsCommon';
import { eventMgr } from '../mgr/EventMgr';
import { TimeInfo, timeMgr } from '../mgr/TimeMgr';
import { Color_C } from 'master/enum/CommonEnum';

const temp_wd = new WebData("", "");

class WebHall extends WebBase {
    private is_hide = false;
    private sessionData = "";

    // private time_ping: TimeInfo = null;
    public constructor() {
        super();
        game.on(Game.EVENT_CLOSE, this.onHide, this);
        game.on(Game.EVENT_SHOW, this.onShow, this);
    }

    protected onData(data:string): void {
        var wd = JSON.parse(data);
        if (wd.Msg == "ping") {
            timeMgr.addTask(10, 1, this.sendPing, true);
            return;
        }
        if (wd.Data == "null") {
            wd.Data = "";
        }

        // 如果被服务器踢了
        if (wd.Msg == "logout" || wd.Msg == "login_failed") {
            console.log("WebControl.kick", data)
            this.reconnect_state = 2;
            this.loginData = "";
            return;
        }
        if (wd.Msg == "loginByIMei" || wd.Msg == "loginByToken") {
            let loginRet = JSON.parse(wd.Data);
            if (loginRet.UserId > 0) {
                // 成功了
                let loginData = new WebData("loginByToken", JSON.stringify({ UserId: loginRet.UserId, Token: loginRet.Session }));
                this.sessionData = loginData.Data;
                this.loginData = JSON.stringify(loginData);
                this.sendCacheData();
                eventMgr.emit("on_login", loginRet);
                return;
            }
        }
        //console.log("onReceiveMessage msg = " + wd.Msg + ",Data = " + wd.Data);
        let info = this.callback.get(wd.Msg);
        if (info) {
            info[0].call(info[1], wd.Data);
            return;
        }
        info = this.callback_once.get(wd.Msg);
        if (info) {
            this.callback_once.delete(wd.Msg);
            info[0].call(info[1], wd.Data);
            return;
        }
    }
    protected onClose(): void {
        super.onClose();
        if (this.is_hide) return;
        let state = this.reconnect_state;
        if (state == 2) return;
        if (state == 0) {
            this.reconnect_state = 1;
            this.reconnect_count = 1
            this.doConnect();
            eventMgr.emit("on_reconn");
            return;
        }
        else if (state == 1) {
            this.reconnect_count++;
        }
        if (this.reconnect_count < 6) {
            timeMgr.addTask(1, 1, this.doConnect, this);
        }
        else {
            eventMgr.emit("on_reconn_fail");
        }
    }
    public onHide(){
        this.is_hide = true;
    }
    public onShow(){
        if(!this.is_hide)return;
        if (!this.isConnected()) {
            this.onClose();
        }
    }
    public getSessionData() {
        return this.sessionData;
    }
    public getLoginData() {
        return this.loginData
    }
    // 退出登陆
    public logOut() {
        this.loginData = "";
        this.closeSocket();
    }
    public sendEmptyCmd(cmd: string, listener: Function, obj: any) {
        temp_wd.Msg = cmd;
        temp_wd.Data = "";
        console.log("%c" + cmd, Color_C.netsend);
        this.sendData(JSON.stringify(temp_wd));
        listener && this.addCallback(cmd, obj, listener);
    }
    
}

export const web_hall = new WebHall();