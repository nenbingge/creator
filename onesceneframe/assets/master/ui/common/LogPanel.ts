import { _decorator, Label } from 'cc';
import { Dialog } from '../../script/base/Dialog';
const {ccclass, property} = _decorator;


@ccclass('LogPanel')
export class LogPanel extends Dialog {

    public lb_log: Label | null = null;

    protected onLoad(){
        this.lb_log = this.node.getComponent(Label);
    }

    public init(data:any){
        if(typeof(data) == "object"){
            data = JSON.stringify(data);
        }
        this.lb_log.string = data;
    }
 
}
