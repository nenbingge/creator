import { _decorator, Node, RichText, Label } from 'cc';
import { Dialog } from '../../script/base/Dialog';
import { UtilsPanel } from '../../script/utils/UtilsPanel';
const { ccclass, property } = _decorator;

export class MsgInfo{
    mode:number;
    title:string;
    desc:string;
    txt_sure:string;
    txt_cancel:string;
    func_sure:Function;  //点击确认函数
    func_cancel:Function;
    target:any;
    show_close:boolean; //是否显示关闭按钮
    constructor(desc:string, mode:number){
        this.desc = desc;
        this.mode = mode;
    }
}
@ccclass('MsgBox')
export class MsgBox extends Dialog {

    public cache_mode: number = 1;

    public lb_title: Label = null;
    public rich_desc: RichText = null;

    public node_close: Node = null;
    
    public node_cancel: Node = null;
    public lb_cancel: Label = null;

    public node_sure: Node = null;
    public lb_sure: Label = null;


    private data: MsgInfo = null;

    public onLoad(): void {
        UtilsPanel.getAllNeedCom(this, this.node, true);
        UtilsPanel.addBtnEvent(this.node_close, this.hide, this);
        UtilsPanel.addBtnEvent(this.node_cancel, this.onCancel, this);
        UtilsPanel.addBtnEvent(this.node_sure, this.onSure, this);
    }
    public init(data: MsgInfo) {
        this.data = data
        this.rich_desc.string = data.desc;
        this.lb_title.string = data.title || ""; 
        this.lb_sure.string = data.txt_sure || "确认";
        this.node_close.active = data.show_close

        let y=this.node_sure.position.y
        if (data.mode == 1) {
            this.node_cancel.active = false;
            this.node_sure.setPosition(12, y);
        } 
        else if (data.mode == 2) {
            this.node_cancel.active = true;
            this.node_sure.setPosition(140, y);
            this.lb_cancel.string = data.txt_cancel || "取消";
        }
    }

    public onSure() {
        this.hide();
        if (this.data.func_sure) this.data.func_sure.call(this.data.target);
    }

    public onCancel() {
        this.hide();
        if (this.data.func_cancel) this.data.func_cancel.call(this.data.target);
    }

}
