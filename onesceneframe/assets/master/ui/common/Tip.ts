import { _decorator, Label, Sprite, SpriteFrame } from 'cc';
import { Dialog } from '../../script/base/Dialog';
import { UtilsPanel } from '../../script/utils/UtilsPanel';
const { ccclass, property } = _decorator;


@ccclass('Tip')
export class Tip extends Dialog {
    public auto_mask = 0;
    public cache_mode: number = 2;
    
    private lb_text: Label = null;

    protected onLoad(){
        UtilsPanel.getAllNeedCom(this, this.node);
    }
    public init(data: string) {
        this.lb_text.string = data;
        this.scheduleOnce(this.hide, 2);
    }
    public updateData(data: any): void {
        this.lb_text.string = data;
        this.unschedule(this.hide);
        this.scheduleOnce(this.hide, 2);
    }
}
