
import { _decorator, Component, Node, UITransform, view, sp } from 'cc';

const { ccclass, property } = _decorator;

@ccclass('Loading')
export class Loading extends Component {

    public node_loading: Node | null = null;
    
    private timer = 0;
    private to_show = false;
    private c_time = 10;
    private s_time = 0.2;

    public init() {
        this.node_loading = this.node.children[1];
    }

    public show(show_time: number, conti_time: number) {
        this.timer = 0;
        this.c_time = conti_time;
        this.s_time = show_time;
        this.to_show = show_time > -1;
    }
    public update(dt: number) {
        this.timer += dt;
        if(this.to_show && this.timer > this.s_time){
            this.to_show = false;
            this.node_loading.active = true;
        }
        if (this.c_time > -1 && this.timer > this.c_time) {
            this.node.active = false;
        }
    }
}


