import { _decorator, assetManager, Component, Node } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('Main')
export class Main extends Component {
    public static info: CallInfo[] = [null, null];

    private load_index = 0;
    private bundles = ["texture", "master"];
    protected onLoad(): void {
        console.time("load bundles:");
        this.loadBundle();
    }
    private loadBundle(){
        assetManager.loadBundle(this.bundles[this.load_index], ()=>{
            this.load_index++;
            if(this.load_index == this.bundles.length)this.begin();
            else this.loadBundle();
        })
    }
    private begin(){
        console.timeEnd("load bundles:");
        let info = Main.info;
        let node_resi = info[0][0].call(info[0][1]);
        info[1][0].call(info[1][1], node_resi);
        this.node.destroy();
    }

}


