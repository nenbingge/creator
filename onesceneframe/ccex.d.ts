declare module "cc"{
    interface Node{
         autoSort():void;
         sortByPosZ():void;
         toX(x:number):void;
         addX(add: number):void;
         toY(y:number):void;
         addY(add: number):void;
         toZ(z: number, need_sort:boolean):void;
         toXYByArr(pos:Array<number>):void;
         to0():void;
         toScaleX(x: number):void;
         toScaleY(y: number):void;
         toScaleXY(scale: number):void;
         toOpacity(opacity: number):void;
    }
    interface Tween<T>{
        toPro(duration: number, props: any, opts?: ITweenOption): Tween<T>;
        byPro(duration: number, props: any, opts?: ITweenOption): Tween<T>;
        bezierTo(duration: number, end_pos: Vec3 | Vec2, c1: Vec3 | Vec2, c2: Vec3 | Vec2, opts?: ITweenOption): Tween<T>;
        bezierToByArr(duration: number, end_pos: Array<number>, c: Array<number>, opts?: ITweenOption): Tween<T>;
    }
}
declare type CallInfo = [Function, any];

interface DataView{
    setUtf8(offset:number, str:string): void;
    getUtf8(offset:number, len:number): string;
}