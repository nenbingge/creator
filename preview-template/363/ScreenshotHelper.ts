
import { _decorator, Node, Camera, RenderTexture, director, view, Size, sys, find, Director } from 'cc';
import { JSB, PREVIEW } from 'cc/env';
import { Canvas2Image } from "./Canvas2Image";
import { downImgByCanvas } from './Canvas2Image2';


export class ScreenshotHelper{
    private static rt: RenderTexture = null;
    private static buffer: ArrayBufferView = null;
    private static camera: Camera = null;
    private static is_wait = false;
    private static size: Size = null;

    public static screenShot(){
        if(this.is_wait)return;
        this.is_wait = true;
        this.getCamera();
    }
    private static getCamera(){
        this.rt = new RenderTexture();
        let size = view.getVisibleSize();
        this.size = new Size(Math.round(size.width), Math.round(size.height));
        this.rt.reset(this.size)
        this.camera = find("Canvas").getComponentInChildren(Camera);
        this.camera.targetTexture = this.rt;
        console.log(this.camera)
        director.on(Director.EVENT_END_FRAME, this.getBuffer, this, true);
    }
    private static getBuffer() {
        // var width = this.targetNode.getComponent(UITransform).width;
        // var height = this.targetNode.getComponent(UITransform).height;
        // var worldPos = this.targetNode.getWorldPosition()

        let size = this.size;
        console.log("size:",size.width, ":", size.height)
        this.buffer = this.rt.readPixels(0, 0, size.width, size.height);
        // console.log("bbb", this.buffer)
        
        this.savaAsImage(size.width, size.height, this.buffer);
        this.camera.targetTexture = null;
        this.is_wait = false;
    }
    private static savaAsImage(width: number, height: number, arrayBuffer: ArrayBufferView) {
        if (sys.isBrowser) {
            let canvas = document.createElement('canvas');
            canvas.width = width;
            canvas.height = height;
            let ctx = canvas.getContext('2d')!;
            let rowBytes = width * 4;
            for (let row = 0; row < height; row++) {
                let sRow = height - 1 - row;
                let imageData = ctx.createImageData(width, 1);
                let start = sRow * width * 4;
                for (let i = 0; i < rowBytes; i++) {
                    imageData.data[i] = arrayBuffer[start + i];
                }
                ctx.putImageData(imageData, 0, row);
            }
            downImgByCanvas(canvas, width, height, "png", "aaa");
        } 
        // else if (sys.isNative) {
        //     let filePath = jsb.fileUtils.getWritablePath() + 'render_to_sprite_image.png';
        //     //@ts-ignore
        //     if (jsb.saveImageData) {
        //         //@ts-ignore
        //         jsb.saveImageData(this._buffer, width, height, filePath).then(()=>{
        //             assetManager.loadRemote<ImageAsset>(filePath, (err, imageAsset)=> {
        //                 if (err) {
        //                     console.log("show image error")
        //                 } else {
        //                     var newNode = instantiate(this.targetNode);
        //                     newNode.setPosition(new Vec3(-newNode.position.x, newNode.position.y, newNode.position.z));
        //                     this.targetNode.parent.addChild(newNode);
                            
        //                     const spriteFrame = new SpriteFrame();
        //                     const texture = new Texture2D();
        //                     texture.image = imageAsset;
        //                     spriteFrame.texture = texture;
        //                     newNode.getComponent(Sprite).spriteFrame = spriteFrame;
        //                     spriteFrame.packable = false;
        //                     spriteFrame.flipUVY = true;
        //                     if (sys.isNative && (sys.os === sys.OS.IOS || sys.os === sys.OS.OSX)) {
        //                         spriteFrame.flipUVY = false;
        //                     }
    
        //                     this.tips.string = `成功保存在设备目录并加载成功: ${filePath}`;
        //                 }
        //             });
        //             log("save image data success, file: " + filePath);
        //             this.tips.string = `成功保存在设备目录: ${filePath}`;
        //         }).catch(()=>{
        //             error("save image data failed!");
        //             this.tips.string = `保存图片失败`;
        //         });
        //     }
        // } 
        // else if (sys.platform === sys.Platform.WECHAT_GAME) {
        //     if (!canvas) {
        //         //@ts-ignore
        //         canvas = wx.createCanvas();
        //         canvas.width = width;
        //         canvas.height = height;
        //     } else {
        //         this.clearCanvas();
        //     }
        //     var ctx = canvas.getContext('2d');
    
        //     var rowBytes = width * 4;
    
        //     for (var row = 0; row < height; row++) {
        //         var sRow = height - 1 - row;
        //         var imageData = ctx.createImageData(width, 1);
        //         var start = sRow * width * 4;
    
        //         for (var i = 0; i < rowBytes; i++) {
        //             imageData.data[i] = arrayBuffer[start + i];
        //         }
    
        //         ctx.putImageData(imageData, 0, row);
        //     }
        //     //@ts-ignore
        //     canvas.toTempFilePath({
        //         x: 0,
        //         y: 0,
        //         width: canvas.width,
        //         height: canvas.height,
        //         destWidth: canvas.width,
        //         destHeight: canvas.height,
        //         fileType: "png",
        //         success: (res) =>{
        //             //@ts-ignore
        //             wx.showToast({
        //                 title: "截图成功"
        //             });
        //             this.tips.string = `截图成功`;
        //             //@ts-ignore
        //             wx.saveImageToPhotosAlbum({
        //                 filePath: res.tempFilePath,
        //                 success: (res)=> {
        //                     //@ts-ignore              
        //                     wx.showToast({
        //                         title: "成功保存到设备相册",
        //                     });
        //                     this.tips.string = `成功保存在设备目录: ${res.tempFilePath}`;
        //                 },
        //                 fail: ()=> {
        //                     this.tips.string = `保存图片失败`;
        //                 }
        //             })
        //         },
        //         fail: ()=> {
        //             //@ts-ignore
        //             wx.showToast({
        //                 title: "截图失败"
        //             });
        //             this.tips.string = `截图失败`;
        //         }
        //     })
        // }
    }
    
}
