
let rt = null;
let buffer = null;
let camera = null;
let is_wait = false;
let real_size = null;


function screenShot() {
	if (is_wait)
		return;
	is_wait = true;
	getCamera();
}
function getCamera() {
	rt = new cc.RenderTexture();
	let size = cc.view.getVisibleSize();
	real_size = new cc.Size(Math.round(size.width), Math.round(size.height));
	rt.reset(real_size);
	camera = (0, cc.find)("Canvas").getComponentInChildren(cc.Camera);
	camera.targetTexture = rt;
	console.log(camera);
	cc.director.on(cc.Director.EVENT_END_FRAME, getBuffer, this, true);
}
function getBuffer() {
	// var width = targetNode.getComponent(UITransform).width;
	// var height = targetNode.getComponent(UITransform).height;
	// var worldPos = targetNode.getWorldPosition()
	let size = real_size;
	console.log("size:", size.width, ":", size.height);
	buffer = rt.readPixels(0, 0, size.width, size.height);
	// console.log("bbb", buffer)
	savaAsImage(size.width, size.height, buffer);
	camera.targetTexture = null;
	is_wait = false;
}
function savaAsImage(width, height, arrayBuffer) {
	if (cc.sys.isBrowser) {
		let canvas = document.createElement('canvas');
		canvas.width = width;
		canvas.height = height;
		let ctx = canvas.getContext('2d');
		let rowBytes = width * 4;
		for (let row = 0; row < height; row++) {
			let sRow = height - 1 - row;
			let imageData = ctx.createImageData(width, 1);
			let start = sRow * width * 4;
			for (let i = 0; i < rowBytes; i++) {
				imageData.data[i] = arrayBuffer[start + i];
			}
			ctx.putImageData(imageData, 0, row);
		}
		window.cocos_util.downImgByCanvas(canvas, width, height, "png", "aaa");
	}
	// else if (sys.isNative) {
	//     let filePath = jsb.fileUtils.getWritablePath() + 'render_to_sprite_image.png';
	//     //@ts-ignore
	//     if (jsb.saveImageData) {
	//         //@ts-ignore
	//         jsb.saveImageData(_buffer, width, height, filePath).then(()=>{
	//             assetManager.loadRemote<ImageAsset>(filePath, (err, imageAsset)=> {
	//                 if (err) {
	//                     console.log("show image error")
	//                 } else {
	//                     var newNode = instantiate(targetNode);
	//                     newNode.setPosition(new Vec3(-newNode.position.x, newNode.position.y, newNode.position.z));
	//                     targetNode.parent.addChild(newNode);
	//                     const spriteFrame = new SpriteFrame();
	//                     const texture = new Texture2D();
	//                     texture.image = imageAsset;
	//                     spriteFrame.texture = texture;
	//                     newNode.getComponent(Sprite).spriteFrame = spriteFrame;
	//                     spriteFrame.packable = false;
	//                     spriteFrame.flipUVY = true;
	//                     if (sys.isNative && (sys.os === sys.OS.IOS || sys.os === sys.OS.OSX)) {
	//                         spriteFrame.flipUVY = false;
	//                     }
	//                     tips.string = `成功保存在设备目录并加载成功: ${filePath}`;
	//                 }
	//             });
	//             log("save image data success, file: " + filePath);
	//             tips.string = `成功保存在设备目录: ${filePath}`;
	//         }).catch(()=>{
	//             error("save image data failed!");
	//             tips.string = `保存图片失败`;
	//         });
	//     }
	// } 
	// else if (sys.platform === sys.Platform.WECHAT_GAME) {
	//     if (!canvas) {
	//         //@ts-ignore
	//         canvas = wx.createCanvas();
	//         canvas.width = width;
	//         canvas.height = height;
	//     } else {
	//         clearCanvas();
	//     }
	//     var ctx = canvas.getContext('2d');
	//     var rowBytes = width * 4;
	//     for (var row = 0; row < height; row++) {
	//         var sRow = height - 1 - row;
	//         var imageData = ctx.createImageData(width, 1);
	//         var start = sRow * width * 4;
	//         for (var i = 0; i < rowBytes; i++) {
	//             imageData.data[i] = arrayBuffer[start + i];
	//         }
	//         ctx.putImageData(imageData, 0, row);
	//     }
	//     //@ts-ignore
	//     canvas.toTempFilePath({
	//         x: 0,
	//         y: 0,
	//         width: canvas.width,
	//         height: canvas.height,
	//         destWidth: canvas.width,
	//         destHeight: canvas.height,
	//         fileType: "png",
	//         success: (res) =>{
	//             //@ts-ignore
	//             wx.showToast({
	//                 title: "截图成功"
	//             });
	//             tips.string = `截图成功`;
	//             //@ts-ignore
	//             wx.saveImageToPhotosAlbum({
	//                 filePath: res.tempFilePath,
	//                 success: (res)=> {
	//                     //@ts-ignore              
	//                     wx.showToast({
	//                         title: "成功保存到设备相册",
	//                     });
	//                     tips.string = `成功保存在设备目录: ${res.tempFilePath}`;
	//                 },
	//                 fail: ()=> {
	//                     tips.string = `保存图片失败`;
	//                 }
	//             })
	//         },
	//         fail: ()=> {
	//             //@ts-ignore
	//             wx.showToast({
	//                 title: "截图失败"
	//             });
	//             tips.string = `截图失败`;
	//         }
	//     })
	// }
}

window.cocos_util.screenShot = screenShot;

