import { _decorator, Component, EventTouch, js, macro, Node, UITransform, Vec3 } from 'cc';
import { PSEle } from './PSEle';
import { GameConst } from '../../master/script/game/GameConst';
import { PSRes } from './PSRes';
import { PSConst, PSState } from './PSConst';
import { UtilsPanel } from '../../master/script/util/UtilsPanel';
import { UtilsCommon } from '../../master/script/util/UtilsCommon';
const { ccclass, property } = _decorator;

const MaxCount = 10;
const OneHeight = 64;
const HalfHeight = 32;
const Tmp_V3 = new Vec3();

@ccclass('PSMap')
export class PSMap extends Component {
    private uitrans: UITransform = null;

    private eles: Array<Array<PSEle>> = null;
    private cur_row = 0;
    private cur_col = 0;
    private beign_x = 0;
    private offset_x:Array<number> = null;
    private offset_y:Array<number> = null;
    private state = PSState.Play;
    private every_count: Array<number> = null;
    private need_move: Array<boolean> = null;
    protected onLoad(): void {
        this.uitrans = this.node.getComponent(UITransform);
        this.eles = new Array(MaxCount);
        this.offset_x = new Array(MaxCount);
        this.offset_y = new Array(MaxCount);
        this.need_move = new Array(MaxCount).fill(false);
        this.every_count = new Array(MaxCount);
        for(let i = 0; i < MaxCount; i++){
            this.eles[i] = new Array(MaxCount).fill(null);
        }
        UtilsPanel.addBtnEvent2(this.node, "onClick", this, "").transition = 0;
        GameConst.getOffsetArr2(MaxCount, HalfHeight, OneHeight, this.offset_y);
    }

    public init(row:number, col:number){
        this.cur_col = col;
        this.cur_row = row;
        this.beign_x = (720 - OneHeight * row) / 2;
        let begin = this.beign_x + HalfHeight;
        GameConst.getOffsetArr2(row, begin, OneHeight, this.offset_x);
        this.uitrans.height = col * OneHeight;
        let ele = null;
        let type = 0;
        for(let i = 0; i < row; i++){
            for(let j = 0; j < col; j++){
                ele = PSRes.instance.pool_ele.alloc();
                ele.node.setParent(this.node);
                ele.node.toXY(this.offset_x[i], this.offset_y[j]);
                type = Math.floor(Math.random() * PSConst.Ele_Count);
                ele.init(type);
                this.eles[i][j] = ele;
            }
            this.every_count[i] = col;
        }
        // for(let i = 0 ;i < col; i++){
        //     this.eles[0][i].init(4);
        //     this.eles[4][i].init(4);
        // }
    }
    public resetState(){
        this.state = PSState.Play;
    }
    private align(){
        let row = this.cur_row;
        this.beign_x = (720 - OneHeight * row) / 2;
        let begin = this.beign_x + HalfHeight;
        GameConst.getOffsetArr2(row, begin, OneHeight, this.offset_x);
        let ele = null;
        for(let i = 0; i < row; i++){
            for(let j = 0; j < this.cur_col; j++){
                ele = this.eles[i][j];
                if(!ele)break;
                ele.move(this.offset_x[i], this.offset_y[j]);
            }
        }
    }
    public clear(){
        for(let i = 0 ; i < this.cur_row; i++){
            PSRes.instance.pool_ele.recyAll2(this.eles[i]);
        }
    }
    private getNears(p:Array<number>, type:number, arr_open:any, set_close:Set<number>){
        let arr = [[p[0] + 1, p[1]], [p[0] - 1, p[1]], [p[0], p[1]+1], [p[0], p[1] - 1]];
        for(let i = 0 ; i < 4 ; i++){
            if(arr[i][0] < 0 || arr[i][0] >= this.cur_row || arr[i][1] < 0 || arr[i][1] >= this.cur_col)continue;
            if(this.eles[arr[i][0]][arr[i][1]]?.type == type){
                // if(!set_close.has(this.p2Key(arr[i])))
                arr_open.push(arr[i]); 
            }
        }
    }
    private p2Key(arr:Array<number>){return arr[0] * 100 + arr[1];};
    private checkLinkCount(row:number, col:number){
        let type = this.eles[row][col].type;
        let result = [];
        let arr_open = [];
        let set_close = new Set<number>();
        arr_open.push([row, col]);
        let temp = null;
        while(arr_open.length > 0){
            temp = arr_open.pop();
            if(set_close.has(this.p2Key(temp)))continue;
            result.push(temp);
            set_close.add(this.p2Key(temp));
            this.getNears(temp, type, arr_open, set_close);
        }
        return result;
    }
    private checkEmpty(){
        let i = 0;
        let have_empty = false;
        while(i < this.cur_row){
            if(this.every_count[i] == 0){
                UtilsCommon.removeArrAt(this.every_count, i);
                let temp = this.eles.splice(i, 1)[0];
                this.eles.splice(this.cur_row - 1, 0, temp);
                this.cur_row--;
                have_empty = true;
            }else{
                i++;
            }
        }
        return have_empty;
    }
    private checkDrop(){
        let total_move = 0 , col_move = 0;
        for(let i = 0; i < this.cur_row; i++){
            if(!this.need_move[i])continue;
            col_move = 0;
            for(let j = 1; j < this.cur_col; j++){
                if(this.eles[i][j] && !this.eles[i][j-1]){
                    this.eles[i][j].move(this.offset_x[i], this.offset_y[j - 1]);
                    this.eles[i][j - 1] = this.eles[i][j];
                    this.eles[i][j] = null;
                    col_move++;
                }
            }
            if(col_move == 0)this.need_move[i] = false;
            else total_move += col_move;
        }
        if(total_move == 0){
            let have_empty = this.checkEmpty();
            if(have_empty){
                this.state = PSState.Move;
                this.align();
                this.scheduleOnce(this.resetState, 0.1);
            }
            else{
                this.state = PSState.Play;
                this.unschedule(this.checkDrop);
            }
        }
    }
    public onClick(e:EventTouch){
        if(this.state != PSState.Play)return;
        e.getUILocation(Tmp_V3 as any);
        this.uitrans.convertToNodeSpaceAR(Tmp_V3, Tmp_V3);
        let row = Math.floor((Tmp_V3.x - this.beign_x) / OneHeight);
        let col = Math.floor(Tmp_V3.y / OneHeight);
        if(!this.eles[row][col])return;
        let result = this.checkLinkCount(row, col);
        // console.log(result);
        if(result.length < 2)return;
        for(let i = 0; i < result.length; i++){
            PSRes.instance.pool_ele.recycle(this.eles[result[i][0]][result[i][1]]);
            this.every_count[result[i][0]]--;
            this.eles[result[i][0]][result[i][1]] = null;
            this.need_move[result[i][0]] = true;
        }
        this.state = PSState.Drop;
        this.schedule(this.checkDrop, 0.1, macro.REPEAT_FOREVER, 0);
    }

}


