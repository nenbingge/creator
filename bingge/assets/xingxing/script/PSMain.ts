import { _decorator, Component, director, find, Node } from 'cc';
import { UtilsPanel } from '../../master/script/util/UtilsPanel';
import { PSMap } from './PSMap';
const { ccclass, property } = _decorator;

@ccclass('PSMain')
export class PSMain extends Component {
    private map: PSMap = null;

    protected onLoad(): void {
        UtilsPanel.getAllNeedCom(this, find("Canvas"));
    }
    protected start(): void {
        this.map.init(8, 10);
    }
    private onBackClick(){
		director.loadScene("lobby");
	}
}


