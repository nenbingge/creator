import { _decorator, Component, Sprite, Label, Tween, log, tween } from "cc";
import { UtilsPanel } from "../../master/script/util/UtilsPanel";


const { ccclass, property } = _decorator;

@ccclass
export default class HandCard extends Component {
    public sp_bg: Sprite = null;
    public sp_point: Sprite = null;
    public sp_xing0: Sprite = null;
    public sp_xing1: Sprite = null;
    public sp_mask: Sprite = null;
    
    public node_ghost: Node = null;
    public sp_corner: Sprite
    public lb_index: Label
    //components
    private have_init = false;
    private tween: Tween = null;
    public point = -1;
    private type = 0;
    public show_point = 0;

    private have_event = false;
    private can_move = false;
    private _pressed = false;
    private have_move = false;
    private onlyClick = false
    private pressTween = null

    public origin_pos = [0, 0];
    public origin_scale = 1
    public origin_angle = 0

    public index = -1;

    public reset() {
        this.unRegisterTouch();
        this.stopTween()
        this.can_move = false;
        this.onlyClick = false
        this.node.zIndex = 0;
        if (!this.have_init) return;
        this.node_ghost.active = false;
        this.sp_corner.node.active = false
        this.node.angle = 0
        this.sp_mask.node.active = false
        this.node.opacity = 255
    }
    public onLoad() {
        UtilsPanel.getAllNeedCom(this, this.node);
        this.showMask(false)
        this.have_init = true;
    }

    public start(): void {
        this.origin_scale = this.node.scale
    }

    public setIndex(index: number) {
        this.index = index;

        if (this.lb_index)
            this.lb_index.string = index + ''
    }

    /**
     * @param onlyClick 只能点击不能拖动
     */
    public setCanMove(state: boolean, onlyClick = false) {
        this.can_move = state;
        this.onlyClick = onlyClick
        if (state){
            this.registerTouch();
        }
        else{
            this._pressed=false
        }
    }

    public setPoint(point: number) {
        this.point = point;
        this.type = point >> 4;
        this.show_point = point & 0xf;
        // log("point:"+point+" type:"+this.type+" showpoint:"+this.show_point);
    }

    public forceGhost() {
        this.node_ghost.active = true;
    }
    public showGhost(ghost: number) {
        if (this.show_point == 15) this.node_ghost.active = false;
        else this.node_ghost.active = this.show_point == (ghost & 15);
    }

    public showCorner(show = true, index) {
        this.sp_corner.node.active = show
        if (index > 0) {
            this.sp_corner.spriteFrame = ResManager.getSfByAtlas("cards", 'tiao' + index);
        }
    }

    public registerTouch() {
        if (this.have_event) return;
        this.have_event = true;
        this.node.on(Node.EventType.TOUCH_START, this._onTouchBegin, this);
        this.node.on(Node.EventType.TOUCH_MOVE, this._onTouchMove, this);
        this.node.on(Node.EventType.TOUCH_END, this._onTouchEnd, this);
        this.node.on(Node.EventType.TOUCH_CANCEL, this._onTouchCancel, this);
    }
    public unRegisterTouch() {
        if (!this.have_event) return;
        this.have_event = false;
        this.node.off(Node.EventType.TOUCH_START, this._onTouchBegin, this);
        this.node.off(Node.EventType.TOUCH_MOVE, this._onTouchMove, this);
        this.node.off(Node.EventType.TOUCH_END, this._onTouchEnd, this);
        this.node.off(Node.EventType.TOUCH_CANCEL, this._onTouchCancel, this);
    }

    public _onTouchBegin(e: any) {
        e.stopPropagation();
        // log("1:",e);
        if (!this.can_move) {
            log('card cant move')
            return;
        }
        // log("2:");
        this._pressed = true;
        this.have_move = false;
        GameInvoker.call(HandEvent.FirstTouch, this.index, this);


        //this.startLongPress()
    }

    startLongPress() {
        this.pressTween = tween(this.node).delay(0.1).call(_ => {
            // this.setOriPos(null,this.node.y+25,this.origin_scale,-10)
            // this.align(null,0.1)
            GameInvoker.call(HandEvent.MoveBegin, this.index, this);
        }).start()
    }

    stopLongPress() {
        if (this.pressTween) {
            this.pressTween.stop()
            this.pressTween = null
        }
    }

    public _onTouchMove(e: any) {
        // log("3:",e);
        if (!this._pressed) return;
        if (this.onlyClick) return

        this.stopLongPress()
        if (!this.have_move) {//处理单击的情况
            let touch = e.touch;

            let deltaMove = touch.getLocation().sub(touch.getStartLocation());
            if (deltaMove.mag() > 1) {

                this.have_move = true;
                GameInvoker.call(HandEvent.MoveBegin, this.index, this);
            }
        }
        else {
            this.stopTween()
            this.node.x += e.getDeltaX();
            this.node.y += e.getDeltaY();
            //if(this.node.y > 266)this.node.y = 266;
            //if(this.node.y < -264)this.node.y = -264;
            GameInvoker.call(HandEvent.MoveCard, this.node.x, this.node.y);
        }
        e.stopPropagation();
    }
    public _onTouchEnd(e: any) {
        if (!this._pressed) return;
        // log("4:",e);
        this.stopLongPress()

        log("_onTouchEnd", this.index, "show_point", this.show_point)

        this._pressed = false;
        if (this.have_move) {
            GameInvoker.call(HandEvent.MoveEnd, this.node.x, this.node.y);
        }
        else {
            log("clickcard index=", this.index)
            GameInvoker.call(HandEvent.ClickCard, this.index);
        }


        e.stopPropagation();
    }

    public _onTouchCancel(e: any) {
        if (!this._pressed) return;
        this.stopLongPress()

        this._pressed = false;
        log("touch cancel!!!");
        if (this.have_move) {
            GameInvoker.call(HandEvent.MoveEnd, this.node.x, this.node.y);
        }
        else {
            GameInvoker.call(HandEvent.ClickCard, this.index);
        }
        this._pressed = false;
        e.stopPropagation();
    }

    public changeGhostPos() {
        this.node_ghost.x = 37;
        this.node_ghost.y = 67;
        this.node_ghost.angle = 270;
    }

    public setShow(show: boolean) {
        if (this.point == -1) show = false;
        let bg_path = show ? "bg1" : "bg0";
        this.sp_bg.spriteFrame = ResManager.getSfByAtlas("cards", bg_path);
        this.sp_point.node.active = this.sp_xing0.node.active = this.sp_xing1.node.active = show;
        if (show) {
            let type = this.type;
            let show_point = this.show_point;
            let is_red = type % 2 == 0;
            let is_normal = type < 4;
            this.sp_xing0.node.active = is_normal;
            if (is_normal)
                this.sp_xing0.spriteFrame = ResManager.getSfByAtlas("cards", "xing_s" + type);
            let prefix = is_red ? "r" : "b";
            this.sp_point.spriteFrame = ResManager.getSfByAtlas("cards", prefix + show_point);
            if (show_point > 10) {
                prefix = is_red ? "rr" : "bb";
                this.sp_xing1.node.x = 20;
                this.sp_xing1.node.y = -17;
                this.sp_xing1.spriteFrame = ResManager.getSfByAtlas("cards", prefix + show_point);
            } else {
                this.sp_xing1.node.x = 20;
                this.sp_xing1.node.y = -40;
                this.sp_xing1.spriteFrame = ResManager.getSfByAtlas("cards", "xing_b" + type);
            }
        }
    }

    public setOriPos(x: number, y: number, scale = null, angle = null) {
        let isSame = this.origin_pos[0] == x && this.origin_pos[1] == y

        if (x != null) {
            this.origin_pos[0] = x;

        }
        this.origin_pos[1] = y;
        if (scale != null) {
            this.origin_scale = scale
        }
        if (angle != null) {
            this.origin_angle = angle
        }
        return isSame
    }


    public moveToOrigin() {
        this.node.x = this.origin_pos[0];
        this.node.y = this.origin_pos[1];
    }

    public align(func: Function, speed = 0.2) {
        this.stopTween()
        //this.moveToOrigin()
        this.tween = tween(this.node)
            .to(speed, {
                x: this.origin_pos[0],
                y: this.origin_pos[1],
                scale: this.origin_scale,
                angle: this.origin_angle
            })
            .call(_ => {
                this.tween = null;
                if (func) func();
            })
            .start();
    }

    public stopTween() {
        if (this.tween) this.tween.stop()
        this.node.stopAllActions()

    }

    public move(time: number, data: any, func: Function) {
        this.stopTween()
        let self = this;
        this.tween = tween(this.node)
            .to(time, data, { easing: 'quadInOut' })
            .call(function () {
                if (func) func();
                self.tween = null;
            })
            .start();
    }
    public ghostMove(x: number, y: number) {
        this.stopTween()
        let self = this;
        this.tween = tween(this.node)
            .to(0.2, { x: x, y: y }, { easing: 'quadInOut' })
            .to(0.2, { angle: 90 })
            .call(function () {
                self.tween = null;
            })
            .start();
    }
    public flip() {
        this.stopTween()
        let self = this;
        let sx = this.node.scaleX
        this.tween = tween(this.node)
            .to(0.1, { scaleX: 0 })
            .call(function () {
                self.setShow(true);
            })
            .to(0.1, { scaleX: sx })
            .start();
    }

    isShowMask() {
        return this.sp_mask.node.active
    }
    showMask(b) {
        this.sp_mask.node.active = b
        if (b) {
            this.sp_mask.spriteFrame = ResManager.getSfByAtlas("cards", "img_mask")
        }
    }

    cancelDrag() {
        this.have_move = false
        this._pressed = false
    }

    setAngle(angle) {
        this.node.angle = this.origin_angle = angle
    }
}
