
import { _decorator, Component, EventTouch, Label, macro, MathBase, Node, NodeEventType, UITransform, Vec3 } from 'cc';
import { MTEle } from './MTEle';
import { MTRes } from './MTRes';
import { GameConst } from '../../master/script/game/GameConst';
import { MapState, MTConst } from './MTConst';
import { UtilsPanel } from '../../master/script/util/UtilsPanel';
import { MTFloor } from './MTFloor';
const { ccclass } = _decorator;

const MaxCount = 10;
const OneHeight = 64;

const Tmp_V3 = new Vec3();

@ccclass('MTMap')
export class MTMap extends Component {

	private node_floor: Node = null;
	private uitrans_ele: UITransform = null;
    private node_barrier: Node = null;
	private node_eff: Node = null;

    private floors: MTFloor[][] = null;
    private eles: MTEle[][] = null;
    private offset_x: Array<number> = null;
    private offset_y: Array<number> = null;
    private flag_remove: boolean[] = null;
    private cur_row = 0;
    private cur_col = 0;
    private state = MapState.NoUse;
    //select_info
    private s_i = [0,0,0,0];

    protected onLoad(): void {
        UtilsPanel.getAllNeedCom(this, this.node);
        this.eles = new Array(MaxCount);
        this.floors = new Array(MaxCount);
        this.offset_x = new Array(MaxCount);
        this.offset_y = new Array(MaxCount);
        this.flag_remove = new Array(MaxCount).fill(false);
        for(let i = 0 ; i < MaxCount; i++){
            this.eles[i] = [];
            this.floors[i] = [];
        }
        this.registerTouch();
    }
    public init(row:number, col:number){
        this.clear();
        this.cur_row = row;
        this.cur_col = col;
        let type = 0;
        let ele = null, floor = null;
        GameConst.getOffsetArr2(row, OneHeight / 2, OneHeight, this.offset_x);
        GameConst.getOffsetArr2(col, OneHeight / 2, OneHeight, this.offset_y);
        this.uitrans_ele.setContentSize(row * OneHeight, col * OneHeight);
        this.node.toXY(-row * OneHeight / 2, -col * OneHeight / 2);
        for(let i = 0; i < row; i++){
            for(let j = 0; j < col; j++){
                ele = MTRes.instance.pool_ele.alloc();
                ele.node.setParent(this.uitrans_ele.node);
                ele.node.toXY(this.offset_x[i], this.offset_y[j]);
                type = Math.floor(Math.random() * MTConst.Ele_Count);
                ele.init(type);
                this.eles[i].push(ele);

                floor = MTRes.instance.pool_floor.alloc();
                floor.node.setParent(this.node_floor);
                floor.node.toXY(this.offset_x[i], this.offset_y[j]);
                let index = Math.random() > 0.5 ? 1 : 0;
                floor.init(index);
                this.floors[i].push(floor);
            }
        }
        this.scheduleOnce(this.checkRemove,1);
    }
    private registerTouch() {
        this.uitrans_ele.node.on(NodeEventType.TOUCH_START, this._onTouchBegin, this);
        this.uitrans_ele.node.on(NodeEventType.TOUCH_END, this._onTouchEnd, this);
        this.uitrans_ele.node.on(NodeEventType.TOUCH_CANCEL, this._onTouchEnd, this);
    }
    public showBoom(row: number, col: number){
        if(this.floors[row][col].type == 1)this.floors[row][col].init(0);
        let eff = MTRes.instance.pool_eff.alloc();
        eff.node.setParent(this.node_eff);
        eff.node.toXY(this.offset_x[row], this.offset_y[col]);
        eff.showBoom();
    }
    private geneOneEle(row:number, col:number){
        let ele = MTRes.instance.pool_ele.alloc();
        ele.node.setParent(this.uitrans_ele.node);
        let type = Math.floor(Math.random() * MTConst.Ele_Count);
        ele.init(type);
        this.eles[row][col] = ele;
        return ele;
    }
    public clear(){
        for(let i = 0 ; i < this.cur_row; i++){
            MTRes.instance.pool_ele.recyAll(this.eles[i]);
            MTRes.instance.pool_floor.recyAll(this.floors[i]);
        }
    }
    private flagRemove(ele:MTEle){
        ele.remove_flag = true;
    }
    private checkRemove(){
        let cur_type = 0;
        let temp_arr = [];
        for(let i = 0 ; i < this.cur_row ; i++){
            for(let j = 0 ; j < this.cur_col; j++){
                if(this.eles[i][j].type != cur_type){
                    if(temp_arr.length > 2){
                        temp_arr.forEach(this.flagRemove);
                    }
                    temp_arr.length = 0;
                    cur_type = this.eles[i][j].type;
                }
                temp_arr.push(this.eles[i][j])
            }
            if(temp_arr.length > 2){
                temp_arr.forEach(this.flagRemove);
            }
            temp_arr.length = 0;
        }
        for(let i = 0 ; i < this.cur_col ; i++){
            for(let j = 0 ; j < this.cur_row; j++){
                if(this.eles[j][i].type != cur_type){
                    if(temp_arr.length > 2){
                        temp_arr.forEach(this.flagRemove);
                    }
                    temp_arr.length = 0;
                    cur_type = this.eles[j][i].type;
                }
                temp_arr.push(this.eles[j][i])
            }
            if(temp_arr.length > 2){
                temp_arr.forEach(this.flagRemove);
            }
            temp_arr.length = 0;
        }
        this.remove();
    }
    private remove(){
        let have_remove = false;
        for(let i = 0 ; i < this.cur_row ; i++){
            for(let j = 0 ; j < this.cur_col; j++){
                if(this.eles[i][j].remove_flag){
                    this.showBoom(i, j);
                    MTRes.instance.pool_ele.recycle(this.eles[i][j]);
                    this.eles[i][j] = null;
                    this.flag_remove[i] = true;
                    have_remove = true;
                }
            }
        }
        if(have_remove){
            this.state = MapState.MoveEle;
            this.schedule(this.checkEmpty, 0.1, macro.REPEAT_FOREVER, 0.6);
        }
        else if(this.state == MapState.Exchange1){
            this.state = MapState.Exchange2;
            let a0 = this.s_i[0], a1 = this.s_i[1];
            this.s_i[0] = this.s_i[2];
            this.s_i[1] = this.s_i[3];
            this.s_i[2] = a0;
            this.s_i[3] = a1;
            this.exchange();
        }
        else{
            this.state = MapState.Idle;
        }
    }
    private checkEmpty(){
        let total = 0, row_move = 0;
        // console.log(this.eles);
        for(let i = 0 ; i < this.cur_row; i++){
            if(!this.flag_remove[i])continue;
            row_move = 0;
            for(let j = 1; j < this.cur_col; j++){
                if(this.eles[i][j] == null)continue;
                if(this.eles[i][j] != null && this.eles[i][j - 1] == null){
                    this.eles[i][j].move(this.offset_x[i], this.offset_y[j - 1]);
                    this.eles[i][j - 1] = this.eles[i][j];
                    this.eles[i][j] = null;
                    row_move++;
                }
            }
            if(this.eles[i][this.cur_col - 1] == null){
                let new_ele = this.geneOneEle(i, this.cur_col - 1);
                new_ele.node.toXY(this.offset_x[i], OneHeight * this.cur_col);
                new_ele.move(this.offset_x[i], this.offset_y[this.cur_col - 1]);
                this.eles[i][this.cur_col - 1] = new_ele;
                row_move++;
            }
            total += row_move;
        }
        if(total == 0){
            this.unschedule(this.checkEmpty);
            this.checkRemove();
        }
    }
    private beginExchange(){
        if(this.s_i[2] < 0)this.s_i[2] = 0;
        else if(this.s_i[2] >= this.cur_row)this.s_i[2] = this.cur_row - 1;
        if(this.s_i[3] < 0)this.s_i[3] = 0;
        else if(this.s_i[3] >= this.cur_col)this.s_i[3] = this.cur_col - 1;
        let offset_h = this.s_i[2] - this.s_i[0];
        let offset_v = this.s_i[3] - this.s_i[1];
        if(offset_h == 0 && offset_v == 0)return;
        if(Math.abs(offset_h) > Math.abs(offset_v)){
            this.s_i[3] = this.s_i[1];
            let add = offset_h > 0 ? 1 : -1;
            this.s_i[2] = this.s_i[0] + add;
        }
        else{
            this.s_i[2] = this.s_i[0];
            let add = offset_v > 0 ? 1 : -1;
            this.s_i[3] = this.s_i[1] + add;
        }
        this.state = MapState.Exchange1;
        this.exchange();
    }
    private exchange(){
        let ele0 = this.eles[this.s_i[0]][this.s_i[1]];
        let ele1 = this.eles[this.s_i[2]][this.s_i[3]];
        ele0.setSelect(false);
        ele0.move(this.offset_x[this.s_i[2]], this.offset_y[this.s_i[3]]);
        ele1.move(this.offset_x[this.s_i[0]], this.offset_y[this.s_i[1]]);
        this.eles[this.s_i[0]][this.s_i[1]] = ele1;
        this.eles[this.s_i[2]][this.s_i[3]] = ele0;
        
        if(this.state == MapState.Exchange2)this.scheduleOnce(this.recoverIdle, 0.2);
        else this.scheduleOnce(this.checkRemove, 0.2);
    }
    private recoverIdle(){
        this.state = MapState.Idle;
    }
    public _onTouchBegin(e: EventTouch) {
        e.propagationStopped = true;
        e.getUILocation(Tmp_V3 as any);
        this.uitrans_ele.convertToNodeSpaceAR(Tmp_V3, Tmp_V3);
        // console.log(Tmp_V3.toString());
        if(this.state == MapState.Idle){
            this.state = MapState.Select;
            this.s_i[0] = Math.floor(Tmp_V3.x / OneHeight);
            this.s_i[1] = Math.floor(Tmp_V3.y / OneHeight);
            this.eles[this.s_i[0]][this.s_i[1]].setSelect(true);
        }
        else if(this.state == MapState.Select){
            this.s_i[2] = Math.floor(Tmp_V3.x / OneHeight);
            this.s_i[3] = Math.floor(Tmp_V3.y / OneHeight);
            
            if(this.s_i[0] == this.s_i[2] && this.s_i[1] == this.s_i[3]){
                this.eles[this.s_i[0]][this.s_i[1]].setSelect(false);
                this.state = MapState.Idle;
            }
            else if((this.s_i[0] == this.s_i[2] && Math.abs(this.s_i[1] - this.s_i[3]) == 1)
             || (this.s_i[1] == this.s_i[3] && Math.abs(this.s_i[0] - this.s_i[2]) == 1)){
                this.beginExchange();
            }
            else{
                this.eles[this.s_i[0]][this.s_i[1]].setSelect(false);
                this.state = MapState.Idle;
            }
        }
    }

    public _onTouchEnd(e: EventTouch) {
        e.propagationStopped = true;
        if(this.state == MapState.Select){
            e.getUILocation(Tmp_V3 as any);
            this.uitrans_ele.convertToNodeSpaceAR(Tmp_V3, Tmp_V3);
            // console.log(Tmp_V3.toString());
            this.s_i[2] = Math.floor(Tmp_V3.x / OneHeight);
            this.s_i[3] = Math.floor(Tmp_V3.y / OneHeight);
            this.beginExchange();
        }
    }

}
