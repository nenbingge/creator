
import { _decorator, Component, Node, Prefab, SpriteAtlas, SpriteFrame } from 'cc';
import { ResPool } from '../../master/script/game/ResPool';
import { MTEle } from './MTEle';
import { MTConst } from './MTConst';
import { MTEff } from './MTEff';
import { MTFloor } from './MTFloor';
const { ccclass, property } = _decorator;

@ccclass('MTRes')
export class MTRes extends Component {
    public static instance: MTRes = null;
    
    @property(Prefab)
    private pre_ele: Prefab = null;
    @property(Prefab)
    private pre_eff: Prefab = null;
    @property(Prefab)
    private pre_floor: Prefab = null;
    @property(SpriteAtlas)
    public atlas_ele: SpriteAtlas = null;

    public pool_ele: ResPool<MTEle> = null;
    public pool_eff: ResPool<MTEff> = null;
    public pool_floor: ResPool<MTFloor> = null;

    public frames_ele: Array<Array<SpriteFrame>> = null;
    public frames_boom: Array<SpriteFrame> = null;
    protected onLoad(): void {
        if(MTRes.instance)console.warn("mtres is have");
        MTRes.instance = this;
        this.pool_ele = new ResPool<MTEle>(MTEle, this.pre_ele, 200);
        this.pool_eff = new ResPool<MTEff>(MTEff, this.pre_eff, 10);
        this.pool_floor = new ResPool<MTFloor>(MTFloor, this.pre_floor, 200);
        this.frames_ele = new Array(MTConst.Ele_Count);
        let frames = null, prefix = "";
        for(let i = 0; i < MTConst.Ele_Count; i++){
            frames = this.frames_ele[i] = [];
            prefix = "e" + i + "_";
            for(let j = 0; j < 9; j++){
                frames.push(this.atlas_ele.getSpriteFrame(prefix + j))
            }
        }
        this.frames_boom = new Array(15);
        for(let i = 0; i < 15; i++){
            this.frames_boom[i] = this.atlas_ele.getSpriteFrame("bomb" + i);
        }
    }
    protected onDestroy(): void {
        this.pool_eff.clear();
        this.pool_ele.clear();
        this.pool_floor.clear();
        MTRes.instance = null;
    }
    public getEle(name:string){
        return this.atlas_ele.getSpriteFrame(name);
    }

}
