
export const Color_Ele = ["blue", "green", "orange", "pink", "purple", "red", "white", "yellow"];
export const Type_Ele = ["bean", "heart", "jelly", "lollipop", "mm", "star", "swirl", "wrappedtrans"];

export enum MapState{
    NoUse,
    Idle,
    Boom,
    MoveEle,
    Select,
    Exchange1,
    Exchange2,
}
export class MTConst{
    public static Ele_Count = 5;
}
