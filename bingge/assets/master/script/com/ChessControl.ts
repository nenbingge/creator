import { _decorator, Component, EventTouch, Node, UITransform, Vec3 } from 'cc';
import { GameConst } from '../game/GameConst';
import { UtilsPanel } from '../util/UtilsPanel';
const { ccclass, property } = _decorator;

const Tmp_V3 = new Vec3();

@ccclass('ChessControl')
export class ChessControl extends Component {
    private uitrans: UITransform = null;

    private arr_x: Array<number> = null;
    private arr_y: Array<number> = null;

    private half_size = 0;
    private call_back: Function = null;
    private target = null;
    private add = 0;
    onLoad() {
        this.uitrans = this.node.getComponent(UITransform);
        UtilsPanel.addBtnEvent2(this.node, "onClick", this, "").transition = 0;
    }

    public init(begin:number, row:number, col:number, offset:number){
        this.arr_x = GameConst.getOffsetArr2(row, begin, offset);
        this.arr_y = GameConst.getOffsetArr2(col, begin, offset);
        let width = this.arr_x[row - 1] + begin;
        let height = this.arr_y[col - 1] + begin;
        this.node.toXY(-width/ 2, -height / 2);
        this.uitrans.setContentSize(width, height);
    }

    public initChessSize(half_size:number, func:Function, target:any){
        this.half_size = half_size;
        this.call_back = func;
        this.target = target;
    }

    public addHeight(add:number){
        this.uitrans.height += add;
        this.add = add;
    }
    public clear(){
        this.call_back = null;
    }
    public onClick(e:EventTouch){
        e.getUILocation(Tmp_V3 as any);
        this.uitrans.convertToNodeSpaceAR(Tmp_V3, Tmp_V3);
        if(Tmp_V3.y + this.add > this.uitrans.height){
            this.call_back.call(this.target, -1, Tmp_V3);
            return;
        }
        let x = -1, y = -1;
        for(let i = 0; i < this.arr_x.length; i++){
            if(Tmp_V3.x > this.arr_x[i] - this.half_size && Tmp_V3.x < this.arr_x[i] + this.half_size){
                x = i;
                break;
            }
        }
        for(let i = 0; i < this.arr_y.length; i++){
            if(Tmp_V3.y > this.arr_y[i] - this.half_size && Tmp_V3.y < this.arr_y[i] + this.half_size){
                y = i;
                break;
            }
        }
        if(x == -1 || y == -1)return;
        this.call_back && this.call_back.call(this.target, x, y);
    }
}