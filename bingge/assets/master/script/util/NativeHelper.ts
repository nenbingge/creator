import { native } from "cc";
import { JSB } from "cc/env";

export class NativeHelper{
    public static init(){
        if(!JSB)return;
        native.bridge.onNative = (arg0:string, arg1: string):void=>{
            switch(arg0){
                case "test":
                    console.log("test:", arg1);
                    break;
            }
        }
    }
    public static hideSplash(){
        if(!JSB)return;
        native.bridge.sendToNative('hideSplash', "");
    }
}