
import { _decorator, SpriteFrame, Node, Sprite, Button, Label, Toggle, EditBox, RichText, sp, isValid, Prefab, instantiate, assetManager, UITransform, UIOpacity, ImageAsset, Component, js, builtinResMgr, UIRenderer, Material, ProgressBar, Layers, view, NodeEventType, Color, tween, Tween } from 'cc';

import { BUILD } from 'cc/env';


var keyboard_height = 0;
export class UtilsPanel {

    public static showNodeByIndex(arr: Array<Node>, index: number) {
        if (index >= arr.length) return;
        arr.forEach((node, i) => {
            node.active = i == index;
        })
    }
    // public static setLbNum(lb, num) {
    //     lb.string = "x" + UtilsFormat.formatMoney(num);
    // }
    // public static setItemIcon(flag: string, url: string, sp_icon: Sprite) {
    //     ResMgr.loadWithTag(flag, url, SpriteFrame, (err: string, sp: SpriteFrame) => {
    //         if (err) return;
    //         if (isValid(sp_icon)) {
    //             sp_icon.spriteFrame = sp;
    //         }
    //     })
    // }
    public static playSeqFrames(interval: number, sprite: Sprite, frames:Array<SpriteFrame>, func:Function){
        let temp = {a : 0};
        let temp_tween = tween(temp).to(interval * frames.length, {a: 1}, {
            onUpdate(target, ratio) {
                if(!sprite.isValid){
                    temp_tween.stop();
                    return;
                }
                let index = Math.floor(ratio * frames.length);
                sprite.spriteFrame = frames[index];
            },
            onComplete:func as any
        }).start();
    }
    public static playLoopFrames(interval: number, sprite: Sprite, frames:Array<SpriteFrame>){
        let temp = {a : 0};
        let temp_tween = tween(temp).to(interval * frames.length, {a: 1}, {
            onUpdate(target, ratio) {
                if(!sprite.isValid){
                    temp_tween.stop();
                    return;
                }
                let index = Math.floor(ratio * frames.length);
                sprite.spriteFrame = frames[index];
            },
        }).repeatForever().start();
        return temp_tween;
    }
    public static setRemoteImg(url:string, sp_icon: Sprite){
        if(!BUILD)return;
        assetManager.loadRemote(url, { maxRetryCount: 1, ext: '.png' }, function (err, tex: ImageAsset) {
            if (err || !isValid(sp_icon)) return;
            sp_icon.spriteFrame = SpriteFrame.createWithImage(tex);
        });
    }
   
    public static limitNode(node: Node, width: number, height: number) {
        if (width == 0 || height == 0) return;
        let uitrans = node.getComponent(UITransform);
        let min: any = Math.min(width / uitrans.width, height / uitrans.height);
        node.toScaleXY(min);
    }
    public static limitHead(node: Node, width: number) {
        if (width == 0) return;
        let uitrans = node.getComponent(UITransform);
        node.toScaleX(width / uitrans.width);
        node.toScaleY(width / uitrans.height);
    }

    public static getSpineNode() {
        let new_node = new Node("spine");
        let spine = new_node.addComponent(sp.Skeleton);
        spine.premultipliedAlpha = false;
        return spine;
    }
   
    public static getAllNeedCom(sc: any, node: Node, recursive: boolean = false) {
        SC = sc;
        let children = node.children;
        if (recursive) {
            for (let i = 0, len = children.length; i < len; i++) {
                children[i].walk(getComByName);
            }
        }
        else {
            for (let i = 0, len = children.length; i < len; i++) {
                getComByName(children[i]);
            }
        }
    }


    public static getCom(node: Node, path: string[], type = null): any {
        for (let i = 0, len = path.length; i < len; i++) {
            node = node.getChildByName(path[i]);
        }
        return type ? node.getComponent(type) : node;
    }

    public static getArrCom(node: Node, name: string, count: number, type = null) {
        let arr = new Array(count);
        let children = node.children;
        let cur_connt = 0;
        for (let i = 0; i < children.length; i++) {
            if (children[i].name.startsWith(name)) {
                cur_connt++;
                let index = parseInt(children[i].name.slice(name.length));
                arr[index] = type ? children[i].getComponent(type) : children[i];
                if (cur_connt >= count) break;
            }
        }
        return arr;
    }
    public static getChildMap(node: Node) {
        let map = new Map<string, Node>()
        let children = node.children;
        for (let i = 0; i < children.length; i++) {
            map.set(children[i].name, children[i]);
        }
        return map;
    }
    public static playSpine(node, name: string, callback = null, track = 0) {
        let com: sp.Skeleton = node.getComponent(sp.Skeleton)
        com.setAnimation(track, name, false)
        if (callback) {
            com.setCompleteListener((e: sp.spine.TrackEntry) => {
                if (e.trackIndex == track) {
                    com.setCompleteListener(null)
                    callback()
                }
            })
        }
    }
    public static autoHideSpine(spine: sp.Skeleton) {
        spine.setCompleteListener(function () {
            spine.node.active = false;
        })
    }
    public static addBtnEvent(btn_node: Node, func: Function, target?: any) {
        let btn = btn_node.getComponent(Button);
        if (!btn) {
            btn = btn_node.addComponent(Button);
            btn.transition = Button.Transition.SCALE;
            btn.target = btn_node;
            btn.zoomScale = 0.9;
            btn.duration = 0.1;
        }
        btn_node.on("click", func, target);
        return btn;
    }
    public static addBtnEvent2(btn_node: Node, func: string, target: any, param: string | number) {
        let btn = btn_node.getComponent(Button);
        if (!btn) {
            btn = btn_node.addComponent(Button);
            btn.transition = Button.Transition.SCALE;
            btn.target = btn_node;
            btn.zoomScale = 0.9;
            btn.duration = 0.1;
        }
        let handler = new Component.EventHandler();
        handler.target = target.node;
        handler.component = js.getClassName(target);
        handler.handler = func;
        //@ts-ignore
        handler.customEventData = param;
        btn.clickEvents.push(handler);
        return btn;
    }
    public static clearBtnEvent(node: Node, remove_btn: boolean) {
        node.off("click");
        let btn = node.getComponent(Button);
        if (btn) {
            btn.clickEvents.length = 0;
            if (remove_btn) btn.destroy();
        }
    }
    public static removeBtns(node: Node) {
        node.walk(function (node1: Node) {
            let btn = node1.getComponent(Button);
            if (btn) btn.destroy();
            node1.off("click");
        })
    }
    public static setNodeGray(node_or_btn: Node | Button, state: boolean, recursive = false) {
        let node = node_or_btn;
        let btn = null;
        if (node_or_btn instanceof Button) {
            btn = node_or_btn;
            node = node_or_btn.node;
        }
        else {
            btn = (node as Node).getComponent(Button);
        }
        if (btn) btn.interactable = state;
        // let mat_name = state ? "ui-sprite-material" : "";
        let mat = state ? null : builtinResMgr.get("ui-sprite-gray-material");

        let func = function (node0: any) {
            let btn = node0.getComponent(Button)
            btn && (btn.interactable = state);
            let com_rendener = node0.getComponent(UIRenderer);
            if (com_rendener && !com_rendener.setAnimation) com_rendener.customMaterial = mat;
        }
        if (recursive) {
            (node as Node).walk(func);
        }
        else {
            func(node);
        }
    }

    public static addToggleCheckEvent(toggleNode: Node, func: string, target: any, params: string) {
        let toggle = toggleNode.getComponent(Toggle);
        if (!toggle) {
            toggle = toggle.addComponent(Toggle);
            toggle.transition = Button.Transition.SCALE;
            toggle.target = toggleNode;
            toggle.zoomScale = 0.9;
            toggle.duration = 0.1;
        }

        let handler = new Component.EventHandler();
        handler.target = target.node;
        handler.component = js.getClassName(target);
        handler.handler = func;
        handler.customEventData = params;
        toggle.checkEvents.push(handler)
    }

    public static getKeyboardOffset(node: Node) {
        let uitrans = node._uiProps.uiTransformComp;
        let bottom = node.getWorldPosition().y - uitrans.anchorY * uitrans.height;
        return keyboard_height - bottom;
    }
    // public static addEditCall(edit: EditBox, func_begin: Function, func_end: Function, target: any) {
    //     let func = function () {
    //         if (keyboard_height > 0) func_begin.call(target);
    //         else {
    //             TSCommon.delayAction(target, function () {
    //                 keyboard_height = JsbCommon.getKeyboardHeight();
    //                 if (edit.isFocused) func_begin.call(target);
    //             }, 0.3)
    //         }
    //     }
    //     edit.node.on("editing-did-began", func, target);
    //     edit.node.on("editing-did-ended", func_end, target);
    // }
}

var SC = null;
function getComByName(node: Node) {
    let name = node.name;
    let com = null;
    switch (name[0]) {
        case "a":
            if (name.startsWith("auto_btn_")) {
                let click_name = name.slice(10);
                let func_name = "on" + name[9].toUpperCase() + click_name + "Click";
                if (SC[func_name]) UtilsPanel.addBtnEvent(node, SC[func_name], SC);
            }
            break;
        case "b":
            if (name.startsWith("btn_")) com = node.getComponent(Button);
            break;
        case "e":
            if (name.startsWith("edit_")) com = node.getComponent(EditBox);
            break;
        case "g":
            // if (name.startsWith("gene_")) com = node.getComponent(Generate);
            break;
        case "i":
            // if (name.startsWith("ilb_")) com = node.getComponent(i18nLabel);
            // else if (name.startsWith("isk_")) com = node.getComponent(i18nSkeleton);
            break;
        case "l":
            if (name.startsWith("lb_")) com = node.getComponent(Label);
            break;
        case "n":
            if (name.startsWith("node_")) com = node;
            break;
        case "r":
            if (name.startsWith("rich_")) com = node.getComponent(RichText);
            break;
        case "s":
            if (name.startsWith("spine_")) com = node.getComponent(sp.Skeleton);
            else if (name.startsWith("sp_")) com = node.getComponent(Sprite);
            else if (name.startsWith("sc_")) {
                name = name.slice(3);
                //@ts-ignore
                com = node._components[0];//cocos creator 中组件排序 如果取不到留意顺序是否出错
            }
            break;
        case "t":
            if (name.startsWith("toggle_")) com = node.getComponent(Toggle);
            break;
        case "u":
            if (name.startsWith("uitrans_")) com = node.getComponent(UITransform);
            if (name.startsWith("uiopca_")) com = node.getComponent(UIOpacity);
            break;
    }
    if (com && SC.hasOwnProperty(name)) {
        if (SC[name]) console.warn("重复调用?", name);
        SC[name] = com;
    }
}
