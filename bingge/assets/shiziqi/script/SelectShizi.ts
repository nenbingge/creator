import { _decorator, Component, director, find, Node } from 'cc';
import { UtilsPanel } from '../../master/script/util/UtilsPanel';
import { SZConst, SZQMode } from './SZConst';
const { ccclass, property } = _decorator;

@ccclass('SelectShizi')
export class SelectShizi extends Component {
    onLoad() {
        UtilsPanel.getAllNeedCom(this, find("Canvas"));
    }

    private onErchiyiClick(){
        SZConst.mode = SZQMode.RSY;
		director.loadScene("erchiyi");
	}
    private onLaoheshangClick(){
        SZConst.mode = SZQMode.LHS;
        director.loadScene("erchiyi");
    }

    private onBackClick(){
		director.loadScene("lobby");
	}
    //update(dt: number) {
    //}
}