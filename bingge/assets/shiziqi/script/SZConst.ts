export enum SZQMode{
    RSY,
    LHS,
}
export enum RSYState{
    Play,
    Select,
    Move,
    Over,
}
export enum LHSState{
    Play,
    Select,
    Move,
    Over,
}
export class SZConst{
    public static colors = ["蓝", "绿", "红", "黄"];
    public static showTip: Function = null;
    public static setChongkai: Function = null;
    public static mode = SZQMode.RSY;
}