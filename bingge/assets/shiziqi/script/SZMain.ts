import { _decorator, Color, Component, director, find, Label, Node } from 'cc';
import { UtilsPanel } from '../../master/script/util/UtilsPanel';
import { GridGraphics } from '../../master/script/com/GridGraphics';
import { ChessControl } from '../../master/script/com/ChessControl';
import { SZMap } from './SZMap';
import { RSYMap } from './RSYMap';
import { SZConst, SZQMode } from './SZConst';
import { LHSMap } from './LHSMap';
const { ccclass, property } = _decorator;

@ccclass('SZMain')
export class SZMain extends Component {
    private gra: GridGraphics = null;
	private chesses: ChessControl = null;
	private node_map: Node = null;
    private map: SZMap = null;
    private lb_tip: Label = null;
    private node_chongkai: Node = null;

    onLoad() {
        UtilsPanel.getAllNeedCom(this, find("Canvas"));
        SZConst.showTip = (str:string)=>{this.lb_tip.string = str;}
        SZConst.setChongkai = (state:boolean)=>{this.node_chongkai.active = state;}
        UtilsPanel.addBtnEvent(this.node_chongkai, this.chongKai, this);
    }

    protected start(): void {
        this.gra.initColor(new Color(0, 0, 0), new Color(0, 0, 0));
        switch(SZConst.mode){
            case SZQMode.RSY:
                this.map = new RSYMap();
                this.map.init(this.chesses, this.gra);
                break;
            case SZQMode.LHS:
                this.map = new LHSMap();
                this.map.init(this.chesses, this.gra);
                break;
        }
    }
    private onBackClick(){
		director.loadScene("shiziqi");
	}

    public chongKai(){
        this.map.clear();
        this.map.initChess();
    }
    //update(dt: number) {
    //}
}