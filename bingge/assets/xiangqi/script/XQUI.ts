import { _decorator, Color, Component, Label, Node } from 'cc';
import { UtilsPanel } from '../../master/script/util/UtilsPanel';
const { ccclass, property } = _decorator;

@ccclass('XQUI')
export class XQUI extends Component {
    private lb_tip: Label = null;

    onLoad() {
        UtilsPanel.getAllNeedCom(this, this.node);
    }
    public showTip(str:string){
        this.lb_tip.string = str;
    }
    public setTipColor(color:Color){
        this.lb_tip.color = color;
    }

    //update(dt: number) {
    //}
}