import { _decorator, Component, Node, Sprite, tween } from 'cc';
import { UtilsPanel } from '../../master/script/util/UtilsPanel';
import { XQRes } from './XQRes';
const { ccclass, property } = _decorator;

@ccclass('XQEle')
export class XQEle extends Component {
    private sp_bg: Sprite = null;
	private sp_ele: Sprite = null;

    public type = 0;
    public site = 0; 
    protected onLoad(): void {
        UtilsPanel.getAllNeedCom(this, this.node);
    }
    public init(type:number, site:number){
        this.type = type;
        this.site = site;
        this.sp_ele.spriteFrame = XQRes.instance.atlas_ele.getSpriteFrame(site + "" + type);
    }
    public move(x:number, y:number){
        this.node.setSiblingIndex(-1);
        tween(this.node)
            .toPro(0.2, {x, y})
            .start();
    }
}