import { _decorator, Component, director, find, Node } from 'cc';
import { UtilsPanel } from '../../master/script/util/UtilsPanel';
import { XQMap } from './XQMap';
import { XQUI } from './XQUI';
const { ccclass, property } = _decorator;

@ccclass('XQMain')
export class XQMain extends Component {
    private map: XQMap = null;
    private ui: XQUI = null;
    
    onLoad() {
        UtilsPanel.getAllNeedCom(this, find("Canvas"));
    }

    protected start(): void {
        this.map.init(this.ui, true);
    }
    
	private onBackClick(){
		director.loadScene("lobby");
	}

    //update(dt: number) {
    //}
}