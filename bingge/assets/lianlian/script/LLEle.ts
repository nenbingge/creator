
import { _decorator, Component, Node, Sprite } from 'cc';
import { LLRes } from './LLRes';
const { ccclass } = _decorator;

@ccclass('LLEle')
export class LLEle extends Component {
    private sp: Sprite = null;
    
    public flag = 0;

    protected onLoad(): void {
        this.sp = this.node.getComponent(Sprite);
    }
    public init(type:number, index:number){
        this.sp.spriteFrame = LLRes.instance.atlas_ele.getSpriteFrame("ele" + type + "_" + index);
        return type * 100 + index;
    }
    public setSelect(state:boolean){
        let scale = state ? 1.1 :  1;
        this.node.toScaleXY(scale);
    }
    public reset(){
        this.node.toScaleXY(1);
    }
}
