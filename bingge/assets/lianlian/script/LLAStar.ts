



let map = null;
let Row = 0, Col = 0;
function isAdjoin(begin:Array<number>, end:Array<number>){
    if(Math.abs(begin[0] + end[0]) + Math.abs(begin[1] + end[1]) == 1)return true;
    return false;
}
function canLine(begin:Array<number>, end:Array<number>){
    if(begin[0] == end[0]){
        let min = Math.min(begin[1], end[1]);
        let max = Math.max(begin[1], end[1]);
        let row = begin[0];
        for(let i = min + 1; i < max ; i++){
            if(map[row][i] != - 1)return false;
        }
        return true;
    }
    else if(begin[1] == end[1]){
        let min = Math.min(begin[0], end[0]);
        let max = Math.max(begin[0], end[0]);
        let col = begin[1];
        for(let i = min + 1; i < max ; i++){
            if(map[i][col] != - 1)return false;
        }
        return true;
    }
    return false;
}
function checkOneCorner(begin:Array<number>, end:Array<number>){
    let cg_point = [begin[0], end[1]];
    if(map[cg_point[0]][cg_point[1]] == -1 && canLine(cg_point, begin) && canLine(cg_point, end))return [begin, cg_point, end];
    cg_point = [end[0], begin[1]];
    if(map[cg_point[0]][cg_point[1]] == -1 && canLine(cg_point, begin) && canLine(cg_point, end))return [begin, cg_point, end];
    return null;
}
function checkTwoCorner(begin:Array<number>, end:Array<number>){
    let arr = null;
    for(let i = begin[0] - 1; i >= 0; i--){
        if(map[i][begin[1]] == -1){
            arr = checkOneCorner([i,begin[1]], end);
            if(arr)return [begin].concat(arr);
        }
        else break;
    }
    for(let i = begin[0] + 1; i < map.length; i++){
        if(map[i][begin[1]] == -1){
            arr = checkOneCorner([i,begin[1]], end);
            if(arr)return [begin].concat(arr);
        }
        else break;
    }
    let col = map[0].length;
    for(let i = begin[1] - 1; i >= 0; i--){
        if(map[begin[0]][i] == -1){
            arr = checkOneCorner([begin[0],i], end);
            if(arr)return [begin].concat(arr);
        }
        else break;
    }
    for(let i = begin[1] + 1; i < col ; i++){
        if(map[begin[0]][i] == -1){
            arr = checkOneCorner([begin[0],i], end);
            if(arr)return [begin].concat(arr);
        }
        else break;
    }
    return null;
}
export class LLAStar {
    public static setMap(_map:any){ map = _map;}
    public static setSize(_row:number, _col:number){
        Row = _row; 
        Col = _col;
        for(let i = 0; i < Col; i++){
            map[Row - 1][i] = -1;
        }
        for(let i = 0 ; i < Row; i++){
            map[i][0] = -1;
            map[i][Col - 1] = -1;
        }
    }
    public static canJoin(begin:Array<number>, end:Array<number>){
        let can = isAdjoin(begin, end) || canLine(begin, end);
        if(can)return [begin, end];
        let arr = checkOneCorner(begin, end);
        if(arr)return arr;
        return checkTwoCorner(begin, end);
    }
    public static logMap(){
        for(let i = Col - 1; i >= 0; i--){
            let arr = [];
            for(let j = 0; j < Row ; j++){
                arr.push(map[j][i]);
            }
            console.log(arr.join(","));
        }
    }
}
