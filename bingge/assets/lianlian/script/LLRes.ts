
import { _decorator, Component, Node, Prefab, SpriteAtlas } from 'cc';
import { ResPool } from '../../master/script/game/ResPool';
import { LLEle } from './LLEle';
const { ccclass, property } = _decorator;

@ccclass('LLRes')
export class LLRes extends Component {
    public static instance: LLRes = null;
    
    @property(SpriteAtlas)
    public atlas_ele: SpriteAtlas = null;
    @property(Prefab)
    private pre_ele: Prefab = null;

    public pool_ele: ResPool<LLEle> = null;

    protected onLoad(): void {
        if(LLRes.instance)console.warn("LLRes is have");
        LLRes.instance = this;
        this.pool_ele = new ResPool<LLEle>(LLEle, this.pre_ele, 200);
    }
    protected onDestroy(): void {
        this.pool_ele.clear();
        LLRes.instance = null;
    }


}
