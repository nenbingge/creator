
'use strict'
/*

Native Caller
bool/string/number/string/function

调用 
    andrid -> org.cocos2dx.javascript.Native.SayHello(String helloString, String cbName);
    ios    -> Native.SayHello : (NSString*) helloString
                         arg1 : (NSString*) cbName;
写法
native.call("SayHello", "hello world", (ok) => { })
native.callClz("Native", "SayHello", "hello world", (ok) => { })
*/

class Native {

    constructor() {
        this.cbIdx = 0
        this.cbs = {}

        var self = this
        window.js_native_cb = function (cbID) {
            let func = self.cbs[cbID]
            if (func) {
				delete self.cbs[cbID]
                let args = Array.prototype.slice.call(arguments)
                args.splice(0, 1)
                func.apply(null, args)
            } else {
                cc.log("no func ", cbID)
            }
        }
    }

    _newCB(f) {
        this.cbIdx ++ 
        let cbID = "" + this.cbIdx
        this.cbs[cbID] = f
        return cbID
    }

    call() {
        let args = Array.prototype.slice.call(arguments)
		if (cc.sys.platform == cc.sys.ANDROID) {
			args.splice(0, 0, "Native")
		} else if (cc.sys.platform == cc.sys.IPHONE || cc.sys.platform == cc.sys.IPAD) {
			args.splice(0, 0, "Native")
		} else {
            return
        }

		this.callClz.apply(this,"V",args)
    }

    callClz(clz,returnType, funcName) {
		let args = Array.prototype.slice.call(arguments)
		args.splice(0, 3)
		let real_args = [clz, funcName]

		if (cc.sys.platform == cc.sys.ANDROID) {
			real_args[0] = "com/games/GameLib/" + clz;
			real_args[2] = "()" + returnType;
			if (args.length > 0) {
				let sig = ""
				args.forEach((v) => {
					switch (typeof v) {
						case 'boolean': sig += "Z"; real_args.push(v); break;
						case 'string': sig += "Ljava/lang/String;"; real_args.push(v); break;
						case 'number': sig += "I"; real_args.push(v); break;
						case 'function': sig += "Ljava/lang/String;"; real_args.push(this._newCB(v)); break;
					}
				})
				real_args[2] = "(" + sig + ")" + returnType;
			}
		} else if (cc.sys.platform == cc.sys.IPHONE || cc.sys.platform == cc.sys.IPAD) {
			if (args.length > 0) {
				for (let i = 0; i < args.length; i++) {
					let v = args[i]
					if (typeof v == "function") {
						real_args.push(this._newCB(v))
					} else {
						real_args.push(v)
					}
					if (i == 0) {
						funcName += ":"
					} else {
						funcName += "arg" + i + ":"
					}
				}

				real_args[1] = funcName
			}
		} else {
			return
		}

		return jsb.reflection.callStaticMethod.apply(jsb.reflection, real_args)
    }
}

window.native = new Native