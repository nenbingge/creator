package org.cocos2dx.jsb;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.util.Log;


import android.content.ClipData;
import android.content.ClipboardManager; //导入需要的库
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AppsFlyerLib;
import com.cocos.game.AppActivity;
import com.google.android.play.core.review.ReviewInfo;
import com.google.android.play.core.review.ReviewManager;
import com.google.android.play.core.review.ReviewManagerFactory;
import com.google.android.play.core.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.mopub.volley.toolbox.HttpResponse;
import com.tencent.bugly.crashreport.CrashReport;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;




import static android.content.Context.MODE_PRIVATE;
import static java.net.Proxy.Type.HTTP;

import org.json.JSONObject;

public class JsbClass {
	private static AppActivity m_activity;
	private static String clibboardText;
	private static int m_adCount = 0;
	private static String TAG = "JsbGame";
	public static String UserId = "JsbId";
	public static String m_language = "en";//多語言

    private static final int TIME_OUT = 10 * 1000; // 超时时间
    private static final String CHARSET = "utf-8"; // 设置编码

	private static String m_autoEnterGame;
	private static String m_autoEnterRoom;

	private static String stopMicFunc;
	private static String playingMicFinshFunc;

	public static void init(AppActivity app,String autoEnterGame,String autoEnterRoom) {
		m_activity = app;
		m_autoEnterGame = autoEnterGame;
		m_autoEnterRoom = autoEnterRoom;
		List<String> permissionList = new ArrayList<>();
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P) {
			permissionList.add(Manifest.permission.READ_PHONE_STATE);
		}
		permissionList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
		permissionList.add(Manifest.permission.READ_EXTERNAL_STORAGE);
		permissionList.add(Manifest.permission.MANAGE_EXTERNAL_STORAGE);
		permissionList.add(Manifest.permission.CAMERA);
		requestPermissions(permissionList);

		Facebook.getInstance().onInit(app);
		GooglePay.getInstance().onInit(app);
		Js.init(app);
		GoogleLogin.getInstance().onInit(app);

		UserFaceImage.init(app);
		//TTAdManagerHolder.setActivity(app);
		GoogleInstallReferrerHelp.getInstance().getInstallReferrer(app);

		ZegoVoiceManager.getInstance().onInit(app);
		Log.v(TAG,"initializing Firebase");
		FirebaseApp.initializeApp(app);
		MyFirebaseMessagingService.getToken();
		//m_strategy = new CrashReport.UserStrategy(app);
//...在这里设置strategy的属性，在bugly初始化时传入
//...
		CrashReport.initCrashReport(app, "2206d6ae4c", true);
	}

	public static void setAutoEnterRoom(String game,String roomId) {
		m_autoEnterGame = game;
		m_autoEnterRoom = roomId;
	}

	public static String getAutoEnterGame() {
		String str = m_autoEnterGame;
		m_autoEnterGame = "";
		return str;
	}

	public static String getAutoEnterRoom() {
		String str = m_autoEnterRoom;
		m_autoEnterRoom = "";
		return str;
	}

	public static void requestPermission(String permission){
		if (ContextCompat.checkSelfPermission(m_activity,
				permission)
				!= PackageManager.PERMISSION_GRANTED){

			ActivityCompat.requestPermissions(m_activity,
					new String[]{permission},
					1);
		}
	}

	public static void requestPermissions(List<String> permissions){
		List<String> permissionList = new ArrayList<>();
		for (String p: permissions) {
			Log.v("JsbGame","requestPermissions:" + p);
			if (ContextCompat.checkSelfPermission(m_activity,
					p)
					!= PackageManager.PERMISSION_GRANTED)
			{
				permissionList.add(p);
			}
		}
		if(!permissionList.isEmpty()){
			ActivityCompat.requestPermissions(m_activity,
					permissionList.toArray(new String[permissionList.size()]),
					1);
		}
	}

	public static String getInstallReferrer() {
		return GoogleInstallReferrerHelp.getInstance().getReferrerUrl();
	}

	public static String getAndroidDeviceName() {
		return Build.MODEL;
	}

	public static String getPartnerID() {
        Log.v("JsbGame","getPartnerID");
		ApplicationInfo appInfo = null;
		try {
			appInfo = m_activity.getPackageManager().getApplicationInfo(
					m_activity.getPackageName(), PackageManager.GET_META_DATA);
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}
		if (appInfo != null)
			return appInfo.metaData.getInt("qudao",10101) + "";
		return "10101";
	}

	public static String getIMEI() {
		Log.v("JsbGame", "getIMEI");
		// MyFirebaseMessagingService.getToken();
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P){
			try {
				final TelephonyManager manager = (TelephonyManager) m_activity.getSystemService(Context.TELEPHONY_SERVICE);
				if (manager.getDeviceId() == null || manager.getDeviceId().equals("")) {
					Log.v("JsbGame", "failed to get imei");
				} else {
					return manager.getDeviceId();
				}
			} catch (Exception e) {
				Log.v("JsbGame", e.toString());
				e.printStackTrace();
			}
			return "00000000000";
		} else {
			SharedPreferences pre = m_activity.getSharedPreferences("Database",MODE_PRIVATE);
			String ret = pre.getString("my_uuid",""); //第二个参数为 取不到时的默认值
			if(ret.equals("")) {
				ret = UUID.randomUUID().toString();
				SharedPreferences.Editor editor = pre.edit();
				editor.putString("my_uuid",ret);
				editor.commit();
			}
			return ret;
		}
	}

	public static String getVersionCode() {
		PackageInfo pi = null;
		try {
			pi = m_activity.getPackageManager().getPackageInfo(
					m_activity.getPackageName(), 0);
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
			return "0";
		}
		Log.v("JsbGame","versionCode = " + pi.versionCode);
		return pi.versionCode + "";
	}

	public static void testReturnCall(String func) {
		Js.call(func,"testReturnCall");
	}

	public static String getCopyZoneString(){
		m_activity.runOnUiThread(
		new Runnable()
		{
			public void run()
			{
				ClipData localClipData = ((ClipboardManager)m_activity.getSystemService(Context.CLIPBOARD_SERVICE)).getPrimaryClip();
				if (localClipData != null)
				{
					ClipData.Item localItem = localClipData.getItemAt(0);
					if ((localItem != null) && (localItem.getText() != null))
						JsbClass.clibboardText = localItem.getText().toString();
				}
			}
		});
      	return clibboardText;
	}

	public static void setCopyZoneString(final String text){
		try
        {           
            Runnable runnable = new Runnable() {
                public void run() {
                    android.content.ClipboardManager clipboard = (android.content.ClipboardManager) m_activity.getSystemService(Context.CLIPBOARD_SERVICE);
                    android.content.ClipData clip = android.content.ClipData.newPlainText("simple text copy", text);
                    clipboard.setPrimaryClip(clip);
                }
            };			
            m_activity.runOnUiThread(runnable); 
        }catch(Exception e){           
            e.printStackTrace();
        }
	}
	public static void sendEmail(final String email) {
		try
		{
			Runnable runnable = () -> {
				Uri uri = Uri.parse ("mailto: " + email);
				Intent intent = new Intent (Intent.ACTION_SENDTO, uri);
				m_activity.startActivity(intent);
			};
			m_activity.runOnUiThread(runnable);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public static void callPhone(final String phoneNumber) {
		try {
			Runnable runnable = () -> {
				Uri uri = Uri.parse("tel: " + phoneNumber);
				Intent intent = new Intent(Intent.ACTION_DIAL, uri);
				m_activity.startActivity(intent);
			};
			m_activity.runOnUiThread(runnable);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}



    public static void facebookLogin(String func) {
		Facebook.getInstance().doLogin(func);
	}

	public static void googleLogin(String func) {
		GoogleLogin.getInstance().doLogin(func);
	}

    public static void facebookShare(final String message, final String name, final String desc, final String url, final String callbackFunc) {
		m_activity.runOnUiThread(
				() -> Facebook.getInstance().shareUrl(message,name,desc,url,callbackFunc));

	}

	public static void googlePay(int nUserID, int nPrice, String strProductID,
								String payFinishedCallback) {
		GooglePay.getInstance().pay(nUserID,nPrice,strProductID,payFinishedCallback);
	}

	public static void onActivityResult(int requestCode, int resultCode, Intent data) {
		Facebook.getInstance().onActivityResult(requestCode,resultCode,data);
		GoogleLogin.getInstance().onActivityResult(requestCode,resultCode,data);
		UserFaceImage.getInstance().onActivityResult(requestCode,resultCode,data);
	}

	//权限通知 针对语音房间
	public static void onRequestPermissionsResult(int requestCode, int grantResults){
		ZegoVoiceManager.getInstance().onRequestPermissionsResult(requestCode, grantResults);
	}

	public static void showRewardedAD(final String adId,final String loadedCallback,final String callback) {
		m_adCount++;
		if(m_adCount % 3 == 0) {
			showGoogleAD(adId, loadedCallback, callback);
			return;
		}
		if(m_adCount % 3 == 1) {
			showCHJAD(adId,loadedCallback, callback);
			return;
		}
		if(m_adCount % 3 == 2) {
			showFBRewardedAD(adId, loadedCallback, callback);
			return;
		}
	}

	public static void showGoogleAD(final String adId,final String loadedCallback,final String callback) {
		//m_activity.runOnUiThread(
		//		() -> GoogleADMob.showRewardedAD(adId,loadedCallback,callback));

	}
	public static void showFBRewardedAD(final String adId,final String loadedCallback,final String callback) {
		//m_activity.runOnUiThread(() -> Facebook.getInstance().showAd(loadedCallback,callback));
		//showCHJAD(adId,loadedCallback,callback);
	}

	public static void showCHJAD(final String adId,final String loadedCallback,final String callback) {
		//showGoogleAD(adId,loadedCallback,callback);

		//m_activity.runOnUiThread(
			//	() -> TTAdManagerHolder.showRewardedAD(loadedCallback,callback));


		//showFBRewardedAD(adId,loadedCallback,callback);
		//showGoogleAD("ca-app-pub-4359357195857629/5041490105",loadedCallback,callback);

	}

	public static void showGoogleReview(final String callback) {
		m_activity.runOnUiThread(
				() -> {
					final ReviewManager manager = ReviewManagerFactory.create(m_activity);
					Task<ReviewInfo> request = manager.requestReviewFlow();
					request.addOnCompleteListener(task -> {
						if (task.isSuccessful()) {
							// We can get the ReviewInfo object
							ReviewInfo reviewInfo = task.getResult();
							Task<Void> flow = manager.launchReviewFlow(m_activity, reviewInfo);
							flow.addOnCompleteListener(task1 -> {
								Js.call(callback,"1");
							});
						} else {
							// There was some problem, log or handle the error code.
							Log.v(TAG, "showGoogleReview failed " + task.getException());
							//@ReviewErrorCode int reviewErrorCode = ((TaskException) task.getException()).getErrorCode();
							Js.call(callback,"0");
						}
					});
				});

	}

	public static void setUserId(final String text){
		CrashReport.setUserId(text);
		JsbClass.UserId=text;
	}
	public static void setLanguage(final String text){
		m_language=text;
	}
	public static void showGoogleBanner(int width , int height, int offset_x , int offset_y){
		//GoogleADMob.showBanner(width, height, offset_x, offset_y);
	}
	public static void closeGoogleBanner(){
		//GoogleADMob.closeBanner();
	}

	public static void setOrientation(String dir){
		if(dir.equals("V")) {
			m_activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
		} else {
			m_activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
		}
	}

	public static void postSDKTrackRecord(String text1,String text2,String text3){
		Map<String, Object> eventValues = new HashMap<String, Object>();
		eventValues.put(AFInAppEventParameterName.CONTENT_ID, "id");

		AppsFlyerLib.getInstance().logEvent(m_activity,
		text1+"_"+text2+'_'+text3,eventValues );

		Log.i(text1,text2+'+'+text3);
	}


	public static boolean isMethodExist(String name){
		Method[] methods = JsbClass.class.getDeclaredMethods();
		for(Method method:methods){
			if(method.getName().equals(name)){
				return true;
			}
		}
		return false;
	}

	public static boolean shareWhatsapp(String file){
		Log.v(TAG, "shareWhatsapp: file"+file);

		//要保存到相册。只放在可读写目录其他应用读不到
		String picPath= JsbClass.saveTextureToLocal(file);
		Uri imgUri = Uri.parse(picPath);
		Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
		//whatsappIntent.setType("text/plain");
		whatsappIntent.setPackage("com.whatsapp");
		//whatsappIntent.putExtra(Intent.EXTRA_TEXT,"The text you wanted to share");
		whatsappIntent.putExtra(Intent.EXTRA_STREAM, imgUri);
		whatsappIntent.setType("image/png");
		whatsappIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

		try {
			m_activity.startActivity(whatsappIntent);
			Log.d(TAG, "start whatsapp activity"+file);

		} catch (android.content.ActivityNotFoundException ex) {
			Log.d(TAG, "shareWhatsapp: Whatsapp have not been installed.");
			return false;
		}
		return true;
	}

	/**
	 * 保存图片到相册，并刷新相册
	 * @param pngPath
	 * @return 返回图片路径
	 */
	public static String saveTextureToLocal( String pngPath) {

		Bitmap bmp = BitmapFactory.decodeFile(pngPath);//从路径中读取 照片
		// fileName ==textureName  尽量和JS保存的一致
		String fileName = "textureName";
		File file = new File(pngPath);
		try {
			FileOutputStream fos = new FileOutputStream(file);
			bmp.compress(Bitmap.CompressFormat.PNG, 100, fos);
			fos.flush();
			fos.close();
			Log.d("保存成功",pngPath );

		} catch (FileNotFoundException e) {
			Log.d("保存错误1",e.toString());

			e.printStackTrace();
		} catch (IOException e) {
			Log.d("保存错误2",e.toString());

			e.printStackTrace();
		}

		// 其次把文件插入到系统图库
		String newpath="";
		try {
			newpath=MediaStore.Images.Media.insertImage(m_activity.getApplicationContext().getContentResolver(),
					file.getAbsolutePath(), fileName, null);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		// 最后通知图库更新
		m_activity.getApplicationContext().getApplicationContext().sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse(file.getAbsolutePath())));
		return newpath;
	}


	public static String pickImage(String path,String uploadType,String token){
		// Bitmap photoBitmap=ImageTools.getPhotoFromSDCard(path,"render_to_sprite_image");
		// Log.d("getUpload",path);
		// byte[] result=ImageTools.bmpToByteArray(photoBitmap,true);
		// return Utils.toHexString(result);


		//path+="render_to_sprite_image.png";
		//File file = new File(path);
		Log.v(TAG,"image path="+path+" type="+uploadType);

		UserFaceImage.getInstance().getUserPic(token,uploadType);
		return "";
	}

	public static String uploadFile(File file, String RequestURL, String type,
            String formId, String cis_key, String accessToken){
		String result = null;
        String BOUNDARY = UUID.randomUUID().toString(); // 边界标识 随机生成
        String PREFIX = "--", LINE_END = "\r\n";
        String CONTENT_TYPE = "multipart/form-data"; // 内容类型
        try {
            URL url = new URL(RequestURL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(TIME_OUT);
            conn.setConnectTimeout(TIME_OUT);
            conn.setDoInput(true); // 允许输入流
            conn.setDoOutput(true); // 允许输出流
            conn.setUseCaches(false); // 不允许使用缓存

            conn.setRequestMethod("POST"); // 请求方式
            // conn.setRequestMethod("GET"); // 请求方式
            conn.setRequestProperty("Charset", CHARSET); // 设置编码
            conn.setRequestProperty("connection", "keep-alive");
            conn.setRequestProperty("Content-Type", CONTENT_TYPE + ";boundary="
                    + BOUNDARY);
            conn.setRequestProperty("action", "upload");
            conn.connect();// 只是建立了一个与服务器的tcp连接，并没有实际发送http请求。

			Log.v(TAG,"before file! =null");
            if (file != null) {
                /**
                 * 当文件不为空，把文件包装并且上传
                 */
				Log.v(TAG,"new DataOutputStream");
                DataOutputStream dos = new DataOutputStream(
                        conn.getOutputStream());
                byte[] obj = new byte[100];
                // obj.wait(type, type);
                dos.write(obj);
				Log.v(TAG,"new StringBuffer()");
                StringBuffer sb = new StringBuffer();
                sb.append(PREFIX);
                sb.append(BOUNDARY);
                sb.append(LINE_END);
                /**
                 * 这里重点注意： name里面的值为服务器端需要key 只有这个key 才可以得到对应的文件
                 * filename是文件的名字，包含后缀名的 比如:abc.png
                 */

                sb.append("Content-Disposition: form-data; name=\"img\"; filename=\""
                        + "file"+ "\"" + LINE_END);
                sb.append("Content-Type: application/octet-stream; charset="
                        + CHARSET + LINE_END);
                sb.append(LINE_END);

                dos.write(sb.toString().getBytes());
                InputStream is = new FileInputStream(file);
                byte[] bytes = new byte[1024];
                int len = 0;
                while ((len = is.read(bytes)) != -1) {
                    dos.write(bytes, 0, len);
                }
                is.close();
				Log.v(TAG,"dos.write(LINE_END.getBytes());");
                dos.write(LINE_END.getBytes());
                byte[] end_data = (PREFIX + BOUNDARY + PREFIX + LINE_END)
                        .getBytes();

                dos.write(end_data);

                dos.flush();
                /**
                 * 获取响应码 200=成功 当响应成功，获取响应的流
                 */
                int res = conn.getResponseCode();
                Log.d(TAG,"res code="+res);
                if (res == 200) {
                    InputStream input = conn.getInputStream();
                    StringBuffer sb1 = new StringBuffer();
                    int ss;
                    while ((ss = input.read()) != -1) {
                        sb1.append((char) ss);
                    }
                    result = sb1.toString();
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
	}

	/**
	 * @return Asia/Shanghai
	 */
	public static String getTimeZone(){
		String str= TimeZone.getDefault().getID();
		return str;
	}

	/**
	 * @return zh_HK
	 */
	public static String getSystemLanguage(){
		Locale locale = Locale.getDefault();
		//Locale.getDefault() 和 LocaleList.getAdjustedDefault().get(0) 同等效果，还不需要考虑版本问题，推荐直接使用
		String language = locale.getLanguage() + "-" + locale.getCountry();
		return language;
	}

	/**
	 * @return 没权限,或者获取不到返回""
	 */
	public static String getPhoneNum(){
		//init的时候应该请求过权限，这里不判断，getLine1Number报红
		if (ActivityCompat.checkSelfPermission(m_activity.getBaseContext(), Manifest.permission.READ_PHONE_STATE)
				== PackageManager.PERMISSION_GRANTED){
			TelephonyManager tMgr = (TelephonyManager)m_activity.getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
			String strNum = "";
			try {
				strNum=tMgr.getLine1Number(); //tMgr.getLine1Number();
			}catch (Exception e){

			}
			return strNum;
		}
		return "";
	}

	public static boolean startMIC() {
		return Mic.startMIC();
	}

	public static void stopMIC(String func) {
		stopMicFunc = func;
		m_activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// TODO 自动生成的方法存根
				if (!Mic.stopMIC()) {
					nativeStopMic(null, 0);
				}
			}
		});
	}

	public static void recieveMIC(final int nChair, final String pBuf, final int nLen,String func) {
		playingMicFinshFunc = func;
		Log.v("JniCommon","recieveMIC chair = " + nChair + ", func = " + func + ",len = " + nLen);
		m_activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// TODO 自动生成的方法存根
				Mic.recieveMIC(nChair, Utils.toByteArray(pBuf), nLen);
			}
		});
	}

	public static void nativeStopMic(byte[] micData, int length)
	{
		if(!stopMicFunc.equals("") ){
			if (micData == null) {
				Js.call(stopMicFunc, 0,"" );
			} else {
				final String strMic = Utils.toHexString(micData);
				Js.call(stopMicFunc,strMic.length(), strMic );
			}
			stopMicFunc = "";
		}
	}

	public static void nativePlayEnd(final int nChair)
	{
		if(!playingMicFinshFunc.equals("")) {
			Js.call(playingMicFinshFunc, nChair);
			playingMicFinshFunc = "";
		}
	}

	/** 登录语聊房间
	 *
	 * @param roomID
	 * @param userId 用户信息
	 * @param scenario
	 * @param soundlevelsRet 拉流分贝
	 * @param broadcoastRet 广播
	 * @param barrageRet 弹幕
	 * @param customCmdRet v
	 * @param callback
	 */
	public static void loginAudioRoom(String roomID, String userId, int scenario, String soundlevelsRet,
									  String broadcoastRet, String barrageRet, String customCmdRet, String callback){
		ZegoVoiceManager.getInstance().login(roomID, userId, scenario, soundlevelsRet,
				 broadcoastRet, barrageRet, customCmdRet, callback);
	}

	/** 退出语聊房间 */
	public static void logoutAudioRoom(){
		ZegoVoiceManager.getInstance().logout();
	}

	/** 用户上下麦
	 *
	 * @param isPushing  true 上麦 false 下麦
	 */
	public static void enableMic(boolean isPushing){
		if(isPushing){
			ZegoVoiceManager.getInstance().startPublishing();
		}
		else{
			ZegoVoiceManager.getInstance().stopPublishing();
		}
	}

	/** 禁用micphone
	 *
	 * @param mute   true禁用 false激活
	 * @param isRealse  true释放SDK对micphone的占用(切后台可以调用)
	 */
	public static void muteMicrophone(boolean mute, boolean isRealse){
		ZegoVoiceManager.getInstance().muteMicrophone(mute, isRealse);
	}

	//判断是否禁用micphone
	public static boolean isMicrophoneMuted(){
		return ZegoVoiceManager.getInstance().isMicrophoneMuted();
	}

	/** 禁用听筒设备
	 *
	 * @param mute true禁用 false激活
	 * @return
	 */
	public static void muteSpeaker(boolean mute){
		ZegoVoiceManager.getInstance().muteSpeaker(mute);
	}

	//判断是否禁用听筒设备
	public static boolean isSpeakerMuted(){
		return ZegoVoiceManager.getInstance().isSpeakerMuted();
	}

	/** 禁用流
	 *
	 * @param mute   true禁用 false激活
	 * @param StreamID  流id 空表示所有
	 */
	public static void mutePlayStreamAudio(boolean mute, String StreamID){
		ZegoVoiceManager.getInstance().mutePlayStreamAudio(mute,StreamID);
	}

	/** 获取音浪强度
	 *
	 * @param userid
	 * @return
	 */
	public static String getSoundLevel(String userid){
		return ZegoVoiceManager.getInstance().getSoundLevel(userid);
	}

	/** 获取当前房间状态 */
	public static int getRoomStatus(){
		return ZegoVoiceManager.getInstance().getRoomStatus();
	}

	/** 上麦 需要检测麦克风权限
	 *
	 */
	public static void requestTakeMicPermission(String Func){
		ZegoVoiceManager.getInstance().requestTakeMicPermission(Func);
	}


	/** 设置
	 *
	 */
	public static void jumpPhonePermissionSet(){
		if(JsbClass.m_activity != null){
			JsbClass.m_activity.jumpPhonePermissionSet();
		}

	}

	//广播
	public static void sendBroadcastMessage(String message, String callback){
		ZegoVoiceManager.getInstance().sendBroadcastMessage( message, callback);
	}
	public static void sendBarrageMessage(String message, String callback){
		ZegoVoiceManager.getInstance().sendBarrageMessage( message, callback);
	}
	public static void sendCustomCommand(String command, String userId, String callback){
		ZegoVoiceManager.getInstance().sendCustomCommand( command,  userId,  callback);
	}

	private static boolean postOnce=false;
	public static void postException(int category, String name, String reason, String stack){
		if(postOnce){//只上傳一次
			return;
		}
		postOnce=true;

		String serverIp="http://www.cocobaloot.com/errorlog/clientlog.aspx";
		String msg = category+name+reason+stack;
		try {
			String lastUrl = serverIp + "?UserId="+JsbClass.UserId+"&Msg="+URLEncoder.encode(msg, "utf-8");
			URL url = new URL(lastUrl);

			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			int statusCode = connection.getResponseCode();
			InputStream inputStream = statusCode == 200 ? connection.getInputStream() : connection.getErrorStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			StringBuilder response = new StringBuilder();
			String line;
			while ((line = reader.readLine()) != null) {
				response.append(line);
			}
			reader.close();
			inputStream.close();
			connection.disconnect();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String getRemoteMessageToken() {
		return MyFirebaseMessagingService.Token;
	}
}