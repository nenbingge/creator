package com.cocos.game;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.VideoView;

import com.bingge.demo.R;
import com.cocos.lib.CocosActivity;
import com.cocos.lib.CocosHelper;
import com.cocos.lib.JsbBridge;
import com.cocos.service.SDKWrapper;

public class NativeHelper {
    private static final String TAG = "NativeHelper:";
    private static CocosActivity app;
    private static ImageView sSplashBgImageView = null;
    private static VideoView vv = null;
    private static int mVideoCurrantPosition = 0;
    private static int hide_count = 0;

    public static void init(CocosActivity app){
        NativeHelper.app = app;
        CocosHelper.setKeepScreenOn(true);
        showSplash();
        initJsFunc();
    }
    private static void showSplash() {
        sSplashBgImageView = new ImageView(app);
        //纯色
        sSplashBgImageView.setBackgroundColor(app.getResources().getColor(R.color.splashBg));
        //背景图
//        sSplashBgImageView.setImageResource(R.drawable.bj2);
//        //logo
////        sSplashBgImageView.setImageResource(R.drawable.splash_logo_with_bg);
        sSplashBgImageView.setScaleType(ImageView.ScaleType.FIT_XY);
//
////        app.addContentView(R.drawable.activity_welcome);
//
        app.addContentView(sSplashBgImageView,
                new WindowManager.LayoutParams(
                        FrameLayout.LayoutParams.MATCH_PARENT,
                        FrameLayout.LayoutParams.MATCH_PARENT
                )
        );

        vv = new VideoView(app);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER;
        vv.setBackgroundColor(app.getResources().getColor(R.color.splashBg));
        app.addContentView(vv, params);
        vv.setVideoURI(Uri.parse("android.resource://com.bingge.demo/" + R.raw.splash));
//        vv.setMediaController(new MediaController(app));
////        vv.setY(-50);
        vv.start();
        vv.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                    @Override
                    public boolean onInfo(MediaPlayer mp, int what, int extra) {
                        if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START){
                            Handler hander = new Handler();
                            hander.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    vv.setBackgroundResource(R.drawable.videobg);
                                }
                            }, 50);
                        }

                        return true;
                    }
                });
            }
        });
        vv.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                Log.d(TAG, "onCompletion: ");
                hideSplash();
            }
        });
    }

    public static void hideSplash() {
        Log.d(TAG, "hideSplash: ");
        hide_count++;
        if(hide_count < 2)return;
        app.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (vv != null) {
//                    sSplashBgImageView.setVisibility(View.GONE);
                    ((ViewGroup)sSplashBgImageView.getParent()).removeView(sSplashBgImageView);
                    sSplashBgImageView = null;
                    vv.stopPlayback();
                    ((ViewGroup)vv.getParent()).removeView(vv);
                    vv = null;
                    JsbBridge.sendToScript("test", "finished");
                }
            }
        });
    }
    
    public static void onResume() {
        if(vv != null){
            vv.seekTo(mVideoCurrantPosition);
            vv.start();
        }
    }
    public static void onPause() {
        if(vv != null) {
            mVideoCurrantPosition = vv.getCurrentPosition();
            vv.pause();
        }
    }

    public static void initJsFunc(){
        JsbBridge.setCallback(new JsbBridge.ICallback() {
            @Override
            public void onScript(String arg0, String arg1) {
                Log.d(TAG, "onScript: ");
                switch (arg0){
                    case"hideSplash":
                        hideSplash();
                }
            }
        });
    }
}
