package com.games.GameLib;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;

import com.shengsheng.fish.R;

public class UpdateManager {

	private Activity mContext;
	
	//提示语
	private String updateMsg = "有最新的软件包哦，亲快下载吧~";
	private String updateTitle = "软件版本更新";

	//返回的安装包url
	private String apkUrl;
	private String checkUpdateURL;
	
	
	private Dialog noticeDialog;
	
	private Dialog downloadDialog;
	 /* 下载包安装路径 */
    //private static final String savePath = "/sdcard/temp_plat_download/";
	private  static String savePath;
    
    private static String saveFileName;

    /* 进度条与通知ui刷新的handler和msg常量 */
    private ProgressBar mProgress;

    
    private static final int DOWN_UPDATE = 1;
    
    private static final int DOWN_OVER = 2;
    
    private static final int CHECK_UPDATE = 3;
    
    private int progress;
    
    private Thread downLoadThread;
    private Thread m_checkUpdateThread;
    
    private boolean interceptFlag = false;
   
    private Handler mHandler = new Handler(){
    	public void handleMessage(Message msg) {
    		switch (msg.what) {
			case DOWN_UPDATE:
				mProgress.setProgress(progress);
				break;
			case DOWN_OVER:
				
				installApk();
				break;
			case CHECK_UPDATE:
				showNoticeDialog();
				break;
			default:
				break;
			}
    	};
    };
    
	public UpdateManager(Activity context) {
		this.mContext = context;
		this.savePath = context.getExternalFilesDir(null)+"/download";
		PackageInfo pi = null;
		try {
			pi = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
		} catch (NameNotFoundException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		if(pi == null)
			return;
		checkUpdateURL = "http://fish.ss2007.com:8088/update/checkupdate?version=" + pi.versionCode
				+ "&PartnerID=" + JsbGame.getPartnerID();
		saveFileName = savePath + "/PlatformUpdate_" + JsbGame.getPartnerID() + "_" + pi.versionCode + ".apk";
	}

	//外部接口让主Activity调用
	public void checkUpdateInfo(){
		m_checkUpdateThread = new Thread(checkUpdateRunnable);
		m_checkUpdateThread.start();	
	}
	
	private void showNoticeDialog(){
		AlertDialog.Builder builder = new Builder(mContext);
		builder.setTitle(updateTitle);
		builder.setMessage(updateMsg);
		builder.setPositiveButton("下载", new OnClickListener() {			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				showDownloadDialog();			
			}
		});
		builder.setNegativeButton("以后再说", new OnClickListener() {			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();				
			}
		});
		noticeDialog = builder.create();
		noticeDialog.show();
	}
	
	private void showDownloadDialog(){
		AlertDialog.Builder builder = new Builder(mContext);
		builder.setTitle(updateTitle);
		
		final LayoutInflater inflater = LayoutInflater.from(mContext);
		View v = inflater.inflate(R.layout.progress, null);
		mProgress = (ProgressBar)v.findViewById(R.id.progress);
		
		builder.setView(v);
		builder.setNegativeButton("取消", new OnClickListener() {	
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				interceptFlag = true;
			}
		});
		downloadDialog = builder.create();
		downloadDialog.show();

		downloadApk();
	}

	private Runnable mdownApkRunnable = new Runnable() {	
		@Override
		public void run() {
			try {
				if(apkUrl == null)
					return;
				URL url = new URL(apkUrl);
			
				HttpURLConnection conn = (HttpURLConnection)url.openConnection();
				conn.connect();
				int length = conn.getContentLength();
				InputStream is = conn.getInputStream();
				
				File file = new File(savePath);
				if(!file.exists()){
					file.mkdir();
				}
				String apkFile = saveFileName;
				File ApkFile = new File(apkFile);
				FileOutputStream fos = new FileOutputStream(ApkFile);
				
				int count = 0;
				byte buf[] = new byte[1024];
				
				do{   		   		
		    		int numread = is.read(buf);
		    		count += numread;
		    	    progress =(int)(((float)count / length) * 100);
		    	    //更新进度
		    	    mHandler.sendEmptyMessage(DOWN_UPDATE);
		    		if(numread <= 0){	
		    			//下载完成通知安装
		    			mHandler.sendEmptyMessage(DOWN_OVER);
		    			break;
		    		}
		    		fos.write(buf,0,numread);
		    	}while(!interceptFlag);//点击取消就停止下载.
				
				fos.close();
				is.close();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch(IOException e){
				e.printStackTrace();
			}
			
		}
	};
	
	 /**
     * 下载apk
     * @param url
     */
	
	private void downloadApk(){
		downLoadThread = new Thread(mdownApkRunnable);
		downLoadThread.start();
	}
	 /**
     * 安装apk
     * @param url
     */
	private void installApk(){
		if (Build.VERSION.SDK_INT >= 9) {
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            Uri contentUri = FileProvider.getUriForFile(mContext, "com.shengsheng.fish.fileprovider", new File(saveFileName));
            intent.setDataAndType(contentUri, "application/vnd.android.package-archive");
            mContext.startActivityForResult(intent, 10);
        } else {
        	File apkfile = new File(saveFileName);
            if (!apkfile.exists()) {
                return;
            }    
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setDataAndType(Uri.parse("file://" + apkfile.toString()), "application/vnd.android.package-archive"); 
            mContext.startActivity(i);
        }
	}
	
	private Runnable checkUpdateRunnable = new Runnable() 
	{	
		@Override
		public void run() 
		{		
			String result = "";
			try {
				//System.out.println("checkUpdateURL:" + checkUpdateURL);
	            URL url=new URL(checkUpdateURL);
	            HttpURLConnection connection= (HttpURLConnection) url.openConnection();
	            connection.setRequestMethod("GET");
	            connection.setConnectTimeout(5*1000);
	            connection.connect();
	            InputStream inputStream=connection.getInputStream();
	            byte[] data=new byte[1024];
	            StringBuffer sb=new StringBuffer();
	            int length=0;
	            while ((length=inputStream.read(data))!=-1){
	                String s=new String(data, Charset.forName("utf-8"));
	                sb.append(s);
	            }
	            result=sb.toString();
	            int ii = result.indexOf(".apk"); 
	            //System.out.println("aa result:" + result);
	            result = result.substring(0, ii+4);
	            inputStream.close();
	            connection.disconnect();
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
			System.out.println("update result:" + result);
			// 解析是否有新版本，提示title，提示内容，下载地址
			//result = "1|新版本标题|新版本内容|http://183.61.62.150/down.myapp.com/android_new/285/10160285/17599798/9DA7429F99458BFDFCBC4B65B29E51C7.apk?mkey=53747fdaa07e048c&f=2484&p=.apk";
			String[] resultStrings = result.split("[|]");
			if(resultStrings.length < 1)
				return;
			try
			{
				if(Integer.parseInt(resultStrings[0]) > 0)
				{
					updateTitle = resultStrings[1];
					updateMsg = resultStrings[2];
					apkUrl = resultStrings[3];
					mHandler.sendEmptyMessage(CHECK_UPDATE);
				}
			}
			catch(NumberFormatException e)
			{
				return;
			}
		}
	};
	
}

