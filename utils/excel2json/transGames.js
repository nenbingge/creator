

const fs = require("fs");
const xlsx = require("node-xlsx");


const data_path = "games";
const add_path = "../assets/games/";

//为""时解析所有游戏
//有内容只解析内容   ep: const singge_game = "youxi.xlsx";
const singge_game = "";


//i18n/label/
parseAllExcel();



function parseAllExcel() {
	let all_path = fs.readdirSync(data_path);
	if (singge_game.length > 0) {
		parseExcel(singge_game);
	}
	else {
		for (let i = 0; i < all_path.length; i++) {
			parseExcel(all_path[i]);
		}
	}

}

function parseExcel(path) {
	console.log(path)
	var names = [];
	var langArr = [];
	var keys={}
	var list = xlsx.parse(data_path + "/" + path);
	var tableData = list[0].data;
	var item_len = tableData[0].length;
	let len = tableData.length;
	for (let col = 2; col < item_len; col++) {
		names.push(tableData[0][col]);
		langArr.push({});
	}
	for (let i = 0; i < names.length; i++) {
		obj = langArr[i];
		let is_ara = names[i] == "ara";

		for (let line = 2; line < len; line++) {
			let item = tableData[line];
			if (item.length == 0) {
				break;
			}
			let key = item[0];
			if (obj[key]) {
				console.log("有重复的索引", key);
				return;
			}
			if(is_ara){
				obj[key] = " " + item[i + 2] + " ";
			}
			else{
				obj[key] = item[i + 2];
			}
		}		
	}
	let qianzhui = path.split(".")[0];
	let path1 = add_path + qianzhui + "/i18n";
	let path2 = add_path + qianzhui + "/i18n/label";

	if(!fs.existsSync(path1))fs.mkdirSync(path1);
	if(!fs.existsSync(path2))fs.mkdirSync(path2);

	for (let i = 0; i < names.length; i++) {
		if(names[i] != "ara")continue;
		let str = JSON.stringify(langArr[i], null, 4);
		if(names[i] == "ara")str = str.replace(/\\\\n/g, "\\n");
		writeFile(add_path + qianzhui + "/i18n/label/" + names[i] + ".json", str);
	}
}


function writeFile(fileName, data) {
	// console.log(fileName)
	// console.log(data)
	fs.writeFile(fileName, data, 'utf-8', complete);
	function complete(err) {
		if (!err) {
			console.log(fileName + " 生成成功");
		}
	}
}
