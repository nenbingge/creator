import {HotFix,UpgradeState} from "../HotFix";

let HallControl = require('HallControl')
cc.Class({
    extends: cc.Component,

    properties: {       
        progressBar:cc.ProgressBar,
        labelProgress:cc.Label,
        btnCancel:cc.Node,
        // loadingPrefab:cc.Prefab,
    },

   
    onLoad () {        
        this.node.on(cc.Node.EventType.TOUCH_START,function(event){            
            event.stopPropagation();
        },this);
         
        cc.log("engine version = " + cc.ENGINE_VERSION);
        HallControl.getInstance().startWebControl();
        cc.director.preloadScene("login");
        HotFix.checkUpgrade(this.onState.bind(this),this);
        if(cc.sys.isNative) {
            cc.tween(this.node)
                .delay(5)
                .call(this.onTimeOut.bind(this))
                .start();
        }
    },

    onState(state,param){
        console.log("onState " + state);
        if(state == UpgradeState.Progress) {
            this.progressBar.node.active = this.labelProgress.node.active = true;
            if(param == null || param == undefined || isNaN(param))
                param = 0;
            console.log("onProgress " + param);
            this.progressBar.progress = param;
            this.labelProgress.string = Math.floor(param * 100) + "%";
            return;
        }
        // if(!this.checkToken()) {
        //     cc.log("加载登陆界面");
            cc.director.loadScene("login");
        // }
    },

    onTimeOut(){        
        this.btnCancel.active = true;
    },

    onBtnCancel(){
        HotFix.stopUpgrade();
    },

    onBtnRecover() {
        HotFix.removeUpdateFiles();
    },

    // checkToken() {
    //     // 看看有没有带参数过来
    //     if(cc.sys.isNative) {
    //         return false;
    //     }
    //     // 如果是主动退出，则不自动登陆
    //     if(TSCommon.getCookie("forcelogout", "0") == "1") {
    //         TSCommon.setCookie("forcelogout", "0");
    //         return false;
    //     }
    //     var params = window.location.href;
    //     var paramBegin = params.indexOf("?");
    //     var paramJson;
    //     if (paramBegin > 0) {
    //         params = params.slice(paramBegin + 1);
    //         var p = params.split("&");
    //         for (var key in p) {
    //             var param1 = p[key].split("=");
    //             if (param1[0] == "casinodata") {
    //                 require("../HallResources").getInstance().showLoading();
    //                 //paramJson = decodeURI(param1[1]);
    //                 paramJson = decodeURIComponent((param1[1] + '').replace(/\+/g, '%20'));
    //                 if(paramJson == "guest") {
    //                     //this.onGuestLogin();
    //                     HallControl.getInstance().guestLogin()
    //                     //'login_type'   '2'
    //                     TSCommon.setCookie(CliConst.LOGIN_TYPE,CliConst.LOGIN_TYPE_GUEST)
    //                     cc.log('游客登陆')
    //                     return true;
    //                 } else {
    //                     console.log("paramJson =" ,paramJson);
    //                     // 解析json数据
    //                     var obj = JSON.parse(paramJson);
    //                     TSCommon.addEvent("onOtherDeviceKicked",function(){
    //                         require("../HallResources").getInstance().removeLoading();
    //                         cc.log("section timeout");
    //                         __ShowConfirmDialog("","Your session has expired,please re-login");
    //                     },this);
    //                     HallControl.getInstance().loginByToken(obj.UserID,obj.Token);
    //                     return true
    //                 }
    //             }
    //         }
    //     }
    //     return false;
    // },
});
