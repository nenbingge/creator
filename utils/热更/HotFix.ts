// 热更逻辑类
// 热更遵循creator默认规范，参考http://docs.cocos.com/creator/manual/zh/advanced-topics/assets-manager.html
// 程序在第一个界面执行更新检查，调用HotFix.checkUpgrade
// 热更模块本身区分web及native，如果是web，直接回调Failed
// 热更后会自动重启游戏
// 热更界面由上层根据消息制作
// 生成的Project.manifest放入assets\resources目录
// 注意发布包需要两次Build：Build->生成Project.manifest->Build
// main.js需要添加searchPath，因为main.js每次build都会重建，所以需要修改template代码，
//      具体位置为C:\CocosCreator\resources\static\build-templates\shares

import {Domain} from "./Domain";
import {TSCommon} from "./TSCommon";
// 更新通知消息类型
export const enum UpgradeState {
	Progress = 0,	// 更新进度，后面带进度百分比参数
    Finished,		// 更新完成，自动重启应用
    Failed,         // 更新中止,或者不需要更新
    Updated,        // 已经更新过了
};

export class HotFix{
    private static _onState:Function;
    private static _target:any;
    private static _am:any;
    private static _upgrading:boolean;
    private static _localManifest:string;
    private static _updatedModules:string[] = new Array();    
    //private static download_root = "http://dl.bet24.ng/hotfix/";
    private static download_root = "http://dl.bet24.ng/bet24_hotfix/subpackages/";
    private static _moduleName:string;


    private static versionCompareHandle(versionA,versionB):Number{
        console.log("JS Custom Version Compare: version A is " + versionA + ', version B is ' + versionB);
        var vA = versionA.split('.');
        var vB = versionB.split('.');
        for (var i = 0; i < vA.length; ++i) {
            var a = parseInt(vA[i]);
            var b = parseInt(vB[i] || 0);
            if (a === b) {
                continue;
            }
            else {
                return a - b;
            }
        }
        if (vB.length > vA.length) {
            return -1;
        }
        else {
            return 0;
        }
    }
 
    private static isModuleUpdated(moduleName:string):boolean {
        for (var i = 0; i < HotFix._updatedModules.length; i++) {
            if(HotFix._updatedModules[i] == moduleName) {
                return true;
            }
        }
        return false;    
    }

    private static createModule_manifest(moduleName:string) {
        if(!moduleName || moduleName == "") {
            return
        }
        // 先看看安装包中有没有manifest
        var localManifest = "subpackages/" + moduleName + "/project.manifest";
        var localData = jsb.fileUtils.getStringFromFile(localManifest);
        if(localData == "") {
            var m = {
                packageUrl:HotFix.download_root + moduleName,
                remoteManifestUrl:HotFix.download_root + moduleName + "/project.manifest",
                remoteVersionUrl:HotFix.download_root + moduleName + "/version.manifest",
                version:"0",
                assets : {},
                searchPaths:{}
            }
            localData = JSON.stringify(m)
        }
        // 这个manifest文件不能跟下载路径同名 
        // local manifest = moduleName .. "/project.manifest"
        // if require("jni/JniBridge").isWin32() then   
        
        var dir = jsb.fileUtils.getWritablePath() + "hotfix_asset/subpackages/" + moduleName;
        jsb.fileUtils.createDirectory(dir);
        jsb.fileUtils.writeStringToFile(localData,dir + "/project.manifest");
    }

    public static isGameInstalled(moduleName) : boolean{
        if(!cc.sys.isNative) {
            return true
        }

        var manifest = jsb.fileUtils.getWritablePath() + "hotfix_asset/subpackages/" + moduleName + "/project.manifest";
        if(!jsb.fileUtils.isFileExist(manifest)) {
            HotFix.createModule_manifest(moduleName);
            return HotFix.isGameInstalled(moduleName);           
        }
        var data = jsb.fileUtils.getStringFromFile(manifest);
        var m = JSON.parse(data);
        if(!m || !m.version || m.version == "") {            
            return false;
        }
        console.log("isGameInstalled version : " + m.version);
        return m.version != "0";        
    }

    private static checkModuleUpdate(moduleName:string,onState:Function,target:any) {
        HotFix._localManifest = jsb.fileUtils.getWritablePath() + "hotfix_asset/subpackages/" + moduleName + "/project.manifest";
        if(!jsb.fileUtils.isFileExist(HotFix._localManifest)) {
            HotFix.createModule_manifest(moduleName);               
        }
        HotFix.checkUpdate(moduleName);        
    }
    
    public static checkUpgrade(onState:Function,target:any,moduleName:string){
        HotFix._onState = onState;
        HotFix._target = target;
        if (!cc.sys.isNative) {
            onState.call(target,UpgradeState.Finished);
            return;            
        }
        if(HotFix._upgrading){
            console.log("HotFix.checkVersion upgrading");
            onState.call(target,UpgradeState.Failed);
            return;
        }

        HotFix._upgrading = true;
        HotFix._moduleName = moduleName;
        if(!moduleName) {
            HotFix._updatedModules = new Array();
        }        
        console.log("checkUpgrade --- ",jsb.fileUtils.getSearchPaths());
        
        
        if(onState == null || target == null){
            console.log("HotFix checkUpgrade invalid args" );
            return;
        }    
       
        // 如果已经更新过了则直接成功
        if(moduleName) {
            if(HotFix.isModuleUpdated(moduleName)) {
                HotFix._upgrading = false;
                HotFix._am.setEventCallback(null);
                onState.call(target,UpgradeState.Updated);
                return;
            }
            HotFix.checkModuleUpdate(moduleName,onState,target);            
            //HotFix.checkUpdate();
        } else {
            let localManifestUrl = "version/project";
            cc.loader.loadRes(localManifestUrl, function (err, asset) {
                if(err) {
                    HotFix._am.setEventCallback(null);
                    HotFix._upgrading = false;
                    HotFix._onState.call(HotFix._target,UpgradeState.Failed);
                    console.log("checkUpgrade local manifest failed ",err);
                    return;
                }
                HotFix._localManifest = asset.nativeUrl;
                console.log("checkUpgrade local manifest = " + HotFix._localManifest);
                HotFix.checkUpdate();        
            });
        }        
    }

    private static checkUpdate(moduleName) {
        // 加载运行路径
        var storagePath = ((jsb.fileUtils ? jsb.fileUtils.getWritablePath() : '/') + 'hotfix_asset/');
        if(moduleName) {
            storagePath = storagePath + "subpackages/" + moduleName + "/";
        }

        // 创建assetmanager         
        HotFix._am = new jsb.AssetsManager('', storagePath, HotFix.versionCompareHandle);

        HotFix._am.setVerifyCallback(function (path, asset) {
            // When asset is compressed, we don't need to check its md5, because zip file have been deleted.
            var compressed = asset.compressed;
            // Retrieve the correct md5 value.
            var expectedMD5 = asset.md5;
            // asset.path is relative path and path is absolute.
            var relativePath = asset.path;
            // The size of asset file, but this value could be absent.
            var size = asset.size;
            if (compressed) {
               console.log("Verification passed : " + relativePath);
                return true;
            }
            else {
                console.log("Verification passed : " + relativePath + ' (' + expectedMD5 + ')');
                return true;
            }
        });

        if (cc.sys.os === cc.sys.OS_ANDROID) {
            // Some Android device may slow down the download process when concurrent tasks is too much.
            // The value may not be accurate, please do more test and find what's most suitable for your game.
            HotFix._am.setMaxConcurrentTask(2);
        }

        // 检查版本
        HotFix.checkVersion();
    }

    private static startDownload(){
        if (HotFix._am) {
            HotFix._am.setEventCallback(HotFix.onUpgradeHandler);

            if (HotFix._am.getState() === jsb.AssetsManager.State.UNINITED) {
                // Resolve md5 url
                var url = HotFix._localManifest;
                if (cc.loader.md5Pipe) {
                    url = cc.loader.md5Pipe.transformURL(url);
                }
                HotFix._am.loadLocalManifest(url);
                cc.log("startDownload",url);
            }           
            HotFix._am.update();           
        }
    }

    private static onCheckHandler(event){
        console.log('onCheckHandler Code: ' + event.getEventCode());  
        var failed = false;     
        var errorMsg = "";
        switch (event.getEventCode())
        {
            case jsb.EventAssetsManager.ERROR_NO_LOCAL_MANIFEST:
                console.log("No local manifest file found, hot update skipped.",HotFix._localManifest);
                failed = true;
                errorMsg = "Local package is damaged,please try to re-install.";
                break;
            case jsb.EventAssetsManager.ERROR_DOWNLOAD_MANIFEST:
            case jsb.EventAssetsManager.ERROR_PARSE_MANIFEST:
                console.log("Fail to download manifest file, hot update skipped.",HotFix._localManifest);
                errorMsg = "Download failed,please try again later.";
                break;
            case jsb.EventAssetsManager.ALREADY_UP_TO_DATE:
                console.log("Already up to date with the latest remote version.");
                break;
            case jsb.EventAssetsManager.NEW_VERSION_FOUND:
                console.log('New version found, please try to update.'); 
                HotFix.startDownload();
                return;  
            case jsb.EventAssetsManager.UPDATE_PROGRESSION:
                return;
        }
        // 如果不用下载，则通知上层        
        HotFix._am.setEventCallback(null);
        HotFix._upgrading = false;
        if(failed) {            
            HotFix._onState.call(HotFix._target,UpgradeState.Failed); 
            return;
        }
        if(HotFix._moduleName) {
            HotFix._updatedModules.push(HotFix._moduleName);
        }        
        HotFix._onState.call(HotFix._target,UpgradeState.Finished); 
    }

    private static onUpgradeHandler(event){
        console.log("onUpgradeHandler --",event.getEventCode());
        var needRestart = false;      
        var failed = false;  
        var errorMsg = "";
        switch (event.getEventCode())
        {
            case jsb.EventAssetsManager.ERROR_NO_LOCAL_MANIFEST:
                console.log('No local manifest file found, hot update skipped.');
                failed = true;
                errorMsg = "Local package is damaged,please try to re-install.";
                break;
            case jsb.EventAssetsManager.UPDATE_PROGRESSION:
                HotFix._onState.call(HotFix._target,UpgradeState.Progress,event.getPercent());
                return;                
            case jsb.EventAssetsManager.ERROR_DOWNLOAD_MANIFEST:
            case jsb.EventAssetsManager.ERROR_PARSE_MANIFEST:
                console.log('Fail to download manifest file, hot update skipped.');
                errorMsg = "Download failed,please try again later.";
                failed = true;
                break;
            case jsb.EventAssetsManager.ALREADY_UP_TO_DATE:
                console.log('Already up to date with the latest remote version.');
                failed = false;
                break;
            case jsb.EventAssetsManager.UPDATE_FINISHED:
                console.log('Update finished. ' + event.getMessage());                
                needRestart = true;                                
                break;
            case jsb.EventAssetsManager.UPDATE_FAILED:
                console.log('Update failed. ' + event.getMessage());
                failed = true;
                break;
            case jsb.EventAssetsManager.ERROR_UPDATING:
                console.log('Asset update error: ' + event.getAssetId() + ', ' + event.getMessage());
                failed = true; 
                errorMsg = "Download failed,please try again later.";
                break;
            case jsb.EventAssetsManager.ERROR_DECOMPRESS:
                console.log(event.getMessage()); 
                failed = true;
                errorMsg = "Download failed,please try again later.";
                break;
            case jsb.EventAssetsManager.NEW_VERSION_FOUND:
            case jsb.EventAssetsManager.ASSET_UPDATED:
                return;
            default:
                console.log("unhandled onUpgradeHandler",event.getEventCode());
                break;
        }

        if (failed) {
            HotFix._onState.call(HotFix._target,UpgradeState.Failed,errorMsg);  
            HotFix._am.setEventCallback(null);
            HotFix._upgrading = false;
            return
        } else {
            HotFix._upgrading = false;
            HotFix._am.setEventCallback(null);
            if(HotFix._moduleName) {
                HotFix._updatedModules.push(HotFix._moduleName);                
            }           
            HotFix._onState.call(HotFix._target,UpgradeState.Finished);
        }

        if (needRestart && !HotFix._moduleName) {
            cc.log("restarting.........");
            cc.audioEngine.stopAll();            
            cc.game.restart();
        }
    }

    private static checkVersion(){
        if (HotFix._am.getState() === jsb.AssetsManager.State.UNINITED) {
            // 加载本地manifest
            // Resolve md5 url           
            var url = HotFix._localManifest;
            if (cc.loader.md5Pipe) {
                url = cc.loader.md5Pipe.transformURL(url);
            }
            console.log(url);
            HotFix._am.loadLocalManifest(url);

            if (!HotFix._am.getLocalManifest() || !HotFix._am.getLocalManifest().isLoaded()) {
                console.log('Failed to load local manifest ...');
                HotFix._onState.call(HotFix._target,UpgradeState.Failed);
                this._upgrading = false;
                return;
            }
            Domain.Version = HotFix._am.getLocalManifest().getVersion();
            Domain.Version = Domain.Version.replace(/\./g,"");
            HotFix._am.setEventCallback(HotFix.onCheckHandler);         
            HotFix._am.checkUpdate();            
        }        
    }

    public static stopUpgrade(){
        if(HotFix._am) {
            HotFix._am.setEventCallback(null);
        }
        HotFix._upgrading = false;
        HotFix._onState.call(HotFix._target,UpgradeState.Failed);
    }

    public static removeUpdateFiles() {
        if(cc.sys.isNative) {
            jsb.fileUtils.removeDirectory(jsb.fileUtils.getWritablePath() + "hotfix_asset/");
            HotFix._upgrading = false;
            HotFix._am.setEventCallback(null);
            cc.log("restarting.........");
            cc.audioEngine.stopAll();            
            cc.game.restart();
        }
    }
};