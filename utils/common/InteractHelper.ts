import { _decorator, Component, Prefab, instantiate, Vec3 } from 'cc';

import { Interaction } from "./Interaction";

const { ccclass, property } = _decorator;

@ccclass('InteractHelper')
export class InteractHelper extends Component {

    @property(Prefab)
    public pre_interact: Prefab = null;

    public pool = [];

    private allocProp() {
        if (this.pool.length > 0) {
            return this.pool.pop();
        }
        let node = instantiate(this.pre_interact);
        let interact = node.getComponent(Interaction);
        interact.setPool(this.pool);
        return interact;
    }

    public showAction(index: number, begin_pos: Vec3, to_pos: Vec3) {
        let count = 0;
        if (index == 9) {
            this.schedule(function () {
                count++;
                this.showOne(index, begin_pos, to_pos, count);
            }, 0.1, 4, 0.1);
        }
        this.showOne(index, begin_pos, to_pos, 0);
    }

    public showOne(index: number, begin_pos: Vec3, to_pos: Vec3, count: number) {
        let interact = this.allocProp();
        this.node.addChild(interact.node);
        interact.showAction(index, begin_pos, to_pos, count);
    }

    public onDestroy() {
        for (let i = 0, l = this.pool.length; i < l; i++) {
            this.pool[i].node.destroy();
        }
        this.pool = [];
    }
}
