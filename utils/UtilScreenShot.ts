import { Camera, RenderTexture, view, Node, UITransform, ImageAsset, Texture2D, SpriteFrame, director, Director, isValid } from "cc";
import { UtilSava } from "./UtilSave";

class Task{
    node:Node
    func:Function
    target:any
    constructor(node:Node, func: Function, target?:any){
        this.node = node;
        this.func = func;
        this.target = target;
    }
}
export class UtilScreenShot {
    private m_camera: Camera = null;
    private rt: RenderTexture = null;
    
    private tasks:Array<Task> = [];

    constructor(camera: Camera){
        this.m_camera = camera;
        this.rt = new RenderTexture();
        this.rt.reset({
            width: view.getVisibleSize().width,
            height: view.getVisibleSize().height,
        })
        this.m_camera.targetTexture = this.rt;
        this.m_camera.enabled = false;
    }
    public commitTask(node:Node, func:Function, target?:any){
        this.tasks.push(new Task(node, func, target));
        if(this.m_camera.enabled)return;
        this.m_camera.enabled = true;
        director.once(Director.EVENT_AFTER_DRAW, this.onReady, this);
    }
    private shotNode(target:Node){
        let world_pos = target.getWorldPosition();
        let ui_trans = target.getComponent(UITransform);
        let width = ui_trans.width, height = ui_trans.height;
        let x = world_pos.x - width * ui_trans.anchorX;
        let y = world_pos.y - height * ui_trans.anchorY;
        let buffer = this.rt.readPixels(Math.round(x), Math.round(y), width, height);
        buffer = UtilSava.flipImgBuffer(buffer, width, height);
        let img = new ImageAsset();
        img.reset({
            _data: buffer,
            width: width,
            height: height,
            format: Texture2D.PixelFormat.RGBA8888,
            _compressed: false
        });
        let texture = new Texture2D();
        texture.image = img;
        let sf = new SpriteFrame();
        sf.texture = texture;
        sf.packable = false;
        return sf;
    }
    private onReady(){
        let task = null;
        for(let i = 0; i < this.tasks.length; i++){
            task = this.tasks[i];
            if(!isValid(task.node))continue;
            task.func.call(task.target, this.shotNode(task.node));
        }
        this.m_camera.enabled = false;
    }
     
}
