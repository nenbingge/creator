/*
    截图工具
    把一个节点内容生成一张图片
    进页面先调init(需要截图得节点，一个只渲染前面节点得相机，缩放倍率)
    cutOneScreen保存下当前屏幕内节点显示得数据
    如遇节点高度大于屏幕可以保存下数据后将节点上移一个屏幕继续cutOneScreen
    cutOneScreen返回true时就是截取高度已经够了
    调getSpriteFrame即可拿到SpriteFrame

*/


var _canvas = null;
var _texture = null;
var _img = null;
var _camera = null;

var need_height = 0;
var need_width = 0;
var zoom_scale = 0;

var cur_height = 0;
var temp_data = [];
var height_arr = [];
var picData = [];


function init(node_show , camera , ratio){
    zoom_scale = ratio;
    _camera = camera;
    need_height = node_show.height / ratio;
    need_width = node_show.width / ratio;
    if(cc.sys.isBrowser){
        if(!_canvas){
            _canvas = document.createElement('canvas');
        }
        _canvas.height = need_height;
        _canvas.width = need_width;
    }
    if(!_texture){
        _texture = new cc.RenderTexture();
    }
    _texture.initWithSize(cc.visibleRect.width / zoom_scale , cc.visibleRect.height / zoom_scale , cc.gfx.RB_FMT_S8);
    _camera.targetTexture = _texture;

    cur_height = 0;
    temp_data = [];
    height_arr = [];
}

function createImg () {
    if(!_img){
        _img = document.createElement("img");
    }
    _img.src = _canvas.toDataURL("image/png");
}

function getSpriteFrame(){
    cc.log('getSpriteFrame1')
    let texture = new cc.Texture2D();
    if(cc.sys.isBrowser){
        cc.log('getSpriteFrame2')
        let ctx = _canvas.getContext('2d');
        setCtxData(ctx , temp_data , height_arr , need_width);
        createImg();
        texture.initWithElement(_img);
        // document.body.appendChild(img);
        // document.body.removeChild(img);
    }else{
        setPicData(temp_data , height_arr , need_width);
        texture.initWithData(picData, 32, need_width, need_height);
    }
    let spriteFrame = new cc.SpriteFrame();
    spriteFrame.setTexture(texture);
    return spriteFrame;
}

function cutOneScreen(){
    _camera.render();
    let add_height = Math.min(need_height*zoom_scale-cur_height,cc.visibleRect.height);
    let data = _texture.readPixels();
    temp_data.push(data);
    height_arr.push(add_height / zoom_scale);
    cur_height += add_height;
    return cur_height >= need_height * zoom_scale;
}

 //web专用
function setCtxData(ctx , all_data , heights , width){
    let cur_add_height = 0;
    var height = cc.visibleRect.height / zoom_scale;
    let rowBytes = width * 4;
    for(let i = 0 ; i < all_data.length ; i++){
        let data = all_data[i];
        let add_height = heights[i];
        for(let row = 0 ,bottom = height - 1 ; row < add_height ; row++ , bottom--){
            let start = bottom * width * 4;
            let imageData = ctx.createImageData(width, 1);
            for (let j = 0; j < rowBytes; j++) {
                imageData.data[j] = data[start + j];
            }
            ctx.putImageData(imageData, 0, row + cur_add_height);
        }
        cur_add_height += height;
    }
}
//jsb专用
function setPicData(all_data , heights , width){
    picData = new Uint8Array(width * need_height * 4);
    let cur_height = 0;
    let height = cc.visibleRect.height / zoom_scale;
    let rowBytes = width * 4;
    for(let i = 0 ; i < all_data.length ; i++){
        let data = all_data[i];
        let add_height = heights[i];
        for(let row = 0 ,bottom = height - 1 ; row < add_height ; row++ , bottom--){
            let start = bottom * width * 4;
            let re_start = cur_height * rowBytes + row * rowBytes;
            for (let j = 0; j < rowBytes; j++) {
                picData[re_start + j] = data[start + j];
            }
        }
        cur_height += height;
    }
}

saveImg(picture_name){
    if(CC_JSB){
        let filePath = jsb.fileUtils.getWritablePath() + picture_name + '.png';
        let picData = CutPictureUtil.getPicData();
        let size = CutPictureUtil.getPictureSize();
        let success = jsb.saveImageData(picData, size[1], size[0], filePath)
        if (success) {
            if(cc.sys.os == cc.sys.OS_ANDROID){
                //自己在java写将图片保存到相册
                let ret = JsbCommon.saveImg2Gallery(filePath , picture_name);
                return ret;
                // jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity", "saveTextureToLocal", "(Ljava/lang/String;)V", filePath);
            }
            return 1;
            //
        }
        else {
            return 2;
        }
    }else{
        return 3;
    }
},
//JsbCommon
// public static saveImg2Gallery(path_name:string , save_name:string):string{
//     if(TSCommon.isAndroid()){
//         return native.callClz("JsbGame","Ljava/lang/String;","saveImageToGallery",
//                                 path_name, save_name);
//         // return jsb.reflection.callStaticMethod("org/cocos2dx/javascript/JsbGame", "saveImageToGallery",
//         // "(Ljava/lang/String;Ljava/lang/String;)V", path_name,save_name);
//     }
//     cc.log("JsbCommon.saveImg");
//     return "fail";
// }


//java
// public static String saveImageToGallery(final String bmpPath , final String name) {
//         //
//     int permission = ContextCompat.checkSelfPermission(m_activity,Manifest.permission.WRITE_EXTERNAL_STORAGE);
//     if (permission != PackageManager.PERMISSION_GRANTED){
//         return "no permission";
//     }
//     Context context = (Context)m_activity;
//     File imgFile = new File(bmpPath);
//     if (!imgFile.exists()) {
//         return "no exists";
//     }

//     //插入图片到系统相册
//     try {
//         MediaStore.Images.Media.insertImage(context.getContentResolver(), bmpPath, name, null);
//         //保存图片后发送广播通知更新数据库
//         Uri uri = Uri.parse(bmpPath);
//         context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri));
//         Log.d("fwefhweufhweufwe", "saveImageToGallery3: ");
//         return "success";
//     } catch (Exception e) {
//         // TODO Auto-generated catch block
//         e.printStackTrace();
//     }
//     return "fail";
// }

function getPictureSize(){
    return [need_height , need_width];
}
function getPicData(){
    return picData;
}

function clearData(){
    _camera = null;
    _texture = null;
    cur_height = 0;
    temp_data = [];
    height_arr = [];
    picData = [];
}

module.exports = {
    // createCanvas: createCanvas,
    // initTexture: initTexture,
    init: init,
    cutOneScreen: cutOneScreen,
    getSpriteFrame: getSpriteFrame,
    getPictureSize: getPictureSize,
    getPicData: getPicData,
    clearData: clearData,
}