
class Info{
    public key: string;
    public p: cc.Vec2;
    constructor(p:cc.Vec2){
        this.p = p;
        this.key = p.x + ":" + p.y;
    }
}
const ROUND = [[1,0],[1,-1],[0,-1],[-1,-1],[-1,0],[-1,1],[0,1],[1,1]];
export class AStar {

    private static map: Array<Array<number>> = null;

    public static setMap(map:any){
        AStar.map = map;
    }

    public static calCost(start: cc.Vec2, goal: cc.Vec2){
        let diff = Math.abs(Math.abs(start.x - goal.x) - Math.abs(start.y - goal.y)) * (20 - 14);
        return (Math.abs(start.x - goal.x) + Math.abs(start.y - goal.y)) * 10 - diff; 
    }
    public static betweenCost(start: cc.Vec2, end: cc.Vec2){
        let add = Math.abs(start.x - end.x) + Math.abs(start.y - end.y); 
        return add == 2 ? 14 : 10;
    }

    public static v2ToKey(v: cc.Vec2){
        return v.x + ":" + v.y; 
    }
    public static getNearPoint(p: cc.Vec2){
        let arr = new Array();
        let x = 0 , y = 0;
        for(let i = 0; i < ROUND.length ; i++){
            x = p.x + ROUND[i][0];
            y = p.y + ROUND[i][1];
            if(x < 0 || y < 0 || x >= AStar.map.length || y >= AStar.map[x].length)continue;
            if(AStar.map[x][y] == 0)arr.push(new Info(cc.v2(x, y)));
        }
        return arr;
    }

    private static getLessInfo(open_arr:Array<any>, f_score:any){
        let index = open_arr.length - 1;
        let min = f_score.get(open_arr[index].key);
        for(let i = index - 1 ; i >= 0 ; i--){
            let score = f_score.get(open_arr[i].key);
            if(score < min){
                min = score;
                index = i;
            }
        }
        return open_arr.splice(index, 1)[0];
    }
    private static getPaths(map: any, info: Info){
        let result = new Array();
        result.push(info.p);
        info = map.get(info.key);
        while(info){
            result.unshift(info.p);
            info = map.get(info.key);
        }
        return result;
    }
    public static find(begin:any, end:any){
        let close_set = new Set();
        //需要走的路径
        let open_arr = new Array();
        open_arr.push(new Info(begin));

        let key_begin = open_arr[0].key;
        //open_arr每个点的前一步
        let map_parent = new Map();

        //起点到对应点的消耗
        let g_score = new Map();
        g_score.set(key_begin, 0);

        //对应点到起点和终点的总消耗
        let f_score = new Map();
        f_score.set(key_begin, AStar.calCost(begin, end));

        while(open_arr.length > 0){
            //先找到路径点中消耗最少的
            let current = AStar.getLessInfo(open_arr, f_score);
            console.log(current.key);
            if(end.equals(current.p)){
                //如果是终点就结束
                return AStar.getPaths(map_parent, current);
            }
            //查找过的点就记录下来
            close_set.add(current.key);

            //得到这个点一步能到达的点
            let nears = AStar.getNearPoint(current.p);
            for(let i = 0 ; i < nears.length ; i++){
                let near = nears[i];
                //如果有在记录中直接跳过
                if(close_set.has(near.key))continue;
                //算出这个点到起点的消耗
                let near_cost = g_score.get(current.key) + AStar.betweenCost(current.p, near.p);
                //查看之前是否到过这个点
                let near_in_open = open_arr.find(item=>item.key == near.key);
                if(near_in_open){
                    //之前的路径消耗少的话也跳过
                    if(near_cost >= g_score.get(near.key))continue;
                }
                else{
                    open_arr.push(near);
                }
                //加入路径中，并同步父节点和消耗
                map_parent.set(near.key, current);
                g_score.set(near.key, near_cost);
                f_score.set(near.key, AStar.calCost(near.p, end));
            }
        }
        return null;
    }

}
